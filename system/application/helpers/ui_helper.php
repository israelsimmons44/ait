<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if (! function_exists('contenedor')){
	 function contenedor($contenido){	 	
	 	$str=<<<EOF
<div class="ui-dialog ui-widget ui-widget-content ui-corner-all" style="position:relative">
   <div style="min-height: 109px; width: auto;" class="ui-dialog-content ui-widget-content" id="dialog">
   	$contenido
   </div>
</div>
EOF;
return $str;
	 }
}