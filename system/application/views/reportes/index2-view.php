<div class="ui-dialog ui-widget ui-widget-content ui-corner-all" style="float:left;position:relative;margin-right:20px;width:890px;">
   <div class="ui-dialog-titlebar ui-widget-header ui-corner-top ui-helper-clearfix">
      <span id="ui-dialog-title-dialog" class="ui-dialog-title">Reportes</span>
      <a class="ui-dialog-titlebar-close ui-corner-all" href="#"></a>
   </div>
   <div style="height:auto; width: auto;overflow: hidden;" class="ui-dialog-content ui-widget-content" id="dialog">
      
      <div id="aquivaelgrafico" style="width:600px;float:right;"></div>
      
      
   </div>
   
</div>

<span style='margin-left:15px;' id='resultado-ajax'></span>

<script>

var chart1; 
$(function(){
	
	chart1=new Highcharts.Chart({
		"chart":{"renderTo":"aquivaelgrafico",'defaultSeriesType': 'column'},
		"title":{"text":"Este es el título"},
		"plotOptions": {
			column: {
				allowPointSelect: true,
				cursor: 'pointer',
				dataLabels: {
					enabled: true
					
				}
			}
		},
		tooltip: {
			formatter: function() {
				return ''+
					this.x +': '+ this.y +' ';
			}
		},

		xAxis: {
			categories: [
				'Ene', 
				'Feb', 
				'Mar', 
				'Abr', 
				'May'
			]
		},
		yAxis: {
			min: 0,
			title: {
				text: 'Solicitudes'
			}
		},
		plotOptions: {
			column: {
				pointPadding: 0.2,
				borderWidth: 0
			}
		},


		"series":[
			{"name":"Procesados","data":[50,15,21,21,13]},
			{"name":"Impresos","data":[40,10,18,15,5]}
		]
	})
})

</script>