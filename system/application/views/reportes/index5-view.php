<div class="ui-dialog ui-widget ui-widget-content ui-corner-all" style="float:left;position:relative;margin-right:20px;width:890px;">
   <div class="ui-dialog-titlebar ui-widget-header ui-corner-top ui-helper-clearfix">
      <span id="ui-dialog-title-dialog" class="ui-dialog-title">Reportes</span>
      <a class="ui-dialog-titlebar-close ui-corner-all" href="#"></a>
   </div>
   <div style="height:auto; width: auto;overflow: hidden;" class="ui-dialog-content ui-widget-content" id="dialog">
      
      
      	<form method="post">
			<table>
				<tr><td>Fecha Inicio</td><td><input name="fechaInicio" id="fechainicio" class="fecha"/></td></tr>
				<tr><td>Fecha Fin</td><td><input name="fechaFin" id="fechafin" class="fecha"/></td></tr>
				<tr><td>&nbsp;</td><td><button>Filtrar</button></td></tr>
			</table>
		</form>
		<table>
			<thead>
				<tr><th>Nombre</th><th>Cantidad</th></tr>
			</thead>
			<tbody>
				{tabla}
				<tr><td>{nombre}</td><td>{count}</td></tr>
				{/tabla}
			</tbody>
		</table>
      
   </div>
   
</div>



<script>
	$(function(){
		$('.fecha').datepicker({dateFormat:'dd-mm-yy',altFormat:'yy-mm-dd'});
		$('button').button();
	});
</script>