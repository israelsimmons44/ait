<div class="ui-dialog ui-widget ui-widget-content ui-corner-all" style="float:left;position:relative;margin-right:20px;width:890px;">
	<div class="ui-dialog-titlebar ui-widget-header ui-corner-top ui-helper-clearfix">
    	<span id="ui-dialog-title-dialog" class="ui-dialog-title">Entrega de Documentos</span>
      	<a class="ui-dialog-titlebar-close ui-corner-all" href="#"></a>
	</div>
	<div style="min-height: 109px; width: auto;overflow: hidden;" class="ui-dialog-content ui-widget-content" id="dialog">
   	
   	
   				<form id="formpersona" name="formpersona">
		      		<table>
			      		<tr><td>C&eacute;dula</td><td>{cedula}{btn}</td></tr>
			      		<tr><td>Nombre</td><td><input id="formanombre" name="formanombre" READONLY="READONLY" type="text" size="40" /></td></tr>
		      			<tr><td>Tipo de Personal</td><td><input id="formatipopersonal" READONLY="READONLY" name="formatipopersonal" type="text" size="40" /></td></tr>
		      		</table>
		      	</form>
   	
   		<div id="pendientes"></div>
   		
	</div>
</div>
<span id="resultado-ajax"></span>

<script>
	$(function(){
		$('button').button();
		
		$('.display').dataTable({"aaSorting": [[ 0, "asc" ]],		
			"bJQueryUI": true,
			"sPaginationType": "full_numbers",
			"iDisplayLength": 25,
			"oLanguage": {
						"sZeroRecords": "No se encontraron registros!!!"
												
			}
		})
	})
	
	{javascript}
</script>