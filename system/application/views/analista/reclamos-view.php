<div class="ui-dialog ui-widget ui-widget-content ui-corner-all" style="float:left;position:relative;margin-right:20px;width:890px;">
   <div class="ui-dialog-titlebar ui-widget-header ui-corner-top ui-helper-clearfix">
      <span id="ui-dialog-title-dialog" class="ui-dialog-title">Consulta de Reclamos</span>
      <a class="ui-dialog-titlebar-close ui-corner-all" href="#"></a>
   </div>
   <div style="height:auto; width: auto;overflow: hidden;" class="ui-dialog-content ui-widget-content" id="dialog">
		<table id="tablaReclamos" width="100%" cellspacing="0" cellpadding="5">
			<thead>
				<tr><th>Id</th><th>Fecha</th><th>C&eacute;dula</th><th>Correo</th><th>Descripci&oacute;n</th><th>Acci&oacute;n</th><th>Estatus</th></tr>
			</thead>
			<tbody>
			{reclamos}
				<tr height="30"><td align="center">{id_reclamo}</td><td align="center">{fecha}</td><td align="right">{cedula}</td><td>{login}</td><td>{descripcion}</td><td align="center">{abrirconsultarcaso}</td><td>{estatus}</td></tr>
			{/reclamos}
			</tbody>
		</table>
   		
   </div>
   
   
</div>

<script type="text/javascript">
<!--
	$(function(){
		
		$('#tablaReclamos').dataTable({"aaSorting": [[ 0, "desc" ]],
			"bJQueryUI": true,
			"sPaginationType": "full_numbers",
			"iDisplayLength": 10,
			"oLanguage": {
						"sZeroRecords": "No se encontraron registros!!!"
												
			}


});
	});
//-->
</script>