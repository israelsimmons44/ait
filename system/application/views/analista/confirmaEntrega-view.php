<div class="ui-dialog ui-widget ui-widget-content ui-corner-all" style="float:left;position:relative;margin-right:20px;width:890px;">
	<div class="ui-dialog-titlebar ui-widget-header ui-corner-top ui-helper-clearfix">
    	<span id="ui-dialog-title-dialog" class="ui-dialog-title">Entrega de Documentos - Confirmaci&oacute;n</span>
      	<a class="ui-dialog-titlebar-close ui-corner-all" href="#"></a>
	</div>
	<div style="min-height: 109px; width: auto;overflow: hidden;" class="ui-dialog-content ui-widget-content" id="dialog">
   	
   		<form method="post" id="form1" name="form1" action="<?=base_url()?>index.php/analista/actualizaEntrega">
   			<input type="hidden" name="id_solicitud" id="id_solicitud" value="{idsolicitud}"/>
   			<table>
   				<tr><td>Recibe:</td><td><select name="recibe" id="recibe"><option value="0">El Titular</option><option value="1">Un Emisario</option></select></td></tr>
   				<tr id="cedulaemisario"><td>C&eacute;dula:</td><td><input type="text" name="cedulaemisario" size='8'/></td></tr>
   				<tr id="nombreemisario"><td>Nombre:</td><td><input type="text" name="nombreemisario" size='30'/></td></tr>
   				<tr><td>&nbsp;</td><td><button>Enviar</button></td></tr>
   			</table>
   		</form>
   		
	</div>
</div>
<span id="resultado-ajax"></span>

<script>
	$(function(){
		$('#cedulaemisario').hide();
		$('#nombreemisario').hide();
		$('#recibe').bind('change',algo);
		$('button').button();
		
	})
	
	function algo(){
		if ($('#recibe').val()==0){
			$('#cedulaemisario').hide();
			$('#nombreemisario').hide();
		}else{
			$('#cedulaemisario').show();
			$('#nombreemisario').show();
		}
	}
</script>