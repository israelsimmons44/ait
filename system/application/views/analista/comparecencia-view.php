<style>
	label{
		display: block;
	}
	
	.campo{
		padding: 10px;
		float:left;
	}
	
	.next{
		clear: both;
	}
</style>

<div class="ui-dialog ui-widget ui-widget-content ui-corner-all" style="float:left;position:relative;margin-right:20px;width:890px;">
   <div class="ui-dialog-titlebar ui-widget-header ui-corner-top ui-helper-clearfix">
      <span id="ui-dialog-title-dialog" class="ui-dialog-title">Crear Acta de Comparecencia</span>
      <a class="ui-dialog-titlebar-close ui-corner-all" href="#"></a>
   </div>
   <div style="min-height: 109px; width: auto;overflow: hidden;" class="ui-dialog-content ui-widget-content" id="dialog">
   	

      
		<div id="btnImprime"></div>
      
      
      
      <div class="ui-dialog ui-widget ui-widget-content ui-corner-all" style="float:left;position:relative;margin-right:20px;width:80%">
      	<form name="formajubipen" id="formajubipen">
      		<div class="campo"><label for="#fldcedula">C&eacute;dula</label><?=$fldcedula?><button id="btnBuscar" onclick="javascript:buscarJubiPen();return false;">Buscar</button></div>
      		<div class="campo next"><label for="#fldnombre">Nombre</label><?=$fldnombre?></div>
      		<div class="campo"><label for="#fldjubipen">Tipo de Funcionario</label><?=$fldjubipen?></div>
      		<div class="campo next"><label for="#fldcargo">Cargo</label><?=$fldcargo?></div>
      		<div class="campo"><label for="#fldcargo">Direcci&oacute;n</label><?=$flddireccion?></div>
      		<div class="campo next"><label for="#fldjubipen">Teléfonos</label><?=$fldtelefonos?></div>
      		<div class="campo"><label for="#fldcargo">E-mail</label><?=$fldemail?></div>
      		<div class="campo next"><label for="#fldSobreviviente">Recibe Pension de Sobreviviente por el fallecimiento de:</label><?=$fldSobreviviente?></div>
      	</form>
      	<div class="campo next"><?=$btnbtn1?></div>
      </div>
      
      
      </div>
      
      
      
   </div>


<span id="resultado-ajax"></span>

<script type="text/javascript">
<!--
	$(function(){
		$('#btnBuscar').button();
		$('#btn1').button();
	});

<?=$javascript?>

	function buscarJubiPen(){		
		$.ajax({
			url:"<?=base_url()?>index.php/analista/buscaJubiPen",
			type:"post",
			data:$('#formajubipen').serialize(),
			success:function(data){
				if (data!='false'){
					var myDatadata=JSON.parse(data);
					$('#nombre').val(myDatadata.nombre);
					$('#jubipen').val(myDatadata.id_tipo_personal);
					$('#cargo').val(myDatadata.cargo);
					$('#direccion').val(myDatadata.direccion);
					$('#telefonos').val(myDatadata.telefonos);
					$('#email').val(myDatadata.email);
				}else{
					alert('la persona no esta ni jubilada ni pensionada');
					$('#nombre').val("");
					$('#jubipen').val("");
					$('#cargo').val("");
					$('#direccion').val("");
					$('#telefonos').val("");
					$('#email').val("");
				}
			}
		})
	}
//-->
</script>
