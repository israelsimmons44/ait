<style>
	.odd{
		background-color:#ddd;
	}
</style>

<div class="ui-dialog ui-widget ui-widget-content ui-corner-all" style="float:left;position:relative;margin-right:20px;width:890px;">
   <div class="ui-dialog-titlebar ui-widget-header ui-corner-top ui-helper-clearfix">
      <span id="ui-dialog-title-dialog" class="ui-dialog-title">Solicitudes Activas en Proceso</span>
      <a class="ui-dialog-titlebar-close ui-corner-all" href="#"></a>
   </div>
   <div style="height:auto; width: auto;overflow: hidden;" class="ui-dialog-content ui-widget-content" id="dialog">
   	
      	{solicitudesactivas}
      
   </div>
   
</div>

<span style='margin-left:15px;' id='resultado-ajax'></span>

<div class="apple_overlay" id="overlay1">

	<!-- the external content is loaded inside this tag -->
	<div class="contentWrap"></div>

</div>

<script>

$("a[rel]").overlay({
	mask: 'black',
	effect: 'apple',

	onBeforeLoad: function() {

		// grab wrapper element inside content
		var wrap = this.getOverlay().find(".contentWrap");

		// load the page specified in the trigger
		wrap.load(this.getTrigger().attr("href"));
	}

});


$(function() {
	
	$('table').dataTable({"aaSorting": [[ 1, "asc" ]],
						"bStateSave": true,		
						"bJQueryUI": true,
						"sPaginationType": "full_numbers",
						"iDisplayLength": 10,
						"oLanguage": {
									"sZeroRecords": "No se encontraron registros!!!"
															
						}


	 });

	
});
</script>
