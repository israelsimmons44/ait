{persona}

<table id="recibos-table" width="100%" cellspacing="0">
<thead>
<tr><th>Ref.</th><th>A&ntilde;o</th><th width="500">Descripci&oacute;n</th><th>Acci&oacute;n</th></tr>
</thead>
<tbody>
{recibos}
<tr><td>{id_historico_nomina}</td><td>{anio}</td><td>{descripcion}</td><td>{imprime}</td></tr>
{/recibos}
</tbody>

</table>

<script>
	$(function(){
		$('#recibos-table').dataTable({"aaSorting": [[ 0, "desc" ]],
			"bAutoWidth":false,
			"bJQueryUI": true,
			"sPaginationType": "full_numbers",
			"iDisplayLength": 10,
			"oLanguage": {
						"sZeroRecords": "No se encontraron registros!!!"
												
			}
});
	})
</script>