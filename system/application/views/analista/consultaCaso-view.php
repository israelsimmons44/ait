<div class="ui-dialog ui-widget ui-widget-content ui-corner-all" style="float:left;position:relative;margin-right:20px;width:890px;">
   <div class="ui-dialog-titlebar ui-widget-header ui-corner-top ui-helper-clearfix">
      <span id="ui-dialog-title-dialog" class="ui-dialog-title">Detalle de Caso</span>
      <a class="ui-dialog-titlebar-close ui-corner-all" href="#"></a>
   </div>
   <div style="height:auto; width: auto;overflow: hidden;" class="ui-dialog-content ui-widget-content" id="dialog">
		
		
		<button onclick="javascript:abreform()">Agregar Observaci&oacute;n</button>

		<table width="100%" cellspacing="0" id="tablaCaso">
		<thead>
			<tr><th>Id</th><th>Fecha</th><th>Observaciones</th><th>Estatus</th></tr>
		</thead>
		<tbody>
		{detallecaso}
			<tr height="30"><td>{id_detalle_caso}</td><td>{fecha}</td><td>{observaciones}</td><td>{descripcion}</td></tr>
		{/detallecaso}
		</tbody>
		</table>
   		
   </div>
   
   
</div>

<span id="resultado-ajax"></span>

<div id="formularioDialogo">
{formulario}
</div>

<script type="text/javascript">
<!--
function abreform(){
	$('#formularioDialogo').dialog('open');
}

$(function(){
	
	$('#formularioDialogo').dialog({'autoOpen':false, 'width':500, 'height':300});
	$('#tablaCaso').dataTable({"aaSorting": [[ 0, "desc" ]],
		"bJQueryUI": true,
		"sPaginationType": "full_numbers",
		"iDisplayLength": 10,
		"oLanguage": {
					"sZeroRecords": "No se encontraron registros!!!"
											
		}


});
});
//-->
</script>
