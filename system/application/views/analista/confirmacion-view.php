<div class="ui-dialog ui-widget ui-widget-content ui-corner-all" style="float:left;position:relative;margin-right:20px;width:890px;">
	<div class="ui-dialog-titlebar ui-widget-header ui-corner-top ui-helper-clearfix">
    	<span id="ui-dialog-title-dialog" class="ui-dialog-title">Reimpresi&oacute;n</span>
      	<a class="ui-dialog-titlebar-close ui-corner-all" href="#"></a>
	</div>
	<div style="min-height: 109px; width: auto;overflow: hidden;" class="ui-dialog-content ui-widget-content" id="dialog">
   	
		<img style="float:left;" src="<?=base_url()?>images/warning.png"/><h2 style="float:left;">Confirmaci&oacute;n Requerida</h2>  
	
		<div style="margin-left:80px;">
			<p style="clear: both;">
				La impresi&oacute;n ya fue realizada por <?=$usuarioImpresion?> persona desea reimprimir?
			</p>
			

			<a class="confir" href='<?=base_url()?>index.php/imprimirplanilla/index/<?=$idSolicitud?>/OK'>Reimprimir</a>
			<a class="confir" id="volv" onclick="javascript:history.back();return false;">Cancelar</a>
		</div>
	</div>
</div>


<script>
	$(function(){
		$('.confir').button();
		
	})
</script>