<div style="float:right"><img src="http://rrhh.mppre.gob.ve/fotos/{cedula}.jpg" height="200"/></div>
<div id="datospersonales" style="clear:left;">
<h3>Datos Personales</h3>
<table>
<tr><td>C&eacute;dula:</td><td>{cedula}</td></tr>
<tr><td>Nombres:</td><td>{nombres}</td></tr>
<tr><td>Apellidos:</td><td>{apellidos}</td></tr>
<tr><td>Sexo:</td><td>{sexo}</td></tr>
<tr><td>Fecha de Nac.:</td><td>{fecha_nacimiento}</td></tr>
</table>
</div>

<div id="datostrabajador">
<h3>Datos de trabajador</h3>
<table id="datos" class="display" cellspacing="0">
<thead>
<tr><th>Ingreso</th><th>Egreso</th><th>Tipo Personal</th><th>Cargo</th><th>Dependencia</th><th>Lugar de Pago</th><th>Estatus</th></tr>
</thead>
<tbody>
{trabajador}
<tr><td align="center" width="40">{fecha_ingreso}</td><td align="center" width="40">{fecha_egreso}</td><td>{tipopersonal}</td><td>{descripcion_cargo}</td><td>{dependencia}</td><td>{lugarpago}</td><td align="center">{estatus}</td></tr>
{/trabajador}
</tbody>
</table>
</div>

<div style="width:46%;float:left;">
<h3>Solicitudes</h3>
<table width="100%">
<tr><td>Nombre</td><td>Fecha</td><td align="center">Estatus</td></tr>
{solicitudes}
	<tr><td>{nombre}</td><td>{fecha}</td><td align="center">{estatus}</td></tr>
{/solicitudes}
</table>
</div>

<div style="width:46%;float:right;">
<h3>Conceptos Fijos</h3>
<table width="100%">
<tr><td>Cod</td><td>Descripci&oacute;n</td><td>Monto</td></tr>
{conceptosfijos}
<tr><td>{cod_concepto}</td><td>{descripcion}</td><td align="right">{monto}</td></tr>
{/conceptosfijos}
</table>
</div>

<script>
	$(function(){
		$('#datos').dataTable({
			"bAutoWidth":false,
			"bJQueryUI": true,
			"sPaginationType": "full_numbers",
			"iDisplayLength": 10,
			"oLanguage": {
						"sZeroRecords": "No se encontraron registros!!!"
												
			}


});
		
	})
</script>