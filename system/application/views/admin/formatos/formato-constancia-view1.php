<script type="text/javascript" src="<?=base_url()?>js/ckeditor/ckeditor.js" ></script>

<div class="ui-dialog ui-widget ui-widget-content ui-corner-all" style="float:left;position:relative;margin-right:20px;width:890px;">
   <div class="ui-dialog-titlebar ui-widget-header ui-corner-top ui-helper-clearfix">
      <span id="ui-dialog-title-dialog" class="ui-dialog-title">Modificación de Formato de Constancia {tipoconstancia} de Trabajo</span>
      <a class="ui-dialog-titlebar-close ui-corner-all" href="#"></a>
   </div>
   <div style="min-height: 109px; width: auto;" class="ui-dialog-content ui-widget-content" id="dialog">
   	
      <p>

      	<form method='post' id='form1' name='form1'>
      		<input type='hidden' name='idtramite' id='idtramite' value='20'><!-- Este idtramite=20 es una constancia de trabajo -->
      		<input type='hidden' name='nro_formato' id='nro_formato' value='{nroFormato}'/>
	      	<table width='100%'>
	      		<tr><td width='150'>Tipo de Personal</td><td>
	      		<select name="idtipopersonal" id="idtipopersonal">
	      			<option value="0">Seleccione...</option>
	      			<option value="14">CONTRATADO</option>
	      			<option value="13">ADMINISTRATIVO</option>
	      			<option value="20">DIPLOM&Aacute;TICO</option>
	      			<option value="42">JUBILADOS</option>
	      			<option value="43">PENSIONADOS</option>
	      		</select>
	      		</td></tr>
	      		<tr><td width='150'><label for='editor'>Formato</label></td><td><textarea id='editor' name='editor' >{valor}</textarea></td></tr>
	      		
	      		<tr><td width='150'></td><td><button name='btn' id='btn' onclick='javascript:enviar();return false;' >Guardar Formato</button></td></tr>
	      	</table>
      	</form>
<script>
$('button').button();	

function enviar (){
	
		var texto=escape(CKEDITOR.instances.editor.getData());
		$.ajax({
			url:'<?=base_url()?>/index.php/admin/grabaFormatoConstancia',
			type:'post',
			data:$('#form1').serialize() + '&valor=' + texto,
			success:function(data){ $('#resultado-ajax').html(data) }
		});
	
}$(document).ready(function(){
			var editor = CKEDITOR.replace( 'editor',
		{
			toolbar : [ ['Format','-', 'Bold', 'Italic', 'Underline', '-','NumberedList','BulletedList','-','Outdent','Indent','-','Table' ] ]
		});

		$('#idtipopersonal').change(function(){
			$.ajax({
				url:'<?=base_url()?>/index.php/admin/obtenerFormato',
				data:$('#form1').serialize(),
				type:'post',
				success:function(data){CKEDITOR.instances.editor.setData(data)}
				})

		});

		})</script>
      </p>
      
   </div>
</div>

<span style='margin-left:15px;' id='resultado-ajax'></span>