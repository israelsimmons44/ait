<div class="ui-dialog ui-widget ui-widget-content ui-corner-all" style="float:left;position:relative;margin-right:20px;width:890px;">
   <div class="ui-dialog-titlebar ui-widget-header ui-corner-top ui-helper-clearfix">
      <span id="ui-dialog-title-dialog" class="ui-dialog-title">Reclamos y Sugerencias</span>
      <a class="ui-dialog-titlebar-close ui-corner-all" href="#"></a>
   </div>
   <div style="min-height: 109px; width: auto;overflow: hidden;" class="ui-dialog-content ui-widget-content" id="dialog">
   	
		   	<div id="tabs">
		   		<ul>
		   			<li><a href="#tab1">Reclamos</a></li>
		   			<li><a href="#tab2">Sugerencias</a></li>
		   		</ul>
		   		<div id="tab1">{formulario}</div>
		   		<div id="tab2">
		   			<form id="formulario2">
		   				<input type="hidden" id="cedula" name="cedula" value="{cedula}"/>
		   				<input type="hidden" id="usuario" name="usuario" value="{usuario}"/>
		   				<table>
		   					<tr><td width="150">Sugerencia</td><td><textarea id="sugerencia" name ="sugerencia" cols="40" rows="7"></textarea></td></tr>
		   					<tr><td>&nbsp;</td><td><button id="btnSugerencia" onclick="javascript:enviarSugerencia();return false;">Enviar</button></td></tr>
		   				</table>
		   			</form>
		   		</div>
		   	</div>
		   
      </div>
   </div>
   
   
<div id="cargando"><img src="<?=base_url()?>images/ajax-load.gif"/></div>
<span style='margin-left:15px;' id='resultado-ajax'></span>

<script>
	$(function(){
		$('#tabs').tabs();

		$('#btnSugerencia').button();
		
	})
	
	function enviarSugerencia(){
		$('#resultado-ajax').hide();
		$('#cargando').show()
		$.ajax({
			"url":"<?=base_url()?>index.php/reclamos/procSugerencia",
			"type":'post',
			"data":$('#formulario2').serialize(),
			"success":function(data){ $('#resultado-ajax').html(data); $('#cargando').hide();$('#resultado-ajax').show(); }
		});
	}

</script>