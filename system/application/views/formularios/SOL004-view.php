<div class="ui-dialog ui-widget ui-widget-content ui-corner-all" style="width:570px;">
   <div class="ui-dialog-titlebar ui-widget-header ui-corner-top ui-helper-clearfix">
      <span id="ui-dialog-title-dialog" class="ui-dialog-title">Formulario</span>
      <a class="ui-dialog-titlebar-close ui-corner-all" href="#"></a>
   </div>
   <div style="min-height: 109px; width: auto;" class="ui-dialog-content ui-widget-content" id="dialog">
   	<h3>{titulo}</h3>
      <p>
      	<form id="form1" name="form1">
      	{cedula}
      	{codtramite}
      	
      	<table width="100%">
      		<tr><td colspan="2"><strong>DATOS DE SOLICITANTE:</strong></td></tr>
      		<tr><td width="100">Teléfono ofc.</td><td>{telefono}</td></tr>
      		<tr><td width="100">Extensión</td><td>{extension}</td></tr>
      		<tr><td width="100">Teléfono hab.</td><td>{telefonohab}</td></tr>
      		<tr><td>Celular</td><td>{telefonocel}</td></tr>
      	</table>
      	
      	
      	<table width="100%">
      	
      		<tr><td colspan="2"><strong>DATOS DE HIJOS:</strong></td></tr>
      		<tr><td>Nombres y Apellidos</td><td>Fecha de Nacimiento</td></tr>
      		
      		<tr><td>{nombreh1}</td><td>{fechah1}</td></tr>
      		<tr><td>{nombreh2}</td><td>{fechah2}</td></tr>
      		<tr><td>{nombreh3}</td><td>{fechah3}</td></tr>
      		<tr><td>{nombreh4}</td><td>{fechah4}</td></tr>
      		<tr><td>{nombreh5}</td><td>{fechah5}</td></tr>
      	</table>
      	{btn}
      	</form>
      </p>
   </div>
</div>

<span style='margin-left:15px;' id='resultado-ajax'></span>

<script type="text/javascript">
<!--
{javascript}
//-->
</script>