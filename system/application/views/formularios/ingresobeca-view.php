<form method="post" action="<?php echo base_url();?>index.php/formularios/ProcAgregaBeca">
<div class="ui-dialog ui-widget ui-widget-content ui-corner-all" style="float:left;position:relative;margin-right:40px;width:890px;">
   <div class="ui-dialog-titlebar ui-widget-header ui-corner-top ui-helper-clearfix">
      <span id="ui-dialog-title-dialog" class="ui-dialog-title">
          <h3>{nombre_fam}</h3>
      </span>
      <a class="ui-dialog-titlebar-close ui-corner-all" href="#"></a>
   </div>
<table width="890" height="208">
   <tr><td></td></tr>
   <tr><td height="10" colspan="5" align="center" valign="middle" bgcolor="#CCCCCC">
       <h4>Datos Adicionales</h4></td>
   </tr>
   <tr><td></td></tr> 
   <tr>
       <td>Ref.:</td><td>{id_familiar}<?= form_hidden('id_familiar','{id_familiar}'); ?></td>
   </tr>
   <tr>
         <td>C&eacute;dula Hijo(a):</td>
         <td>{cedula_familiar}<?= form_hidden('cedula_familiar','{cedula_familiar}'); ?></td>
   </tr>
   <tr>
    <td width="245">Nivel Educativo: </td>
    <td width="109">
      <select id="nivel_educativo" name="nivel_educativo">
         <option value="P">Preescolar</option>
         <option value="B">Básica</option>
         <option value="D">Segundaria/Diversificado</option>
         <option value="U">Universitaria</option>
         <option value="E">Especial</option>
      </select>
    </td>
    <td width="280">Grado / Semestre: </td>
    <td>
        <input id="grado_semestre" name="grado_semestre" type="text" size="10" />
    </td>
  </tr>
  <tr>
   <td>Año Escolar:</td>
   <td>
      <select id="anio_escolar" name="anio_escolar">
         <option value="2014">2014</option>
         <option value="2015">2015</option>
         <option value="2016">2016</option>
         <option value="2017">2017</option>
      </select>
   </td>
   <td>Calificación: </td>
   <td>       
        <select id="promedio_notas" name="promedio_notas">
         <option value="A">A</option>
         <option value="B">B</option>
         <option value="C">C</option>
         <option value="9">9</option>
         <option value="8">8</option>
         <option value="7">7</option>
         <option value="6">6</option>
         <option value="5">5</option>
         <option value="20">20</option>
         <option value="19">19</option>
         <option value="18">18</option>
         <option value="17">17</option>
         <option value="16">16</option>
         <option value="15">15</option>
         <option value="14">14</option>
         <option value="13">menor o igual a 13</option>
      </select>
   </td>
  </tr>
  <tr>
    <td>Institución: </td>
    <td colspan="3">
        <input id="instituto" name="instituto" type="text" size="60" />
    </td>
  </tr>
  <tr>
    <td>Carrera: </td>
    <td colspan="3">
        <input id="carrera_especialidad" name="carrera_especialidad" type="text" size="60" />
    </td>
  </tr>
  <tr>
    <td>Becas: </td>
    <td>
      <select id="id_tipo_beca" name="id_tipo_beca">
         <option value="1">Preescolar/Básica</option>
         <option value="2">Secundaria/Diversificada</option>
         <option value="3">Universitaria</option>
         <option value="4">Especial</option>
      </select>
    </td>
    <td width="100">Utiles: </td>
    <td>
      <td>
        <input width="50" type="checkbox" name="utiles" value="X"/>
      </td>
    </td>
  </tr>
  <tr><td></td></tr>
  <tr><td></td></tr>
  <tr><td></td></tr>
</table>
<table width="890" height="208">
  <tr><td></td></tr>
  <tr>
      <td height="10" colspan="5" align="center" valign="middle" bgcolor="#CCCCCC">
         <h4>Documentos Consignados </h4>
      </td>
  </tr>
  <tr>
    <td>Constancia de Inscripción(*): </td>
    <td>
        <input type="checkbox" name="inscripcion" value="X" />
    </td>
    <td>Constancia de Calificación(*): </td>
    <td>
        <input type="checkbox" name="calificacion" value="X"/>
    </td>
  </tr>
  <tr>
    <td>Copia Carnet Trabajador(*): </td>
    <td>
        <input type="checkbox" name="carnet" value="X" />
    </td>
    <td>Copia Cédula Trabajador(*): </td>
    <td>
        <input type="checkbox"  name="cifuncionario" value="X"/>
    </td>
  </tr>
  <tr>
    <td>Copia Cédula Beneficiario: </td>
    <td>
        <input type="checkbox"  name="cibeneficiario" value="X" />
    </td>
    <td>Partida Nacimiento Beneficiario(*): </td>
    <td>
        <input type="checkbox" name="partidanacimiento" value="X" />
    </td>
  </tr>
  <tr>
    <td>Informe Médico Actual (niño especial): </td>
    <td>
        <input type="checkbox" name="informe" value="X" />
    </td>
    <td>Copia del Contrato (nuevo ingreso): </td>
    <td>
        <input type="checkbox" name="contrato" value="X" />
    </td>
  </tr>
  
  <tr>
    <td width="380">
        <font color="red">(*) Recaudos Obligatorios</font> </td>
  </tr>
  <tr><td></td></tr>     
  <tr><td></td></tr><tr><td></td></tr>
  <tr>
        <td></td>
        <td>
            <button id="guardar" colspan="5" aling="center" type="submit">Guardar</button>
        </td>
  </tr>
  <tr><td></td></tr>
  <tr><td></td></tr>
  <tr><td></td></tr>
</table>
</form>
</div>
<script>
$(function(){
	$('#guardar').button();
});
</script>

