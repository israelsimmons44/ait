<div style="float:right">
    <img src="http://rrhh.mppre.gob.ve/fotos/{cedula}.jpg" height="250"/>
</div>

<div id="datospersonales" style="clear:left">
<h3>Datos del Trabajador</h3>
<table>
   <tr><td>C&eacute;dula:</td><td>{cedula}</td></tr>
   <tr><td>Nombres:</td><td>{nombres}</td></tr>
   <tr><td>Apellidos:</td><td>{apellidos}</td></tr>
   <tr><td>Dependencia:</td><td>{dependencia}</td></tr>
   <tr><td>Cargo:</td><td>{cargo}</td></tr>
   <tr><td>Direcci&oacute;n de Habitaci&oacute;n:</td><td>{direccion_residencia}</td></tr>
   <tr><td>Celular:</td><td>{telefono_celular}</td></tr>
   <tr><td>Tel&eacute;fono Oficina:</td><td>{telefono_oficina}</td></tr>
   <tr><td>Tel&eacute;fono Habitación:</td><td>{telefono_residencia}</td></tr>
   <tr><td>Email:</td><td>{email}</td></tr>
</table>
</div>

<div id="datostrabajador">
<h3>Datos Hijo(s) del Trabajador</h3>
<table id="datos" class="display" cellspacing="0">
    <thead>
    <tr><th>C&eacute;dula</th><th>Nombre</th><th>Sexo</th><th>Fecha Nacimiento</th><th>Edad</th><th>Agrega</th><th>Imprime</th></tr>
    </thead>
    <tbody>
    {hijostrabajador}
    <tr>
        <td align="center" width="40">{cedula_familiar}</td>
        <td align="center" width="80">{nombre_fam}</td>
        <td align="center" width="20">{sexo}</td>
        <td align="center" width="40">{fecha_nacimiento}</td>
        <td align="center" width="20">{edad}</td>
        <td align="center" width="60">   
            <a href="{link1}/{id_familiar}">
              <font color='blue'>
                <b>{mensaje1}</b>
              </font>
            </a>
        </td>
        <td align="center" width="60">
            <a href="{link2}/{id_familiar}">
             <font color='blue'>
                <b>{mensaje2}</b>
              </font>
            </a>
        </td>
        
    </tr>
    {/hijostrabajador}
    </tbody>
</table>
</div>

<script>

$(function(){

		$('#datos').dataTable({
			"bAutoWidth":false,
			"bJQueryUI": true,
			"sPaginationType": "full_numbers",
			"iDisplayLength": 10,
			"oLanguage": {
                            "sZeroRecords": "No se encontraron registros por cualquiera de las siguientes causas:<br>- No posee hijos(as) registrados en sistema<br>- Edad de Hijos(as) no aptos para asignación de Becas y/o Útiles"
												
			}


});
		
	})
</script>
