<form method="post" action="<?php echo base_url();?>index.php/formularios/ProcAgregaGuarderiaMensual">
<div class="ui-dialog ui-widget ui-widget-content ui-corner-all" style="float:left;position:relative;margin-right:40px;width:890px;">
   <div class="ui-dialog-titlebar ui-widget-header ui-corner-top ui-helper-clearfix">
      <span id="ui-dialog-title-dialog" class="ui-dialog-title">
          <h3>{nombre_fam}</h3>
      </span>
      <a class="ui-dialog-titlebar-close ui-corner-all" href="#"></a>
   </div>
<table width="890" height="108">
   <tr><td></td></tr>
   <tr><td height="10" colspan="5" align="center" valign="middle" bgcolor="#CCCCCC">
       <h4>Datos Adicionales</h4></td>
   </tr>
   <tr><td></td></tr> 
   <tr>
       <td>Ref.:</td><td>{id_familiar}<?= form_hidden('id_familiar','{id_familiar}'); ?></td>
   </tr>
   <tr>
         <td>C&eacute;dula Hijo(a):</td>
         <td>{cedula_familiar}<?= form_hidden('cedula_familiar','{cedula_familiar}'); ?></td>
         <td>Año Escolar:</td>
         <td>
                <select id="anio_escolar" name="anio_escolar">
                    <option value="2014">2014</option>
                    <option value="2015">2015</option>
                    <option value="2016">2016</option>
                    <option value="2017">2017</option>
                </select>
         </td>
   </tr>
 
  <tr>
    <td>Maternal o Guardería: </td>
    <td colspan="3">
        <input id="instituto" name="instituto" type="text" size="60" />
    </td>
  </tr>
  <tr>
    <td>Rif (Ejemplo V123456789): </td>
    <td colspan="3">
        <input id="rif" name="rif" type="text" size="10" />
    </td>
  </tr>  
</table>
<table width="890" height="108">
  <tr>
      <td height="10" colspan="5" align="center" valign="middle" bgcolor="#CCCCCC">
         <h4>Detalle de Facturas </h4>
      </td>
  </tr>
  <tr>
    <td width="180">Número de Factura: </td>
    <td>
        <input id="numero_factura" align="left" name="numero_factura" type="text" size="13" />
    </td>
    <td width="30" >Mes: </td>
    <td>
        <select id="mes_factura" name="mes_factura">
         <option value="NS">Seleccione...</option>
         <option value="1">Enero</option>
         <option value="2">Febrero</option>
         <option value="3">Marzo</option>
         <option value="4">Abril</option>
         <option value="5">Mayo</option>
         <option value="6">Junio</option>
         <option value="7">Julio</option>
         <option value="8">Agosto</option>
         <option value="9">Septiembre</option>
         <option value="10">Octubre</option>
         <option value="11">Noviembre</option>
         <option value="12">Diciembre</option>
       </select>
     </td>
     <td width="150">
         <button id="guardar" colspan="5" aling="center" type="submit">Guardar</button>
    </td>
    </tr>
</table>    
</div>
</form>
<script>
$(function(){
	$('#guardar').button();
});
</script>
