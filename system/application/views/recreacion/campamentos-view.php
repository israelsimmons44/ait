<div class="ui-dialog ui-widget ui-widget-content ui-corner-all" style="float:left;position:relative;margin-right:20px;width:890px;">
   <div class="ui-dialog-titlebar ui-widget-header ui-corner-top ui-helper-clearfix">
      <span id="ui-dialog-title-dialog" class="ui-dialog-title">Consultar Hijos(as)</span>
      <a class="ui-dialog-titlebar-close ui-corner-all" href="#"></a>
   </div>
   <div style="min-height: 109px; width: auto;overflow: hidden;" class="ui-dialog-content ui-widget-content" id="dialog">
		   <table id="detallehijos" class='display' cellspacing='0';>
			<thead>
				<tr><th>C&eacute;dula Familiar</th><th>Nombre</th><th>Imprime Planilla</th></tr>
			</thead>
			{campamentos}
				<tr height='30px' width: auto><td>{cedula_familiar}</td><td>{nombre_familiar}</td><td>{imprime}</td></tr>
			{/campamentos}
			</table>	
		   
    </div>
</div>

<span style='margin-left:15px;' id='resultado-ajax'></span>

<script>
	$(function(){
		$('#detallehijos').dataTable({"aaSorting": [[ 1, "desc" ],[ 2, "desc" ],[ 3, "desc" ]],
			"bAutoWidth":false,
			"bJQueryUI": true,
			"sPaginationType": "full_numbers",
			"iDisplayLength": 10,
			"aoColumns":[null,null,{"bVisible":false},{"bVisible":false},null,null],
			"oLanguage": {
						"sZeroRecords": "No se encontraron registros!!!"
												
			}
});
	});
</script>
