<form method="post" action="<?php echo base_url();?>index.php/recreacion/ProcAgregaDatosMedicosHijos">
<div class="ui-dialog ui-widget ui-widget-content ui-corner-all" style="float:left;position:relative;margin-right:15px;width:950px;">
   <div class="ui-dialog-titlebar ui-widget-header ui-corner-top ui-helper-clearfix">

      <span id="ui-dialog-title-dialog" class="ui-dialog-title"><h3>{nombre_fam}</h3></span>
   
      <a class="ui-dialog-titlebar-close ui-corner-all" href="#"></a>
   </div>
<table>
   <tr>
       <td></td>
   </tr>
   <tr>
       <td></td>
   </tr>
   <tr>
      <td height="20" colspan="2" align="center" valign="middle" bgcolor="#CCCCCC">
      <h4>DATOS ADICIONALES</h4>
      </td>
   </tr>
    <tr>
        <td></td>
    </tr>     
    <tr>
        <td></td>
    </tr>    
   <tr><td>Ref.:</td><td>{id_familiar}<?= form_hidden('id_familiar','{id_familiar}'); ?></td></tr>
    <tr><td>C&eacute;dula Hijo(a):</td><td>{cedula_familiar}<?= form_hidden('cedula_familiar','{cedula_familiar}'); ?></td></tr>
    <tr><td>Talla Franela:</td>
            <td>
                <select name="talla_franela">
                        <option value="N">N</option>
                        <option value="6">6</option>
                        <option value="8">8</option>
                        <option value="10">10</option>
                        <option value="12">12</option>
                        <option value="14">14</option>
                        <option value="16">16</option>
				        <option value="S">S</option>
				        <option value="M">M</option>
                        <option value="L">L</option>
		        </select>
		    </td>
    </tr>
    <tr><td>Talla Gorra:</td>
            <td>
                <select name="talla_gorra">
                        <option value="N">N</option>
                        <option value="U">U</option>
		        </select>
		    </td>
    </tr>    
    <tr><td>Talla Pantal&oacute;n:</td>
            <td>
                <select name="talla_pantalon">
                        <option value="N">N</option>
                        <option value="6">6</option>
                        <option value="8">8</option>
                        <option value="10">10</option>
                        <option value="12">12</option>
                        <option value="14">14</option>
                        <option value="16">16</option>
				        <option value="S">S</option>
				        <option value="M">M</option>
                        <option value="L">L</option>
		        </select>
		    </td>
    </tr>
    <tr><td>Direcci&oacute;n:</td><td><textarea name="direccion_hijo" rows="1" cols="60"></textarea></td></tr>
    <tr>
        <td></td>
    </tr>     
    <tr>
        <td></td>
    </tr>     
   <tr>
       <td height="20" colspan="2" align="center" valign="middle" bgcolor="#CCCCCC">
      <h4>DATOS M&Eacute;DICOS</h4>
       </td>
   </tr>
    <tr>
        <td></td>
    </tr>     
    <tr>
        <td></td>
    </tr>    
    <tr><td>Enfermedades:</td><td><textarea name="enfermedad" rows="1" cols="60"></textarea></td></tr>
    <tr><td>Al&eacute;rgico a (alimentos, medicamentos u otros):</td><td><textarea name="alergico" rows="1" cols="60"></textarea></td></tr>
    <tr><td>Medicamento indicado (nombre y dosis):</td><td><textarea name="medicamento" rows="1" cols="60"></textarea></td></tr>
    <tr><td>M&eacute;dico Tratante:</td><td><input name="medicotratante" id="medicotratante" type="text" size="38"/></td></tr>
    <tr><td>Tel&eacute;fono M&eacute;dico:</td><td><input name="telefono_medico" id="telefono_medico" type="text" size="12"/>&nbsp;&nbsp;&nbsp;&nbsp;<b><i>Ej: 0414-5555555</i></b></td></tr>
    <tr>
        <td></td>
    </tr>     
    <tr>
        <td></td>
    </tr>   
   <tr>
      <td height="20" colspan="2" align="center" valign="middle" bgcolor="#CCCCCC">
          <h4>DATOS REPRESENTANTE AUTORIZADO (Obviar si el niño(a) ser&aacute; retirado(a) por el trabajador)</h4>
      </td>
   </tr>
    <tr>
        <td></td>
    </tr>     
    <tr>
        <td></td>
    </tr>    
    <tr><td>C&eacute;dula:</td><td><input name="cedula_personaretira" id="cedula_personaretira" type="text" size="12"/></td></tr>
    <tr><td>Apellidos y Nombres:</td><td><input name="nombre_personaretira" id="nombre_personaretira" type="text" size="60"/></td></tr>
    <tr><td>Parentesco:</td><td><input name="parentesco" id="parentesco" type="text" size="20"/></td></tr>
    <tr><td>Direcci&oacute;n:</td><td><textarea name="direccion_personaretira" rows="1" cols="60"></textarea></td></tr>
    <tr><td>Tel&eacute;fono Habitacion:</td><td><input name="telefono_personaretira" id="telefono_personaretira" type="text" size="12"/>&nbsp;&nbsp;&nbsp;&nbsp;<b><i>Ej: 0212-4444444</i></b></td></tr>
    <tr><td>Celular:</td><td><input name="celular_personaretira" id="celular_personaretira" type="text" size="12"/>&nbsp;&nbsp;&nbsp;&nbsp;<b><i>Ej: 0414-5555555</i></b></td></tr>
    <tr>
        <td></td>
    </tr>
    <tr>
        <td></td>
    </tr>
    <tr>
        <td></td>
    </tr>
    <tr>
        <td></td>
    </tr>     
    <tr>
        <td></td>
        <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <button id="guardar" aling="center" type="submit">Guardar</button>
        </td>
    </tr>
    <tr>
         <td></td>
    </tr>
    <tr>
         <td></td>
    </tr>
    <tr>
        <td></td>
    </tr>
    <tr>
        <td></td>
    </tr>
</table>
</form>
</div>
<script>
$(function(){
	$('#guardar').button();
});
</script>
