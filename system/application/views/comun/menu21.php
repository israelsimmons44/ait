<img src="<?=base_url()."images/todos.jpg"?>">
<div style="background-image:url('<?=base_url()."images/bgmain.jpg"?>');background-color: #fff;height:50px;color: black;">

</div>

<style>
<!--
	
	.fg-button { float:left; padding: .4em 1em; text-decoration:none !important; cursor:pointer; position: relative; text-align: center; zoom: 1; }
	.fg-button .ui-icon { position: absolute; top: 50%; margin-top: -8px; left: 50%; margin-left: -8px; }
		
	button.fg-button { width:auto; overflow:visible; } /* removes extra button width in IE */
	
	.fg-button-icon-left { padding-left: 2.1em; }
	.fg-button-icon-right { padding-right: 2.1em; }
	.fg-button-icon-left .ui-icon { right: auto; left: .2em; margin-left: 0; }
	.fg-button-icon-right .ui-icon { left: auto; right: .2em; margin-left: 0; }
	.fg-button-icon-solo { display:block; width:8px; text-indent: -9999px; }	 /* solo icon buttons must have block properties for the text-indent to work */	

-->
</style>


<script type="text/javascript">
<!--
	
	$(function(){
		$('.fg-button').hover(
    		function(){ $(this).removeClass('ui-state-default').addClass('ui-state-focus'); },
    		function(){ $(this).removeClass('ui-state-focus').addClass('ui-state-default'); }
    	);

		$('#mnuAdministracion').menu({content:$('#mnuAdministracion').next().html(), flyOut:true});
		$('#mnuanalista').menu({content:$('#mnuanalista').next().html(), flyOut:true});
		$('#mnutramites').menu({content:$('#mnutramites').next().html(), flyOut:true});
		$('#mnuprueba').menu({content:$('#mnuprueba').next().html(), flyOut:true});
		$('#mnuseguimiento').menu({content:$('#mnuseguimiento').next().html(), flyOut:true});
	});
	
//-->
</script>

<div id="menu" class="ui-widget-header ui-corner-bottom" style="margin-bottom:15px;float:left;display:block; width:898px;">
<?php if ($permisos['administrador']=="t"){?>
<a tabindex="1" href="#administracion" class="fg-button fg-button-icon-right ui-widget ui-state-default ui-corner-bottom" id="mnuAdministracion"><span class="ui-icon ui-icon-triangle-1-s"></span>Administración</a>
<div id="administracion" class="hidden">
	<ul>
		<li><a href="#">Recaudos</a></li>
		<li><a href="#">Solicitudes</a></li>
		<li><a href="#">Formatos</a>
			<ul>
				<li><a href="<?=base_url()?>index.php/admin/formatoConstancia/1">Constancia mensual</a>
				<li><a href="<?=base_url()?>index.php/admin/formatoConstancia/2">Constancia anual</a>
				<li><a href="#">Constancia otro</a>
				
			</ul>
		</li>
		
		<li><a href="#">Par&aacute;metros</a>
			<ul>
				<li><a href="<?=base_url()?>index.php/admin/tipoReclamo">Tipo de Reclamo</a></li>
				
			</ul>
		</li>
		
		
		<li><a href="<?=base_url()?>index.php/admin/verSugerencias">Sugerencias</a></li>
		<li><a href="<?=base_url()?>index.php/reportes">Reportes</a></li>
		<li><a href="#">Usuarios</a></li>
	</ul>
</div>
<?php }?>

<?php if ($permisos['analista']=="t"){?>
<a tabindex="2" href="#analista" class="fg-button fg-button-icon-right ui-widget ui-state-default ui-corner-bottom" id="mnuanalista"><span class="ui-icon ui-icon-triangle-1-s"></span>Analista</a>
<div id="analista" class="hidden">

	<ul>
		<?php if ($permisos['entrega']=="t"){?>
		<li><a href="<?=base_url()."index.php/analista/entregaDocumentos"?>">Entrega de documentos</a></li>
		<?php }?>
		<li><a href="<?=base_url()."index.php/analista/solicitudespendientes"?>">Solicitudes pendientes</a></li>
		<li><a href="<?=base_url()."index.php/analista/crearsolicitud"?>">Crear Solicitud</a></li>
		<li><a href="#">Verificaci&oacute;n</a>
			<ul>
				<li><a href="<?=base_url()."index.php/analista/verificadorRecibo"?>">Recibo de Pago</a></li>
				<li><a href="<?=base_url()."index.php/analista/verificadorConstancia"?>">Constancia de Trabajo</a></li>
			</ul>
		</li>
		<li><a href="<?=base_url()."index.php/analista/recibos"?>">Recibos de Pago</a></li>
		<li><a href="<?=base_url()."index.php/analista/comparecencia"?>">Acta de Comparecencia</a></li>
		<li><a href="<?=base_url()."index.php/analista/verReclamos"?>">Ver Reclamos</a></li>
		<li><a href="#">Listados</a>
			<ul>
				<li><a href="<?=base_url()."index.php/analista/verDatosProcesados"?>">Solicitudes Procesadas</a></li>
				
			</ul>
		</li>
		<li><a href="<?=base_url()."index.php/analista/personal"?>">Consulta de Personal</a></li>
		
	</ul>
	
</div>



<?php }?>


<?php if ($permisos['seguimiento']=="t"){?>
<a tabindex="2" href="#seguimiento" class="fg-button fg-button-icon-right ui-widget ui-state-default ui-corner-bottom" id="mnuseguimiento"><span class="ui-icon ui-icon-triangle-1-s"></span>Seguimiento</a>
<div id="analista" class="hidden">

	<ul>
		<li><a href="<?=base_url()."index.php/gestion"?>">Registro</a></li>		
		<li><a href="<?=base_url()."index.php/gestion/consulta"?>">Consulta</a></li>
		<li><a href="#">Reportes</a>
			<ul>
				<li><a href="<?=base_url()."index.php/reportes/totalesSeguimiento"?>">Total Anual</a>
			</ul>
		</li>
	</ul>
	
</div>
<?php }?>

<a tabindex="3" href="#tramites" class="fg-button fg-button-icon-right ui-widget ui-state-default ui-corner-bottom" id="mnutramites"><span class="ui-icon ui-icon-triangle-1-s"></span>Trabajador</a>
<div id="analista" class="hidden">
	<ul>
		<!-- <li><a href="<?=base_url()."index.php/main/solicitud/"?>">Solicitud</a>
			<ul>
				<li><a href="#">kjsdalfbaslkdf</a></li>
				<li><a href="#">kjsdalfbaslkdf</a></li>
				<li><a href="#">kjsdalfbaslkdf</a></li>
				<li><a href="#">kjsdalfbaslkdflkjnr </a></li>
				
			</ul>
		</li>
		 -->
		<li><a href="<?=base_url()."index.php/recibos/"?>">Recibos de Pago</a></li>
		<li><a href="<?=base_url()."index.php/main/solicitud2/"?>">Crear Solicitud</a></li>
		<li><a href="<?=base_url()."index.php/main/consultatramite/"?>">Consulta de Solicitudes</a></li>
		<li><a href="<?=base_url()."index.php/reclamos/reclamo"?>">Reclamos y sugerencias</a></li>
		
	</ul>
</div>


<?php if ($this->session->userdata('usuario')=="israel.simmons998"){?>
<a tabindex="4" href="#desarrollo" class="fg-button fg-button-icon-right ui-widget ui-state-default ui-corner-bottom" id="mnuprueba"><span class="ui-icon ui-icon-triangle-1-s"></span>Desarrollo</a>
<div id="desarrollo" class="hidden">
	<ul>
		<li><a href="<?=base_url()."index.php/prueba/"?>">Prueba 1</a></li>
		<li><a href="#">Prueba 2</a></li>
		<li><a href="<?=base_url()."index.php/prueba/prueba4"?>">Prueba 4</a></li>
		<li><a href="<?=base_url()."index.php/prueba/pdf"?>">Prueba PDF</a></li>
		<li><a href="<?=base_url()."index.php/prueba/tcpdf"?>">Prueba TCPDF</a></li>
		<li><a href="<?=base_url()."index.php/prueba/prueba5"?>">Prueba 5</a></li>
		<li><a href="<?=base_url()."index.php/prueba/prueba6"?>">Prueba 6</a></li>
	</ul>
</div>
<?php }?>
<a style="" href="<?=base_url()."index.php/comun/logout/"?>"><div class="fg-button ui-widget ui-state-default ui-corner-br" style="float:right;height:15px;display:block;">Cerrar Sesión</div></a>


</div>