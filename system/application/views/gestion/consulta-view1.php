<div class="ui-dialog ui-widget ui-widget-content ui-corner-all" style="float:left;position:relative;margin-right:20px;width:1000px;margin-left: -50px;">
	<div class="ui-dialog-titlebar ui-widget-header ui-corner-top ui-helper-clearfix">
    	<span id="ui-dialog-title-dialog" class="ui-dialog-title">Consulta de Gesti&oacute;n</span>
      	<a class="ui-dialog-titlebar-close ui-corner-all" href="#"></a>
	</div>
	<div style="min-height: 109px; width: auto;overflow: hidden;" class="ui-dialog-content ui-widget-content" id="dialog">
		
		
		<div id="tabs">
			<ul>
				
				<li><a href="#tab2">Por persona</a></li>
			</ul>
			
			
			
			<div id="tab2">
				<form id="form2" name="form2" method="post">
					<table>
						<tr><td>C&eacute;dula</td><td><input name="cedula" id="cedula" type="text"/><button type="submit" id="btnbuscar" onclick="javascript:buscaPersona();return false;">Buscar</button></td></tr>
					</table>
				</form>
				
				<div id="resultado-busqueda" style="margin-top:50px;"></div>
				
				<div id="formulario-actualizacion" title="Actualizaci&oacute;n">
					<h3 id="label-tramite"></h3>
					<form name="frmActualizacion" id="frmActualizacion">
						<input type="hidden" id="id_seguimiento" name="id_seguimiento">
						<table>
							<tr><td>Fecha Enviado</td><td><input class="datepick" name="fecha_enviada" id="fecha_enviada-frm"/></td></tr>
							<tr><td>Fecha Recibido Firmado</td><td><input class="datepick" name="fecha_recibida_firmada" id="fecha_recibida-firmada-frm"/></td></tr>
							<tr><td>Fecha Entregado</td><td><input class="datepick" name="fecha_entregada" id="fecha_entregada-frm"/></td></tr>
							<tr><td>&nbsp;</td><td><button id="btn-actualiza" onclick="javascript:actualizaSeguimiento();return false;">Actualizar</button><button id="btn-reset" onclick="javascript:reset1();return false;">Volver</button></td></tr>
							
						</table>
					</form>
				</div>
				
			</div>
		
		</div>
		
		
		
	</div>
</div>



<div id="resultado-ajax"></div>

<script>
	$(function(){
		$('#formulario-actualizacion').hide();
		$('#tabs').tabs();
		$('#btnbuscar').button();
		$('#btn-actualiza').button();
		$('#btn-reset').button();
		
		$('.datepick').datepicker({ dateFormat: "dd-mm-yy" });
		
		
		$('.display').dataTable({	
			"bJQueryUI": true,
			"sPaginationType": "full_numbers",
			"iDisplayLength": 10,
			"oLanguage": {
						"sZeroRecords": "No se encontraron registros!!!"
												
			}
		});

	});

	function reset1(){
		$('#resultado-busqueda').show();
		$('#formulario-actualizacion').hide();
	}
	
	function muestraDialog(idseguimiento){
		$.ajax({
			url:'<?=base_url()?>index.php/gestion/getDetalleSeguimiento',
			type:'post',
			data:'id_seguimiento='+idseguimiento,
			success:function(data){
				var datos = JSON.parse(data);
				$('#fecha_enviada-frm').val(datos.fecha_enviada);
				$('#fecha_recibida-firmada-frm').val(datos.fecha_recibida_firmada);
				$('#fecha_entregada-frm').val(datos.fecha_entregada);
				$('#label-tramite').html('Tr&aacute;mite Nro. '+datos.id_seguimiento+'<br> '+datos.nombre+'<br> '+'registrado el '+datos.fecha_solicitud);
				$('#id_seguimiento').val(datos.id_seguimiento);
				$('#resultado-busqueda').hide();
				$('#formulario-actualizacion').show();
			}
		});
		
	}

	function buscaPersona(){
		$.ajax({
			url:'<?=base_url()?>index.php/gestion/buscaPersona',
			type:'post',
			data:$('#form2').serialize(),
			success:function(data){$('#resultado-busqueda').html(data);

			$('#tablamod').dataTable({
				"bJQueryUI": true,
				"sPaginationType": "full_numbers",
				"iDisplayLength": 10,
				"oLanguage": {
							"sZeroRecords": "No se encontraron registros!!!"
													
				}
			});


			}
		})
	}

	function actualizaSeguimiento(){
		$.ajax({
			url:'<?=base_url()?>index.php/gestion/actualizaSeguimiento',
			data:$('#frmActualizacion').serialize(),
			type:'post',
			success:function(data){
				$('#resultado-busqueda').html('');
				var datos = JSON.parse(data);
				$('#resultado-ajax').html(datos.mensaje);
				buscaPersona();
				reset1();
			}
		})
	}
	
</script>