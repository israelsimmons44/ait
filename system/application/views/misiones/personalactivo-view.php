<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<link href="<?= base_url() ?>css/mapa.css"   rel="stylesheet" type="text/css" />
<title></title>
</head>
<body>
<div>
<div id="insetBgd">
	<h1 class="insetType">{dependencia}</h1>
</div>

<table id="datos" class="tabla" cellspacing="0" align="center">
<thead>
	<tr >
		<td class="titulo_tabla">C&eacute;dula</td>
		<td class="titulo_tabla">Nombre</td>
		<td class="titulo_tabla">Cargo</td>
		<td class="titulo_tabla">Fecha Ingreso</td>
	</tr>
</thead>

<tbody>
{personalactivo}
  <tr class="linea">
    <td class="celda">{cedula}</td>
    <td class="celda">{nombre}</td>
	<td class="celda">{descripcion_cargo}</td>
    <td class="celda">{fecha_inicio}</td>
   </tr>
{/personalactivo}
</tbody>
</table>
</div>
</body>
</html>