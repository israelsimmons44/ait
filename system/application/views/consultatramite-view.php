<style>
	.odd{
		background-color:#ddd;
		
	}
	
	.norm{
		background-color:#fff;
	}
	
	.elemento{
		margin-bottom:5px;
		display:block;
	}
	
	.elemento label{
		font-weight:bold;
	}
	
	.elemento p{
		margin-top:0px;
		margin-bottom:0px;
	}
	
	
	.tabla tr{
		height:25px;
	}
	
	.tabla td{
		padding:3px;
	
	}
	
	
	
</style>

<div class="ui-dialog ui-widget ui-widget-content ui-corner-all" style="float:left;position:relative;margin-right:20px;width:890px;">
   <div class="ui-dialog-titlebar ui-widget-header ui-corner-top ui-helper-clearfix">
      <span id="ui-dialog-title-dialog" class="ui-dialog-title">Consulta de Estado de Solicitudes</span>
      <a class="ui-dialog-titlebar-close ui-corner-all" href="#"></a>
   </div>
   <div style="height:auto; width: auto;overflow: hidden;" class="ui-dialog-content ui-widget-content" id="dialog">
   	
      
      	<div style="float:left;width:350px;">
			<h3>
				Datos
			</h3>
						
			<div class="elemento">
				<label>Cédula:</label>
				<p>{cedula}</p>
			</div>
			<div class="elemento">
				<label>Nombre:</label>
				<p>{nombre}</p>
			</div>
			<div class="elemento">
				<label>Tipo de Personal:</label>
				<p>{tipopersonal}</p>
			</div>
			<div class="elemento">
				<label>fecha de Ingreso:</label>
				<p>{fechaingreso}</p>
			</div>
			<div class="elemento">
				<label>Cargo:</label>
				<p>{cargo}</p>
			</div>
			<div class="elemento">
				<label>Adscripción:</label>
				<p>{adscripcion}</p>
			</div>
			
			
		</div>
		
		<div style="float:right;width:480px;">
			<h3>Solicitudes</h3>
			{solicitudes}
		</div>
      
   </div>
</div>

<span style='margin-left:15px;' id='resultado-ajax'></span>

<script>
	function eliminaSolicitud(idsolicitud){
		$.ajax({
			url:'<?php echo base_url()?>/index.php/main/eliminaSolicitud/'+idsolicitud,
			conext:document.body,
			success:function(data){
				$('#'+idsolicitud).remove();
				$('#resultado-ajax').html(data)
			}
			
		});
		
	}


	$(function(){
		//$('#solicitudes tr').addClass('norm');
		//$('#solicitudes tr:odd').addClass('odd');
		$('table').dataTable({"aaSorting": [[ 0, "asc" ]],
			"bJQueryUI": true,
			"sScrollY": 200,
			
			"iDisplayLength": 20,
			"bPaginate": false,
			"bFilter": false,
			"oLanguage": {
						"sZeroRecords": "No se encontraron registros!!!"
			}
		});
		$('.ui-corner-tl').hide();
	})
</script>