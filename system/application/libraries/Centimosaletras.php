<?php
class Centimosaletras {	
	
  function basico($numero) {
       $valor   = array ('uno','dos','tres','cuatro','cinco','seis','siete','ocho','nueve','diez',
                      'once','doce','trece','catorce','quince','dieciséis','diecisiete',
                      'dieciocho','diecinueve','veinte','veintiuno','veintidos','ventitres',
                      'veinticuatro','veinticinco','veintiséis','veintisiete','veintiocho','veintinueve');
       return $valor[$numero - 1];
     }
        
     function decenas($n) {
       $decenas  = array (30=>'treinta',40=>'cuarenta',50=>'cincuenta',60=>'sesenta',
                       70=>'setenta',80=>'ochenta',90=>'noventa');
    if( $n <= 29) return basico($n);
    $x = $n % 10;
       if ( $x == 0 ) {
           return $decenas[$n];
       } else return  $decenas[$n - $x].' y '. basico($x);
     }
     
function convertir($n) {
         switch (true) {
             case ( $n >= 1 && $n <= 29)    : return basico($n); break;
             case ( $n >= 30 && $n < 100)  : return decenas($n); break;
         }
      }
     
     
}