<?php
class CI_Persona{
	private $idPersona="";
	private $idTrabajador="";
	private $cedula="";
	private $nombre="";
	private $sueldo="";
	private $cargo="";
	private $gentilicio="ciudadano";
	private $sexo;
	private $fechaIngreso;
	private $dbSigefirrhh;
	private $tipoPersonal;
	private $adscripcion;
	private $idTipoPersonal;
	private $codigoNomina;
	private $telefonoCelular;
	private $nombres;
	private $apellidos;
	private $lugarPago;
	private $conceptoSalario=array('0001','0505','0409','0410','0411','0412','4000','0408','0500');
	private $conceptoSalarioExcluido=array('0036','1600','1602','0037');
	
	function __construct(){
		$this->CI = &get_instance();
		$this->dbSigefirrhh = $this->CI->load->database('sigefirrhh',TRUE);
		
	}
	
	function getPersona($cedula){
		$sql="select p.id_personal, t.id_trabajador, p.cedula, t.codigo_nomina,p.telefono_celular, t.sueldo_basico, t.fecha_ingreso, cuenta_nomina, c.descripcion_cargo, tp.nombre as nombretipopersonal, p.fecha_nacimiento, p.primer_nombre, p.segundo_nombre, p.primer_apellido, p.segundo_apellido, p.sexo, d.nombre as adscripcion, t.id_tipo_personal, lp.nombre as lugarpago from trabajador t
			inner join cargo c using(id_cargo) 
			inner join tipopersonal tp using(id_tipo_personal) 
			inner join personal p using(id_personal)
			left outer join dependencia d using (id_dependencia)
			left outer join lugarpago lp using(id_lugar_pago)
			where id_personal = (select id_personal from personal where cedula=$cedula) and t.estatus='A' and t.id_tipo_personal not in (31,62,73)";
		
		$res=$this->dbSigefirrhh->query($sql);
			if ($res->num_rows==1){
			$row=$res->row();
			$this->setNombre($row->primer_nombre." ".$row->segundo_nombre." ".$row->primer_apellido." ".$row->segundo_apellido );
			$this->setCargo($row->descripcion_cargo);
			$this->setSueldo($row->sueldo_basico);
			$this->setCedula($cedula);
			$this->setIdPersona($row->id_personal);
			$this->setIdTrabajador($row->id_trabajador);
			$this->setSexo($row->sexo);
			$this->setTipoPersonal($row->nombretipopersonal);
			$this->setFechaIngreso($row->fecha_ingreso);
			$this->setAdscripcion(utf8_encode(trim($row->lugarpago)));
			$this->setIdTipoPersonal($row->id_tipo_personal);
			$this->setCodigoNomina($row->codigo_nomina);
			$this->setTelefonoCelular($row->telefono_celular);
			$this->setNombres($row->primer_nombre." ".$row->segundo_nombre);
			$this->setApellidos($row->primer_apellido." ".$row->segundo_apellido);
			if($this->sexo=='F'){
				$this->setGentilicio('ciudadana');
			}
			$return=$this->arregloDatos();		
			return $return;
		}else{
			return FALSE;
		}
	}
	
	function getSueldoConceptoFijo(){
		$conceptos="";
		foreach ($this->conceptoSalarioExcluido as $valor){
			$conceptos.="'$valor',";
		}
		$conceptos=substr($conceptos,0,strlen($conceptos)-1);
		
		$sql="select c.descripcion, case when ftp.id_frecuencia_pago=3 then monto*2 else monto end from conceptofijo cf
				inner join conceptotipopersonal ctp using(id_concepto_tipo_personal)
				inner join frecuenciatipopersonal ftp on ftp.id_frecuencia_tipo_personal=ctp.id_frecuencia_tipo_personal
				inner join concepto c using(id_concepto)
				where id_trabajador = (select id_trabajador from trabajador where id_personal=$this->idPersona) and c.cod_concepto in ($conceptos) and cf.estatus='A'";
		$sql="select c.descripcion, case when ftp.id_frecuencia_pago=3 then monto*2 else monto end from conceptofijo cf
				inner join conceptotipopersonal ctp using(id_concepto_tipo_personal)
				inner join frecuenciatipopersonal ftp on ftp.id_frecuencia_tipo_personal=ctp.id_frecuencia_tipo_personal
				inner join concepto c using(id_concepto)
				where id_trabajador = (select id_trabajador from trabajador where id_personal=$this->idPersona and estatus='A' and id_tipo_personal not in (31,62,73)) and c.cod_concepto not in ($conceptos) and c.cod_concepto<'5000' and cf.estatus='A'";
		//echo $sql;
		$res=$this->dbSigefirrhh->query($sql);
		$sueldo=0;
		foreach($res->result_array() as $valor){
			$sueldo+=$valor['monto'];
		}
		$arr['sueldo']=$sueldo;
		$arr['detalle']=$res->result_array();
		return $arr;
	}
	
	private function arregloDatos(){
		$arr['nombre']=$this->nombre;
		$arr['cedula']=$this->cedula;
		$arr['fechaingreso']=$this->fechaIngreso;
		$arr['sueldo']=$this->getSueldoConceptoFijo();
		$arr['sueldo']=$arr['sueldo']['sueldo'];
		$arr['detallesueldo']=$arr['sueldo']['detalle'];
		$arr['cargo']=$this->cargo;
		$arr['idpersona']=$this->idPersona;
		$arr['idtrabajador']=$this->idTrabajador;
		$arr['sexo']=$this->sexo;
		$arr['gentilicio']=$this->gentilicio;
		$arr['adscripcion']=$this->adscripcion;
		$arr['idtipopersonal']=$this->idTipoPersonal;
		$arr['codigonomina']=$this->codigoNomina;
		$arr['telefonocelular']=$this->telefonoCelular;
		$arr['nombres']=$this->nombres;
		$arr['apellidos']=$this->apellidos;
		return $arr;
	}
	
	/**
	 * @return the $cedula
	 */
	public function getCedula() {
		return $this->cedula;
	}

	/**
	 * @return the $nombre
	 */
	public function getNombre() {
		return $this->nombre;
	}

	/**
	 * @return the $sueldo
	 */
	public function getSueldo() {
		return $this->sueldo;
	}

	/**
	 * @return the $cargo
	 */
	public function getCargo() {
		return $this->cargo;
	}

	/**
	 * @param $cedula the $cedula to set
	 */
	public function setCedula($cedula) {
		$this->cedula = $cedula;
	}

	/**
	 * @param $nombre the $nombre to set
	 */
	public function setNombre($nombre) {
		$this->nombre = $nombre;
	}

	/**
	 * @param $sueldo the $sueldo to set
	 */
	public function setSueldo($sueldo) {
		$this->sueldo = $sueldo;
	}

	/**
	 * @return the $lugarPago
	 */
	public function getLugarPago() {
		return $this->lugarPago;
	}

	/**
	 * @param field_type $lugarPago
	 */
	public function setLugarPago($lugarPago) {
		$this->lugarPago = $lugarPago;
	}

	/**
	 * @param $cargo the $cargo to set
	 */
	public function setCargo($cargo) {
		$this->cargo = $cargo;
	}
	/**
	 * @return the $idPersona
	 */
	public function getIdPersona() {
		return $this->idPersona;
	}

	/**
	 * @return the $idTrabajador
	 */
	public function getIdTrabajador() {
		return $this->idTrabajador;
	}

	/**
	 * @param $idPersona the $idPersona to set
	 */
	public function setIdPersona($idPersona) {
		$this->idPersona = $idPersona;
	}

	/**
	 * @param $idTrabajador the $idTrabajador to set
	 */
	public function setIdTrabajador($idTrabajador) {
		$this->idTrabajador = $idTrabajador;
	}
	/**
	 * @return the $gentilicio
	 */
	public function getGentilicio() {
		return $this->gentilicio;
	}

	/**
	 * @return the $sexo
	 */
	public function getSexo() {
		return $this->sexo;
	}

	/**
	 * @param $gentilicio the $gentilicio to set
	 */
	public function setGentilicio($gentilicio) {
		$this->gentilicio = $gentilicio;
	}

	/**
	 * @param $sexo the $sexo to set
	 */
	public function setSexo($sexo) {
		$this->sexo = $sexo;
	}
	/**
	 * @return the $fechaIngreso
	 */
	public function getFechaIngreso() {
		return $this->fechaIngreso;
	}

	/**
	 * @param $fechaIngreso the $fechaIngreso to set
	 */
	public function setFechaIngreso($fechaIngreso) {
		$this->fechaIngreso = $fechaIngreso;
	}
	/**
	 * @return the $tipoPersonal
	 */
	public function getTipoPersonal() {
		return $this->tipoPersonal;
	}

	/**
	 * @param $tipoPersonal the $tipoPersonal to set
	 */
	public function setTipoPersonal($tipoPersonal) {
		$this->tipoPersonal = $tipoPersonal;
	}
	/**
	 * @return the $adscripcion
	 */
	public function getAdscripcion() {
		return $this->adscripcion;
	}

	/**
	 * @param $adscripcion the $adscripcion to set
	 */
	public function setAdscripcion($adscripcion) {
		$this->adscripcion = $adscripcion;
	}
	/**
	 * @return the $idTipoPersonal
	 */
	public function getIdTipoPersonal() {
		return $this->idTipoPersonal;
	}

	/**
	 * @param $idTipoPersonal the $idTipoPersonal to set
	 */
	public function setIdTipoPersonal($idTipoPersonal) {
		$this->idTipoPersonal = $idTipoPersonal;
	}
	/**
	 * @return the $codigoNomina
	 */
	public function getCodigoNomina() {
		return $this->codigoNomina;
	}

	/**
	 * @param $codigoNomina the $codigoNomina to set
	 */
	public function setCodigoNomina($codigoNomina) {
		$this->codigoNomina = $codigoNomina;
	}
	/**
	 * @return the $telefonoCelular
	 */
	public function getTelefonoCelular() {
		return $this->telefonoCelular;
	}

	/**
	 * @param $telefonoCelular the $telefonoCelular to set
	 */
	public function setTelefonoCelular($telefonoCelular) {
		$this->telefonoCelular = $telefonoCelular;
	}
	/**
	 * @return the $nombres
	 */
	public function getNombres() {
		return $this->nombres;
	}

	/**
	 * @return the $apellidos
	 */
	public function getApellidos() {
		return $this->apellidos;
	}

	/**
	 * @param $nombres the $nombres to set
	 */
	private function setNombres($nombres) {
		$this->nombres = $nombres;
	}

	/**
	 * @param $apellidos the $apellidos to set
	 */
	private function setApellidos($apellidos) {
		$this->apellidos = $apellidos;
	}








	
}