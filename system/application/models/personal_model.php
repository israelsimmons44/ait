<?php
class personal_model extends Model{
	function __construct(){
		parent::Model();
		$this->sigefirrhh=$this->load->database('sigefirrhh',TRUE);
		
	}
	
	function getPersonalActivo($cedula){
		$sql="select id_trabajador, t.estatus, sueldo_basico, paso, fecha_ingreso, fecha_egreso, cuenta_nomina, tp.nombre as tipopersonal, descripcion_cargo, d.nombre as dependencia, lp.nombre as lugarpago from trabajador t
					inner join tipopersonal tp using(id_tipo_personal)
					left outer join cargo using (id_cargo)
					left outer join dependencia d using(id_dependencia)
					left outer join lugarpago lp using(id_lugar_pago)
					where cedula=".$cedula." and t.estatus='A' and id_tipo_personal not in (62,73) order by fecha_ingreso desc";
		
		$res=$this->sigefirrhh->query($sql);
		if ($res->num_rows()>0){
			$personaRow=$res->row();
			return $personaRow->id_trabajador;
		}else{
			return FALSE;
		}
		
	}
	
	function getPersonal($cedula){
		$arrReturn=FALSE;
		$sql="select id_personal,cedula, primer_nombre, segundo_nombre, primer_apellido, segundo_apellido, sexo, fecha_nacimiento 
		      from personal  
		      where cedula=$cedula";
		$resPersonal=$this->sigefirrhh->query($sql);
		if ($resPersonal->num_rows()>0){
			$personal=$resPersonal->row();
			$arrReturn['personal']=$personal;
			
			$sql="select id_trabajador, t.estatus, sueldo_basico, paso, fecha_ingreso, fecha_egreso, cuenta_nomina, tp.nombre as tipopersonal, descripcion_cargo, d.nombre as dependencia, lp.nombre as lugarpago from trabajador t
					inner join tipopersonal tp using(id_tipo_personal)
					left outer join cargo using (id_cargo)
					left outer join dependencia d using(id_dependencia)
					left outer join lugarpago lp using(id_lugar_pago)
					where cedula=".$cedula." order by fecha_ingreso desc";
			$resTrabajador=$this->sigefirrhh->query($sql);			
			
			$arrReturn['trabajador']=$resTrabajador->result_array();
			
			foreach ($arrReturn['trabajador'] as $key=>$value) {
				$arrReturn['trabajador'][$key]['fecha_ingreso']=pgDate($value['fecha_ingreso']);
				$arrReturn['trabajador'][$key]['fecha_egreso']=pgDate($value['fecha_egreso']);
				$arrReturn['trabajador'][$key]['tipopersonal']=utf8_encode($value['tipopersonal']);
				$arrReturn['trabajador'][$key]['dependencia']=utf8_encode($value['dependencia']);
				$arrReturn['trabajador'][$key]['descripcion_cargo']=utf8_encode($value['descripcion_cargo']);
			}
			
		}
		
		return $arrReturn;
	}
	
//////////////////////////////////// CÁLCULO DE CONCEPTOS PARA CONSTANCIA (NOMINAS QUINCENALES) ///////////////////////////////	
	
	function getSueldoAnual($idTrabajador){
		$return=FALSE;
		$sql="select 
					cast(sum(case when fp.cod_frecuencia_pago=3 then monto*2 else monto end)as decimal (10,2)) as asignacion,
	                cast(sum(case when fp.cod_frecuencia_pago=3 then monto*2 else monto end)*2.25 as decimal (10,2)) as vacacional,
	                cast(sum(case when fp.cod_frecuencia_pago=3 then monto*2 else monto end)*8.26 as decimal (10,2))  as aguinaldo
				from conceptofijo cf
				inner join conceptotipopersonal ctp using( id_concepto_tipo_personal)
				inner join concepto c using(id_concepto)
				inner join frecuenciatipopersonal ftp on ftp.id_frecuencia_tipo_personal=ctp.id_frecuencia_tipo_personal and ftp.id_tipo_personal=ftp.id_tipo_personal
				inner join frecuenciapago fp using(id_frecuencia_pago)
				where id_trabajador=$idTrabajador and c.cod_concepto not in ('0036','1600','1602','0037','3999','4102','4008') and c.cod_concepto<'5000'";
		$res=$this->sigefirrhh->query($sql);
		if ($res->num_rows()>0){
			$return=$res->row();
		}
		
		return $return;

	}
	
//////////////////////////////////// CÁLCULO DE CONCEPTOS PARA CONSTANCIA OBREROS ///////////////////////////////	
	
	function getSueldoAnualObrero($idTrabajador){
		$return=FALSE;
		$sql="select 
                    cast (sum(case 
                                  when c.cod_concepto='0003' then (monto/7)*30
                                  else monto*4
                              end) as decimal (10,2)) as asignacion_obrero,
                    cast (sum(case 
                                  when c.cod_concepto='0003' then (monto/7)*30
                                  else monto*4
                              end)*2.25 as decimal (10,2)) as vacacional_obrero,
                    cast (sum(case 
                                  when c.cod_concepto='0003' then (monto/7)*30
                                  else monto*4
                              end)*8.26 as decimal (10,2)) as aguinaldo_obrero 				
               from conceptofijo cf
                    inner join conceptotipopersonal ctp using( id_concepto_tipo_personal)
                    inner join concepto c using(id_concepto)
                    inner join frecuenciatipopersonal ftp on 
                               ftp.id_frecuencia_tipo_personal=ctp.id_frecuencia_tipo_personal 
                               and ftp.id_tipo_personal=ftp.id_tipo_personal
                    inner join frecuenciapago fp using(id_frecuencia_pago)
               where id_trabajador=$idTrabajador 
                     and c.cod_concepto not in ('0036','1600','1602','0037','3999','4102','4008') and c.cod_concepto<'5000'";
		
		$res=$this->sigefirrhh->query($sql);
		if ($res->num_rows()>0){
			$return=$res->row();
		}
		
		return $return;

	}	

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////	
	
	function getConceptosFijos($idTrabajador){
		$return=FALSE;
		
		$sql="select id_tipo_personal from trabajador where id_trabajador=$idTrabajador";
		$resTipoTrabajador=$this->sigefirrhh->query($sql);
		$rowIdTipoTrabajador=$resTipoTrabajador->row();
		$idTipoTrabajador=$rowIdTipoTrabajador->id_tipo_personal;
		$conceptosExc="'0061','0079', '5006', '0059','0069', '0062','0076', '0063','0077', '5007','0072','5008'";
		if ($idTipoTrabajador>1){
			$sql="select c.cod_concepto, c.descripcion, case when ftp.cod_frecuencia_pago=3 then monto*2 else monto end as monto  from conceptofijo  cf
					inner join conceptotipopersonal ctp using(id_concepto_tipo_personal)
					inner join concepto c using(id_concepto)
					inner join frecuenciatipopersonal ftp on ftp.id_frecuencia_tipo_personal=cf.id_frecuencia_tipo_personal
					where id_trabajador=$idTrabajador and c.cod_concepto<'5000' and c.cod_concepto not in ('0036','1600','1602','0037','4102',$conceptosExc) order by c.cod_concepto";
		}else{
			$sql="select c.cod_concepto, c.descripcion, monto*4 as monto  from conceptofijo  cf
					inner join conceptotipopersonal ctp using(id_concepto_tipo_personal)
					inner join concepto c using(id_concepto)
					inner join frecuenciatipopersonal ftp on ftp.id_frecuencia_tipo_personal=cf.id_frecuencia_tipo_personal
					where id_trabajador=$idTrabajador and c.cod_concepto<'5000' and c.cod_concepto not in ('0036','1600','1602','0037','0003','4102',$conceptosExc) 

					union
					select c.cod_concepto, c.descripcion, monto/7*30 as monto  from conceptofijo  cf
										inner join conceptotipopersonal ctp using(id_concepto_tipo_personal)
										inner join concepto c using(id_concepto)
										inner join frecuenciatipopersonal ftp on ftp.id_frecuencia_tipo_personal=cf.id_frecuencia_tipo_personal
										where id_trabajador=$idTrabajador and c.cod_concepto='0003' order by cod_concepto";
		}
		
		$res=$this->sigefirrhh->query($sql);
		if ($res->num_rows()>0){
			$return=$res->result_array();
		}
		return $return;
	}
	
	
	function getJubiPen($cedula){
		$sql="select t.id_tipo_personal, p.cedula, c.descripcion_cargo as cargo, p.direccion_residencia as direccion,
				trim(primer_nombre)||' '||trim(segundo_nombre)||' '||trim(primer_apellido)||' '||trim(segundo_apellido) as nombre
				from personal p
				inner join trabajador t using (id_personal)
				inner join cargo c using(id_cargo)
				where id_tipo_personal in (42,43) and estatus='A' and p.cedula=$cedula";
		
		$res=$this->sigefirrhh->query($sql);
		if ($res->num_rows()>0){
			$row=$res->result_array();
			return $row[0];
		}else{
			return FALSE;
		}
	}
	
}