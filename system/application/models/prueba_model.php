<?php
class Prueba_Model extends Model{
	function __construct(){
		parent::Model();
		$this->load->database();
	}
	
	function getNivelAcademico($term){
		$term=strtolower($term);
		$sql="select id_nivel_academico as codigo, descripcion as value, descripcion as label from nivelacademico where lower(descripcion) like '%$term%'";
		$res=$this->db->query($sql);
		if ($res->num_rows>0){
			return json_encode($res->result_array());
		}else{
			return FALSE;
		}
	}
}