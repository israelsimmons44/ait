<?php
class recreacion_model extends Model{	
	function __construct(){
		parent::Model();
		$this->load->database();
		$this->sigefirrhh=$this->load->database('sigefirrhh',TRUE);
		$this->load->helper('funciones');
	}

///////////////////////////////////// FUNCIÓN PARA CONSULTAR EN BD DATOS DEL TRABAJADOR Y SU(S) HIJO(S) /////////////////////////	

	function getPersonal($cedula){
		$arrReturn=FALSE;
		$sql="select p.id_personal, p.cedula, p.primer_nombre, p.segundo_nombre, p.primer_apellido, p.segundo_apellido,
		             d.nombre as dependencia, c.descripcion_cargo as cargo,
		             p.direccion_residencia, p.telefono_celular, p.telefono_oficina, p.telefono_residencia, p.email 
		      from personal p, trabajador t, cargo c, dependencia d  
		      where p.cedula=t.cedula and t.id_dependencia=d.id_dependencia 
		            and t.id_cargo=c.id_cargo and p.cedula=$cedula";
		$resPersonal=$this->sigefirrhh->query($sql);
		if ($resPersonal->num_rows()>0){
			$personal=$resPersonal->row();
			$arrReturn['personal']=$personal;
			/*
			$sql="select cedula, id_familiar, 
			             cedula_familiar, p_apellido_fam||' '||s_apellido_fam||' '||p_nombre_fam||' '||s_nombre_fam as nombre_fam,
			             sexo, fecha_nacimiento, edad
			       from  vgrupofamiliar      
				   where parentesco='H' and edad between 5 and 16 and cedula=".$cedula." order by cedula_familiar";
			*/
			$sql="select * from ((select vgru.cedula, vgru.id_familiar, vgru.cedula_familiar, vgru.p_apellido_fam||' '||vgru.s_apellido_fam||' '||vgru.p_nombre_fam||' '||vgru.s_nombre_fam
                                as nombre_fam,vgru.sexo, vgru.fecha_nacimiento, vgru.edad
                            from  vgrupofamiliar  vgru   
                            where vgru.parentesco='H'
                            and vgru.edad between 5
                            and 16 and vgru.cedula=$cedula)
                            union
                            (select vgru.cedula, vgru.id_familiar, vgru.cedula_familiar, vgru.p_apellido_fam||' '||vgru.s_apellido_fam||' '||vgru.p_nombre_fam||' '||vgru.s_nombre_fam
                                as nombre_fam,vgru.sexo, vgru.fecha_nacimiento, vgru.edad
                            from  vgrupofamiliar  vgru   
                            where vgru.parentesco='H'
                            and vgru.edad>16
                            and vgru.nino_excepcional='S'
                            and vgru.cedula=$cedula)) AS consultahijo order by cedula_familiar";
			 
			$resHijosTrabajador=$this->sigefirrhh->query($sql);			
			$arrReturn['hijostrabajador']=$resHijosTrabajador->result_array();
			
			foreach ($arrReturn['hijostrabajador'] as $key=>$value) {
				$arrReturn['hijostrabajador'][$key]['fecha_nacimiento']=pgDate($value['fecha_nacimiento']);
				$arrReturn['hijostrabajador'][$key]['nombre_fam']=utf8_encode($value['nombre_fam']);
			}
			
		}
		
		return $arrReturn;
	}
	
/////////////////////////////////// FUNCIÓN PARA CONSULTAR EN BD DATOS DE UN FAMILIAR EN ESPECÍFICO /////////////////////////	
	
	function getHijo($idFamiliar){
		$arrReturn=FALSE;
		$sql="select  id_familiar, cedula_familiar, p_apellido_fam||' '||s_apellido_fam||' '||p_nombre_fam||' '||s_nombre_fam as nombre_fam,
		      sexo, fecha_nacimiento, edad
		      from  vgrupofamiliar      
			where id_familiar=$idFamiliar";
		$resHijos = $this->sigefirrhh->query($sql);			
		$var = $resHijos->row();
		/*
		foreach ($var as $key=>$value) {
			$var[$key]['nombre_fam']=utf8_encode($value['nombre_fam']);
		}
		*/
		return $var ;
	
	}
	
//////////////////////////// FUNCIÓN PARA CONSULTAR EN BD DATOS DEL TRABAJADOR Y UN HIJO EN ESPECÍFICO /////////////////////////	
	
    function getPadreHijo($idFamiliar){
		$arrReturn=FALSE;
		$sql="select p.cedula, p.primer_apellido||' '||p.segundo_apellido||' '||p.primer_nombre||' '||p.segundo_nombre as nombre,
		             d.nombre as dependencia, c.descripcion_cargo as cargo,
		             p.direccion_residencia, p.telefono_celular, p.telefono_oficina, p.telefono_residencia, p.email,
                     f.cedula_familiar, 
                     f.primer_apellido||' '||f.segundo_apellido||' '||f.primer_nombre||' '||f.segundo_nombre as nombre_fam,
		             f.sexo, f.fecha_nacimiento, ('now'::text::date - f.fecha_nacimiento) / 365 as edad 
		      from personal p, trabajador t, cargo c, dependencia d, familiar f  
		      where p.cedula=t.cedula and t.id_dependencia=d.id_dependencia and p.id_personal=f.id_personal and
		            t.id_cargo=c.id_cargo and t.estatus='A' and f.id_familiar=$idFamiliar";
		
		$resPadreHijo = $this->sigefirrhh->query($sql);			
		//$var = $resPadreHijo->row_array();
		$var = $resPadreHijo->row();
		
		/*
		foreach ($var as $key=>$value) {
			$var[$key]['nombre']=utf8_encode($value['nombre']);
			$var[$key]['dependencia']=utf8_encode($value['dependencia']);
			$var[$key]['cargo']=utf8_encode($value['cargo']);
			$var[$key]['direccion_residencia']=utf8_encode($value['direccion_residencia']);
			$var[$key]['email']=utf8_encode($value['email']);
			$var[$key]['nombre_fam']=utf8_encode($value['nombre_fam']);
		}
		*/
		return $var ;
	}

////////////////////////// FUNCIÓN PARA CONSULTAR EN BD DATOS ADICIONALES DE UN HIJO EN ESPECÍFICO /////////////////////////	
	
    function getDatosHijoAdicionales ($idFamiliar){

		$sql="select * from planvacacionalcampamento where id_familiar=$idFamiliar";
		
		$resDatosHijoAdicional = $this->db->query($sql);			
		$var = $resDatosHijoAdicional->row();

		return $var ;
	}	

////////////////////////// FUNCIÓN PARA INGRESAR DATOS ADICIONALES EN BD ////////////////////////////////////////////////////	
	
	function guardaDatosMedicosHijos ($data){
	
		$guardardatos=$this->db->insert('planvacacionalcampamento', $data);
		
	
	}

}