<?php
class recibos_model extends Model{	
	function __construct(){
		parent::Model();
		$this->load->database();
		$this->sigefirrhh=$this->load->database('sigefirrhh',TRUE);
		$this->load->helper('funciones');
	}
	
	function getRecibos($cedula){
		$this->benchmark->mark('mod_getRecibos_start');
		$persona=$this->_getPersona($cedula);
		$in="";
		$resultado=FALSE;
		
		if ($persona){
			foreach ($persona as $value) {
				$in.=$value.",";
			}
			
			$in=substr($in, 0,strlen($in)-1);
			$anio=intval(date('Y'));
			$mes=intval(date('m'));
			$fechaFin="";
			if ($mes==1){
			
			}else{
				$mesanterior=$mes-1;
			}
			
			$dia=intval(date('d'));
			
			if ($dia>=16){
				$filtro=$anio."-".$mes."-16";
			}else{
				$filtro=$anio."-".$mes."-01";
			}
			
			if (date('w')==5){
				$week=intval(date('W'));
			}else{
				$week=intval(date('W'))-1;
			}
			
			$sql="select fecha_fin from semana order by id_semana desc limit 1";
			$sql="select fecha_fin from semana where anio=$anio and semana_anio=$week";
			//echo $sql;
			$resWeek=$this->sigefirrhh->query($sql);
			
			if ($resWeek->num_rows>0){
				$fechaFinSemana=$resWeek->row();
				$fechaFin=$fechaFinSemana->fecha_fin;
			}
			
			if ($fechaFin==""){
				$sql="select * from semana order by id_semana desc limit 1";
				$resWeek=$this->sigefirrhh->query($sql);
				$fechaFinSemana=$resWeek->row();
				$fechaFin=$fechaFinSemana->fecha_fin;
			}
			
			if ($this->session->userdata('usuario')=='israel.simmons998'){
				$sql="select id_historico_nomina,hq.anio, hq.mes, semana_quincena,id_nomina_especial,ne.descripcion,hq.fecha,hq.id_tipo_personal, ne.fecha_pago as fecha_pago_nomina_especial from historiconomina hq
					left outer join nominaespecial ne using (id_nomina_especial)
					where id_trabajador in ($in) order by hq.anio desc, hq.mes desc, hq.semana_quincena asc";
			}else{
				$sql="select id_historico_nomina,hq.anio, hq.mes, semana_quincena,id_nomina_especial,ne.descripcion,hq.fecha,hq.id_tipo_personal, ne.fecha_pago as fecha_pago_nomina_especial from historiconomina hq
					left outer join nominaespecial ne using (id_nomina_especial)
					where id_trabajador in ($in) and case when id_tipo_personal>1 then fecha < '$filtro' else fecha <='$fechaFin' end order by hq.anio desc, hq.mes desc, hq.semana_quincena asc";
			}		
			
			//echo $sql;
			$res=$this->sigefirrhh->query($sql);
			
			if ($res->num_rows()>0){
				$resultado=$res->result_array();
				$resultado2="";
				
				foreach ($resultado as $key=>$value) {
					if ($value['id_nomina_especial']>0 and is_null($value['fecha_pago_nomina_especial'])){						
						if ($this->session->userdata('usuario')=='israel.simmons998'){
							$resultado2[$key]=$resultado[$key];
							$resultado2[$key]['pagado']=FALSE;
							if($value['id_tipo_personal']>1){
								$quincenas=quincenas($value['anio'], $value['mes'],$value['semana_quincena']);
								$resultado2[$key]['quincenatexto']=$quincenas[2];
							}else{
								$anio=$value['anio'];
								$semana=$value['semana_quincena'];
								$mes=mes($value['mes']);
								$sql="select * from semana where anio=$anio and semana_anio=$semana";
								$resSemana=$this->sigefirrhh->query($sql);
								$rowSemana=$resSemana->row();
								$resultado2[$key]['quincenatexto']="Semana $rowSemana->semana_mes de ".$mes;
							}
						}
					}else{
						$resultado2[$key]=$resultado[$key];
						$resultado2[$key]['pagado']=TRUE;
						if($value['id_tipo_personal']>1){
							$quincenas=quincenas($value['anio'], $value['mes'],$value['semana_quincena']);
							$resultado2[$key]['quincenatexto']=$quincenas[2];
						}else{
							$anio=$value['anio'];
							$semana=$value['semana_quincena'];
							$mes=mes($value['mes']);
							$sql="select * from semana where anio=$anio and semana_anio=$semana";
							
							$resSemana=$this->sigefirrhh->query($sql);
							$rowSemana=$resSemana->row();
							
							$resultado2[$key]['quincenatexto']="Semana $rowSemana->semana_mes de ".$mes;
						}
					}
				}
			}
		}
		
		$this->benchmark->mark('mod_getRecibos_end');
		return $resultado2;
	}
	
	function getPersonaRecibo($idHistoricoNomina){
		$sql="select p.cedula,trim(primer_nombre) as nombre1 , trim(segundo_nombre) as nombre2 , trim(primer_apellido) as apellido1, trim(segundo_apellido) as apellido2, 
					c.descripcion_cargo as cargo,
					lp.nombre as dependencia, 
					hq.fecha, 
					tp.nombre as tipo, tp.id_tipo_personal, hq.id_nomina_especial,ne.descripcion as nominaesp, c.grado, t.paso, t.codigo_nomina, hq.semana_quincena, hq.mes, hq.anio
				from historiconomina hq
					inner join trabajador t using(id_trabajador)
					inner join personal p using (id_personal)
					inner join cargo c on c.id_cargo=t.id_cargo
					left outer join lugarpago lp on lp.id_lugar_pago=t.id_lugar_pago
					inner join tipopersonal tp on tp.id_tipo_personal=t.id_tipo_personal
					left outer join nominaespecial ne using (id_nomina_especial)
				where id_historico_nomina=$idHistoricoNomina";
		
		$res=$this->sigefirrhh->query($sql);
		
		
		if ($res->num_rows()>0){
			$rowRecibo=$res->row();
			
			$mes=substr($rowRecibo->fecha,5,2);
			$anio=substr($rowRecibo->fecha,0,4);
			if ($rowRecibo->id_tipo_personal>1){
				$fechas=quincenas($anio, $mes,$rowRecibo->semana_quincena);
				if ($rowRecibo->semana_quincena==1){
					$rowRecibo->fechaQuincena=$fechas[0];
				}elseif ($rowRecibo->semana_quincena==2){
					$rowRecibo->fechaQuincena=$fechas[1];
				}
			}else{
				if ($rowRecibo->id_nomina_especial!=""){
					$rowRecibo->fechaQuincena=pgDate($rowRecibo->fecha);
				}else{
					$sql="select * from semana where anio=$rowRecibo->anio and mes='$rowRecibo->mes' and semana_anio=$rowRecibo->semana_quincena";
					$fechaSemanas=$this->sigefirrhh->query($sql);
					$rowFecha=$fechaSemanas->row();
					
					$rowRecibo->fechaQuincena=pgDate($rowFecha->fecha_fin);
				}
				
			}
			
			
			
			
		}else{
			$rowRecibo=FALSE;
		}		
		
		return $rowRecibo;
	}
	
	function getRecibo($idHistoricoNomina){
		$this->benchmark->mark('mod_getRecibo_start');
		$arrRes=FALSE;
		$sql="select c.cod_concepto,c.descripcion,monto_asigna,monto_deduce from historicoquincena hq
			inner join conceptotipopersonal ctp using (id_concepto_tipo_personal)
			inner join concepto c on ctp.id_concepto=c.id_concepto
			where id_historico_nomina=$idHistoricoNomina and c.cod_concepto not in ('5000','0061', '5006', '0059', '0062', '0063', '5007') order by c.cod_concepto";
		$res=$this->sigefirrhh->query($sql);
		
		if ($res->num_rows()>0){
			
			if ($res->num_rows()>0){
				$arrRes=$res->result_array();
			}
		}else{
			$sql="select c.cod_concepto,c.descripcion,monto_asigna,monto_deduce from historicosemana hq
				inner join conceptotipopersonal ctp using (id_concepto_tipo_personal)
				inner join concepto c on ctp.id_concepto=c.id_concepto
				where id_historico_nomina=$idHistoricoNomina and c.cod_concepto not in ('5000','0061', '5006', '0059', '0062', '0063', '5007') order by c.cod_concepto";
			$res1=$this->sigefirrhh->query($sql);
			
			$arrRes=$res1->result_array();
		}
		
		
		
		$this->benchmark->mark('mod_getRecibo_end');
		return $arrRes;
	}
	
	function _getPersona($cedula){
		$this->benchmark->mark('mod_getPersona_start');
		$idTrabajador=FALSE;
		
		$sql="select id_trabajador, p.primer_nombre, p.segundo_nombre, p.primer_apellido,p.segundo_apellido, tp.nombre, tp.id_tipo_personal from trabajador t
			inner join personal p using(id_personal)
			inner join tipopersonal tp using (id_tipo_personal)
			where p.cedula=$cedula";
		$res=$this->sigefirrhh->query($sql);
		foreach ($res->result_array() as $value) {
			$idTrabajador[]=$value['id_trabajador'];
		}
		
		$this->benchmark->mark('mod_getPersona_end');
		return $idTrabajador;
	}
}