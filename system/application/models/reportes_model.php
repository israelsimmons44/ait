<?php
class reportes_model extends Model{
	function __construct(){
		parent::Model();
		$this->load->database();
	}
	
	function getConstancias(){
		$sql="select id_tramite,nombre,'Solicitado' as estatus,1,  count(*) from solicitud
				inner join tramite using(id_tramite)
				where id_tramite=20
				group by id_tramite,nombre 
				union
				select id_tramite,nombre,'Impreso' as estatus, 2,count(*) from solicitud
				inner join tramite using(id_tramite)
				where solicitud.estatus='I' and id_tramite=20
				group by id_tramite,nombre 
				union
				select id_tramite,nombre,'Procesado' as estatus,3, count(*) from solicitud
				inner join tramite using(id_tramite)
				where solicitud.estatus='P' and id_tramite=20
				group by id_tramite,nombre 
				order by id_tramite,4";
		$res=$this->db->query($sql);
		return json_encode($res->result_array());
	}
	
	function getTotalConstancias($estatus=""){
		$filtro="";
		if ($estatus!=""){
			$filtro=" and estatus='".strtoupper($estatus)."'"; 
		}
		$sql="select date_part('month',fecha) as mes,count(*) as total from solicitud where id_tramite=20 $filtro group by date_part('month',fecha)";
		$res=$this->db->query($sql);
		return $res->result_array();
	}
	
	function getDataSolicitudesPie($fecha1,$fecha2){
		$sql="select t.nombre, count(*) from solicitud s
				inner join tramite t using (id_tramite)
				where fecha between '$fecha1' and '$fecha2' 
				group by t.nombre order by t.nombre";
		
		//echo $sql;
		$res=$this->db->query($sql);
		return $res->result_array();
	}
	
	function getTotalesSeguimiento($fecha1, $fecha2){		
		$return = FALSE;
		$sql="select id_tramite,nombre, count(*) as cuenta from seguimiento
				inner join tramite using(id_tramite)
				where fecha_solicitud between '$fecha1' and '$fecha2'
				group by id_tramite,nombre";		
		
		$res=$this->db->query($sql);
		if ($res){
			if ($res->num_rows()>0){
				$return=$res->result_array();
			}
		}
		
		return $return;
	}
	
	function getTotalesTrimestreSeguimiento($anho,$trimestre){
		switch ($trimestre) {		
			case 1:
				$fecha1=$anho.'-01-01';
				$fecha2=$anho.'-03-31';
			break;
			
			case 2:
				$fecha1=$anho.'-04-01';
				$fecha2=$anho.'-06-30';
			break;
			
			case 3:
				$fecha1=$anho.'-07-01';
				$fecha2=$anho.'-09-30';
			break;
			
			case 4:
				$fecha1=$anho.'-10-01';
				$fecha2=$anho.'-12-31';
			break;
		}
		
		return $this->getTotalesSeguimiento($fecha1, $fecha2);
	}
	
	function getTramites(){
		$sql="select id_tramite, nombre from tramite order by nombre";
		$res=$this->db->query($sql);
		$arreglo=$res->result_array();
		foreach ($arreglo as $value) {
			$arr[$value['id_tramite']]=array('nombre'=>$value['nombre'],'t1'=>0,'t2'=>0,'t3'=>0,'t4'=>0,'total'=>0);
		}
		
		return $arr;
		
	}
	
	function getFechasTrimestre($anho, $trimestre){
		switch ($trimestre) {		
			case 1:
				$fecha1=$anho.'-01-01';
				$fecha2=$anho.'-03-31';
			break;
			
			case 2:
				$fecha1=$anho.'-04-01';
				$fecha2=$anho.'-06-30';
			break;
			
			case 3:
				$fecha1=$anho.'-07-01';
				$fecha2=$anho.'-09-30';
			break;
			
			case 4:
				$fecha1=$anho.'-10-01';
				$fecha2=$anho.'-12-31';
			break;
		}
		
		$arr[]=$fecha1;
		$arr[]=$fecha2;
		return $arr;
	}
	
	function getSeguimientoUsuario($fecha1, $fecha2){
		$return = FALSE;
		$sql="select id_usuario, count(*) as cuenta from seguimiento s
				inner join tramite t using(id_tramite)
				left outer join usuario u using(id_usuario)
				where fecha_solicitud between '$fecha1' and '$fecha2'
				group by id_usuario ";
		//echo $sql;
		$res=$this->db->query($sql);
		if ($res){
			if ($res->num_rows()>0){
				$return=$res->result_array();
			}
		}
		
		return $return;
	}
	
	function getSeguimientoUsuarioTrimestre($anho,$trimestre){
		$fecha=$this->getFechasTrimestre($anho, $trimestre);
		
		$return=$this->getSeguimientoUsuario($fecha[0], $fecha[1]);
		
		
		return $return;
	}
	
	function getAnalistas(){
		$sql="select id_usuario, nombre from usuario order by nombre";
		$res=$this->db->query($sql);
		$arr=$res->result_array();
		
		foreach ($arr as $value) {
			$resultado[$value['id_usuario']]=array('nombre'=>$value['nombre'],'t1'=>0,'t2'=>0,'t3'=>0,'t4'=>0,'total'=>0);
		}
		$resultado['9999']=array('nombre'=>'Usuarios directos','t1'=>0,'t2'=>0,'t3'=>0,'t4'=>0,'total'=>0);
		return $resultado;
	}
	
	
}