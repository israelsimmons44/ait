<?php
class reclamos_model extends Model{
	function __construct(){
		parent::Model();
		$this->load->database();
	}
	
	function addReclamo($post){
		$this->benchmark->mark('insert_addreclamo_start');
		$id=FALSE;
		try {
			$this->db->insert('reclamo',$post);
			$id=$this->db->insert_id();	
		} catch (Exception $e) {
			//throw $e;
			log_message(1,"Error insertando en reclamo",$e);
		}
		$this->benchmark->mark('insert_addreclamo_end');
		return $id;
	}
	
	function getReclamo($id){
		$this->benchmark->mark('getReclamo_start');
		$sql="select * from reclamo where id_reclamo=$id";
		$res=$this->db->query($sql);
		if ($res->num_rows()==0){
			$res=FALSE;	
		}
		$this->benchmark->mark('getReclamo_end');
		return $res->row();
	}
	
	function getReclamos(){
		$this->benchmark->mark('getReclamos_start');
		$sql="select r.*, id_caso, ec.descripcion as estatusdescripcion from reclamo r
				left outer join caso c using(id_reclamo) 
				left outer join estatuscaso ec using(id_estatus_caso)
				order by fecha asc limit 100";
		$res=$this->db->query($sql);
		if ($res->num_rows()==0){
			return FALSE;	
		}
		$this->benchmark->mark('getReclamos_end');
		
		$arrResult=$res->result_array();
		
		foreach ($arrResult as $key=>$value) {
			$arrResult[$key]['fecha']=pgDate($value['fecha']);
			if ($arrResult[$key]['id_caso']!=""){
				$arrResult[$key]['abrirconsultarcaso']="<a href='".base_url()."index.php/analista/consultaCaso/".$value['id_caso']."'><img src='".base_url()."images/search.png'/></a><a href='".base_url()."index.php/correo/index/".$value['id_reclamo']."'><img src='".base_url()."images/mail.png'/></a>";
			}else{
				$arrResult[$key]['abrirconsultarcaso']="<a href='".base_url()."index.php/analista/generaCaso/".$value['id_reclamo']."'><img src='".base_url()."images/add.png' height='16'/></a><a href='".base_url()."index.php/correo/index/".$value['id_reclamo']."'><img src='".base_url()."images/mail.png'/></a>";
			}
			
		}
		
		return $arrResult;
	}
	
	function addSugerencia($post){
		$id=FALSE;
		if (trim($post['sugerencia'])!=""){
			$this->db->insert('sugerencia',$post);
			$id=$this->db->insert_id();	
		}
		
		return $id;
	}
}