<?php
class misiones_model extends Model{	
	function __construct(){
		parent::Model();
		$this->load->database();
		$this->sigefirrhh=$this->load->database('sigefirrhh',TRUE);
		$this->load->helper('funciones');
	}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//									  PERSONAL SERVICIO EXTERIOR ACTIVO POR MISION                                            //           //                  //                                                            
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////	
	
	function getPersonalActivo($idDependencia){
		
		//$idDependencia=1219;
		
		$sql="select * from vpersonalservicioexterior where id_dependencia=$idDependencia";
		
		$resPersonal=$this->sigefirrhh->query($sql);
				
		$var=$resPersonal->result_array();
		
		foreach ($var as $key=>$value) {
			$var[$key]['nombre']=utf8_encode($value['nombre']);
			$var[$key]['descripcion_cargo']=utf8_encode($value['descripcion_cargo']);
			$var[$key]['dependencia']=utf8_encode($value['dependencia']);
			$var[$key]['fecha_inicio']=pgdate($value['fecha_inicio']);
		}
		
		return $var;
	}
	
	function getDependencia($idDependencia){
			
		$sql="select dependencia from vpersonalservicioexterior where id_dependencia=$idDependencia limit 1";		
		$resPersonal=$this->sigefirrhh->query($sql);				
		$row = $resPersonal->row();
		
		return  utf8_encode($row->dependencia);
	}	
	
}