<?php
class gestion_model extends Model{
	function __construct(){
		parent::Model();
		$this->load->database();
		$this->sigefirrhh=$this->load->database('sigefirrhh',TRUE);
		
	}
	
	function getTramites(){
		$sql="select id_tramite as valor, nombre as descripcion from tramite where estatus='T'";
		$res=$this->db->query($sql);
		return $res->result_array();
	}
	
	function getAnalistas(){
		$sql="select id_usuario as valor, login as descripcion from usuario";
		$res=$this->db->query($sql);
		return $res->result_array();
	}
	
	function insert($data){
		$this->db->insert('seguimiento',$data);
	}
	
	function actualizar($idSeguimiento,$post){
		$return=FALSE;
		$this->db->where('id_seguimiento',$idSeguimiento);
		$return=$this->db->update('seguimiento',$post);
		return $return;
	}
	
	function vistaSeguimiento($anho, $mes){
		$arrMes=array(0,31,28,31,30,31,30,31,31,30,31,30,31);
		$sql="select cedula, u.nombre, fecha_solicitud, fecha_enviada, fecha_recibida_firmada,fecha_entregada, \"login\" from seguimiento 
				inner join tramite using (id_tramite)
				left join usuario u using (id_usuario)";
		$sql="select id_seguimiento, s.cedula, t.nombre as tramite, u.nombre, fecha_solicitud, fecha_enviada, fecha_recibida_firmada, fecha_entregada from seguimiento s
				left outer join solicitud sol using(id_solicitud,id_tramite)
				inner join tramite t using (id_tramite)
				inner join usuario u using(id_usuario) where fecha_solicitud between '$anho-$mes-01' and '$anho-$mes-".$arrMes[$mes]."' 
				order by fecha_solicitud ";
		$res=$this->db->query($sql);
		return $res->result_array();
	}
	
	function getNombresPersonalSeguimiento(){
		$return=FALSE;
		
		$sql="select cedula,primer_nombre || ' ' || primer_apellido as nombre from personal";
		$resNombres=$this->sigefirrhh->query($sql);
		if ($resNombres){
			if ($resNombres->num_rows()>0){
				$arrReturn=$resNombres->result_array();
				foreach ($arrReturn as $value) {
					$arrNombre[$value['cedula']]=$value['nombre'];
				}
				$return = $arrNombre;
			}
		}
		
		return $return;
	}
	
	function getTramitesPersona($cedula){
		$return = FALSE;
		$sql="select id_seguimiento, t.nombre, fecha_solicitud, fecha_enviada, fecha_recibida_firmada, fecha_entregada, t.estatus , case when u.nombre ='' then u.nombre else usuario_impresion||'*' end  as analista from seguimiento s
				inner join tramite t using(id_tramite)
				left outer join solicitud sol using(id_solicitud)
				left outer join usuario u using(id_usuario)
				where s.cedula=$cedula";
		$res=$this->db->query($sql);
		
		if ($res){
			if ($res->num_rows()>0){
				$return=$res->result_array();
			}
		}
		
		return $return;
	}
	
	function getDetalleSeguimiento($idSeguimiento){
		$return=FALSE;
		$sql="select * from seguimiento
				inner join tramite using(id_tramite)  
				where id_seguimiento=$idSeguimiento";
		$res=$this->db->query($sql);
		if ($res){
			if ($res->num_rows()==1){
				$row=$res->result_array();
				$return=$row[0];
			}
		}
		
		return $return;
	}
}