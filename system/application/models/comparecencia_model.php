<?php
class comparecencia_model extends Model{
	function __construct(){
		parent::Model();
		$this->load->database();
	}
	
	function insertaActa($post){
		$id=FALSE;
		$post['fecha']=date('Y-m-d');
		if ($this->db->insert('comparecencia',$post)){
			$id=$this->db->insert_id();
		}
		
		return $id;
		
	}
	
	function getActa($id){
		$return=FALSE;
		$sql="select * from comparecencia where id_comparecencia=$id";
		$res=$this->db->query($sql);
		if ($res){
			if ($res->num_rows()==1){
				$row=$res->result_array();
				$return=$row[0];
			}
		}
		
		return $return;
	}
}