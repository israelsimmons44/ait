<?php
class caso_model extends Model{
	function __construct(){
		parent::Model();
		$this->load->database();
	}
	
	function getCasos($estatus){
		$arrCasos=FALSE;
		$sql="select * from caso where id_estatus_caso=$estatus";
		$res=$this->db->query($sql);
		if ($res->num_rows()>0){
			$arrCasos=$res->result_array();
		}
		
		return $arrCasos;
	}
	
	function getCaso($id){
		$rowCaso=FALSE;
		$sql="select * from caso where id_caso=$id";
		$sql="select * from caso c
				inner join detallecaso dc using(id_caso)
				inner join estatuscaso ec on ec.id_estatus_caso=dc.id_estatus_caso
				where id_caso=$id";
		$res=$this->db->query($sql);
		if($res->num_rows()>0){
			$rowCaso=$res->result_array();
		}
		return $rowCaso;
	}
	
	function addCaso($post){	
		$this->db->trans_start();	
		if (key_exists('idusuario', $post)){
			$dataCaso['id_usuario']=intval($post['idusuario']);
		}else{
			$dataCaso['id_usuario']=intval($this->session->userdata('idusuario'));
		}
		
		$dataCaso['id_estatus_caso']=1;
		$dataCaso['fecha_abierto']=date('Y-m-d');
		$dataCaso['id_reclamo']=intval($post['id_reclamo']);
		
		$this->db->insert('caso',$dataCaso);
		$id_caso=$this->db->insert_id();
		
		$data['id_estatus_caso']=1;
		$data['id_usuario']=$dataCaso['id_usuario'];
		$data['fecha']=date('Y-m-d');
		$data['observaciones']=$post['observaciones'];
		$data['id_caso']=$id_caso;
		
		$this->db->insert('detallecaso',$data);
		$this->db->trans_complete();
		
		return $id_caso;
	}
	
	function addDetalleCaso($post){
		$id=FALSE;
		$post['fecha']=date('Y-m-d');
		
		if ($post['id_estatus_caso']=='3'){
			$this->db->where(array('id_caso'=>$post['id_caso']));
			$this->db->update('caso',array('id_estatus_caso'=>3));
			
			$sql="select * from caso where id_caso=".$post['id_caso'];
			$res=$this->db->query($sql);
			if ($res){
				if ($res->num_rows()>0){
					$resultado=$res->row();
				}
			}
			
			$this->db->where(array('id_reclamo'=>$resultado->id_reclamo));
			$this->db->update('reclamo',array('estatus'=>'CE'));
			
		}
		$this->db->insert('detallecaso',$post);
		$id=$this->db->insert_id();
		
		return $id;
		
	}
}