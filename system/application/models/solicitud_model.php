<?php
class Solicitud_Model extends Model{
	function __construct(){
		parent::Model();
		$this->load->database();
		$this->sigefirrhh=$this->load->database('sigefirrhh',TRUE);
                
	}
	
	function getEntregables($cedula){
		$return=FALSE;
		$sql="select * from solicitud s
				inner join tramite t using(id_tramite) 
				left outer join seguimiento using(id_solicitud) 
				where s.estatus in ('R','I') and entregable='t' and s.cedula=$cedula";
		$res=$this->db->query($sql);
		
		if ($res){
			if ($res->num_rows>0){
				$return=$res->result_array();			
				
			}
		}
		
		return $return;
	}
	
	function actualizaEntrega($id,$persona=FALSE){
		//echo $id;
		$this->db->where('id_solicitud',$id);
		$this->db->update('solicitud',array('estatus'=>'E'));
		
		$this->db->where('id_solicitud',$id);
		$fecha=date('Y-m-d');
		
		if ($persona!=FALSE){
			$this->db->update('seguimiento',array('fecha_entregada'=>$fecha,'cedula_recibido'=>$persona['cedula'],'nombre_recibido'=>$persona['nombre']));
		}else{
			$this->db->update('seguimiento',array('fecha_entregada'=>$fecha));
		}
		
				
		
	}
	
	function getTramites(){
		$permisos=$this->session->userdata('permisos');
		
		if($permisos['analista']=='t'){
			$sql="select id_tramite, cod_tramite, nombre from tramite where estatus in ('A','S') order by nombre";
		}else{
			$sql="select id_tramite, cod_tramite, nombre from tramite where estatus='A' order by nombre";
		}
		
		
		$res=$this->db->query($sql);
		$arr=$res->result_array();
		/*
		foreach ($arr as $key=>$value) {
			$arr[$key]['nombre']=$arr[$key]['nombre'];
		}
		*/
		
		return $arr;
	}
	
	function getTramite($codTramite){
		$sql="select * from tramite where cod_tramite='$codTramite'";
		$res=$this->db->query($sql);
		
		if ($res->num_rows > 0){
			$tramite= $res->result_array();
			
			return $tramite[0];
		}else{
			return FALSE;
		}
		
	}
	
	function getRecaudos($codTramite){
		$arr['detalle']=FALSE;
		$sql="select * from tramite where cod_tramite='$codTramite'";
		$resTramite=$this->db->query($sql);
		$titulo=$resTramite->row();
		$arr['titulo']=$titulo->nombre;
		
		$sql="select r.id_recaudo, t.id_tramite,t.cod_tramite, r.nombre as nombre_recaudo, ruta_planilla, t.nombre as nombre_tramite, r.campo, r.tipo as tipo_campo, r.valor  from tramiterecaudo tr
				inner join recaudo r using(id_recaudo)
				inner join tramite t using(id_tramite) 
				where id_tramite in (select id_tramite from tramite where cod_tramite='$codTramite')";
		
		$res=$this->db->query($sql);
		if ($res->num_rows()>0){
			$arr['detalle']=$res->result_array();
			
			foreach($arr['detalle'] as $row){
				$arr['detallado'][]=array($row['nombre_recaudo']);
			}
		}
		
		return $arr;
	}
	
	function insertCaso($arr){
		$this->db->insert('solicitud',$arr);		
	}
	
	function getRecaudosForm($codTramite){
		$html="";
		$this->load->helper('form');
		$arr['detalle']=FALSE;
		$sql="select * from tramite where cod_tramite='$codTramite'";
		$resTramite=$this->db->query($sql);
		$titulo=$resTramite->row();
		$arr['titulo']=$titulo->nombre;
		$html.="<h3>$titulo->nombre</h3>";
		
		$sql="select r.id_recaudo, t.id_tramite,t.cod_tramite, r.nombre as nombre_recaudo, ruta_planilla, t.nombre as nombre_tramite, r.campo, r.tipo as tipo_campo, r.valor, tooltip  from tramiterecaudo tr
				inner join recaudo r using(id_recaudo)
				inner join tramite t using(id_tramite) 
				where id_tramite in (select id_tramite from tramite where cod_tramite='$codTramite')";
		
		$res=$this->db->query($sql);
		
		if ($res->num_rows()>0){
			$arr=$res->result();
			$html.= "<form name='frmsolicitud' id='frmsolicitud'>";
			$html.= form_hidden("id_tramite",$arr[0]->id_tramite);
			$html.="<table>";
			
			foreach ($arr as $valor){
				switch (trim($valor->tipo_campo)){
					case "select":
						if (substr(strtolower($valor->valor),0,6)=='select'){
							$resSolicitud=$this->db->query($valor->valor);
							if ($resSolicitud->num_rows > 0){
								$lista[0]="Elija una opción";
								foreach ($resSolicitud->result() as $valorSol){
									$lista[$valorSol->id_nivel_academico]=$valorSol->descripcion;
								}
							}
							$html.="<tr><td><label for='$valor->campo'>$valor->nombre_recaudo</label></td><td>".form_dropdown($valor->campo,$lista,''," title='$valor->tooltip'")."</td></tr>";
						}else{
							$lista=split(",",$valor->valor);						
							$html.="<tr><td><label for='$valor->campo'>$valor->nombre_recaudo</label></td><td>".form_dropdown($valor->campo,$lista,'',"title='$valor->tooltip'")."</td></tr>";
						}
						
						break;
					case "hidden":
						$html.=form_hidden($valor->campo,$valor->valor);
						break;
					case "texto":
						$html.="<tr><td><label for='$valor->campo'>$valor->nombre_recaudo</label></td><td>".form_input($valor->campo,"","class ='required' title='$valor->tooltip'")."</td></tr>";
						//$html.="<label for='$valor->valor'>$valor->nombre_recaudo</label>".form_input($valor->campo);
						break;
					case "checkbox":
						$html.="<tr><td>$valor->nombre_recaudo</td<td>".form_checkbox($valor->campo,$valor->valor)."</td></tr>";
						
						break;
				}
			}
			$html.="</table></form>";
			
		}
		
		return $html;
	}
	
	function setSolicitud($arr, $imprimePlanilla=TRUE){
		$this->load->helper('funciones');
		
		$str="";
		$error=0;
		$cedula=0;
		$idTramite=0;
		$idSolicitud=0;
		$codTramite="";
		$sol=FALSE;
		
		if(key_exists('cedula',$arr)){
			$cedula=$arr['cedula'];
		}
		
		$sql="select * from trabajador where cedula=$cedula and estatus='A'";
		$resVerificaActivo=$this->sigefirrhh->query($sql);
		
		if ($resVerificaActivo->num_rows()==0){
			echo mensajeGrowl("El trabajador se encuentra egresado por tanto no puede hacer esta solicitud","error");
			return $sol;	
		}
		
		if (key_exists('codtramite',$arr)){
			$codTramite=$arr['codtramite'];
			$res=$this->getTramite($codTramite);
			if ($res){
				$resultado=$res['id_tramite'];
				$arr['id_tramite']=$resultado;
				
			}
		}
		
		if (key_exists('id_tramite',$arr)){
			$idTramite=$arr['id_tramite'];			
		}
		
		$sql="select * from solicitud where cedula=$cedula and id_tramite=$idTramite and id_tramite not in (6) and estatus not in ('P','E')";
		
		$res=$this->db->query($sql);
		if ($res->num_rows>0){
			echo mensajeGrowl("Este trámite ya se encuentra en proceso ".$this->session->userdata('usuario').", si desea mas información al respecto, haga click aqui <a style='color:#0f0;text-weight:strong;' href='".base_url()."/index.php/main/consultatramite'>Consulta de Trámite</a>","alert");
			$sol['id_solicitud']=$res->row()->id_solicitud;
		}else{
			$sql="select nextval('solicitud_id_solicitud_seq') as proximo";
			$resNextVal=$this->db->query($sql);
			$idSolicitud=$resNextVal->row()->proximo;
			$sol['id_solicitud']=$idSolicitud;
			$sol['usuario']=$this->session->userdata('usuario');
			$sol['fecha']=date("Y-m-d H:i");
			$sol['cedula']=$cedula;
			if ($idTramite>0){
				$sol['id_tramite']=$idTramite;
			}
			
			$this->db->insert('solicitud',$sol);
			
			foreach ($arr as $key=>$valor){
				$str.="<br>".$key.": ".$valor."";
				$sql="";
				$detSol['id_solicitud']=$idSolicitud;
				$detSol['parametro']=$key;
				$detSol['valor']=$valor;
				
				if ($key=="cedula"){
					$sol['cedula']=$valor;
				}
				
				$res=$this->db->insert('detallesolicitud',$detSol);
				
			}
			
			$idUsuario=$this->session->userdata('idusuario');
			if ($idUsuario>0){
				$arrSeguimiento=array('cedula'=>$cedula,'id_solicitud'=>$idSolicitud, 'id_tramite'=>$idTramite,'fecha_solicitud'=>date("Y-m-d"),'id_usuario'=>$idUsuario);
			}else{
				$arrSeguimiento=array('cedula'=>$cedula,'id_solicitud'=>$idSolicitud, 'id_tramite'=>$idTramite,'fecha_solicitud'=>date("Y-m-d"));
			}
			
			$this->db->insert('seguimiento',$arrSeguimiento);
			
			if ($error==0){
				if ($imprimePlanilla==TRUE){
					echo mensajeGrowl("La solicitud se ha grabado correctamente, para imprimir la planilla haga click <a  style='color:#0f0;text-weight:strong;' href='".base_url()."index.php/imprimirplanilla/index/$idSolicitud'>aqui</a>");
				}else{
					echo mensajeGrowl("La solicitud se ha grabado correctamente con el número $idSolicitud. <br/>se ha enviado un correo a su cuenta para mayor información");
				}
				
				
			}else{
				echo mensajeGrowl("Hubo errores intentando grabar la solicitud","error");
			}
			
		}
		return $sol;
		
	}
	
	function _registraSeguimiento($arr){
		$idUsuario=$this->session->userdata('idusuario');
		if ($idUsuario>0){
			$arrSeguimiento=array('cedula'=>$cedula,'id_solicitud'=>$idSolicitud, 'id_tramite'=>$idTramite,'fecha_solicitud'=>date("Y-m-d"),'id_usuario'=>$idUsuario);
		}else{
			$arrSeguimiento=array('cedula'=>$cedula,'id_solicitud'=>$idSolicitud, 'id_tramite'=>$idTramite,'fecha_solicitud'=>date("Y-m-d"));
		}
		
		$this->db->insert('seguimiento',$arrSeguimiento);
	}
	
	function setComparecencia($idComparecencia,$post){
		if ($idComparecencia==0){
			$this->db->insert('comparecencia',$post);
		}else{
			$this->db->where('id_comparecencia',$idComparecencia);
			$this->db->update('comparecencia',$post);
		}
		
		$this->_registraSeguimiento($post);
		
	}
	
	function _getTipoConstancia($idSolicitud){
		$return=1;
		$sql="select valor from detallesolicitud where id_solicitud=$idSolicitud and parametro='tipoconstancia'";
		$res=$this->db->query($sql);
		if ($res->num_rows()>0){
			$arr=$res->row();
			switch ($arr->valor) {
				case 1:
					$return="MENSUAL";
				break;
				
				case 2:
					$return="ANUAL";
				break;
				case 3:
					$return="AMBAS";
				break;
			}
		}
		
		return $return;
	}
	
	function getSolicitudes($idTramite=""){
		$html="";
		
		if ($idTramite==""){
			$sql="select *, solicitud.estatus as estat from solicitud 
				inner join tramite using(id_tramite) where solicitud.estatus in ('A','I') order by id_solicitud";
		}else{
			$sql="select *, solicitud.estatus as estat from solicitud 
				inner join tramite using(id_tramite) where id_tramite='$idTramite'";
		}
		
		$res=$this->db->query($sql);
		if ($res->num_rows>0){
			
			$html="<table class='display' cellspacing='0'; width='100%'><thead><tr><th>&nbsp;</th><th>Solicitud</th><th>C&eacute;dula</th><th>Trámite</th><th>Fecha de Solicitud</th><th>&nbsp;</th><th>&nbsp;</th></tr></thead><tbody>";
			$row=$res->result();
			foreach ($row as $value){
				$href="";
				$estatus=$value->estat;
				$permisos=$this->session->userdata('permisos');
				if ($permisos['administrador']=='t'){
					switch ($estatus) {
						case "I":
							$href="<a href='".base_url()."index.php/admin/actualizaProcesado/$value->id_solicitud'><img src='".base_url()."images/process.png' /></a>";
							break;
					}
				}
				
				
				
				if ($value->id_tramite==20){
					$tipoSolicitud=$this->_getTipoConstancia($value->id_solicitud);
					$html.="<tr height='30px'>
					            <td align='center'>
					                <a href='".base_url()."index.php/analista/detalleSolicitud/$value->id_solicitud' rel='#overlay1'>
					                      <img src='".base_url()."images/search.png' />
					                </a>
					            </td>
					            <td align='center'>$value->id_solicitud</td>
					            <td align='right'>".number_format($value->cedula,0,",",".")."</td>
					            <td>$value->nombre - $tipoSolicitud</td>
					            <td align='center'>$value->fecha</td>
					            <td>
					                <a href='".base_url()."index.php/imprimirplanilla/index/$value->id_solicitud'>
					                       <img src='".base_url()."images/flechita.png' />
					                </a>$href</td><td>$value->estat
					             </td>
					         </tr>";
				}else{
					$html.="<tr height='30px'>
					            <td align='center'>
					                 <a href='".base_url()."index.php/analista/detalleSolicitud/$value->id_solicitud' rel='#overlay1'>
					                       <img src='".base_url()."images/search.png' />
					                 </a>
					            </td>
					            <td align='center'>$value->id_solicitud</td>
					            <td align='right'>".number_format($value->cedula,0,",",".")."</td>
					            <td>$value->nombre</td>
					            <td align='center'>$value->fecha</td>
					            <td>
					                 <a href='".base_url()."index.php/imprimirplanilla/index/$value->id_solicitud'>
					                        <img src='".base_url()."images/flechita.png' />
					                 </a>$href
					            </td>
					            <td>$value->estat</td>
					         </tr>";
				}
				
			}
			$html.="</tbody></table>";
		}
		
		return $html;
	}

	function getSolicitudesActivas($idTramite=""){
		$html="";
		
		if ($idTramite==""){
			$sql="select *, solicitud.estatus as estat from solicitud 
				inner join tramite using(id_tramite) where solicitud.estatus='A' order by id_solicitud";
		}else{
			$sql="select *, solicitud.estatus as estat from solicitud 
				inner join tramite using(id_tramite) where id_tramite='$idTramite'";
		}
		
		$res=$this->db->query($sql);
		if ($res->num_rows>0){
			
			$html="<table class='display' cellspacing='0'; width='100%'><thead><tr><th>&nbsp;</th><th>Solicitud</th><th>C&eacute;dula</th><th>Trámite</th><th>Fecha de Solicitud</th><th>&nbsp;</th><th>&nbsp;</th></tr></thead><tbody>";
			$row=$res->result();
			foreach ($row as $value){
				$href="";
				$estatus=$value->estat;
				$permisos=$this->session->userdata('permisos');
				/*
				if ($permisos['administrador']=='t'){
					switch ($estatus) {
						case "I":
							$href="<a href='".base_url()."index.php/admin/actualizaProcesado/$value->id_solicitud'><img src='".base_url()."images/process.png' /></a>";
							break;
					}
				}
				*/
				
				
				if ($value->id_tramite==20){
					$tipoSolicitud=$this->_getTipoConstancia($value->id_solicitud);
					$html.="<tr height='30px'>
					            <td align='center'>
					                <a href='".base_url()."index.php/analista/detalleSolicitud/$value->id_solicitud' rel='#overlay1'>
					                      <img src='".base_url()."images/search.png' />
					                </a>
					            </td>
					            <td align='center'>$value->id_solicitud</td>
					            <td align='right'>".number_format($value->cedula,0,",",".")."</td>
					            <td>$value->nombre - $tipoSolicitud</td>
					            <td align='center'>$value->fecha</td>
					            <td>
					                <a href='".base_url()."index.php/imprimirplanilla/index/$value->id_solicitud'>
					                       <img src='".base_url()."images/flechita.png' />
					                </a>$href</td><td>$value->estat
					             </td>
					         </tr>";
				}else{
					$html.="<tr height='30px'>
					            <td align='center'>
					                 <a href='".base_url()."index.php/analista/detalleSolicitud/$value->id_solicitud' rel='#overlay1'>
					                       <img src='".base_url()."images/search.png' />
					                 </a>
					            </td>
					            <td align='center'>$value->id_solicitud</td>
					            <td align='right'>".number_format($value->cedula,0,",",".")."</td>
					            <td>$value->nombre</td>
					            <td align='center'>$value->fecha</td>
					            <td>
					                 <a href='".base_url()."index.php/imprimirplanilla/index/$value->id_solicitud'>
					                        <img src='".base_url()."images/flechita.png' />
					                 </a>$href
					            </td>
					            <td>$value->estat</td>
					         </tr>";
				}
				
			}
			$html.="</tbody></table>";
		}
		
		return $html;
	}	
	
	function getSolicitudesPersona($cedula,$avanzado=FALSE){
		$html="";
		$sql="select id_solicitud,cod_tramite, nombre, fecha, solicitud.estatus from solicitud 
				inner join tramite using(id_tramite)
				where cedula=$cedula";
		
		$res=$this->db->query($sql);
		if ($res->num_rows>0){
			if ($avanzado==FALSE){
				$html="<h4 style='margin-bottom:5px;'>Solicitudes en proceso</h4><table width='100%'>";
				$row=$res->result();
				foreach ($row as $value){
					$html.="<tr><td><a href='".base_url()."index.php/main/consultatramite'>$value->nombre</a></td></tr>";
				}
				$html.="</table>";
			}else{
				$html=$res->result_array();
			}
			
			
		}
		
		return $html;
		
	}
	
	function deleteSolicitud($idSolicitud){
		$this->load->helper('funciones');
		$sql="delete from detallesolicitud where id_solicitud=$idSolicitud";
		$this->db->query($sql);
		
		$sql="delete from solicitud where id_solicitud=$idSolicitud";
		$this->db->query($sql);
		echo mensajeGrowl("La solicitud fué eliminada!!!");
		
	}
	
	function getParametrosSolicitud($idSolicitud){
		$this->load->helper('funciones');
		$sql="select * from solicitud 
				iner join tramite using(id_tramite)
				where id_solicitud=$idSolicitud";
		//echo $sql;
		
		$res=$this->db->query($sql);
		$row=$res->row();
		$arr['cedula']=$row->cedula;
		$arr['idTramite']=$row->id_tramite;
		$arr['tituloTramite']=$row->nombre;
		$arr['codTramite']=$row->cod_tramite;
		$arr['fecha']=pgDate($row->fecha);
		
		$sql="select parametro, ds.valor as valords, r.nombre from detallesolicitud ds
				inner join recaudo r on r.campo=ds.parametro
				where id_solicitud=$idSolicitud";
		//echo $sql;
		
		$ros=$this->db->query($sql);
		if($ros->num_rows>0){
			$arr['detalle']=$ros->result_array();
		}else{
			$arr['detalle']=FALSE;
		}
		
		return $arr;	
	}
	
	function getDatosSolicitud($fecha1,$fecha2){
		$sql="select * from solicitud s
			left join(
				select id_solicitud,valor as telcel from detallesolicitud where parametro='telcel') telefono using (id_solicitud)
			left join(
				select id_solicitud,valor as telhab from detallesolicitud where parametro='telhab') telefonohab using (id_solicitud)
			inner join tramite using(id_tramite)
			where s.estatus='P' and fecha between '$fecha1' and '$fecha2'
			order by cedula";
		$res=$this->db->query($sql);
		return $res->result_array();
	}
	
	function getDetalleSolicitud($idSolicitud){
		$sql="select * from solicitud s
			left join(
				select id_solicitud,valor as telcel from detallesolicitud where parametro='telcel') telefono using (id_solicitud)
			left join(
				select id_solicitud,valor as telhab from detallesolicitud where parametro='telhab') telefonohab using (id_solicitud)
			inner join tramite using(id_tramite)
			where  s.id_solicitud=$idSolicitud
			order by cedula";
		$sql="select nombre,ds.valor from detallesolicitud ds
				left join recaudo r on r.campo=ds.parametro
				where id_solicitud=$idSolicitud";
		$res=$this->db->query($sql);
		$arr=$res->result_array();
		$html="<table>";
		foreach ($arr as $value) {
			$html.="<tr><td>".$value['nombre']."</td><td>".$value['valor']."</td></tr>";
		}
		$html.="</table>";
		return $html;
		
	}
	
	function getSolicitud($idSolicitud){
		$sql="select * from solicitud where id_solicitud=$idSolicitud";
		
		$sql="select id_solicitud, id_tramite, cedula, cod_tramite, t.nombre, solicitud.estatus, usuario.nombre from solicitud
				inner join tramite t using(id_tramite) 
				left outer join usuario on login=usuario_impresion
				where id_solicitud=$idSolicitud";
		
		$res=$this->db->query($sql);
		
		$arr=$res->result_array();
		return $arr[0];
	}
	
	function setPlanillaImpresa($idSolicitud){
		$impreso=FALSE;
		$sql="select * from solicitud where id_solicitud=$idSolicitud";
		$res=$this->db->query($sql);
		if ($res){
			if ($res->num_rows()==1){
				$arrRes=$res->row();
				$impreso=$arrRes->estatus;
			}
		}
		
		if ($impreso!="I"){
			$this->db->where('id_solicitud',$idSolicitud);
			$this->db->update('solicitud',array('estatus'=>"I",'usuario_impresion'=>$this->session->userdata('usuario')));
		}
		
	}
	
	function getTramitesEnDeuda($fecha){
		$return=FALSE;
		$sql="select * from solicitud where fecha='$fecha'";
		$res=$this->db->query($sql);
		if ($res->num_rows()>0){
			$return=$res->result_array();
		}
		
		return $return;
	}
	
///////////////////////////////////// FUNCIÓN PARA CONSULTAR EN BD DATOS DEL TRABAJADOR Y SU(S) HIJO(S) /////////////////////////	

	function getPersonal($cedula){
		$arrReturn=FALSE;
		$sql="select p.id_personal, p.cedula, p.primer_nombre, p.segundo_nombre, p.primer_apellido, p.segundo_apellido,
		             d.nombre as dependencia, c.descripcion_cargo as cargo,
		             p.direccion_residencia, p.telefono_celular, p.telefono_oficina, p.telefono_residencia, p.email 
		      from personal p, trabajador t, cargo c, dependencia d  
		      where t.id_tipo_personal not in (31,62,73,81)
                            and p.cedula=t.cedula 
                            and t.id_dependencia=d.id_dependencia 
		            and t.id_cargo=c.id_cargo 
                            and p.cedula=$cedula";
		$resPersonal=$this->sigefirrhh->query($sql);
		if ($resPersonal->num_rows()>0){
			$personal=$resPersonal->row();
			$arrReturn['personal']=$personal;
			$sql="select vgru.cedula, vgru.id_familiar, vgru.cedula_familiar, vgru.p_apellido_fam||' '||vgru.s_apellido_fam||' '||vgru.p_nombre_fam||' '||vgru.s_nombre_fam
                                as nombre_fam,vgru.sexo, vgru.fecha_nacimiento, vgru.edad
                            from  vgrupofamiliar  vgru   
                            where vgru.id_tipo_personal not in (31,62,73,81)
                            and vgru.parentesco='H'
                            and vgru.edad between 0
                            and 6 and vgru.cedula=$cedula  order by cedula_familiar";
			$resHijosTrabajador=$this->sigefirrhh->query($sql);			
			$arrReturn['hijostrabajador']=$resHijosTrabajador->result_array();
			foreach ($arrReturn['hijostrabajador'] as $key=>$value) {
				$arrReturn['hijostrabajador'][$key]['fecha_nacimiento']=pgDate($value['fecha_nacimiento']);
				$arrReturn['hijostrabajador'][$key]['nombre_fam']=utf8_encode($value['nombre_fam']);
			}
			
		}
		
		return $arrReturn;
	}
        
        ///////////////////////////////////// FUNCIÓN PARA CONSULTAR EN BD DATOS DEL TRABAJADOR Y SU(S) HIJO(S) /////////////////////////	

	function getPersonalBeca($cedula){
		$arrReturn=FALSE;
		$sql="select p.id_personal, p.cedula, p.primer_nombre, p.segundo_nombre, p.primer_apellido, p.segundo_apellido,
		             d.nombre as dependencia, c.descripcion_cargo as cargo,
		             p.direccion_residencia, p.telefono_celular, p.telefono_oficina, p.telefono_residencia, p.email 
		      from personal p, trabajador t, cargo c, dependencia d  
		      where t.id_tipo_personal not in (31,62,73,81)
                            and p.cedula=t.cedula 
                            and t.id_dependencia=d.id_dependencia 
		            and t.id_cargo=c.id_cargo 
                            and p.cedula=$cedula";
		$resPersonal=$this->sigefirrhh->query($sql);
		if ($resPersonal->num_rows()>0){
			$personal=$resPersonal->row();
			$arrReturn['personal']=$personal;
			$sql="select * from ((select vgru.cedula, vgru.id_familiar, vgru.cedula_familiar, vgru.p_apellido_fam||' '||vgru.s_apellido_fam||' '||vgru.p_nombre_fam||' '||vgru.s_nombre_fam
                                as nombre_fam,vgru.sexo, vgru.fecha_nacimiento, vgru.edad
                            from  vgrupofamiliar  vgru   
                            where vgru.id_tipo_personal not in (31,62,73,81)
                            and vgru.parentesco='H'
                            and vgru.edad between 3
                            and 21 
                            and vgru.cedula=$cedula)
                            union
                            (select vgru.cedula, vgru.id_familiar, vgru.cedula_familiar, vgru.p_apellido_fam||' '||vgru.s_apellido_fam||' '||vgru.p_nombre_fam||' '||vgru.s_nombre_fam
                                as nombre_fam,vgru.sexo, vgru.fecha_nacimiento, vgru.edad
                            from  vgrupofamiliar  vgru   
                            where vgru.id_tipo_personal not in (31,62,73,81)
                            and vgru.parentesco='H'
                            and vgru.edad>21
                            and vgru.nino_excepcional='S'
                            and vgru.cedula=$cedula)) AS consultahijo order by cedula_familiar";
			 
			$resHijosTrabajador=$this->sigefirrhh->query($sql);			
			
			$arrReturn['hijostrabajador']=$resHijosTrabajador->result_array();
			
			foreach ($arrReturn['hijostrabajador'] as $key=>$value) {
				$arrReturn['hijostrabajador'][$key]['fecha_nacimiento']=pgDate($value['fecha_nacimiento']);
				$arrReturn['hijostrabajador'][$key]['nombre_fam']=utf8_encode($value['nombre_fam']);
			}
			
		}
		
		return $arrReturn;
	}
        
        
        ///////////////////////////////////// FUNCIÓN PARA CONSULTAR EN BD DATOS DEL TRABAJADOR Y SU(S) HIJO(S) /////////////////////////	

	function getPersonalUtiles($cedula){
		$arrReturn=FALSE;
		$sql="select p.id_personal, p.cedula, p.primer_nombre, p.segundo_nombre, p.primer_apellido, p.segundo_apellido,
		             d.nombre as dependencia, c.descripcion_cargo as cargo,
		             p.direccion_residencia, p.telefono_celular, p.telefono_oficina, p.telefono_residencia, p.email 
		      from personal p, trabajador t, cargo c, dependencia d  
		      where t.id_tipo_personal not in (31,62,73,81)
                            and p.cedula=t.cedula 
                            and t.id_dependencia=d.id_dependencia 
		            and t.id_cargo=c.id_cargo 
                            and p.cedula=$cedula";
		$resPersonal=$this->sigefirrhh->query($sql);
		if ($resPersonal->num_rows()>0){
			$personal=$resPersonal->row();
			$arrReturn['personal']=$personal;
			$sql="select * from (
                           (select cedula, id_familiar, cedula_familiar, p_apellido_fam||' '||s_apellido_fam||' '||p_nombre_fam||' '||s_nombre_fam
                            as nombre_fam,sexo, fecha_nacimiento, edad
                            from  vgrupofamiliar     
                            where id_tipo_personal not in (31,62,73,81)
                            and parentesco='H'
                            and edad between 0
                            and 26                             
                            and cedula=$cedula)
                            union
                            (select cedula, id_familiar, cedula_familiar, p_apellido_fam||' '||s_apellido_fam||' '||p_nombre_fam||' '||s_nombre_fam
                                as nombre_fam,sexo, fecha_nacimiento, edad
                            from  vgrupofamiliar    
                            where id_tipo_personal not in (31,62,73,81) 
                            and parentesco='H'
                            and edad>26                             
                            and nino_excepcional='S'
                            and cedula=$cedula)) AS consultahijo order by cedula_familiar";
                        $resHijosTrabajador=$this->sigefirrhh->query($sql);			
			
			$arrReturn['hijostrabajador']=$resHijosTrabajador->result_array();
			
			foreach ($arrReturn['hijostrabajador'] as $key=>$value) {
				$arrReturn['hijostrabajador'][$key]['fecha_nacimiento']=pgDate($value['fecha_nacimiento']);
				$arrReturn['hijostrabajador'][$key]['nombre_fam']=utf8_encode($value['nombre_fam']);
			}
			
		}
		
		return $arrReturn;
	}
        
        /////////////////////////////////////////////////////////////////////////////////////
        function getPersonalBGuarderiaMensual($cedula){
		$arrReturn=FALSE;
		$sql="select p.id_personal, p.cedula, p.primer_nombre, p.segundo_nombre, p.primer_apellido, p.segundo_apellido,
		             d.nombre as dependencia, c.descripcion_cargo as cargo,
		             p.direccion_residencia, p.telefono_celular, p.telefono_oficina, p.telefono_residencia, p.email 
		      from personal p, trabajador t, cargo c, dependencia d  
		      where t.id_tipo_personal not in (31,62,73,81)
                            and p.cedula=t.cedula 
                            and t.id_dependencia=d.id_dependencia 
		            and t.id_cargo=c.id_cargo 
                            and p.cedula=$cedula";
		$resPersonal=$this->sigefirrhh->query($sql);
		if ($resPersonal->num_rows()>0){
			$personal=$resPersonal->row();
			$arrReturn['personal']=$personal;
			$sql="select vgru.cedula, vgru.id_familiar, vgru.cedula_familiar, vgru.p_apellido_fam||' '||vgru.s_apellido_fam||' '||vgru.p_nombre_fam||' '||vgru.s_nombre_fam
                                as nombre_fam,vgru.sexo, vgru.fecha_nacimiento, vgru.edad
                            from  vgrupofamiliar  vgru   
                            where vgru.id_tipo_personal not in (31,62,73,81)
                            and vgru.parentesco='H'
                            and vgru.edad between 0
                            and 6 and vgru.cedula=$cedula  order by cedula_familiar";
			$resHijosTrabajador=$this->sigefirrhh->query($sql);			
			$arrReturn['hijostrabajador']=$resHijosTrabajador->result_array();
			foreach ($arrReturn['hijostrabajador'] as $key=>$value) {
				$arrReturn['hijostrabajador'][$key]['fecha_nacimiento']=pgDate($value['fecha_nacimiento']);
				$arrReturn['hijostrabajador'][$key]['nombre_fam']=utf8_encode($value['nombre_fam']);
			}
			
		}
		
		return $arrReturn;             
               
	}
        /////////////////////////////////// FUNCIÓN PARA CONSULTAR EN BD DATOS DE UN FAMILIAR EN ESPECÍFICO /////////////////////////	
	
	function getHijo($idFamiliar){
		$arrReturn=FALSE;
		$sql="select  id_familiar, cedula_familiar, p_apellido_fam||' '||s_apellido_fam||' '||p_nombre_fam||' '||s_nombre_fam as nombre_fam,
		      sexo, fecha_nacimiento, edad
		      from  vgrupofamiliar      
		      where id_familiar=$idFamiliar";
		$resHijos = $this->sigefirrhh->query($sql);			
		$var = $resHijos->row();
		return $var ;
	
	}
	
        ////////////////////////// FUNCIÓN PARA CONSULTAR EN BD DATOS ADICIONALES DE UN HIJO EN ESPECÍFICO /////////////////////////	
	
        function getValidarHijo ($idFamiliar, $anio_escolar){
		$sql="select id_familiar 
                      from guarderia 
                      where id_familiar=$idFamiliar and anio_escolar=$anio_escolar";
		$resDatosHijoAdicional = $this->db->query($sql);			
		$var = $resDatosHijoAdicional->row();
		return $var ;
	}
        
        function getValidarHijoBeca ($idFamiliar, $anio_escolar){
		$sql="select id_familiar 
                      from beca 
                      where id_familiar=$idFamiliar and anio_escolar=$anio_escolar";
		$resDatosHijoBeca = $this->sigefirrhh->query($sql);			
		$var = $resDatosHijoBeca->row();
		return $var ;
	}
        
        function getValidarHijoUtiles ($idFamiliar, $anio_escolar){
		$sql="select id_familiar 
                      from utiles 
                      where id_familiar=$idFamiliar and anio_escolar=$anio_escolar";
		$resDatosHijoUtiles = $this->db->query($sql);			
		$var = $resDatosHijoUtiles->row();
		return $var ;
	}
        
        function getValidarHijoPMensual($idFamiliar, $anio_escolar, $rif, $numero_factura){
                $sql="select id_familiar 
                      from guarderiamensual 
                      where id_familiar=$idFamiliar and anio_escolar=$anio_escolar and 
                      rif='$rif' and numero_factura=$numero_factura";
		$resDatosHijoUtiles = $this->db->query($sql);			
		$var = $resDatosHijoUtiles->row();
		return $var ;
	}
        
        

////////////////////////// FUNCIÓN PARA INGRESAR GUARDERIA ////////////////////////////////////////////////////	
	
	function guardaDatosGuarderias ($data){
		$guardardatos=$this->db->insert('guarderia', $data);
	}
        
        function guardaDatosBeca ($data){
		$guardardatos=$this->sigefirrhh->insert('beca', $data);
	}
        
        function guardaDatosUtiles ($data){
		$guardardatos=$this->db->insert('utiles', $data);
	}
        
        function guardaDatosGuarderiaMensual ($data){
		$guardardatos=$this->db->insert('guarderiamensual', $data);
	}
        
        
   //////////////////////////// FUNCIÓN PARA CONSULTAR EN BD DATOS DEL TRABAJADOR Y UN HIJO EN ESPECÍFICO /////////////////////////	
	
    function getPadreHijo($idFamiliar){
		$arrReturn=FALSE;
		$sql="select p.cedula, p.primer_apellido||' '||p.segundo_apellido||' '||p.primer_nombre||' '||p.segundo_nombre as nombre,
		             d.nombre as dependencia, c.descripcion_cargo as cargo,
		             p.direccion_residencia, p.telefono_celular, p.telefono_oficina, p.telefono_residencia, p.email,
                     f.cedula_familiar, t.id_tipo_personal,
                     f.primer_apellido||' '||f.segundo_apellido||' '||f.primer_nombre||' '||f.segundo_nombre as nombre_fam,
		             f.sexo, f.fecha_nacimiento, ('now'::text::date - f.fecha_nacimiento) / 365 as edad 
		     from personal p, trabajador t, cargo c, dependencia d, familiar f  
		     where  t.id_tipo_personal not in (31,62,73,81)
                            and p.cedula=t.cedula 
                            and t.id_dependencia=d.id_dependencia 
                            and p.id_personal=f.id_personal 
                            and t.id_cargo=c.id_cargo 
                            and t.estatus='A' 
                            and f.id_familiar=$idFamiliar";
		
		$resPadreHijo = $this->sigefirrhh->query($sql);			
		$var = $resPadreHijo->row();
				
		return $var ;
	}     
//////////////////////////// FUNCIÓN PARA MOSTRAR LA INFORMACIÓN REGISTRADA /////////////////////////	
	
            
      function getGuarderia($idFamiliar, $anio_escolar){
		$sql="select instituto, registro_mpppe, constancia_inscripcion, factura_mensual, copia_carnet, ci_funcionario,
                contrato, partida_nacimiento, registro_mercantil, inscripcion_plantel 
                from guarderia 
                where id_familiar=$idFamiliar 
                      and anio_escolar=$anio_escolar";
		$resGuarderia = $this->db->query($sql);			
		$var = $resGuarderia->row();
		return $var ;
	}
        
        function getBeca($idFamiliar, $anio_escolar){
		$sql="select nivel_educativo, grado_semestre, anio_escolar, promedio_notas, instituto, carrera_especialidad, 
                      inscripcion, calificacion, carnet, cifuncionario, cibeneficiario, partidanacimiento, informe, contrato, 
                      utiles,id_tipo_beca
                      from beca 
                      where id_familiar=$idFamiliar and anio_escolar=$anio_escolar";
		$resBeca = $this->sigefirrhh->query($sql);			
		$var = $resBeca->row();
		return $var ;
	}
        
        function getUtiles($idFamiliar, $anio_escolar){
		$sql="select nivel_educativo, grado_semestre, anio_escolar, instituto, carrera_especialidad, 
                      inscripcion, carnet, cifuncionario, cibeneficiario, partidanacimiento, informe, contrato
                      from utiles 
                      where id_familiar=$idFamiliar 
                            and anio_escolar=$anio_escolar";
		$resUtiles = $this->db->query($sql);			
		$var = $resUtiles->row();
		return $var ;
	}
        
        function getGuarderiaMensual($idFamiliar, $anio_escolar){
		$sql="select anio_escolar, instituto, rif, numero_factura, mes_factura 
                      from guarderiamensual 
                      where id_familiar=$idFamiliar 
                            and anio_escolar=$anio_escolar";
		$resGuarderiaMensual = $this->db->query($sql);			
		$var = $resGuarderiaMensual->row();
		return $var ;
	}
        
}
