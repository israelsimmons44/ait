<?php
class admin_model extends Model{
	function __construct(){
		parent::Model();
		$this->load->database();
	}
	
	function setFormatoSolicitud2($codTramite, $valor,$nroFormato=1){
		$data['formato']=$valor;
		$this->db->where('cod_tramite',$codTramite);
		$this->db->where('nro_formato',$nroFormato);
		$res=$this->db->update('tramite',$data);
		
		return $res;
	}
	
	
	function setFormatoSolicitud($idTramite, $idTipoPersonal, $valor, $firma="",$nroFormato){	
		$sql="select * from formatotramite where id_tramite=$idTramite and id_tipo_personal=$idTipoPersonal and nro_formato=$nroFormato";
		$res=$this->db->query($sql);
		if ($res->num_rows==0){
			$data['id_tipo_personal']=$idTipoPersonal;
			$data['id_tramite']=$idTramite;
			$data['formato']=$valor;
			$data['firma']=$firma;
			$data['nro_formato']=$nroFormato;
			$res=$this->db->insert('formatotramite',$data);
		}	
		
		
		$data['formato']=$valor;
		$this->db->where('id_tramite',$idTramite);
		$this->db->where('id_tipo_personal',$idTipoPersonal);
		$this->db->where('nro_formato',$nroFormato);
		$res=$this->db->update('formatotramite',$data);
		
		return $res;
	}
	
	function getFormatoSolicitud($idTramite, $idTipoPersonal=0, $nroFormato=1){
		
		if ($idTipoPersonal==0){
			$sql="select * from formatotramite where id_tramite=$idTramite";
		}else{
			$sql="select * from formatotramite where id_tramite=$idTramite and id_tipo_personal=$idTipoPersonal and nro_formato=$nroFormato";
		}
		
		
		//$sql="select * from tramite where cod_tramite='$codTramite'";
		$res=$this->db->query($sql);
		
		if ($res->num_rows>0){
			$arr= $res->result_array();
		}else{
			$arr[0]['formato']="NOY HAY FORMATO DEFINIDO";
		}
		
		return $arr[0];
	}
	
	
	
	function addTipoReclamo($post){
		$id=FALSE;
		$this->db->insert('tiporeclamo',$post);
		$id=$this->db->insert_id();
		return $id;
	}
	
	function getTipoReclamos(){
		$res=$this->db->get('tiporeclamo');
		return $res->result_array();
	}
	
	function getSugerencias(){
		$res=$this->db->get('sugerencia');
		return $res->result_array();
	}
	
	function setSolicitudEstatus($idSolicitud,$estatus='P'){
		$res=FALSE;
		$this->db->where('id_solicitud',$idSolicitud);
		$this->db->update('solicitud',array('estatus'=>$estatus));
		$res=$this->db->affected_rows();
		return $res;
	}
	
	
}