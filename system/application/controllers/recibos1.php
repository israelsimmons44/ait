<?php
class recibos extends Controller{
	function __construct(){
		parent::Controller();
		
		if (!$this->session->userdata('nombre')){
			redirect(base_url()."index.php/comun/login");
		}
		
		$this->load->model('recibos_model','rm');
		$this->load->library('parser');
		$this->load->helper('funciones');
	}
	
	function index(){
		$this->_header();
		$recibos=$this->rm->getRecibos($this->session->userdata('cedula'));		
		if (!$recibos){
			$recibos=array();	
		}
		
		foreach ($recibos as $key=>$value) {
			$recibos[$key]['fecha']=pgDate($value['fecha']);
			$recibos[$key]['imprime']="<a href='".base_url()."index.php/recibos/imprime/".$value['id_historico_nomina']."'><img src='".base_url()."images/printer_on.png' /></a>";

			//verifica si en descripcion esta vacio, si no es asi significa que es una nomina especial
			if ($recibos[$key]['descripcion']==""){
				$recibos[$key]['descripcion']=$recibos[$key]['quincenatexto'];
			}else{
				$recibos[$key]['descripcion']=utf8_encode($recibos[$key]['descripcion']);
			}
			
		}
		
		$data['recibos']=$recibos;
		$this->parser->parse('recibos/index-view',$data);
	}
	
	function imprime(){
		$idHistoricoNomina=$this->uri->segment(3);
		$recibo=$this->rm->getRecibo($idHistoricoNomina);
		$persona=$this->rm->getPersonaRecibo($idHistoricoNomina);
		$this->load->library('pdf');
		
		if ($persona->id_tipo_personal==1){
			$PasoBanda="BANDA";
		}else{
			$PasoBanda="PASO";
		}
		
		$this->pdf->SetMargins(20,25);
		$this->pdf->setPrintFooter(FALSE);
		$this->pdf->setCellPadding(1, 1, 1, 1);
		$this->pdf->AddPage();
		
        $this->pdf->ln();
        $this->pdf->SetFont('courier', '', 6);
        $this->pdf->Cell(115,2,"DIRECCIÓN DE ADMINISTRACIÓN DE PERSONAL","0",0,"L");
        $this->pdf->SetFont('courier', 'B', 6);
        $this->pdf->Cell(25,2,"Fecha de Impresión:","0",0,"L");
        $this->pdf->SetFont('courier', '', 6);
        $this->pdf->Cell(12,2,date('d/m/Y h:i'),"0",0,"L");
        $this->pdf->SetFont('courier', 'BI', 9);
        
        $this->pdf->ln();
        $this->pdf->Cell(115,2,"REPORTE DE RELACIÓN DE PAGO","0",0,"L");
        $this->pdf->SetFont('courier', 'B', 6);
        $this->pdf->Cell(25,2,"Recibo nro:","0",0,"L");
        $this->pdf->SetFont('courier', '', 6);
        $this->pdf->Cell(25,2,$idHistoricoNomina,"0",0,"L");
        $this->pdf->ln();
        $this->pdf->SetFont('courier', 'BI', 9);
        $this->pdf->Cell(85,2,$persona->tipo." RECIBO DEL:".$persona->fechaQuincena,"0",0,"L");
        
        $this->pdf->ln(12);
        
        $this->pdf->SetFont('courier', 'B', 9);
        $this->pdf->MultiCell(22,2,"CÉDULA:","0","L",FALSE,0);
        $this->pdf->SetFont('courier', '', 9);
        $this->pdf->MultiCell(110,2,number_format($persona->cedula,0,",","."),"0","L",FALSE,0);
        $this->pdf->SetFont('courier', 'B', 9);
        $this->pdf->MultiCell(35,2,"CÓDIGO NÓMINA:","0","L",FALSE,0);
        $this->pdf->SetFont('courier', '', 9);
        $this->pdf->MultiCell(15,2,$persona->codigo_nomina,"0","L",FALSE,1);
        
        $this->pdf->SetFont('courier', 'B', 9);
        $this->pdf->MultiCell(22,2,"NOMBRE:","0","L",FALSE,0);
        $this->pdf->SetFont('courier', '', 9);
        $this->pdf->MultiCell(110,2,utf8_encode($persona->apellido1." ".$persona->apellido2.", ".$persona->nombre1." ".$persona->nombre2),"0","L",FALSE,0);
        
        $this->pdf->SetFont('courier', 'B', 9);
        $this->pdf->MultiCell(14,2,"GRADO:","0","L",FALSE,0);        
        $this->pdf->SetFont('courier', '', 9);
        $this->pdf->MultiCell(7,2,$persona->grado,"0","L",FALSE,0);
        
        $this->pdf->SetFont('courier', 'B', 9);
        $this->pdf->MultiCell(12,2,$PasoBanda,"0","L",FALSE,0);
        $this->pdf->SetFont('courier', '', 9);
        $this->pdf->MultiCell(7,2,$persona->paso,"0","L",FALSE,1);
        
        $this->pdf->SetFont('courier', 'B', 9);
        $this->pdf->MultiCell(22,2,"CARGO:","0","L",FALSE,0);
        $this->pdf->SetFont('courier', '', 9);
        $this->pdf->MultiCell(170,2,utf8_encode($persona->cargo),"0","L",FALSE,1);
        
        $this->pdf->SetFont('courier', 'B', 9);
        $this->pdf->MultiCell(22,2,"UBICACIÓN:","0","L",FALSE,0);
        $this->pdf->SetFont('courier', '', 9);
        $this->pdf->MultiCell(90,2,$persona->dependencia,"0","L",FALSE,1);
        
        $this->pdf->ln(12);
        $this->pdf->SetFont('courier', 'B', 8);
        
        $i=0;
        $this->pdf->SetFillColor(240, 240, 240);
        $this->pdf->MultiCell(15,0,"Código","LBT","L",TRUE,0);
		$this->pdf->MultiCell(110,0,"Concepto","LBT","L",TRUE,0);
		$this->pdf->MultiCell(25,0,"Asignación","LBT","R",TRUE,0);
		$this->pdf->MultiCell(25,0,"Deducción","LRBT","R",TRUE,1);
        $this->pdf->SetFont('courier', '', 8);
        $totalAsigna=0;
        $totalDeduce=0;
        foreach ($recibo as $value) {
        	if ($i%2!=0){
				$this->pdf->SetFillColor(240, 240, 240);
        	}else{
        		$this->pdf->SetFillColor(255,255, 255);
        	}
        	$i++;
        	$this->pdf->MultiCell(15,0,$value['cod_concepto'],"L","L",TRUE,0);
        	$this->pdf->MultiCell(110,0,utf8_encode($value['descripcion']),"L","L",TRUE,0);
        	if (intval($value['monto_asigna'])>0){
        		$this->pdf->MultiCell(25,0,number_format($value['monto_asigna'],2,",","."),"L","R",TRUE,0);
        	}else{
        		$this->pdf->MultiCell(25,0,"","L","R",TRUE,0);
        	}
        	
        	if (intval($value['monto_deduce'])>0){
        		$this->pdf->MultiCell(25,0,number_format($value['monto_deduce'],2,",","."),"RL","R",TRUE,1);
        	}else{
        		$this->pdf->MultiCell(25,0,"","RL","R",TRUE,1);
        	}
        	
        	$totalAsigna+=$value['monto_asigna'];
        	$totalDeduce+=$value['monto_deduce'];
        }
        
        $this->pdf->SetFillColor(255,255, 255);
        $this->pdf->SetFont('courier', 'B', 8);
        $this->pdf->MultiCell(125,0,"TOTAL ASIGNACIONES / DEDUCCIONES","LRBT","L",TRUE,0);       	
       	$this->pdf->MultiCell(25,0,number_format($totalAsigna,2,",","."),"LRBT","R",TRUE,0);
		$this->pdf->MultiCell(25,0,number_format($totalDeduce,2,",","."),"LRBT","R",TRUE,1);
		$this->pdf->ln();
		$this->pdf->MultiCell(125,0,"NETO A COBRAR","LRBT","L",TRUE,0);       	
		$this->pdf->MultiCell(50,0,number_format($totalAsigna-$totalDeduce,2,",","."),"LRBT","R",TRUE,1);
		$this->pdf->SetFont('courier', '', 8);
        $this->pdf->Cell(175,2,sha1("recibo $idHistoricoNomina ".$persona->cedula.' '.$totalAsigna.' '.$totalDeduce),"0",0,"C");
		
		$style = array(
		    'position' => '',
		    'align' => 'C',
		    'stretch' => false,
		    'fitwidth' => true,
		    'cellfitalign' => '',
		    'border' => false,
		    'hpadding' => 'auto',
		    'vpadding' => 'auto',
		    'fgcolor' => array(0,0,0),
		    'bgcolor' => false, //array(255,255,255),
		    'text' => FALSE,
		    'font' => 'helvetica',
		    'fontsize' => 8,
		    'stretchtext' => 4
		);
		$this->pdf->ln();
		//$this->pdf->write1DBarcode('11738998', 'C39E', '', '', '', 10, .4, $style, 'N');
		
		
		
		//$this->pdf->write2DBarcode(("recibo de pago ".$persona->cedula.' '.$totalAsigna.' '.$totalDeduce), 'QRCODE', '', '', 50, 50, $style, 'N');
		
		//$this->pdf->write1DBarcode($persona->cedula.' '.$totalAsigna.' '.$totalDeduce, 'C39E+', '', '', 120, 25, 0.4, $style, 'T');
		
		
		$this->pdf->ln(8*3);
		$this->pdf->setXY(120,230);
		$this->pdf->Cell(72,2,"DIRECCIÓN DE ADMINISTRACIÓN DE PERSONAL","T",0,"L");
		$this->pdf->setXY(20,255);
		$this->pdf->SetFont('courier', '', 7);
		$this->pdf->Cell(180,2,"Este documento debe estar sellado y firmado para que tenga validez.","",0,"C");
		
		
		$this->pdf->Output('recibo_pago_'.$persona->cedula.'.pdf','D');
	
	}
	
	function _header(){
		$this->benchmark->mark('carga_header_start');
		$data['title']="Atenci&oacute;n Integral al Trabajador";
		$this->load->view('main-view',$data);
		$deta['permisos']=$this->session->userdata('permisos');
		$this->load->view('comun/menu2',$deta);
		$this->benchmark->mark('carga_header_end');
	}
}