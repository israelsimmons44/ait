<?php
class Escolaridad extends Controller{
	function __construct(){
		
		parent::Controller();
		
		if (!$this->session->userdata('nombre')){
			redirect(base_url()."index.php/comun/login");
		}
		
		$this->load->model('escolaridad_model','rl');
		$this->load->library('parser');
		$this->load->library('pdf');
		$this->load->helper('funciones');
        }
           

////////////////////////////////////////////// FUNCIÓN QUE CARGA EL BANER Y MENÚ /////////////////////////	
	
	function _header(){
		$data['title']="Atenci&oacute;n Integral al Trabajador";
		$this->load->view('main-view',$data);
		$deta['permisos']=$this->session->userdata('permisos');
		$this->load->view('comun/menu2',$deta);		
	}
	
////////////////////////////////////////////// FUNCIÓN QUE CARGA FORMULARIO DE CONSULTA DE PERSONAL POR CÉDULA /////////////////////////	
	
	function personalBuscar(){                 
                $this->_header();
		$data=array();
		$this->load->library('formulario');
                $this->formulario->setAction(base_url()."index.php/escolaridad/consultaHijosS");
		$this->formulario->addInput('cedula','C&eacute;dula Trabajador','','required:true');
		$this->formulario->addButton('btnEnviar','Consultar','enviar');
		$data['formulario']=$this->formulario->outputHTML();
		$this->parser->parse('escolaridad/hijos-view',$data);
	}	
                          
//////////////////////// FUNCIÓN PARA MOSTRAR DATOS DE CONSULTA DE HIJOS (OPCIÓN PARA ANALISTAS DE AIT)/////////////////////////	
	
	function consultaHijosS(){
		$data['cedula']="";
		$data['nombres']="";
		$data['apellidos']="";
		$data['dependencia']="";
		$data['cargo']="";
		$data['direccion_residencia']="";
		$data['telefono_celular']="";
		$data['telefono_oficina']="";
		$data['telefono_residencia']="";
		$data['email']="";
		$data['hijostrabajador']="";
		$cedula=$this->input->post('cedula');
		$resPersonal=$this->rl->getPersonal($cedula);
		$arrPersonal=$resPersonal['personal'];
                if ($arrPersonal){
			$data['cedula']=$arrPersonal->cedula;
			$data['nombres']=utf8_encode($arrPersonal->primer_nombre." ".$arrPersonal->segundo_nombre);
			$data['apellidos']=utf8_encode($arrPersonal->primer_apellido." ".$arrPersonal->segundo_apellido);
			$data['dependencia']=utf8_encode($arrPersonal->dependencia);
			$data['cargo']=utf8_encode($arrPersonal->cargo);
			$data['direccion_residencia']=utf8_encode($arrPersonal->direccion_residencia);
			$data['telefono_celular']=$arrPersonal->telefono_celular;
			$data['telefono_oficina']=$arrPersonal->telefono_oficina;
			$data['telefono_residencia']=$arrPersonal->telefono_residencia;
			$data['email']=utf8_encode($arrPersonal->email);
			$data['hijostrabajador']=$resPersonal['hijostrabajador'];
		}else{
			$data['hijostrabajador']=array();
			$data['nombres']="LA C&Eacute;DULA QUE INGRES&Oacute; NO ESTA REGISTRADA";
		}	
 		
		$this->parser->parse('escolaridad/detallehijos-view',$data);
	}	
	
////////////////////////////////////////////// FUNCIÓN PARA MOSTRAR DATOS DE CONSULTA DE HIJOS /////////////////////////	
	
	function consultaHijos(){
		$this->_header();
		$data['cedula']="";
		$data['nombres']="";
		$data['apellidos']="";
		$data['dependencia']="";
		$data['cargo']="";
		$data['direccion_residencia']="";
		$data['telefono_celular']="";
		$data['telefono_oficina']="";
		$data['telefono_residencia']="";
		$data['email']="";
		$data['hijostrabajador']="";
		$resPersonal=$this->rem->getPersonal($this->session->userdata('cedula'));
		$arrPersonal=$resPersonal['personal'];
		if ($arrPersonal){
			$data['cedula']=$arrPersonal->cedula;
			$data['nombres']=utf8_encode($arrPersonal->primer_nombre." ".$arrPersonal->segundo_nombre);
			$data['apellidos']=utf8_encode($arrPersonal->primer_apellido." ".$arrPersonal->segundo_apellido);
			$data['dependencia']=utf8_encode($arrPersonal->dependencia);
			$data['cargo']=utf8_encode($arrPersonal->cargo);
			$data['direccion_residencia']=utf8_encode($arrPersonal->direccion_residencia);
			$data['telefono_celular']=$arrPersonal->telefono_celular;
			$data['telefono_oficina']=$arrPersonal->telefono_oficina;
			$data['telefono_residencia']=$arrPersonal->telefono_residencia;
			$data['email']=utf8_encode($arrPersonal->email);
			$data['hijostrabajador']=$resPersonal['hijostrabajador'];
		}else{
			$data['hijostrabajador']=array();
			$data['nombres']="LA C&Eacute;DULA QUE INGRES&Oacute; NO ESTA REGISTRADA";
		}	
 
		
		$this->parser->parse('recreacion/detalleconsultahijos-view',$data);
		
	}

//////////////////////// FUNCIÓN PARA DIRECCIONAR A FORMULARIO DE CARGA DE DATOS ADICIONALES DE HIJOS /////////////////////////	
	
	function AgregaDatosHijos () {
		$this->load->helper('form');
		$this->_header();
		$idFamiliar=$this->uri->segment(3);
		$data = $this->rl->getHijo($idFamiliar);
		$this->jquery->output();
		$this->parser->parse('escolaridad/ingresoescolaridad-view',$data);
	}
	
////////////////////// FUNCIÓN PARA INGRESAR DATOS ADICIONALES EN BD Y GENERAR PLANILLA DE INSCRIPCIÓN /////////////////////////	
	
	function ProcAgregaEscolaridad () {
		$data['id_familiar']=$this->input->post ('id_familiar');
                $idfamiliar = $data['id_familiar'];
                
		$data['nivel_educativo']=$this->input->post ('nivel_educativo');
                $data['grado_semestre']=$this->input->post ('grado_semestre');
		$data['anio_escolar']=$this->input->post ('anio_escolar');
		$data['promedio_notas']=$this->input->post ('promedio_notas');
                $data['instituto']=$this->input->post ('instituto');
                $data['carrera_especialidad']=$this->input->post ('carrera_especialidad');
                $data['inscripcion']=$this->input->post ('inscripcion');
                
                if ($data['inscripcion']=='X'){
                    $data['inscripcion']=TRUE;
                }else{
                    $data['inscripcion']=FALSE;
                }
                
                
                $data['calificacion']=$this->input->post ('calificacion');
                if ($data['calificacion']=='X'){
                    $data['calificacion']=TRUE;
                }else{
                    $data['calificacion']=FALSE;
                }
                
                $data['carnet']=$this->input->post ('carnet');
                if ($data['carnet']=='X'){
                    $data['carnet']=TRUE;
                }else{
                    $data['carnet']=FALSE;
                }
                
                $data['cifuncionario']=$this->input->post ('cifuncionario');
                if ($data['cifuncionario']=='X'){
                    $data['cifuncionario']=TRUE;
                }else{
                    $data['cifuncionario']=FALSE;
                }
                
                $data['cibeneficiario']=$this->input->post ('cibeneficiario');
                if ($data['cibeneficiario']=='X'){
                    $data['cibeneficiario']=TRUE;
                }else{
                    $data['cibeneficiario']=FALSE;
                }
                
                $data['partidanacimiento']=$this->input->post ('partidanacimiento');
                if ($data['partidanacimiento']=='X'){
                    $data['partidanacimiento']=TRUE;
                }else{
                    $data['partidanacimiento']=FALSE;
                }
                
                $data['informe']=$this->input->post ('informe');
                if ($data['informe']=='X'){
                    $data['informe']=TRUE;
                }else{
                    $data['informe']=FALSE;
                }
                
                $data['contrato']=$this->input->post ('concalificacion');
                if ($data['contrato']=='X'){
                    $data['contrato']=TRUE;
                }else{
                    $data['contrato']=FALSE;
                }
                
                
                $data['tipo_utiles']=$this->input->post ('tipo_utiles');
                $data['tipo_beca']=$this->input->post ('tipo_beca');
                
                $this->_header();
                $exFamiliar=$this->rl->getValidarHijo($data['id_familiar']);
                
                if ($exFamiliar==NULL){
                    if ($data['promedio_notas']==13){
                        $this->load->view('escolaridad/errornotas-view');
                    }else{
                        $this->rl->guardaDatosEscolaridad($data);
		        $this->load->view('escolaridad/datosguardados-view',$data);
                    }
                }else{
                    $this->load->view('escolaridad/errordatos-view');
                }

	}

///////////////////// FUNCIÓN PARA GENERAR PLANILLA DE INSCRIPCIÓN, AUTORIZACIÓN Y PERMISO DE VIAJE /////////////////////////	
	
	function GenerarPlanilla () {
                $idFamiliar=$this->uri->segment(3);
########################################## VARIABLES GENERALES ######################################################        
                $fecha = date('d/m/Y');
                $anio=date("Y");
                $dia = date('d');
                $numMes = date('m');             
                $mes = getMesES($numMes);               
                $anio = date('Y');	    
           
######################################### VARIABLES OBTENIDAS DEL MODELO getPadreHijo ###################################

                $padreHijo = $this->rl->getPadreHijo($idFamiliar);
                $nombrePadre = utf8_encode($padreHijo->nombre);
                $cedulaPadre = $padreHijo->cedula;
                $dependencia = utf8_encode($padreHijo->dependencia);
                $cargo = utf8_encode($padreHijo->cargo);
                $sexo = getSexo($padreHijo->sexo);
                $nombreHijo = utf8_encode($padreHijo->nombre_fam);
                $cedulaHijo = $padreHijo->cedula_familiar;
                $edad = $padreHijo->edad;
                $fechaNacimientoHijo = pgDate($padreHijo->fecha_nacimiento); 
                $direccionPadre = utf8_encode($padreHijo->direccion_residencia);
                $telefonoPadre = $padreHijo->telefono_residencia;
                $telefonoOficina = $padreHijo->telefono_oficina;
                $celularPadre = $padreHijo->telefono_celular;
                $tipoPersonal = $padreHijo->id_tipo_personal;
                $email = utf8_encode($padreHijo->email);
                                     
                switch ($tipoPersonal) {
                    case $tipoPersonal==1:
                        $tipoPersonal='Obrero';
                        break;
                    case $tipoPersonal==13:
                        $tipoPersonal='Administrativo';
                        break;
                    case $tipoPersonal==14:
                        $tipoPersonal='Contratado';
                        break;
                    case $tipoPersonal==20:
                        $tipoPersonal='Diplomatico';
                        break;
                     case $tipoPersonal==42:
                        $tipoPersonal='Jubilado';
                        break;
                     case $tipoPersonal==43:
                        $tipoPersonal='Pensionado';
                        break;
                     case $tipoPersonal==51:
                        $tipoPersonal='Pedro Gual';
                        break;
                     case $tipoPersonal==53:
                        $tipoPersonal='Comisión de Servicio';
                        break;
                     case $tipoPersonal==83:
                        $tipoPersonal='Consejo Nacional de Fronteras';
                        break;
                    case $tipoPersonal==100:
                        $tipoPersonal='Alto Funcionario';
                        break;
                    case $tipoPersonal==101:
                        $tipoPersonal='Alto Nivel';
                        break;
                }
                
###################################### VARIABLES OBTENIDAS DEL MODELO getBecaUtilHijo #############################

                $DatosHijo= $this->rl->getBecaUtilHijo($idFamiliar);
                $nivel_educativo = $DatosHijo->nivel_educativo;
                switch ($nivel_educativo) {
                    case $nivel_educativo=='P':
                        $nivel_educativo='Preescolar';
                        break;
                 case $nivel_educativo=='B':
                        $nivel_educativo='Básica';
                        break;
                 case $nivel_educativo=='D':
                        $nivel_educativo='Diversificado';
                        break;
                 case $nivel_educativo=='U':
                        $nivel_educativo='Universitaria';
                        break;
                 case $nivel_educativo=='E':
                        $nivel_educativo='Especial';
                        break;
                }
                      
                $grado_semestre = $DatosHijo->grado_semestre;
                $anio_escolar = $DatosHijo->anio_escolar;
                $promedio_notas = $DatosHijo->promedio_notas;
                $instituto = $DatosHijo->instituto;
                $carrera_especialidad = $DatosHijo->carrera_especialidad;
                
                
                $inscripcion = $DatosHijo->inscripcion;
                if ($inscripcion==1){
                    $d1='SI';
                }  else {
                    $d1='NO';  
                }  
                
                $calificacion = $DatosHijo->calificacion;
                if ($calificacion==1){
                    $d2='SI';
                }  else {
                    $d2='NO';  
                }  
                
                $carnet = $DatosHijo->carnet;
                if ($carnet==1){
                    $d3='SI';
                }  else {
                    $d3='NO';  
                }  
                
                $cifuncionario = $DatosHijo->cifuncionario;
                if ($cifuncionario==1){
                    $d4='SI';
                }  else {
                    $d4='NO';  
                }  
                
                $cibeneficiario = $DatosHijo->cibeneficiario;
                if ($cibeneficiario==1){
                    $d5='SI';
                }  else {
                    $d5='NO';  
                }  
                
                $partidanacimiento = $DatosHijo->partidanacimiento;
                if ($partidanacimiento==1){
                    $d6='SI';
                }  else {
                    $d6='NO';  
                }  
                
                $informe = $DatosHijo->informe;
                if ($informe==1){
                    $d7='SI';
                }  else {
                    $d7='NO';  
                }  
                
                $contrato = $DatosHijo->contrato;
                if ($contrato==1){
                    $d8='SI';
                }  else {
                    $d8='NO';  
                }  
                
                $tipo_utiles = $DatosHijo->tipo_utiles;
                switch ($tipo_utiles) {
                    case $tipo_utiles=='E':
                        $tipo_utiles='Escolar';
                        break;
                 case $tipo_utiles=='U':
                        $tipo_utiles='Universitaria';
                        break;
                 case $tipo_utiles=='N':
                       $tipo_utiles='Sin Selección';
                        break;
                }
                    
                $tipo_beca = $DatosHijo->tipo_beca;
                switch ($tipo_beca) {
                 case $tipo_beca=='PB':
                      $tipo_beca='Preescolar/Básica';
                      break;
                 case $tipo_beca=='SD':
                      $tipo_beca='Secundaria/Diversificada';
                      break;
                 case $tipo_beca=='UN':
                      $tipo_beca='Universitaria';
                      break;
                 case $tipo_beca=='ES':
                      $tipo_beca='Especial';
                      break;
                 case $tipo_beca=='NS':
                      $tipo_beca='Sin Selección';
                        break;
                }
                //<div align=\"justify\">
    ########################################### GENERAR PLANILLA DE INSCRIPCION #################################################  	    
 
                $this->pdf->SetMargins(20, 25 , 30);     
                $htmlHijo = "
                    <p align=\"center\">
                       <b>INCRIPCIÓN PERÍODO ESCOLAR $anio </b>
                    </p>
                    <p></p>                  
                        <b>DATOS DEL TRABAJADOR</b>
                    <hr />
                    <table width=\"1700\" border=\"0\">
			<tr><td width=\"600\"><b>Cédula:</b></td><td width=\"600\">$cedulaPadre</td></tr>
			<tr><td width=\"600\"><b>Apellidos y Nombres:</b></td><td width=\"1200\">$nombrePadre</td></tr>
			<tr><td width=\"600\"><b>Dependencia:</b></td><td width=\"1200\">$dependencia</td></tr>
			<tr><td width=\"600\"><b>Cargo:</b></td><td width=\"1200\">$cargo</td></tr>
			<tr><td width=\"600\"><b>Dirección Hab.:</b></td><td width=\"1200\">$direccionPadre</td></tr>
			<tr><td width=\"600\"><b>Teléfono Hab.:</b></td><td width=\"600\">$telefonoPadre</td></tr>
			<tr><td width=\"600\"><b>Celular:</b></td><td width=\"600\">$celularPadre</td></tr>
                        <tr><td width=\"600\"><b>Teléfono Ofi.</b></td><td width=\"600\">$telefonoOficina</td></tr>
			<tr><td width=\"600\"><b>Correo Electrónico:</b></td><td width=\"800\">$email</td></tr>
                        <tr><td width=\"600\"><b>Tipo de Personal:</b></td><td width=\"800\">$tipoPersonal</td></tr>
                    </table>
                                     
                    <p></p>  
                       <b>DATOS DEL NIÑO(A)</b>
                    <hr/>
                    <table width=\"2000\" border=\"0\">
			<tr><td width=\"600\"><b>Apellidos y Nombres:</b></td><td width=\"1000\">$nombreHijo</td></tr>
			<tr><td width=\"600\"><b>Cédula:</b></td><td width=\"600\">$cedulaHijo</td></tr>
			<tr><td width=\"600\"><b>Sexo:</b></td><td width=\"600\">$sexo</td></tr>
			<tr><td width=\"600\"><b>Fecha Nacimiento:</b></td><td width=\"600\">$fechaNacimientoHijo</td></tr>
                        <tr><td width=\"600\"><b>Edad:</b></td><td width=\"600\">$edad</td></tr>
                        <tr><td width=\"600\"><b>Institución:</b></td><td width=\"700\">$instituto</td></tr>
                        <tr><td width=\"600\"><b>Carrera o Especialidad:</b></td><td width=\"700\">$carrera_especialidad</td></tr>
                        <tr><td width=\"600\"><b>Nivel Educativo:</b></td><td width=\"600\">$nivel_educativo</td></tr>
                        <tr><td width=\"600\"><b>Grado o Semestre:</b></td><td width=\"600\">$grado_semestre</td></tr>
                        <tr><td width=\"600\"><b>Año Escolar:</b></td><td width=\"600\">$anio_escolar</td></tr>
                        <tr><td width=\"600\"><b>Promedio de Nota:</b></td><td width=\"700\">$promedio_notas</td></tr>
                        <tr><td width=\"600\"><b>Tipo de Utiles:</b></td><td width=\"700\">$tipo_utiles</td></tr>
                        <tr><td width=\"600\"><b>Tipo de Beca:</b></td><td width=\"700\">$tipo_beca</td></tr>
		     </table>
                
                     <p></p>  
                        <b>DOCUMENTOS CONSIGNADOS</b>
                     <hr/>
                     <table width=\"3000\" border=\"0\">
			 <tr><td width=\"1000\"><b>Constancia de Inscripción:</b></td><td width=\"1000\">$d1</td></tr>
                         <tr><td width=\"1000\"><b>Constancia de Calificación:</b></td><td width=\"1000\">$d2</td></tr>
                         <tr><td width=\"1000\"><b>Fotocopia de Carnet Funcionario:</b></td><td width=\"1000\">$d3</td></tr>
                         <tr><td width=\"1000\"><b>Fotocopia Cédula del Funcionario:</b></td><td width=\"1000\">$d4</td></tr>
                         <tr><td width=\"1000\"><b>Fotocopia Cédula del Beneficiario:</b></td><td width=\"1000\">$d5</td></tr>
                         <tr><td width=\"1000\"><b>Partida de Nacimiento del Beneficiario:</b></td><td width=\"1000\">$d6</td></tr>
                         <tr><td width=\"1000\"><b>Informe Médico Actualizado (niño especial):</b></td><td width=\"1000\">$d7</td></tr>
                         <tr><td width=\"1000\"><b>Fotocopia del Contrato:</b></td><td width=\"1000\">$d8</td></tr>
                     </table>
               
                   <p>
                     <b>NOMBRE DEL REPRESENTANTE:</b> 
                   </p>$nombrePadre, FIRMA____________________FECHA:  $fecha	
                
                ";
              // echo "$htmlHijo";
               
                $this->pdf->SetFontSize(10);
                $this->pdf->AddPage('R', 'letter');
                $this->pdf->writeHTML($htmlHijo, true, false, true, false, '');               
        	$this->pdf->output('Escolaridad.pdf', 'D');
     }

}