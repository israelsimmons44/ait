<?php
class Formularios extends Controller{
	function __construct(){
		parent::Controller();
		$this->load->library('formulario');
                if (!$this->session->userdata('nombre')){
			redirect(base_url()."index.php/comun/login");
		}
		$this->load->library('parser');
		$this->load->model('solicitud_model',"sm");
		$this->load->helper('funciones');
                $this->load->library('form_validation');
                $this->load->library('pdf');
                
               
	}
	
	function index(){
		//$this->load->view('formularios/index-view');
		
	}
		
        function _header(){
		$this->benchmark->mark('carga_header_start');
		$data['title']="Atenci&oacute;n Integral al Trabajador";
		$this->load->view('main-view',$data);
		$deta['permisos']=$this->session->userdata('permisos');
		$this->load->view('comun/menu2',$deta);
		$this->benchmark->mark('carga_header_end');
	}
	
        

        /*
	 * Formulario de Bono Guarderia
	 */
        
	function SOL011(){
            //$this->_header();
            $data['recaudosTitulo']="";
            $data['recaudos']=array();
            $data['titulo']="Bono Guarder&iacute;a";
            $this->formulario->addScript("$('#wrapper').css('display','none');");
            $this->formulario->setNombreForm('form1');
            $this->formulario->setAction(base_url()."index.php/formularios/procesaSOL011");
            $this->formulario->addInput('cedula','C&eacute;dula Trabajador','','required:true');
            $this->formulario->addButton('btnEnviar','Consultar','enviar');
            $data['formulario']=$this->formulario->outputHTML();
            $data['mensaje']='Bono Guardería (Solicitud)';
            $this->parser->parse('formularios/consultahijos-view',$data);
        }
       ///////////////////////////////////////////////////////////////////////////////////////// 
	function procesaSOL011(){
		$data['cedula']="";
		$data['nombres']="";
		$data['apellidos']="";
		$data['dependencia']="";
		$data['cargo']="";
		$data['direccion_residencia']="";
		$data['telefono_celular']="";
		$data['telefono_oficina']="";
		$data['telefono_residencia']="";
		$data['email']="";
		$data['hijostrabajador']="";
		$cedula=$this->input->post('cedula');
                $resPersonal=$this->sm->getPersonal($cedula);
		$arrPersonal=$resPersonal['personal'];
                if ($arrPersonal){
			$data['cedula']=$arrPersonal->cedula;
			$data['nombres']=utf8_encode($arrPersonal->primer_nombre." ".$arrPersonal->segundo_nombre);
			$data['apellidos']=utf8_encode($arrPersonal->primer_apellido." ".$arrPersonal->segundo_apellido);
			$data['dependencia']=utf8_encode($arrPersonal->dependencia);
			$data['cargo']=utf8_encode($arrPersonal->cargo);
			$data['direccion_residencia']=utf8_encode($arrPersonal->direccion_residencia);
			$data['telefono_celular']=$arrPersonal->telefono_celular;
			$data['telefono_oficina']=$arrPersonal->telefono_oficina;
			$data['telefono_residencia']=$arrPersonal->telefono_residencia;
			$data['email']=utf8_encode($arrPersonal->email);
			$data['hijostrabajador']=$resPersonal['hijostrabajador'];
		}else{
			$data['hijostrabajador']=array();
			$data['nombres']="LA C&Eacute;DULA QUE INGRES&Oacute; NO ESTA REGISTRADA";
		}
                $data['mensaje1']='Inscripción Guardería';
                $data['link1']=base_url().'index.php/formularios/AgregaDatosHijosGuarderia';
                $data['mensaje2']='Planilla Guardería';
                $data['link2']=base_url().'index.php/formularios/GenerarPlanillaGuarderia';
                $this->parser->parse('formularios/consultatrabajador-view',$data);
                
	}
	//////////////////////////////////////////Agregar Datos /////////////////////////////       
	function AgregaDatosHijosGuarderia () {
		$this->load->helper('form');
		$this->_header();
		$idFamiliar=$this->uri->segment(3);
		$data = $this->sm->getHijo($idFamiliar);
		$this->jquery->output();
		$this->parser->parse('formularios/ingresoguarderia-view',$data);
	}
	////////////////////// FUNCIÓN PARA INGRESAR DATOS ADICIONALES EN BD  /////////////////////////	
	function ProcAgregaGuarderia () {
                $data['id_familiar']=$this->input->post ('id_familiar');
                $idfamiliar = $data['id_familiar'];
           	$data['anio_escolar']=$this->input->post ('anio_escolar');
                $data['instituto']=$this->input->post ('instituto');
                $data['registro_mpppe']=$this->input->post ('registro_mpppe');
                $data['constancia_inscripcion'] = ($this->input->post ('constancia_inscripcion') == 'X')? TRUE:FALSE;
                $data['factura_mensual']=($this->input->post ('factura_mensual') == 'X')? TRUE:FALSE;
                $data['copia_carnet']=($this->input->post ('copia_carnet') == 'X')? TRUE:FALSE;
                $data['ci_funcionario']=($this->input->post ('ci_funcionario') == 'X')? TRUE:FALSE;
                $data['contrato']=($this->input->post ('contrato') == 'X')? TRUE:FALSE;
                $data['partida_nacimiento']=($this->input->post ('partida_nacimiento') == 'X')? TRUE:FALSE;
                $data['registro_mercantil']=($this->input->post ('registro_mercantil') == 'X')? TRUE:FALSE;
                $data['inscripcion_plantel']=($this->input->post ('inscripcion_plantel') == 'X')? TRUE:FALSE;
             
                $this->form_validation->set_rules('anio_escolar', 'anio_escolar', 'required');
                $this->form_validation->set_rules('constancia_inscripcion', 'constancia', 'required');
                $this->form_validation->set_rules('factura_mensual', 'factura_mensual', 'required');
                $this->form_validation->set_rules('copia_carnet', 'copia_carnet', 'required');
                $this->form_validation->set_rules('ci_funcionario', 'ci_funcionario', 'required');
                $this->form_validation->set_rules('partida_nacimiento', 'partida_nacimiento', 'required');
                $this->form_validation->set_rules('registro_mercantil', 'registro_mercantil', 'required');
                $this->form_validation->set_rules('inscripcion_plantel', 'inscripcion_plantel', 'required');
                
                if ($this->form_validation->run() == FALSE){
                    $this->_header();
                    $data['mensaje']='ERROR!!! Recaudos Incompletos';
                    $this->load->view('formularios/mensaje-view',$data);
                }else{
                   $exFamiliar=$this->sm->getValidarHijo($data['id_familiar'],$data['anio_escolar'] );
                   if ($exFamiliar==NULL){
                        $this->sm->guardaDatosGuarderias($data);
                        $this->_header();
                        $data['mensaje']='Información Registrada con Exito!!!, debe IMPRIMIR la Planilla';
                        $this->load->view('formularios/mensaje-view',$data);
		   }else{
                        $this->_header();
                        $data['mensaje']='ERROR!!! Niño ya Registrado';
                        $this->load->view('formularios/mensaje-view',$data);
                    }
                 }
	}
       ///////////////////// FUNCIÓN PARA GENERAR PLANILLA DE INSCRIPCIÓN /////////////////////////	
        function GenerarPlanillaGuarderia () {
                $idFamiliar=$this->uri->segment(3);
                $anio=date("Y");
                $anioEscolar= $anio+1;
                $exFamiliar=$this->sm->getValidarHijo($idFamiliar,$anioEscolar );
                
                if ($exFamiliar==NULL){
                        $this->_header();
                        $data['mensaje']='ERROR!!! No se ha realizado la Inscripción del Niño';
                        $this->load->view('formularios/mensaje-view',$data);
	        }else{
                  ########################################## VARIABLES GENERALES ######################################################        
                    $fecha = date('d/m/Y');
                    $dia = date('d');
                    $numMes = date('m');             
                    $mes = getMesES($numMes);               
                    $anio = date('Y');
                    ######################################### VARIABLES OBTENIDAS DEL MODELO getPadreHijo ###################################

                    $padreHijo = $this->sm->getPadreHijo($idFamiliar);
                    $nombrePadre = utf8_encode($padreHijo->nombre);
                    $cedulaPadre = $padreHijo->cedula;
                    $dependencia = utf8_encode($padreHijo->dependencia);
                    $cargo = utf8_encode($padreHijo->cargo);
                    $sexo = getSexo($padreHijo->sexo);
                    $nombreHijo = utf8_encode($padreHijo->nombre_fam);
                    $cedulaHijo = $padreHijo->cedula_familiar;
                    $edad = $padreHijo->edad;
                    $fechaNacimientoHijo = pgDate($padreHijo->fecha_nacimiento); 
                    $direccionPadre = utf8_encode($padreHijo->direccion_residencia);
                    $telefonoPadre = $padreHijo->telefono_residencia;
                    $telefonoOficina = $padreHijo->telefono_oficina;
                    $celularPadre = $padreHijo->telefono_celular;
                    $tipoPersonal = $padreHijo->id_tipo_personal;
                    $email = utf8_encode($padreHijo->email);
                    $arr_tipo_personal = array ('1'=>'Obrero','13'=>'Administrativo','14'=>'Contratado','20'=>'Diplomatico','42'=>'Jubilado','43'=>'Pensionado','51'=>'Pedro Gual','53'=>'Comisión de Servicio','83'=>'Consejo Nacional de Frontera','100'=>'Alto Funcionario', '101'=>'Alto Nivel');
                ###################################### VARIABLES OBTENIDAS DEL MODELO getGuarderia #############################
                    $DatosHijo= $this->sm->getGuarderia($idFamiliar,$anioEscolar);
                    $instituto = $DatosHijo->instituto;
                    $registro_mpppe = $DatosHijo->registro_mpppe;
                    
                    $constancia_inscripcion = $DatosHijo->constancia_inscripcion;
                    if ($constancia_inscripcion==1){
                        $d1='SI';
                    }  else {
                        $d1='NO';  
                    }  
                    
                    $factura_mensual = $DatosHijo->factura_mensual;
                    if ($factura_mensual==1){
                        $d2='SI';
                    }  else {
                        $d2='NO';  
                    }  
                    
                    $copia_carnet = $DatosHijo->copia_carnet;
                    if ($copia_carnet==1){
                        $d3='SI';
                    }  else {
                        $d3='NO';  
                    }  
                    
                    $ci_funcionario = $DatosHijo->ci_funcionario;
                    if ($ci_funcionario==1){
                        $d4='SI';
                    }  else {
                        $d4='NO';  
                    }  
                    
                    $contrato = $DatosHijo->contrato;
                    if ($contrato==1){
                        $d5='SI';
                    }  else {
                        $d5='NO';  
                    }  
                    
                    $partida_nacimiento = $DatosHijo->partida_nacimiento;
                    if ($partida_nacimiento==1){
                        $d6='SI';
                    }  else {
                        $d6='NO';  
                    } 
                    
                    $registro_mercantil = $DatosHijo->registro_mercantil;
                    if ($registro_mercantil==1){
                        $d7='SI';
                    }  else {
                        $d7='NO';  
                    } 
                    
                    $inscripcion_plantel = $DatosHijo->inscripcion_plantel;
                    if ($inscripcion_plantel==1){
                        $d8='SI';
                    }  else {
                        $d8='NO';  
                    } 
                ########################################### GENERAR PLANILLA DE INSCRIPCION #################################################  	    
 
                        $this->pdf->SetMargins(20, 25 , 30);     
                        $htmlHijo = "
                            <p align=\"center\">
                            <b>SOLICITUD BONO GUARDERIA - AÑO: $anio </b>
                            </p>
                            <p></p>                  
                                <b>DATOS DEL TRABAJADOR</b>
                            <hr />
                            <table width=\"1700\" border=\"0\">
                                <tr><td width=\"600\"><b>Cédula:</b></td><td width=\"600\">$cedulaPadre</td></tr>
                                <tr><td width=\"600\"><b>Apellidos y Nombres:</b></td><td width=\"1200\">$nombrePadre</td></tr>
                                <tr><td width=\"600\"><b>Dependencia:</b></td><td width=\"1200\">$dependencia</td></tr>
                                <tr><td width=\"600\"><b>Cargo:</b></td><td width=\"1200\">$cargo</td></tr>
                                <tr><td width=\"600\"><b>Dirección Hab.:</b></td><td width=\"1200\">$direccionPadre</td></tr>
                                <tr><td width=\"600\"><b>Teléfono Hab.:</b></td><td width=\"600\">$telefonoPadre</td></tr>
                                <tr><td width=\"600\"><b>Celular:</b></td><td width=\"600\">$celularPadre</td></tr>
                                <tr><td width=\"600\"><b>Teléfono Ofi.</b></td><td width=\"600\">$telefonoOficina</td></tr>
                                <tr><td width=\"600\"><b>Correo Electrónico:</b></td><td width=\"800\">$email</td></tr>
                                <tr><td width=\"600\"><b>Tipo de Personal:</b></td><td width=\"800\">$arr_tipo_personal[$tipoPersonal]</td></tr>
                            </table>
                            <p></p>  
                            <b>DATOS DEL NIÑO(A)</b>
                            <hr/>
                            <table width=\"2000\" border=\"0\">
                                <tr><td width=\"600\"><b>Apellidos y Nombres:</b></td><td width=\"1000\">$nombreHijo</td></tr>
                                <tr><td width=\"600\"><b>Cédula:</b></td><td width=\"600\">$cedulaHijo</td></tr>
                                <tr><td width=\"600\"><b>Sexo:</b></td><td width=\"600\">$sexo</td></tr>
                                <tr><td width=\"600\"><b>Fecha Nacimiento:</b></td><td width=\"600\">$fechaNacimientoHijo</td></tr>
                                <tr><td width=\"600\"><b>Edad:</b></td><td width=\"600\">$edad</td></tr>
                                <tr><td width=\"600\"><b>Institución o Colegio:</b></td><td width=\"700\">$instituto</td></tr>
                                <tr><td width=\"600\"><b>Nro. de Registro en MPPPE:</b></td><td width=\"700\">$registro_mpppe</td></tr>
                            </table>

                            <p></p>  
                            <b>DOCUMENTOS CONSIGNADOS</b>
                            <hr/>
                            <table width=\"3000\" border=\"0\">
                                <tr><td width=\"1000\"><b>Constancia de Inscripción:</b></td><td width=\"1000\">$d1</td></tr>
                                <tr><td width=\"1000\"><b>Factura de Mensualidad Original (con formato del SENIAT):</b></td><td width=\"1000\">$d2</td></tr>
                                <tr><td width=\"1000\"><b>Fotocopia de Carnet Funcionario:</b></td><td width=\"1000\">$d3</td></tr>
                                <tr><td width=\"1000\"><b>Fotocopia Cédula del Funcionario:</b></td><td width=\"1000\">$d4</td></tr>
                                <tr><td width=\"1000\"><b>Fotocopia del Contrato:</b></td><td width=\"1000\">$d5</td></tr>
                                <tr><td width=\"1000\"><b>Partida de Nacimiento del Beneficiario:</b></td><td width=\"1000\">$d6</td></tr>
                                <tr><td width=\"1000\"><b>Registro Mercantil del Plantel:</b></td><td width=\"1000\">$d7</td></tr>
                                <tr><td width=\"1000\"><b>Constancia de inscripción del plantel en el MPPPE:</b></td><td width=\"1000\">$d8</td></tr>
                            </table>
                            <p>
                            <b>OBSERVACIONES:__________________________________________________________________</b>
                            <b>___________________________________________________________________________________</b>
                            <b>___________________________________________________________________________________</b>
                            <p></p>                       
                            <b>NOMBRE DEL REPRESENTANTE:</b></p>$nombrePadre, FIRMA____________________FECHA:  $fecha	

                        ";
                   
                        $this->pdf->SetFontSize(10);
                        $this->pdf->AddPage('R', 'letter');
                        $this->pdf->writeHTML($htmlHijo, true, false, true, false, '');               
                        $this->pdf->output('Guarderia.pdf', 'D');
               }
     }
        
	/*
	 * Formulario de Autorización Banco de Venezuela Fideicomiso
	 */
	function AUT001(){
		$data['recaudosTitulo']="";
		$data['recaudos']=array();
		
		$data['titulo']="Autorización Banco de Venezuela Fideicomiso";
		$this->formulario->setNombreForm('form1');
		$this->formulario->setAction(base_url()."index.php/formularios/procesaAUT001");
		$this->formulario->addHidden('cedula',$this->session->userdata('cedula'));
		$this->formulario->addHidden('codtramite','AUT001');
		$valores[]=array('valor'=>'1','descripcion'=>'CORRIENTE');
		$valores[]=array('valor'=>'2','descripcion'=>'AHORROS');
		$this->formulario->addSelect('tipocuenta','Tipo de Cuenta',$valores,'','required:true');
		$this->formulario->addInput('cuenta_fideicomiso','Nro. Cuenta','','required:true');

		$this->formulario->addButton('btn','Grabar Datos','enviar');
		$data['formulario']=$this->formulario->outputHTML();
		
		$this->parser->parse('formularios/formulario-gen-view',$data);
	}
	
	function procesaAUT001(){
		$porAnalista=FALSE;
		$this->load->helper('funciones');
		if ($this->session->userdata('cedulatramite')!=""){
			$_POST['cedula']=$this->session->userdata('cedulatramite');
			$porAnalista=TRUE;
		}
		$sol=$this->sm->setSolicitud($_POST);

		$this->load->library('smtpmail');
		
		$mail=new SMTPMAIL;
		$mail->set_host('zmail.mppre.gob.ve');
		$correo=$this->session->userdata('usuario')."@mppre.gob.ve";
		
		if ($porAnalista==FALSE){
			$msg="Hemos recibido su Autorización para vincular su fideicomiso a su cuenta del Banco de Venezuela, la misma está registrada bajo el número: ".$sol['id_solicitud']."<br> ".$this->session->userdata('usuario')." Pronto nos comunicaremos con usted por esta misma via para avisarle acerca de los avances.";
			$res=$mail->sendmail($correo, "Sistema de Solicitudes AIT",utf8_decode($msg),"atencion.altrabajador@mppre.gob.ve");
		}
	}
	
	
	/*
	 * Formulario de Adelanto de prestaciones
	 */
	function SOL001(){
		//$this->_header();
		$data['titulo']="Computo Vacacional";
		$data['formulario']="";
		
		$data['recaudosTitulo']="";
		$data['recaudos']=array();
		
		$this->formulario->setNombreForm('form1');
		$this->formulario->setAction(base_url()."index.php/formularios/procesaSOL001");
		$this->formulario->addHidden('cedula',$this->session->userdata('cedula'));
		$this->formulario->addHidden('codtramite','SOL001');
		$this->formulario->addButton('btn','Grabar Datos','enviar');
		$data['formulario']=$this->formulario->outputHTML();
		
		
		$this->parser->parse('formularios/formulario-gen-view',$data);
		
	}
	
	function procesaSOL001(){
		$porAnalista=FALSE;
		$this->load->helper('funciones');
		if ($this->session->userdata('cedulatramite')!=""){
			$_POST['cedula']=$this->session->userdata('cedulatramite');
			$porAnalista=TRUE;
		}
		$sol=$this->sm->setSolicitud($_POST);

		$this->load->library('smtpmail');
		
		$mail=new SMTPMAIL;
		$mail->set_host('zmail.mppre.gob.ve');
		$correo=$this->session->userdata('usuario')."@mppre.gob.ve";
		
		if ($porAnalista==FALSE){
			$msg="Hemos recibido su solicitud de Cómputo Vacacional, la misma está registrada bajo el número: ".$sol['id_solicitud']."<br> ".$this->session->userdata('usuario')." Pronto nos comunicaremos con usted por esta misma via para avisarle acerca de los avances.";
			$res=$mail->sendmail($correo, "Sistema de Solicitudes AIT",utf8_decode($msg),"atencion.altrabajador@mppre.gob.ve");
		}
	}
	
	/*
	 * Formulario de Adelanto de prestaciones
	 */
	function SOL002(){
		//$this->_header();
		$data['recaudosTitulo']="";
		$data['recaudos']=array();
		
		$data['titulo']="Adelanto de Prestaciones";
		$this->formulario->setNombreForm('form1');
		$this->formulario->setAction(base_url()."index.php/formularios/procesaSOL002");
		$this->formulario->addHidden('cedula',$this->session->userdata('cedula'));
		$this->formulario->addHidden('codtramite','SOL002');
		$this->formulario->addCheckBox('casado','Casado', TRUE);
		$reglaDependencia='required:{depends:function(element){return ($(\'#casado:checked\').val()==\'casado\')}}';
		$this->formulario->addInput('nombres_conyuge','Nombre del Conyuge','',$reglaDependencia, array('size'=>20));
		$this->formulario->addInput('apellidos_conyuge','Apellido del Conyuge','',$reglaDependencia, array('size'=>20));
		$this->formulario->addInput('cedula_conyuge','Cédula del Conyuge','',$reglaDependencia);
		$this->formulario->addInput('telefono_conyuge','Teléfono del Conyuge','',$reglaDependencia);
		$this->formulario->addInput('montoprestaciones','Monto a Solicitar','','required:true');
		$this->formulario->addCheckBox('construccionvivienda','Construcción de vivienda');
		$this->formulario->addCheckBox('adquisicionvivienda','Adquisición de vivienda');
		$this->formulario->addCheckBox('presupuesto','Mejora o Reparación de Vivienda');
		$this->formulario->addCheckBox('liberacionhipoteca','Liberación de hipoteca');
		$this->formulario->addCheckBox('informemedico','Pensión Escolar');
		$this->formulario->addCheckBox('permisoconstruccion','Gastos Médicos y/u Hospitalarios');
		$this->formulario->addButton('btn','Grabar Datos','enviar');
		$data['formulario']=$this->formulario->outputHTML();
		
		$this->parser->parse('formularios/formulario-gen-view',$data);
		
	}
	
	function procesaSOL002(){
		$porAnalista=FALSE;
		$this->load->helper('funciones');
		if ($this->session->userdata('cedulatramite')!=""){
			$_POST['cedula']=$this->session->userdata('cedulatramite');
			$porAnalista=TRUE;
		}
		$sol=$this->sm->setSolicitud($_POST);

		$this->load->library('smtpmail');
		
		$mail=new SMTPMAIL;
		$mail->set_host('zmail.mppre.gob.ve');
		$correo=$this->session->userdata('usuario')."@mppre.gob.ve";
		
		if ($porAnalista==FALSE){
			$msg="Hemos recibido su solicitud de Adelanto de Prestaciones, la misma está registrada bajo el número: ".$sol['id_solicitud']."<br> ".$this->session->userdata('usuario')." Pronto nos comunicaremos con usted por esta misma via para avisarle acerca de los avances.";
			$res=$mail->sendmail($correo, "Sistema de Solicitudes AIT",utf8_decode($msg),"atencion.altrabajador@mppre.gob.ve");
		}
		
	}
	
	/*
	 * Formulario de Prima por profesionalizacion
	 */
	function PRI002(){
		//$this->_header();
		$data['titulo']="Prima por Profesionalización";
		
		$this->formulario->setNombreForm('form1');
		$this->formulario->setAction(base_url()."index.php/formularios/procesaPRI002");
		$this->formulario->addInput('telefono','Teléfono Oficina','','', array('size'=>10));
		$this->formulario->addInput('extension','Extensión','','', array('size'=>5));
		$this->formulario->addInput('telefonohab','Teléfono Habitación','','', array('size'=>10));
		$this->formulario->addInput('telefonocel','Teléfono Celular','','', array('size'=>10));
		$arr="select id_nivel_academico as valor, descripcion from nivelacademico";		
		$this->formulario->addSelect('nivelacademico','Nivel Académico',$arr,0,'required:true');
		$this->formulario->addHidden('cedula',$this->session->userdata('cedula'));
		$this->formulario->addHidden('codtramite','PRI002');
		$this->formulario->addButton('btn','Grabar Datos','enviar');
		$data['formulario']=$this->formulario->outputHTML();
		
		$data['recaudosTitulo']="Recaudos a Consignar";
		$data['recaudos']=array(
			array('descripcion'=>'Fotocopia de la C&eacute;dula de Identidad'),
			array('descripcion'=>'Fotocopia del t&iacute;tulo con vista del original')
		);
		
		$this->parser->parse('formularios/formulario-gen-view',$data);
		
	}	
	
	function procesaPRI002(){
		$porAnalista=FALSE;
		$this->load->helper('funciones');
		if ($this->session->userdata('cedulatramite')!=""){
			$_POST['cedula']=$this->session->userdata('cedulatramite');
			$porAnalista=TRUE;
		}
		$sol=$this->sm->setSolicitud($_POST);

		$this->load->library('smtpmail');
		
		$mail=new SMTPMAIL;
		$mail->set_host('zmail.mppre.gob.ve');
		$correo=$this->session->userdata('usuario')."@mppre.gob.ve";
		
		if ($porAnalista==FALSE){
			$msg="Hemos recibido su solicitud de Prima por Profesionalización, la misma está registrada bajo el número: ".$sol['id_solicitud']."<br> ".$this->session->userdata('usuario')." Pronto nos comunicaremos con usted por esta misma via para avisarle acerca de los avances.";
			$res=$mail->sendmail($correo, "Sistema de Solicitudes AIT",utf8_decode($msg),"atencion.altrabajador@mppre.gob.ve");
		}
	}
	
	/*
	 * Formulario de Ayuda por Matrimonio
	 */
	function SOL003(){
		//$this->_header();
		$data['titulo']="Bonificaci&oacute;n por Matrimonio";
		$data['recaudosTitulo']="";
		$data['recaudos']=array();
		
		$this->formulario->setNombreForm('form1');
		$this->formulario->setAction(base_url()."index.php/formularios/procesaSOL003");
		$this->formulario->addHidden('cedula',$this->session->userdata('cedula'));
		$this->formulario->addHidden('codtramite','SOL003');
		$this->formulario->addInput('cuentabancaria','Nro de Cuenta','','', array('size'=>20));
		$this->formulario->addInput('telefono','Teléfono Oficina','','', array('size'=>10));
		$this->formulario->addInput('extension','Extensión','','', array('size'=>5));
		$this->formulario->addInput('telefonohab','Teléfono Habitación','','', array('size'=>10));
		$this->formulario->addInput('telefonocel','Teléfono Celular','','', array('size'=>10));
		$this->formulario->addDatepicker('fechamatrimonio','Fecha de Matrimonio','required:true');
		$this->formulario->addInput('nombres_conyuge','Nombres del Conyuge','','required:true');
		$this->formulario->addInput('apellidos_conyuge','Apellidos del Conyuge','','required:true');
		$this->formulario->addInput('destinoruta','Destino (Ruta)');
		$this->formulario->addDatepicker('fechasalida','Fecha de Salida');
		$this->formulario->addDatepicker('fecharetorno','Fecha de Retorno');
		
		$data['btn']=$this->formulario->addButton('btn','Grabar Datos','enviar');		
		
		$data['formulario']=$this->formulario->outputHTML();
		
		$this->parser->parse('formularios/formulario-gen-view',$data);
		
	}
	
	function procesaSOL003(){
		$porAnalista=FALSE;
		$this->load->helper('funciones');
		if ($this->session->userdata('cedulatramite')!=""){
			$_POST['cedula']=$this->session->userdata('cedulatramite');
			$porAnalista=TRUE;
		}
		$sol=$this->sm->setSolicitud($_POST);

		$this->load->library('smtpmail');
		
		$mail=new SMTPMAIL;
		$mail->set_host('zmail.mppre.gob.ve');
		$correo=$this->session->userdata('usuario')."@mppre.gob.ve";
		
		if ($porAnalista==FALSE){
			$msg="Hemos recibido su solicitud de Ayuda por Matrimonio, la misma está registrada bajo el número: ".$sol['id_solicitud']."<br> ".$this->session->userdata('usuario')." Pronto nos comunicaremos con usted por esta misma via para avisarle acerca de los avances.";
			$res=$mail->sendmail($correo, "Sistema de Solicitudes AIT",utf8_decode($msg),"atencion.altrabajador@mppre.gob.ve");
		}
	}
	
	/*
	 * Formulario de Prima por Hijos
	 */
	function SOL004(){
		//$this->_header();
		$data['titulo']="Prima por Hijos";
		$data['recaudosTitulo']="Recaudos a Consignar";
		$data['recaudos']=array(
			array('descripcion'=>'Fotocopia de la C&eacute;dula de Identidad'),
			array('descripcion'=>'Partida de Nacimiento de el(los) niño(s)')
		);
		
		$this->formulario->setNombreForm('form1');
		$this->formulario->setAction(base_url()."index.php/formularios/procesaSOL004");
		$data['cedula']=$this->formulario->addHidden('cedula',$this->session->userdata('cedula'));
		$data['codtramite']=$this->formulario->addHidden('codtramite','SOL004');
		$data['telefono']=$this->formulario->addInput('telefono','Teléfono','','required:true', array('size'=>10));
		$data['extension']=$this->formulario->addInput('extension','Extension','','', array('size'=>5));
		$data['telefonohab']=$this->formulario->addInput('telefonohab','Telefono','','', array('size'=>10));
		$data['telefonocel']=$this->formulario->addInput('telefonocel','Telefono','','required:true', array('size'=>10));
		
		$data['nombreh1']=$this->formulario->addInput('nombreh1','Nombre','','required:true', array('size'=>30));
		$data['fechah1']=$this->formulario->addDatePicker('fechanach1','Fecha de Nac.');
		
		$data['nombreh2']=$this->formulario->addInput('nombreh2','Nombre','','', array('size'=>30));
		$data['fechah2']=$this->formulario->addDatePicker('fechanach2','Fecha de Nac.');
		
		$data['nombreh3']=$this->formulario->addInput('nombreh3','Nombre','','', array('size'=>30));
		$data['fechah3']=$this->formulario->addDatePicker('fechanach3','Fecha de Nac.');
		
		$data['nombreh4']=$this->formulario->addInput('nombreh4','Nombre','','', array('size'=>30));
		$data['fechah4']=$this->formulario->addDatePicker('fechanach4','Fecha de Nac.');
		
		$data['nombreh5']=$this->formulario->addInput('nombreh5','Nombre','','', array('size'=>30));
		$data['fechah5']=$this->formulario->addDatePicker('fechanach5','Fecha de Nac.');

		$data['btn']=$this->formulario->addButton('btn','Grabar Datos','enviar');
		
		$this->formulario->output();
		$data['javascript']=$this->formulario->getJavascript();
		
		$this->parser->parse('formularios/SOL004-view',$data);
	}	
	
	function procesaSOL004(){
		$porAnalista=FALSE;
		$this->load->helper('funciones');
		if ($this->session->userdata('cedulatramite')!=""){
			$_POST['cedula']=$this->session->userdata('cedulatramite');
			$porAnalista=TRUE;
		}
		$sol=$this->sm->setSolicitud($_POST);

		$this->load->library('smtpmail');
		
		$mail=new SMTPMAIL;
		$mail->set_host('zmail.mppre.gob.ve');
		$correo=$this->session->userdata('usuario')."@mppre.gob.ve";
		
		if ($porAnalista==FALSE){
			$msg="Hemos recibido su solicitud de Prima por Hijos, la misma está registrada bajo el número: ".$sol['id_solicitud']."<br> ".$this->session->userdata('usuario')." Pronto nos comunicaremos con usted por esta misma via para avisarle acerca de los avances.";
			$res=$mail->sendmail($correo, "Sistema de Solicitudes AIT",utf8_decode($msg),"atencion.altrabajador@mppre.gob.ve");
		}
	}
	
	
	/*
	 * Formulario de Antecedentes de Servicio
	 */
	function SOL005(){
		//$this->_header();
		$data['titulo']="Antecedentes de Servicio";
		$this->formulario->setNombreForm('form1');
		$this->formulario->setAction(base_url()."index.php/formularios/procesaSOL005");
		$this->formulario->addHidden('cedula',$this->session->userdata('cedula'));
		$this->formulario->addHidden('codtramite','SOL005');
		$this->formulario->addDatePicker('fechaingreso','Fecha de Ing.','required:true');
		$this->formulario->addDatePicker('fechaegreso','Fecha de Egr.');
		$this->formulario->addInput('uadscripcion','Última Dirección de Adscripción','','required:true', array('size'=>30));
		$this->formulario->addInput('ucargo','Último Cargo desempe&ntilde;ado','','required:true', array('size'=>30));
		$this->formulario->addInput('ultimosueldo','Último Sueldo Básico','','', array('size'=>10));
		$this->formulario->addInput('otrasasignaciones','Otra Asignaciones','','', array('size'=>10));

		$data['btn']=$this->formulario->addButton('btn','Grabar Datos','enviar');
		
		$data['formulario']=$this->formulario->outputHTML();
		
		$data['recaudosTitulo']="";
		$data['recaudos']=array();
		
		$this->parser->parse('formularios/formulario-gen-view',$data);
		
	}
	
	function procesaSOL005(){
		$porAnalista=FALSE;
		$this->load->helper('funciones');
		if ($this->session->userdata('cedulatramite')!=""){
			$_POST['cedula']=$this->session->userdata('cedulatramite');
			$porAnalista=TRUE;
		}
		$sol=$this->sm->setSolicitud($_POST);

		$this->load->library('smtpmail');
		
		$mail=new SMTPMAIL;
		$mail->set_host('zmail.mppre.gob.ve');
		$correo=$this->session->userdata('usuario')."@mppre.gob.ve";
		
		if ($porAnalista==FALSE){
			$msg="Hemos recibido su solicitud de Antecedentes de Servicio, la misma está registrada bajo el número: ".$sol['id_solicitud']."<br> ".$this->session->userdata('usuario')." Pronto nos comunicaremos con usted por esta misma via para avisarle acerca de los avances.";
			$res=$mail->sendmail($correo, "Sistema de Solicitudes AIT",utf8_decode($msg),"atencion.altrabajador@mppre.gob.ve");
		}
	}
	
	/*
	 * Formulario de Vacaciones
	 */
	function SOL006(){
		//$this->_header();
		$data['titulo']="Vacaciones";
		$this->formulario->setNombreForm('form1');
		$this->formulario->setAction(base_url()."index.php/formularios/procesaSOL006");
		$this->formulario->addHidden('cedula',$this->session->userdata('cedula'));
		$this->formulario->addHidden('codtramite','SOL006');
		$data['jefeinmediato']=$this->formulario->addInput('jefeinmediato','Jefe Inmediato','','required:true, minlength:5', array('size'=>30));
		$data['director']=$this->formulario->addInput('director','Director','','required:true', array('size'=>30));

		$data['btn']=$this->formulario->addButton('btn','Grabar Datos','enviar');
		
		$data['formulario']=$this->formulario->outputHTML();
		
		$this->parser->parse('formularios/formulario-gen-view',$data);
		
	}

	function procesaSOL006(){
		$porAnalista=FALSE;
		$this->load->helper('funciones');
		if ($this->session->userdata('cedulatramite')!=""){
			$_POST['cedula']=$this->session->userdata('cedulatramite');
			$porAnalista=TRUE;
		}
		$sol=$this->sm->setSolicitud($_POST);

		$this->load->library('smtpmail');
		
		$mail=new SMTPMAIL;
		$mail->set_host('zmail.mppre.gob.ve');
		$correo=$this->session->userdata('usuario')."@mppre.gob.ve";
		
		if ($porAnalista==FALSE){
			$msg="Hemos recibido su solicitud de Vacaciones, la misma está registrada bajo el número: ".$sol['id_solicitud']."<br> ".$this->session->userdata('usuario')." Pronto nos comunicaremos con usted por esta misma via para avisarle acerca de los avances.";
			$res=$mail->sendmail($correo, "Sistema de Solicitudes AIT",utf8_decode($msg),"atencion.altrabajador@mppre.gob.ve");
		}
	}
	
	/*
	 * Formulario de Bono hijos Excepcionales
	 */
	function SOL007(){
		//$this->_header();
		$data['titulo']="Bono Hijos Excepcionales";
		$this->formulario->setNombreForm('form1');
		$this->formulario->setAction(base_url()."index.php/formularios/procesaSOL007");
		$data['cedula']=$this->formulario->addHidden('cedula',$this->session->userdata('cedula'));
		$data['codtramite']=$this->formulario->addHidden('codtramite','SOL007');
		
		$data['nombreh1']=$this->formulario->addInput('nombreh1','Nombre','','required:true', array('size'=>30));
		$data['fechah1']=$this->formulario->addDatePicker('fechanach1','Fecha de Nac.');
		
		$data['nombreh2']=$this->formulario->addInput('nombreh2','Nombre','','', array('size'=>30));
		$data['fechah2']=$this->formulario->addDatePicker('fechanach2','Fecha de Nac.');
		

		$data['btn']=$this->formulario->addButton('btn','Grabar Datos','enviar');
		
		$this->formulario->output();
		$data['javascript']=$this->formulario->getJavascript();;
		
		$this->parser->parse('formularios/SOL007-view',$data);
		
	}
	
	function procesaSOL007(){
		$porAnalista=FALSE;
		$this->load->helper('funciones');
		if ($this->session->userdata('cedulatramite')!=""){
			$_POST['cedula']=$this->session->userdata('cedulatramite');
			$porAnalista=TRUE;
		}
		$sol=$this->sm->setSolicitud($_POST);

		$this->load->library('smtpmail');
		
		$mail=new SMTPMAIL;
		$mail->set_host('zmail.mppre.gob.ve');
		$correo=$this->session->userdata('usuario')."@mppre.gob.ve";
		
		if ($porAnalista==FALSE){
			$msg="Hemos recibido su solicitud de Bono Hijos Excepcionales, la misma está registrada bajo el número: ".$sol['id_solicitud']."<br> ".$this->session->userdata('usuario')." Pronto nos comunicaremos con usted por esta misma via para avisarle acerca de los avances.";
			$res=$mail->sendmail($correo, "Sistema de Solicitudes AIT",utf8_decode($msg),"atencion.altrabajador@mppre.gob.ve");
		}
	}
	
	/*
	 * Formulario de Constancia de Trabajo
	 */
	function SOL008(){
		//$this->_header();
		$data['recaudosTitulo']="";
		$data['recaudos']=array();
		$data['titulo']="Constancia de Trabajo";
		$this->formulario->setNombreForm('form1');
		$this->formulario->setAction(base_url()."/index.php/formularios/procesaConstancia");
		$this->formulario->addHidden('cedula',$this->session->userdata('cedula'));
		$this->formulario->addHidden('codtramite','SOL008');
		$this->formulario->addInput('telhab','Tel&eacute;fono Oficina','','', array('size'=>20));
		$this->formulario->addInput('telcel','Tel&eacute;fono Celular','','', array('size'=>20));
		$valores[]=array('valor'=>'1','descripcion'=>'MENSUAL');
		$valores[]=array('valor'=>'2','descripcion'=>'ANUAL');
		$valores[]=array('valor'=>'3','descripcion'=>'AMBAS');
		$this->formulario->addSelect('tipoconstancia','Tipo de Constancia',$valores,'','required:true');
		$data['btn']=$this->formulario->addButton('btn','Grabar Datos','enviar');
		$data['recaudos']=array();
		$data['formulario']=$this->formulario->outputHTML();
		
		$this->parser->parse('formularios/formulario-gen-view',$data);
		
	}
	
	function procesaConstancia(){
		$porAnalista=FALSE;
		$this->load->helper('funciones');
		if ($this->session->userdata('cedulatramite')!=""){
			$_POST['cedula']=$this->session->userdata('cedulatramite');
			$porAnalista=TRUE;
		}
		
		$sol=$this->sm->setSolicitud($_POST,FALSE);

		$this->load->library('smtpmail');
		
		$mail=new SMTPMAIL;
		$mail->set_host('zmail.mppre.gob.ve');
		$correo=$this->session->userdata('usuario')."@mppre.gob.ve";
		
		if ($porAnalista==FALSE){
			$msg="Hemos recibido su solicitud de Constancia de Trabajo, la cual está registrada bajo el número: ".$sol['id_solicitud']."<br> ".$this->session->userdata('usuario')." Pronto nos comunicaremos con usted por esta misma via para avisarle acerca de los avances.";
			$res=$mail->sendmail($correo, "Sistema de Solicitudes AIT",utf8_decode($msg),"atencion.altrabajador@mppre.gob.ve");
		}
	}
	
	
	/*
	 * Formulario de Ayuda por Nacimiento
	 */
	function SOL009(){
		//$this->_header();
		$data['titulo']="Ayuda por Nacimiento";
		$this->formulario->setNombreForm('form1');
		$this->formulario->setAction(base_url()."index.php/formularios/procesaSOL009");
		$data['cedula']=$this->formulario->addHidden('cedula',$this->session->userdata('cedula'));
		$data['codtramite']=$this->formulario->addHidden('codtramite','SOL009');
		$data['cedula_conyuge']=$this->formulario->addInput('cedula_conyuge','Cédula del Conyuge','','');
		$data['nombres_conyuge']=$this->formulario->addInput('nombres_conyuge','Nombre del Conyuge','','', array('size'=>20));
		$data['apellidos_conyuge']=$this->formulario->addInput('apellidos_conyuge','Apellido del Conyuge','','', array('size'=>20));
		$data['telefono_conyuge']=$this->formulario->addInput('telefono_conyuge','Teléfono del Conyuge','','');
		$data['lugartrabajo']=$this->formulario->addInput('lugartrabajo','Lugar de trabajo','','');
		$data['cuentabanco']=$this->formulario->addInput('cuentabanco','Cuenta Bancaria','','required:true, rangelength:[20,20]', array('size'=>20));
		$data['telefono']=$this->formulario->addInput('telefono','Telefono Oficina','','required:true', array('size'=>10));
		$data['extension']=$this->formulario->addInput('extension','Extensión','','required:true', array('size'=>5));
		$data['telefonocel']=$this->formulario->addInput('telefonocel','Telefono Celular','','', array('size'=>10));
		$data['nombreh1']=$this->formulario->addInput('nombreh1','Nombre y apellidos del menor','','required:true', array('size'=>20));
		$data['fechanach1']=$this->formulario->addDatePicker('fechanach1','Fecha de Nac.','required:true');
		$data['nombreh2']=$this->formulario->addInput('nombreh2','Nombre y apellidos del menor','','', array('size'=>20));
		$data['fechanach2']=$this->formulario->addDatePicker('fechanach2','Fecha de Nac.','');
		$data['btn']=$this->formulario->addButton('btn','Grabar Datos','enviar');
		
		//$data['formulario']=$this->formulario->outputHTML();
		$this->formulario->output();
		$data['javascript']=$this->formulario->getJavascript();
		
		$this->parser->parse('formularios/SOL009-view',$data);
		
	}
	
	function procesaSOL009(){
		$porAnalista=FALSE;
		$this->load->helper('funciones');
		if ($this->session->userdata('cedulatramite')!=""){
			$_POST['cedula']=$this->session->userdata('cedulatramite');
			$porAnalista=TRUE;
		}
		$sol=$this->sm->setSolicitud($_POST);

		$this->load->library('smtpmail');
		
		$mail=new SMTPMAIL;
		$mail->set_host('zmail.mppre.gob.ve');
		$correo=$this->session->userdata('usuario')."@mppre.gob.ve";
		
		if ($porAnalista==FALSE){
			$msg="Hemos recibido su solicitud de Ayuda por Nacimiento, la misma está registrada bajo el número: ".$sol['id_solicitud']."<br> ".$this->session->userdata('usuario')." Pronto nos comunicaremos con usted por esta misma via para avisarle acerca de los avances.";
			$res=$mail->sendmail($correo, "Sistema de Solicitudes AIT",utf8_decode($msg),"atencion.altrabajador@mppre.gob.ve");
		}
	}
	
	/*
	* Formulario de Beca
	*/
        
	/*function TRA008(){
            //$this->_header();
            $data['recaudosTitulo']="";
            $data['recaudos']=array();
            $data['titulo']="Bono Guarder&iacute;a";
            $this->formulario->addScript("$('#wrapper').css('display','none');");
            $this->formulario->setNombreForm('form1');
            $this->formulario->setAction(base_url()."index.php/formularios/procesaTRA008");
            $this->formulario->addInput('cedula','C&eacute;dula Trabajador','','required:true');
            $this->formulario->addButton('btnEnviar','Consultar','enviar');
            $data['formulario']=$this->formulario->outputHTML();
            $data['mensaje']='Beca Escolar (Solicitud)';
            $this->parser->parse('formularios/consultahijos-view',$data);
        }*/
       ///////////////////////////////////////////////////////////////////////////////////////// 
	function TRA008(){
		/*$data['cedula']="";
		$data['nombres']="";
		$data['apellidos']="";
		$data['dependencia']="";
		$data['cargo']="";
		$data['direccion_residencia']="";
		$data['telefono_celular']="";
		$data['telefono_oficina']="";
		$data['telefono_residencia']="";
		$data['email']="";
		$data['hijostrabajador']="";*/
                $this->formulario->addScript("$('#wrapper').css('display','none');");
                //$this->_header();
                if ($this->session->userdata('cedulatramite')!=""){
			$_POST['cedula']=$this->session->userdata('cedulatramite');			
		}
		$cedula=$_POST['cedula'];
                $resPersonal=$this->sm->getPersonalBeca($cedula);
		$arrPersonal=$resPersonal['personal'];
                if ($arrPersonal){
			$data['cedula']=$arrPersonal->cedula;
			$data['nombres']=utf8_encode($arrPersonal->primer_nombre." ".$arrPersonal->segundo_nombre);
			$data['apellidos']=utf8_encode($arrPersonal->primer_apellido." ".$arrPersonal->segundo_apellido);
			$data['dependencia']=utf8_encode($arrPersonal->dependencia);
			$data['cargo']=utf8_encode($arrPersonal->cargo);
			$data['direccion_residencia']=utf8_encode($arrPersonal->direccion_residencia);
			$data['telefono_celular']=$arrPersonal->telefono_celular;
			$data['telefono_oficina']=$arrPersonal->telefono_oficina;
			$data['telefono_residencia']=$arrPersonal->telefono_residencia;
			$data['email']=utf8_encode($arrPersonal->email);
			$data['hijostrabajador']=$resPersonal['hijostrabajador'];
		}else{
			$data['hijostrabajador']=array();
			$data['nombres']="LA C&Eacute;DULA QUE INGRES&Oacute; NO ESTA REGISTRADA";
		}	
 		$data['mensaje1']='Inscripción Beca';
                $data['link1']=base_url().'index.php/formularios/AgregaDatosHijosBecas';
                $data['mensaje2']='Planilla Beca';
                $data['link2']=base_url().'index.php/formularios/GenerarPlanillaBeca';
                $this->parser->parse('formularios/consultatrabajador-view',$data);
                
	}
	//////////////////////////////////////////Agregar Datos /////////////////////////////       
	function AgregaDatosHijosBecas () {
                $this->formulario->addScript("$('#wrapper').css('display','none');");
		$this->load->helper('form');
		$this->_header();
		$idFamiliar=$this->uri->segment(3);
		$data = $this->sm->getHijo($idFamiliar);
		$this->jquery->output();
		$this->parser->parse('formularios/ingresobeca-view',$data);
	}
	////////////////////// FUNCIÓN PARA INGRESAR DATOS ADICIONALES EN BD  /////////////////////////	
	function ProcAgregaBeca() {
                $data['id_familiar']=$this->input->post ('id_familiar');
                $idfamiliar = $data['id_familiar'];
                $data['nivel_educativo']=$this->input->post ('nivel_educativo');
                $data['grado_semestre']=$this->input->post ('grado_semestre');
		$data['anio_escolar']=$this->input->post ('anio_escolar');
		$data['promedio_notas']=$this->input->post ('promedio_notas');
                $data['instituto']=$this->input->post ('instituto');
                $data['carrera_especialidad']=$this->input->post ('carrera_especialidad');
                $data['inscripcion'] = ($this->input->post ('inscripcion') == 'X')? TRUE:FALSE;            
                $data['calificacion'] = ($this->input->post ('calificacion') == 'X')? TRUE:FALSE;
                $data['carnet']=($this->input->post ('carnet') == 'X')? TRUE:FALSE;
                $data['cifuncionario']=($this->input->post ('cifuncionario') == 'X')? TRUE:FALSE;
                $data['cibeneficiario']=($this->input->post ('cibeneficiario') == 'X')? TRUE:FALSE;
                $data['partidanacimiento']=($this->input->post ('partidanacimiento') == 'X')? TRUE:FALSE;
                $data['informe']=($this->input->post ('informe') == 'X')? TRUE:FALSE;
                $data['contrato']=($this->input->post ('contrato') == 'X')? TRUE:FALSE;
                $data['utiles'] = ($this->input->post ('utiles') == 'X')? TRUE:FALSE;
                $data['id_tipo_beca']=intval($this->input->post ('id_tipo_beca'));
                $data['tipo_beca']='';
                
                $this->form_validation->set_rules('inscripcion', 'inscripcion', 'required');
                $this->form_validation->set_rules('calificacion', 'calificacion', 'required');
                $this->form_validation->set_rules('carnet', 'carnet', 'required');
                $this->form_validation->set_rules('cifuncionario', 'cifuncionario', 'required');
                $this->form_validation->set_rules('partidanacimiento', 'partidanacimiento', 'required');
                if ($this->form_validation->run() == FALSE){
                    $this->_header();
                    $data['mensaje']='ERROR!!! Recaudos Incompletos';
                    //$data['link1']=base_url().'index.php/formularios/AgregaDatosHijosBecas';
                    $this->load->view('formularios/mensaje-view',$data);
                }else{
                      if ($data['promedio_notas']==13){
                        $this->_header();
                        $data['mensaje']='Problema con la calificación. Debe pasar por Bienestar Social';
                        $this->load->view('formularios/mensaje-view',$data);
                      }else{ 
                        $exFamiliar=$this->sm->getValidarHijoBeca($data['id_familiar'],$data['anio_escolar'] );
                        if ($exFamiliar==NULL){
                                $this->sm->guardaDatosBeca($data);
                                $this->_header();
                                $data['mensaje']='Información Registrada con Exito!!!, debe IMPRIMIR la Planilla';
                                $this->load->view('formularios/mensaje-view',$data);
                        }else{
                                $this->_header();
                                $data['mensaje']='ERROR!!! Niño ya Registrado';
                                $this->load->view('formularios/mensaje-view',$data);
                        }
                      }
                  }
               
                
                
	}
       ///////////////////// FUNCIÓN PARA GENERAR PLANILLA DE INSCRIPCIÓN /////////////////////////	
        function GenerarPlanillaBeca () {
                $idFamiliar=$this->uri->segment(3);
                $anio=date("Y");
                $anioEscolar= $anio+1;
                $exFamiliar=$this->sm->getValidarHijoBeca($idFamiliar,$anioEscolar );
                
                if ($exFamiliar==NULL){
                        $this->_header();
                        $data['mensaje']='ERROR!!! No se ha realizado la Inscripción del Niño';
                        $this->load->view('formularios/mensaje-view',$data);
	        }else{
                  ########################################## VARIABLES GENERALES ######################################################        
                    $fecha = date('d/m/Y');
                    $dia = date('d');
                    $numMes = date('m');             
                    $mes = getMesES($numMes);               
                    $anio = date('Y');
                    ######################################### VARIABLES OBTENIDAS DEL MODELO getPadreHijo ###################################

                    $padreHijo = $this->sm->getPadreHijo($idFamiliar);
                    $nombrePadre = utf8_encode($padreHijo->nombre);
                    $cedulaPadre = $padreHijo->cedula;
                    $dependencia = utf8_encode($padreHijo->dependencia);
                    $cargo = utf8_encode($padreHijo->cargo);
                    $sexo = getSexo($padreHijo->sexo);
                    $nombreHijo = utf8_encode($padreHijo->nombre_fam);
                    $cedulaHijo = $padreHijo->cedula_familiar;
                    $edad = $padreHijo->edad;
                    $fechaNacimientoHijo = pgDate($padreHijo->fecha_nacimiento); 
                    $direccionPadre = utf8_encode($padreHijo->direccion_residencia);
                    $telefonoPadre = $padreHijo->telefono_residencia;
                    $telefonoOficina = $padreHijo->telefono_oficina;
                    $celularPadre = $padreHijo->telefono_celular;
                    $tipoPersonal = $padreHijo->id_tipo_personal;
                    $email = utf8_encode($padreHijo->email);
                    $arr_tipo_personal = array ('1'=>'Obrero','13'=>'Administrativo','14'=>'Contratado','20'=>'Diplomatico','42'=>'Jubilado','43'=>'Pensionado','51'=>'Pedro Gual','53'=>'Comisión de Servicio','83'=>'Consejo Nacional de Frontera','100'=>'Alto Funcionario', '101'=>'Alto Nivel');
                    $arr_tipo_beca = array ('0'=>'s/tipo Beca','1'=>'Preescolar/Básica','2'=>'Secundaria/Diversificada','3'=>'Especial','4'=>'Universitaria'); 
###################################### VARIABLES OBTENIDAS DEL MODELO getBeca #############################
                    $DatosHijo= $this->sm->getBeca($idFamiliar,$anioEscolar);
                    $arr_nivel_educativo = array ('P'=>'Preescolar','B'=>'Básica','D'=>'Diversificado','U'=>'Universitaria','E'=>'Especial');
                    $nivel_educativo = $DatosHijo->nivel_educativo;
                    $grado_semestre = $DatosHijo->grado_semestre;
                    $anio_escolar = $DatosHijo->anio_escolar;
                    $promedio_notas = $DatosHijo->promedio_notas;
                    $instituto = $DatosHijo->instituto;
                    $carrera_especialidad = $DatosHijo->carrera_especialidad;               
                    $utiles= $DatosHijo->utiles;
                    $id_tipo_beca =$DatosHijo->id_tipo_beca;   
                    if ($utiles==1){
                        $utiles_des='Solicitado';
                    }else{
                        $utiles_des='No Solicitado';
                    }
                    
                    $inscripcion = $DatosHijo->inscripcion;
                    if ($inscripcion==1){
                        $d1='SI';
                    }  else {
                        $d1='NO';  
                    }  
                    
                    $calificacion = $DatosHijo->calificacion;
                    if ($calificacion==1){
                        $d2='SI';
                    }  else {
                        $d2='NO';  
                    }  
                    
                    $carnet = $DatosHijo->carnet;
                    if ($carnet==1){
                        $d3='SI';
                    }  else {
                        $d3='NO';  
                    }  
                    
                    $cifuncionario = $DatosHijo->cifuncionario;
                    if ($cifuncionario==1){
                        $d4='SI';
                    }  else {
                        $d4='NO';  
                    }  
                    
                    $cibeneficiario = $DatosHijo->cibeneficiario;
                    if ($cibeneficiario==1){
                        $d5='SI';
                    }  else {
                        $d5='NO';  
                    }  
                    
                    $partidanacimiento = $DatosHijo->partidanacimiento;
                    if ($partidanacimiento==1){
                        $d6='SI';
                    }  else {
                        $d6='NO';  
                    } 
                    
                    $informe = $DatosHijo->informe;
                    if ($informe==1){
                        $d7='SI';
                    }  else {
                        $d7='NO';  
                    } 
                    
                    $contrato = $DatosHijo->contrato;
                    if ($contrato==1){
                        $d8='SI';
                    }  else {
                        $d8='NO';  
                    } 
                ########################################### GENERAR PLANILLA DE INSCRIPCION #################################################  	    
 
                        $this->pdf->SetMargins(20, 25 , 30);     
                        $htmlHijo = "
                            <p align=\"center\">
                            <b>SOLICITUD DE BECA ESCOLAR - AÑO: $anio </b>
                            </p>
                            <p></p>                  
                                <b>DATOS DEL TRABAJADOR</b>
                            <hr />
                            <table width=\"1700\" border=\"0\">
                                <tr><td width=\"600\"><b>Cédula:</b></td><td width=\"600\">$cedulaPadre</td></tr>
                                <tr><td width=\"600\"><b>Apellidos y Nombres:</b></td><td width=\"1200\">$nombrePadre</td></tr>
                                <tr><td width=\"600\"><b>Dependencia:</b></td><td width=\"1200\">$dependencia</td></tr>
                                <tr><td width=\"600\"><b>Cargo:</b></td><td width=\"1200\">$cargo</td></tr>
                                <tr><td width=\"600\"><b>Dirección Hab.:</b></td><td width=\"1200\">$direccionPadre</td></tr>
                                <tr><td width=\"600\"><b>Teléfono Hab.:</b></td><td width=\"600\">$telefonoPadre</td></tr>
                                <tr><td width=\"600\"><b>Celular:</b></td><td width=\"600\">$celularPadre</td></tr>
                                <tr><td width=\"600\"><b>Teléfono Ofi.</b></td><td width=\"600\">$telefonoOficina</td></tr>
                                <tr><td width=\"600\"><b>Correo Electrónico:</b></td><td width=\"800\">$email</td></tr>
                                <tr><td width=\"600\"><b>Tipo de Personal:</b></td><td width=\"800\">$arr_tipo_personal[$tipoPersonal]</td></tr>
                            </table>
                            <p></p>  
                            <b>DATOS DEL NIÑO(A)</b>
                            <hr/>
                            <table width=\"2000\" border=\"0\">
                                <tr><td width=\"600\"><b>Apellidos y Nombres:</b></td><td width=\"1000\">$nombreHijo</td></tr>
                                <tr><td width=\"600\"><b>Cédula:</b></td><td width=\"600\">$cedulaHijo</td></tr>
                                <tr><td width=\"600\"><b>Sexo:</b></td><td width=\"600\">$sexo</td></tr>
                                <tr><td width=\"600\"><b>Fecha Nacimiento:</b></td><td width=\"600\">$fechaNacimientoHijo</td></tr>
                                <tr><td width=\"600\"><b>Edad:</b></td><td width=\"600\">$edad</td></tr>
                                <tr><td width=\"600\"><b>Institución:</b></td><td width=\"700\">$instituto</td></tr>
                                <tr><td width=\"600\"><b>Carrera o Especialidad:</b></td><td width=\"700\">$carrera_especialidad</td></tr>
                                <tr><td width=\"600\"><b>Nivel Educativo:</b></td><td width=\"600\">$arr_nivel_educativo[$nivel_educativo]</td></tr>
                                <tr><td width=\"600\"><b>Grado o Semestre:</b></td><td width=\"600\">$grado_semestre</td></tr>
                                <tr><td width=\"600\"><b>Año Escolar:</b></td><td width=\"600\">$anio_escolar</td></tr>
                                <tr><td width=\"600\"><b>Promedio de Nota:</b></td><td width=\"700\">$promedio_notas</td></tr>
                                <tr><td width=\"600\"><b>Tipo de Beca:</b></td><td width=\"600\">$arr_tipo_beca[$id_tipo_beca]</td></tr>
                                <tr><td width=\"600\"><b>Útiles:</b></td><td width=\"700\">$utiles_des</td></tr>
                           
                            </table>
                            <p></p>  
                            <b>DOCUMENTOS CONSIGNADOS</b>
                            <hr/>
                            <table width=\"3000\" border=\"0\">
                                <tr><td width=\"1000\"><b>Constancia de Inscripción:</b></td><td width=\"1000\">$d1</td></tr>
                                <tr><td width=\"1000\"><b>Constancia de Calificación:</b></td><td width=\"1000\">$d2</td></tr>
                                <tr><td width=\"1000\"><b>Fotocopia de Carnet Funcionario:</b></td><td width=\"1000\">$d3</td></tr>
                                <tr><td width=\"1000\"><b>Fotocopia Cédula del Funcionario:</b></td><td width=\"1000\">$d4</td></tr>
                                <tr><td width=\"1000\"><b>Fotocopia Cédula del Beneficiario:</b></td><td width=\"1000\">$d5</td></tr>
                                <tr><td width=\"1000\"><b>Partida de Nacimiento del Beneficiario:</b></td><td width=\"1000\">$d6</td></tr>
                                <tr><td width=\"1000\"><b>Informe Médico Actualizado (niño especial):</b></td><td width=\"1000\">$d7</td></tr>
                                <tr><td width=\"1000\"><b>Fotocopia del Contrato:</b></td><td width=\"1000\">$d8</td></tr>
                            </table>
                            <p>
                            
                            <p></p>                       
                            <b>NOMBRE DEL REPRESENTANTE:</b></p>$nombrePadre, FIRMA____________________FECHA:  $fecha	

                        ";
                   
                        $this->pdf->SetFontSize(10);
                        $this->pdf->AddPage('R', 'letter');
                        $this->pdf->writeHTML($htmlHijo, true, false, true, false, '');               
                        $this->pdf->output('Guarderia.pdf', 'D');
               }
     }
     
     
        /*
	* Formulario de Utiles
	*/
        
	function TRA009(){
            //$this->_header();
            $data['recaudosTitulo']="";
            $data['recaudos']=array();
            $data['titulo']="Utiles Escolares";
            $this->formulario->addScript("$('#wrapper').css('display','none');");
            $this->formulario->setNombreForm('form1');
            $this->formulario->setAction(base_url()."index.php/formularios/procesaTRA009");
            $this->formulario->addInput('cedula','C&eacute;dula Trabajador','','required:true');
            $this->formulario->addButton('btnEnviar','Consultar','enviar');
            $data['formulario']=$this->formulario->outputHTML();
            $data['mensaje']='Útiles Escolares (Solicitud)';
            $this->parser->parse('formularios/consultahijos-view',$data);
        }
       ///////////////////////////////////////////////////////////////////////////////////////// 
	function procesaTRA009(){
		$data['cedula']="";
		$data['nombres']="";
		$data['apellidos']="";
		$data['dependencia']="";
		$data['cargo']="";
		$data['direccion_residencia']="";
		$data['telefono_celular']="";
		$data['telefono_oficina']="";
		$data['telefono_residencia']="";
		$data['email']="";
		$data['hijostrabajador']="";
		$cedula=$this->input->post('cedula');
                $resPersonal=$this->sm->getPersonalUtiles($cedula);
		$arrPersonal=$resPersonal['personal'];
                if ($arrPersonal){
			$data['cedula']=$arrPersonal->cedula;
			$data['nombres']=utf8_encode($arrPersonal->primer_nombre." ".$arrPersonal->segundo_nombre);
			$data['apellidos']=utf8_encode($arrPersonal->primer_apellido." ".$arrPersonal->segundo_apellido);
			$data['dependencia']=utf8_encode($arrPersonal->dependencia);
			$data['cargo']=utf8_encode($arrPersonal->cargo);
			$data['direccion_residencia']=utf8_encode($arrPersonal->direccion_residencia);
			$data['telefono_celular']=$arrPersonal->telefono_celular;
			$data['telefono_oficina']=$arrPersonal->telefono_oficina;
			$data['telefono_residencia']=$arrPersonal->telefono_residencia;
			$data['email']=utf8_encode($arrPersonal->email);
			$data['hijostrabajador']=$resPersonal['hijostrabajador'];
		}else{
			$data['hijostrabajador']=array();
			$data['nombres']="LA C&Eacute;DULA QUE INGRES&Oacute; NO ESTA REGISTRADA";
		}	
 		$data['mensaje1']='Inscripción Útiles';
                $data['link1']=base_url().'index.php/formularios/AgregaDatosHijosUtiles';
                $data['mensaje2']='Planilla Útiles';
                $data['link2']=base_url().'index.php/formularios/GenerarPlanillaUtiles';
                $this->parser->parse('formularios/consultatrabajador-view',$data);
	}
	//////////////////////////////////////////Agregar Datos /////////////////////////////       
	function AgregaDatosHijosUtiles () {
		$this->load->helper('form');
		$this->_header();
		$idFamiliar=$this->uri->segment(3);
		$data = $this->sm->getHijo($idFamiliar);
		$this->jquery->output();
		$this->parser->parse('formularios/ingresoutiles-view',$data);
	}
	////////////////////// FUNCIÓN PARA INGRESAR DATOS ADICIONALES EN BD  /////////////////////////	
	function ProcAgregaUtiles() {
                $data['id_familiar']=$this->input->post ('id_familiar');
                $idfamiliar = $data['id_familiar'];
                $data['nivel_educativo']=$this->input->post ('nivel_educativo');
                $data['grado_semestre']=$this->input->post ('grado_semestre');
                $data['anio_escolar']=$this->input->post ('anio_escolar');
                $data['instituto']=$this->input->post ('instituto');
                $data['carrera_especialidad']=$this->input->post ('carrera_especialidad');
                $data['inscripcion'] = ($this->input->post ('inscripcion') == 'X')? TRUE:FALSE;            
                $data['carnet']=($this->input->post ('carnet') == 'X')? TRUE:FALSE;
                $data['cifuncionario']=($this->input->post ('cifuncionario') == 'X')? TRUE:FALSE;
                $data['cibeneficiario']=($this->input->post ('cibeneficiario') == 'X')? TRUE:FALSE;
                $data['partidanacimiento']=($this->input->post ('partidanacimiento') == 'X')? TRUE:FALSE;
                $data['informe']=($this->input->post ('informe') == 'X')? TRUE:FALSE;
                $data['contrato']=($this->input->post ('contrato') == 'X')? TRUE:FALSE;
                
                $this->form_validation->set_rules('inscripcion', 'inscripcion', 'required');
                $this->form_validation->set_rules('carnet', 'carnet', 'required');
                $this->form_validation->set_rules('cifuncionario', 'cifuncionario', 'required');
                $this->form_validation->set_rules('partidanacimiento', 'partidanacimiento', 'required');
                
                if ($this->form_validation->run() == FALSE){
                    $this->_header();
                    $data['mensaje']='ERROR!!! Recaudos Incompletos';
                    $this->load->view('formularios/mensaje-view',$data);
                }else{
                    $exFamiliar=$this->sm->getValidarHijoUtiles($data['id_familiar'],$data['anio_escolar'] );
                    if ($exFamiliar==NULL){
                            $this->sm->guardaDatosUtiles($data);
                            $this->_header();
                            $data['mensaje']='Información Registrada con Exito!!!, debe IMPRIMIR la Planilla';
                            $this->load->view('formularios/mensaje-view',$data);
                    }else{
                            $this->_header();
                            $data['mensaje']='ERROR!!! Niño ya Registrado';
                            $this->load->view('formularios/mensaje-view',$data);
                    }
                }
	}
       ///////////////////// FUNCIÓN PARA GENERAR PLANILLA DE INSCRIPCIÓN /////////////////////////	
        function GenerarPlanillaUtiles () {
                $idFamiliar=$this->uri->segment(3);
                $anio=date("Y");
                $anioEscolar= $anio+1;
                $exFamiliar=$this->sm->getValidarHijoUtiles($idFamiliar,$anioEscolar );
                
                if ($exFamiliar==NULL){
                        $this->_header();
                        $data['mensaje']='ERROR!!! No se ha realizado la Inscripción del Niño';
                        $this->load->view('formularios/mensaje-view',$data);
	        }else{
                    ########################################## VARIABLES GENERALES ######################################################        
                    $fecha = date('d/m/Y');
                    $dia = date('d');
                    $numMes = date('m');             
                    $mes = getMesES($numMes);               
                    $anio = date('Y');
                    ######################################### VARIABLES OBTENIDAS DEL MODELO getPadreHijo ###################################

                    $padreHijo = $this->sm->getPadreHijo($idFamiliar);
                    $nombrePadre = utf8_encode($padreHijo->nombre);
                    $cedulaPadre = $padreHijo->cedula;
                    $dependencia = utf8_encode($padreHijo->dependencia);
                    $cargo = utf8_encode($padreHijo->cargo);
                    $sexo = getSexo($padreHijo->sexo);
                    $nombreHijo = utf8_encode($padreHijo->nombre_fam);
                    $cedulaHijo = $padreHijo->cedula_familiar;
                    $edad = $padreHijo->edad;
                    $fechaNacimientoHijo = pgDate($padreHijo->fecha_nacimiento); 
                    $direccionPadre = utf8_encode($padreHijo->direccion_residencia);
                    $telefonoPadre = $padreHijo->telefono_residencia;
                    $telefonoOficina = $padreHijo->telefono_oficina;
                    $celularPadre = $padreHijo->telefono_celular;
                    $tipoPersonal = $padreHijo->id_tipo_personal;
                    $email = utf8_encode($padreHijo->email);
                    $arr_tipo_personal = array ('1'=>'Obrero','13'=>'Administrativo','14'=>'Contratado','20'=>'Diplomatico','42'=>'Jubilado','43'=>'Pensionado','51'=>'Pedro Gual','53'=>'Comisión de Servicio','83'=>'Consejo Nacional de Frontera','100'=>'Alto Funcionario', '101'=>'Alto Nivel');
                    ###################################### VARIABLES OBTENIDAS DEL MODELO getUtiles #############################
                    $DatosHijo= $this->sm->getUtiles($idFamiliar,$anioEscolar);
                    $arr_nivel_educativo = array ('P'=>'Preescolar','B'=>'Básica','D'=>'Diversificado','U'=>'Universitaria','E'=>'Especial');
                    $nivel_educativo = $DatosHijo->nivel_educativo;
                    $grado_semestre = $DatosHijo->grado_semestre;
                    $anio_escolar = $DatosHijo->anio_escolar;
                    $instituto = $DatosHijo->instituto;
                    $carrera_especialidad = $DatosHijo->carrera_especialidad;               
                                
                    $inscripcion = $DatosHijo->inscripcion;
                    if ($inscripcion==1){
                        $d1='SI';
                    }  else {
                        $d1='NO';  
                    }  
                                                          
                    $carnet = $DatosHijo->carnet;
                    if ($carnet==1){
                        $d3='SI';
                    }  else {
                        $d3='NO';  
                    }  
                    
                    $cifuncionario = $DatosHijo->cifuncionario;
                    if ($cifuncionario==1){
                        $d4='SI';
                    }  else {
                        $d4='NO';  
                    }  
                    
                    $cibeneficiario = $DatosHijo->cibeneficiario;
                    if ($cibeneficiario==1){
                        $d5='SI';
                    }  else {
                        $d5='NO';  
                    }  
                    
                    $partidanacimiento = $DatosHijo->partidanacimiento;
                    if ($partidanacimiento==1){
                        $d6='SI';
                    }  else {
                        $d6='NO';  
                    } 
                    
                    $informe = $DatosHijo->informe;
                    if ($informe==1){
                        $d7='SI';
                    }  else {
                        $d7='NO';  
                    } 
                    
                    $contrato = $DatosHijo->contrato;
                    if ($contrato==1){
                        $d8='SI';
                    }  else {
                        $d8='NO';  
                    } 
                ########################################### GENERAR PLANILLA DE INSCRIPCION #################################################  	    
 
                        $this->pdf->SetMargins(20, 25 , 30);     
                        $htmlHijo = "
                            <p align=\"center\">
                            <b>SOLICITUD DE UTILES ESCOLARES - AÑO: $anio </b>
                            </p>
                            <p></p>                  
                                <b>DATOS DEL TRABAJADOR</b>
                            <hr />
                            <table width=\"1700\" border=\"0\">
                                <tr><td width=\"600\"><b>Cédula:</b></td><td width=\"600\">$cedulaPadre</td></tr>
                                <tr><td width=\"600\"><b>Apellidos y Nombres:</b></td><td width=\"1200\">$nombrePadre</td></tr>
                                <tr><td width=\"600\"><b>Dependencia:</b></td><td width=\"1200\">$dependencia</td></tr>
                                <tr><td width=\"600\"><b>Cargo:</b></td><td width=\"1200\">$cargo</td></tr>
                                <tr><td width=\"600\"><b>Dirección Hab.:</b></td><td width=\"1200\">$direccionPadre</td></tr>
                                <tr><td width=\"600\"><b>Teléfono Hab.:</b></td><td width=\"600\">$telefonoPadre</td></tr>
                                <tr><td width=\"600\"><b>Celular:</b></td><td width=\"600\">$celularPadre</td></tr>
                                <tr><td width=\"600\"><b>Teléfono Ofi.</b></td><td width=\"600\">$telefonoOficina</td></tr>
                                <tr><td width=\"600\"><b>Correo Electrónico:</b></td><td width=\"800\">$email</td></tr>
                                <tr><td width=\"600\"><b>Tipo de Personal:</b></td><td width=\"800\">$arr_tipo_personal[$tipoPersonal]</td></tr>
                            </table>
                            <p></p>  
                            <b>DATOS DEL NIÑO(A)</b>
                            <hr/>
                            <table width=\"2000\" border=\"0\">
                                <tr><td width=\"600\"><b>Apellidos y Nombres:</b></td><td width=\"1000\">$nombreHijo</td></tr>
                                <tr><td width=\"600\"><b>Cédula:</b></td><td width=\"600\">$cedulaHijo</td></tr>
                                <tr><td width=\"600\"><b>Sexo:</b></td><td width=\"600\">$sexo</td></tr>
                                <tr><td width=\"600\"><b>Fecha Nacimiento:</b></td><td width=\"600\">$fechaNacimientoHijo</td></tr>
                                <tr><td width=\"600\"><b>Edad:</b></td><td width=\"600\">$edad</td></tr>
                                <tr><td width=\"600\"><b>Institución:</b></td><td width=\"700\">$instituto</td></tr>
                                <tr><td width=\"600\"><b>Carrera o Especialidad:</b></td><td width=\"700\">$carrera_especialidad</td></tr>
                                <tr><td width=\"600\"><b>Nivel Educativo:</b></td><td width=\"600\">$arr_nivel_educativo[$nivel_educativo]</td></tr>
                                <tr><td width=\"600\"><b>Grado o Semestre:</b></td><td width=\"600\">$grado_semestre</td></tr>
                                <tr><td width=\"600\"><b>Año Escolar:</b></td><td width=\"600\">$anio_escolar</td></tr>
                            </table>
                            <p></p>  
                            <b>DOCUMENTOS CONSIGNADOS</b>
                            <hr/>
                            <table width=\"3000\" border=\"0\">
                                <tr><td width=\"1000\"><b>Constancia de Inscripción:</b></td><td width=\"1000\">$d1</td></tr>
                                <tr><td width=\"1000\"><b>Fotocopia de Carnet Funcionario:</b></td><td width=\"1000\">$d3</td></tr>
                                <tr><td width=\"1000\"><b>Fotocopia Cédula del Funcionario:</b></td><td width=\"1000\">$d4</td></tr>
                                <tr><td width=\"1000\"><b>Fotocopia Cédula del Beneficiario:</b></td><td width=\"1000\">$d5</td></tr>
                                <tr><td width=\"1000\"><b>Partida de Nacimiento del Beneficiario:</b></td><td width=\"1000\">$d6</td></tr>
                                <tr><td width=\"1000\"><b>Informe Médico Actualizado (niño especial):</b></td><td width=\"1000\">$d7</td></tr>
                                <tr><td width=\"1000\"><b>Fotocopia del Contrato:</b></td><td width=\"1000\">$d8</td></tr>
                            </table>
                            <p>
                            
                            <p></p>                       
                            <b>NOMBRE DEL REPRESENTANTE:</b></p>$nombrePadre, FIRMA____________________FECHA:  $fecha	

                        ";
                   
                        $this->pdf->SetFontSize(10);
                        $this->pdf->AddPage('R', 'letter');
                        $this->pdf->writeHTML($htmlHijo, true, false, true, false, '');               
                        $this->pdf->output('Guarderia.pdf', 'D');
               }
     }
     
     
     /*
	* Formulario de Guarderia Pago Mensual
	*/
        
	function SOL012(){
            //$this->_header();
            $data['recaudosTitulo']="";
            $data['recaudos']=array();
            $data['titulo']="Bono Guardería - Pago Mensual";
            $this->formulario->addScript("$('#wrapper').css('display','none');");
            $this->formulario->setNombreForm('form1');
            $this->formulario->setAction(base_url()."index.php/formularios/procesaSOL012");
            $this->formulario->addInput('cedula','C&eacute;dula Trabajador','','required:true');
            $this->formulario->addButton('btnEnviar','Consultar','enviar');
            $data['formulario']=$this->formulario->outputHTML();
            $data['mensaje']='Bono Guardería Pago Mensual';
            $this->parser->parse('formularios/consultahijos-view',$data);
        }
       ///////////////////////////////////////////////////////////////////////////////////////// 
	function procesaSOL012(){
                $data['cedula']="";
		$data['nombres']="";
		$data['apellidos']="";
		$data['dependencia']="";
		$data['cargo']="";
		$data['direccion_residencia']="";
		$data['telefono_celular']="";
		$data['telefono_oficina']="";
		$data['telefono_residencia']="";
		$data['email']="";
		$data['hijostrabajador']="";
		$cedula=$this->input->post('cedula');
                $resPersonal=$this->sm->getPersonalBGuarderiaMensual($cedula);
		$arrPersonal=$resPersonal['personal'];
                if ($arrPersonal){
			$data['cedula']=$arrPersonal->cedula;
			$data['nombres']=utf8_encode($arrPersonal->primer_nombre." ".$arrPersonal->segundo_nombre);
			$data['apellidos']=utf8_encode($arrPersonal->primer_apellido." ".$arrPersonal->segundo_apellido);
			$data['dependencia']=utf8_encode($arrPersonal->dependencia);
			$data['cargo']=utf8_encode($arrPersonal->cargo);
			$data['direccion_residencia']=utf8_encode($arrPersonal->direccion_residencia);
			$data['telefono_celular']=$arrPersonal->telefono_celular;
			$data['telefono_oficina']=$arrPersonal->telefono_oficina;
			$data['telefono_residencia']=$arrPersonal->telefono_residencia;
			$data['email']=utf8_encode($arrPersonal->email);
			$data['hijostrabajador']=$resPersonal['hijostrabajador'];
		}else{
			$data['hijostrabajador']=array();
			$data['nombres']="LA C&Eacute;DULA QUE INGRES&Oacute; NO ESTA REGISTRADA";
		}	
 		$data['mensaje1']='Bono Guardería Pago Mensual';
                $data['link1']=base_url().'index.php/formularios/AgregaDatosHijosBMensual';
                $data['mensaje2']='Planilla Bono Guardería';
                $data['link2']=base_url().'index.php/formularios/GenerarPlanillaBMensual';
                $this->parser->parse('formularios/consultatrabajador-view',$data);
	}
	//////////////////////////////////////////Agregar Datos /////////////////////////////       
	function AgregaDatosHijosBMensual () {
		$this->load->helper('form');
		$this->_header();
		$idFamiliar=$this->uri->segment(3);
		$data = $this->sm->getHijo($idFamiliar);
		$this->jquery->output();
		$this->parser->parse('formularios/ingresoguarderiamensual-view',$data);
	}
	////////////////////// FUNCIÓN PARA INGRESAR DATOS ADICIONALES EN BD  /////////////////////////	
	function ProcAgregaGuarderiaMensual() {
                $data['id_familiar']=$this->input->post ('id_familiar');
                $idfamiliar = $data['id_familiar'];
                $data['anio_escolar']=$this->input->post ('anio_escolar');
                $data['instituto']=$this->input->post ('instituto');
                $data['rif'] = $this->input->post ('rif');   
                $data['numero_factura'] = $this->input->post ('numero_factura');            
                $data['mes_factura']=$this->input->post ('mes_factura');
                
                $this->form_validation->set_rules('anio_escolar', 'anio_escolar', 'required');
                $this->form_validation->set_rules('instituto', 'instituto', 'required');
                $this->form_validation->set_rules('rif', 'rif', 'required');
                $this->form_validation->set_rules('numero_factura', 'numero_factura', 'required');
                $this->form_validation->set_rules('mes_factura', 'mes_factura', 'required');
                
                if ($this->form_validation->run() == FALSE){
                    $this->_header();
                    $data['mensaje']='ERROR!!! Recaudos Incompletos';
                    $this->load->view('formularios/mensaje-view',$data);
                }else{
                    $exFamiliar=$this->sm->getValidarHijoPMensual($data['id_familiar'],$data['anio_escolar'], $data['rif'], $data['numero_factura'] );
                    if ($exFamiliar==NULL){
                            $this->sm->guardaDatosGuarderiaMensual($data);
                            $this->_header();
                            $data['mensaje']='Información Registrada con Exito!!!, debe IMPRIMIR la Planilla';
                            $this->load->view('formularios/mensaje-view',$data);
                    }else{
                            $this->_header();
                            $data['mensaje']='ERROR!!! Factura ya Registrado';
                            $this->load->view('formularios/mensaje-view',$data);
                    }
                }
	}
       ///////////////////// FUNCIÓN PARA GENERAR PLANILLA DE INSCRIPCIÓN /////////////////////////	
        function GenerarPlanillaBMensual () {
                $idFamiliar=$this->uri->segment(3);
                $anio=date("Y");
                $anioEscolar= $anio+1;
                $exFamiliar=$this->sm->getGuarderiaMensual($idFamiliar,$anioEscolar );
                
                if ($exFamiliar==NULL){
                        $this->_header();
                        $data['mensaje']='ERROR!!! No se ha realizado el registro del pago mensual';
                        $this->load->view('formularios/mensaje-view',$data);
	        }else{
                    ########################################## VARIABLES GENERALES ######################################################        
                    $fecha = date('d/m/Y');
                    $dia = date('d');
                    $numMes = date('m');             
                    $mes = getMesES($numMes);               
                    $anio = date('Y');
                    ######################################### VARIABLES OBTENIDAS DEL MODELO getPadreHijo ###################################

                    $padreHijo = $this->sm->getPadreHijo($idFamiliar);
                    $nombrePadre = utf8_encode($padreHijo->nombre);
                    $cedulaPadre = $padreHijo->cedula;
                    $dependencia = utf8_encode($padreHijo->dependencia);
                    $cargo = utf8_encode($padreHijo->cargo);
                    $sexo = getSexo($padreHijo->sexo);
                    $nombreHijo = utf8_encode($padreHijo->nombre_fam);
                    $cedulaHijo = $padreHijo->cedula_familiar;
                    $edad = $padreHijo->edad;
                    $fechaNacimientoHijo = pgDate($padreHijo->fecha_nacimiento); 
                    $direccionPadre = utf8_encode($padreHijo->direccion_residencia);
                    $telefonoPadre = $padreHijo->telefono_residencia;
                    $telefonoOficina = $padreHijo->telefono_oficina;
                    $celularPadre = $padreHijo->telefono_celular;
                    $tipoPersonal = $padreHijo->id_tipo_personal;
                    $email = utf8_encode($padreHijo->email);
                    $arr_tipo_personal = array ('1'=>'Obrero','13'=>'Administrativo','14'=>'Contratado','20'=>'Diplomatico','42'=>'Jubilado','43'=>'Pensionado','51'=>'Pedro Gual','53'=>'Comisión de Servicio','83'=>'Consejo Nacional de Frontera','100'=>'Alto Funcionario', '101'=>'Alto Nivel');
                    ###################################### VARIABLES OBTENIDAS DEL MODELO getUtiles #############################
                    $DatosHijo= $this->sm->getGuarderiaMensual($idFamiliar,$anioEscolar);
                    $arr_mes = array ('1'=>'Enero','2'=>'Febrero','3'=>'Marzo','4'=>'Abril','5'=>'Mayo','6'=>'Junio','7'=>'Julio','8'=>'Agosto','9'=>'Septiembre','10'=>'Octubre', '11'=>'Noviembre', '12'=>'Diciembre');
                    $anio_escolar = $DatosHijo->anio_escolar;
                    $instituto = $DatosHijo->instituto;
                    $rif = $DatosHijo->rif;   
                    $numero_factura = $DatosHijo->numero_factura;
                    $mes_factura = $DatosHijo->mes_factura;        
                    
                ########################################### GENERAR PLANILLA DE INSCRIPCION #################################################  	    
 
                        $this->pdf->SetMargins(20, 25 , 30);     
                        $htmlHijo = "
                            <p align=\"center\">
                            <b>SOLICITUD DE PAGO MENSUAL POR BONO GUARDERIA - AÑO: $anio </b>
                            </p>
                            <p></p>                  
                                <b>DATOS DEL TRABAJADOR</b>
                            <hr />
                            <table width=\"1700\" border=\"0\">
                                <tr><td width=\"600\"><b>Cédula:</b></td><td width=\"600\">$cedulaPadre</td></tr>
                                <tr><td width=\"600\"><b>Apellidos y Nombres:</b></td><td width=\"1200\">$nombrePadre</td></tr>
                                <tr><td width=\"600\"><b>Dependencia:</b></td><td width=\"1200\">$dependencia</td></tr>
                                <tr><td width=\"600\"><b>Cargo:</b></td><td width=\"1200\">$cargo</td></tr>
                                <tr><td width=\"600\"><b>Dirección Hab.:</b></td><td width=\"1200\">$direccionPadre</td></tr>
                                <tr><td width=\"600\"><b>Teléfono Hab.:</b></td><td width=\"600\">$telefonoPadre</td></tr>
                                <tr><td width=\"600\"><b>Celular:</b></td><td width=\"600\">$celularPadre</td></tr>
                                <tr><td width=\"600\"><b>Teléfono Ofi.</b></td><td width=\"600\">$telefonoOficina</td></tr>
                                <tr><td width=\"600\"><b>Correo Electrónico:</b></td><td width=\"800\">$email</td></tr>
                                <tr><td width=\"600\"><b>Tipo de Personal:</b></td><td width=\"800\">$arr_tipo_personal[$tipoPersonal]</td></tr>
                            </table>
                            <p></p>  
                            <b>INFORMACION REGISTRADA</b>
                            <hr/>
                            <table width=\"2000\" border=\"0\">
                                <tr><td width=\"600\"><b>Apellidos y Nombres:</b></td><td width=\"1000\">$nombreHijo</td></tr>
                                <tr><td width=\"600\"><b>Cédula:</b></td><td width=\"600\">$cedulaHijo</td></tr>
                                <tr><td width=\"600\"><b>Sexo:</b></td><td width=\"600\">$sexo</td></tr>
                                <tr><td width=\"600\"><b>Fecha Nacimiento:</b></td><td width=\"600\">$fechaNacimientoHijo</td></tr>
                                <tr><td width=\"600\"><b>Edad:</b></td><td width=\"600\">$edad</td></tr>
                                <tr><td width=\"600\"><b>Maternal o Guardería:</b></td><td width=\"700\">$instituto</td></tr>
                                <tr><td width=\"600\"><b>Rif:</b></td><td width=\"700\">$rif</td></tr>
                                <tr><td width=\"600\"><b>Nro. Factura:</b></td><td width=\"600\">$numero_factura</td></tr>
                                <tr><td width=\"600\"><b>Año Escolar:</b></td><td width=\"600\">$anio_escolar</td></tr>
                                <tr><td width=\"600\"><b>Mes Factura:</b></td><td width=\"800\">$arr_mes[$mes_factura]</td></tr>
                            </table>
                            <p></p>  
                             
                            <p>
                            
                            <p></p>                       
                            <b>NOMBRE DEL REPRESENTANTE:</b></p>$nombrePadre, FIRMA____________________FECHA:  $fecha	
                            
                        ";
                        $this->pdf->SetMargins(20, 25 , 30);    
                        $this->pdf->SetFontSize(10);
                        $this->pdf->AddPage('R', 'letter');
                        $this->pdf->writeHTML($htmlHijo, true, false, true, false, '');               
                        
                        $htmlHijo2 = "
                            <p align=\"center\">
                            <b>CONSTANCIA DE RECEPCION DE FACTURA POR BONO GUARDERIA - AÑO: $anio </b>
                            </p>
                            <p></p>                  
                                <b>DATOS DEL TRABAJADOR</b>
                            <hr />
                            <table width=\"1700\" border=\"0\">
                                <tr><td width=\"600\"><b>Cédula:</b></td><td width=\"600\">$cedulaPadre</td></tr>
                                <tr><td width=\"600\"><b>Apellidos y Nombres:</b></td><td width=\"1200\">$nombrePadre</td></tr>
                                <tr><td width=\"600\"><b>Dependencia:</b></td><td width=\"1200\">$dependencia</td></tr>
                                <tr><td width=\"600\"><b>Cargo:</b></td><td width=\"1200\">$cargo</td></tr>
                                <tr><td width=\"600\"><b>Dirección Hab.:</b></td><td width=\"1200\">$direccionPadre</td></tr>
                                <tr><td width=\"600\"><b>Teléfono Hab.:</b></td><td width=\"600\">$telefonoPadre</td></tr>
                                <tr><td width=\"600\"><b>Celular:</b></td><td width=\"600\">$celularPadre</td></tr>
                                <tr><td width=\"600\"><b>Teléfono Ofi.</b></td><td width=\"600\">$telefonoOficina</td></tr>
                                <tr><td width=\"600\"><b>Correo Electrónico:</b></td><td width=\"800\">$email</td></tr>
                                <tr><td width=\"600\"><b>Tipo de Personal:</b></td><td width=\"800\">$arr_tipo_personal[$tipoPersonal]</td></tr>
                            </table>
                            <p></p>  
                            <b>INFORMACION REGISTRADA</b>
                            <hr/>
                            <table width=\"2000\" border=\"0\">
                                <tr><td width=\"600\"><b>Apellidos y Nombres:</b></td><td width=\"1000\">$nombreHijo</td></tr>
                                <tr><td width=\"600\"><b>Cédula:</b></td><td width=\"600\">$cedulaHijo</td></tr>
                                <tr><td width=\"600\"><b>Sexo:</b></td><td width=\"600\">$sexo</td></tr>
                                <tr><td width=\"600\"><b>Fecha Nacimiento:</b></td><td width=\"600\">$fechaNacimientoHijo</td></tr>
                                <tr><td width=\"600\"><b>Edad:</b></td><td width=\"600\">$edad</td></tr>
                                <tr><td width=\"600\"><b>Maternal o Guardería:</b></td><td width=\"700\">$instituto</td></tr>
                                <tr><td width=\"600\"><b>Rif:</b></td><td width=\"700\">$rif</td></tr>
                                <tr><td width=\"600\"><b>Nro. Factura:</b></td><td width=\"600\">$numero_factura</td></tr>
                                <tr><td width=\"600\"><b>Año Escolar:</b></td><td width=\"600\">$anio_escolar</td></tr>
                                <tr><td width=\"600\"><b>Mes Factura:</b></td><td width=\"800\">$arr_mes[$mes_factura]</td></tr>
                            </table>
                            <p></p>  
                             
                            <p>
                            <p></p>                       
                            <b>RECIBIDO POR:</b> ______________________________________________FECHA:  $fecha</p>
                            
                            
                        ";
                         $this->pdf->SetFontSize(10);
                         $this->pdf->AddPage('R', 'letter');
                         $this->pdf->writeHTML($htmlHijo2, true, false, true, false, '');
                         $this->pdf->output('PagoMensual.pdf', 'D');
               }
     }
     
	
}
