<?php
class reportes extends Controller{
	function __construct(){
		parent::Controller();
		
		if (!$this->session->userdata('nombre')){
			redirect(base_url()."index.php/comun/login");
		}
		
		$this->load->model('reportes_model','reportes');
		$this->load->library('highchart');
		$this->load->library('parser');
		$this->output->enable_profiler(FALSE);
		//$this->load->library('charts');
	}
	
	function _header(){
		$data['title']="Reportes - Atenci&oacute;n Integral al Trabajador";
		$this->load->view('main-view',$data);
		$deta['permisos']=$this->session->userdata('permisos');
		$this->load->view('comun/menu2',$deta);
	}
	
	function index(){
		$this->_header();
		$data['datos']=$this->reportes->getConstancias();
		$this->load->view('reportes/index2-view',$data);
	}
	
	function index2(){
		$this->_header();
		
		$serie1=array('nombre'=>'Impresos','data'=>array(1,5));

		$this->highchart->addSerie($serie1);

		$this->highchart->setTipo('column');
		$this->highchart->setTitulo('Relación de solicitudes');
		$this->highchart->setTituloY('Solicitudes 2');
		$this->highchart->setXAxis(array('ene','feb','mar','abr','may'));
		$data['grafico']=$this->highchart->render();
		
		$this->parser->parse('reportes/index3-view',$data);
	}
	
	function index3(){
		$res=$this->reportes->getTotalConstancias();
		$resP=$this->reportes->getTotalConstancias('P');
		$this->_header();
		
		
		foreach ($res as $value) {
			$arr[]=intval($value['total']);
		}
		
		foreach ($resP as $value) {
			$arrP[]=intval($value['total']);
		}
		
		$this->highchart->addSerie(array('nombre'=>'Solicitudes','data'=>$arr));
		$this->highchart->addSerie(array('nombre'=>'Procesadas','data'=>$arrP));
		
		$this->highchart->setTipo('column');
		$this->highchart->setTitulo('Relación de solicitudes');
		$this->highchart->setTituloY('Solicitudes 2');
		$this->highchart->setXAxis(array('feb','mar'));
		$data['grafico']=$this->highchart->render();
		
		$this->parser->parse('reportes/index3-view',$data);
		
	}
	
	function index4(){
		$res=$this->reportes->getDataSolicitudesPie();
		foreach ($res as $value) {
			$arr['data'][]=array($value['nombre'],intval($value['count']));
		}
		$arr['nombre']="Solicitudes";
		$data['datos']=json_encode($arr['data']);
		$this->_header();
		$this->parser->parse('reportes/index4-view',$data);
	}
	
	function index5(){
		$fecha1=date('Y-m')."-01";
		$fecha2=date('Y-m-d');
		
		if ($this->input->post('fechaInicio')){
			$fecha1=$this->input->post('fechaInicio');
			$fecha2=$this->input->post('fechaFin');
		}
		
		$data['fecha1']=$fecha1;
		$data['fecha2']=$fecha2;
		
		$this->load->library('table');
		$res=$this->reportes->getDataSolicitudesPie($fecha1,$fecha2);
		$data['tabla']=$res;

		$this->_header();
		$this->parser->parse('reportes/index5-view',$data);
		
	}
	
	function totalesSeguimiento(){
		$data['resultado']=array();
		$data['resultado1']=array();
		$data['resultado2']=array();
		$data['resultado3']=array();
		$data['resultado4']=array();
		$fecha1=$this->input->post('fecha1');
		$fecha2=$this->input->post('fecha2');
		$anho=$this->input->post('anho');
		
		if ($anho==""){
			$anho=date('Y');
		}
		
		$data['tramites']=$this->reportes->getTramites();
		
		$data['resultado1']=$this->reportes->getTotalesTrimestreSeguimiento($anho,1);
		$data['resultado2']=$this->reportes->getTotalesTrimestreSeguimiento($anho,2);
		$data['resultado3']=$this->reportes->getTotalesTrimestreSeguimiento($anho,3);
		$data['resultado4']=$this->reportes->getTotalesTrimestreSeguimiento($anho,4);	
		
		$data['totalt1']=0;
		$data['totalt2']=0;
		$data['totalt3']=0;
		$data['totalt4']=0;
		$data['totaltotal']=0;
		
		$data['anho']=$anho;
		
		if ($data['resultado1']!=FALSE){
			foreach ($data['resultado1'] as $value) {
				$data['tramites'][$value['id_tramite']]['t1']=$value['cuenta'];
				$data['tramites'][$value['id_tramite']]['total']+=$value['cuenta'];
				$data['totalt1']+=$value['cuenta'];
			}
		}
		
		if ($data['resultado2']!=FALSE){
			foreach ($data['resultado2'] as $value) {
				$data['tramites'][$value['id_tramite']]['t2']=$value['cuenta'];
				$data['tramites'][$value['id_tramite']]['total']+=$value['cuenta'];
				$data['totalt2']+=$value['cuenta'];
			}
		}
		
		if ($data['resultado3']!=FALSE){
			foreach ($data['resultado3'] as $value) {
				$data['tramites'][$value['id_tramite']]['t3']=$value['cuenta'];
				$data['tramites'][$value['id_tramite']]['total']+=$value['cuenta'];
				$data['totalt3']+=$value['cuenta'];
			}
		}
		
		if ($data['resultado4']!=FALSE){
			foreach ($data['resultado4'] as $value) {
				$data['tramites'][$value['id_tramite']]['t4']=$value['cuenta'];
				$data['tramites'][$value['id_tramite']]['total']+=$value['cuenta'];
				$data['totalt4']+=$value['cuenta'];
			}
		}
		
		
		$data['totaltotal']=$data['totalt1']+$data['totalt2']+$data['totalt3']+$data['totalt4'];
		
		
		if ($fecha1!=""){
			$data['resultado']=$this->reportes->getTotalesSeguimiento($fecha1,$fecha2);
		}
		
		$this->_header();
		$this->parser->parse('reportes/seguimiento-view',$data);
		
		
	}
	
	
	function totalesSeguimientoAnalista(){
		$anho=$this->input->post('anho');
		
		if ($anho==""){
			$anho=date('Y');
		}
		$data['anho']=$anho;
		
		$data['totalt1']=0;
		$data['totalt2']=0;
		$data['totalt3']=0;
		$data['totalt4']=0;
		$data['totaltotal']=0;
		
		$resultado1=$this->reportes->getSeguimientoUsuarioTrimestre($anho,1);
		$resultado2=$this->reportes->getSeguimientoUsuarioTrimestre($anho,2);
		$resultado3=$this->reportes->getSeguimientoUsuarioTrimestre($anho,3);
		$resultado4=$this->reportes->getSeguimientoUsuarioTrimestre($anho,4);
		
		$data['analistas']=$this->reportes->getAnalistas();
		
		if ($resultado1){
			foreach ($resultado1 as $value) {
				//echo $value['id_usuario'].": ".$value['cuenta']."<br>";
				if ($value['id_usuario']==""){
					$data['analistas'][9999]['t1']=$value['cuenta'];
					$data['analistas'][9999]['total']+=$value['cuenta'];
				}else{
					$data['analistas'][$value['id_usuario']]['t1']=$value['cuenta'];
					$data['analistas'][$value['id_usuario']]['total']+=$value['cuenta'];
				}
				
				$data['totalt1']+=$value['cuenta'];
			}
		}
		
		if ($resultado2){
			foreach ($resultado2 as $value) {
				//echo $value['id_usuario'].": ".$value['cuenta']."<br>";
				if ($value['id_usuario']==""){
					$data['analistas'][9999]['t2']=$value['cuenta'];
					$data['analistas'][9999]['total']+=$value['cuenta'];
				}else{
					$data['analistas'][$value['id_usuario']]['t2']=$value['cuenta'];
					$data['analistas'][$value['id_usuario']]['total']+=$value['cuenta'];
				}
				$data['totalt2']+=$value['cuenta'];
			}
		}
		
		if ($resultado3){
			foreach ($resultado3 as $value) {
				//echo $value['id_usuario'].": ".$value['cuenta']."<br>";
				if ($value['id_usuario']==""){
					$data['analistas'][9999]['t3']=$value['cuenta'];
					$data['analistas'][9999]['total']+=$value['cuenta'];
				}else{
					$data['analistas'][$value['id_usuario']]['t3']=$value['cuenta'];
					$data['analistas'][$value['id_usuario']]['total']+=$value['cuenta'];
				}
				$data['totalt3']+=$value['cuenta'];
			}
		}
		
		if ($resultado4){
			foreach ($resultado4 as $value) {
				//echo $value['id_usuario'].": ".$value['cuenta']."<br>";
				if ($value['id_usuario']==""){
					$data['analistas'][9999]['t4']=$value['cuenta'];
					$data['analistas'][9999]['total']+=$value['cuenta'];
				}else{
					$data['analistas'][$value['id_usuario']]['t4']=$value['cuenta'];
					$data['analistas'][$value['id_usuario']]['total']+=$value['cuenta'];
				}
				$data['totalt4']+=$value['cuenta'];
			}
		}
		
		$data['totaltotal']=$data['totalt1']+$data['totalt2']+$data['totalt3']+$data['totalt4'];
		
		$this->_header();
		$this->parser->parse('reportes/seguimiento-usuario-view',$data);
	}
	
}