<?php
class reclamos extends Controller{
	function __construct(){
		parent::Controller();
		if (!$this->session->userdata('nombre')){
			redirect(base_url()."index.php/comun/login");
		}
		
		$this->load->library('parser');
		$this->load->model('reclamos_model','rm');
		$this->load->helper('funciones');
		
	}
	
	function index(){
		$this->_header();	
			
	}
	
	function reclamo(){
		$this->_header();	
		$this->rm->getReclamos();
		$this->load->library('formulario');
		
		$this->formulario->setAction(base_url()."index.php/reclamos/procReclamo");
		$this->formulario->addHidden('cedula',$this->session->userdata('cedula'));
		
		$sql="select id_tipo_reclamo as valor, descripcion from tiporeclamo where estatus='A'";
		$this->formulario->addSelect('id_tipo_reclamo','Tipo de Reclamo',$sql);
		$this->formulario->addTextArea("descripcion","Descripci&oacute;n","","required:true",array('cols'=>40,'rows'=>7));
		$this->formulario->addButton("btnEnvio","Enviar", "enviar");
		
		$data['formulario']=$this->formulario->outputHTML();
		
		$data['cedula']=$this->session->userdata('cedula');
		$data['usuario']=$this->session->userdata('usuario');
		
		$this->parser->parse('reclamos/index-view',$data);
	}
	
	function procReclamo(){
		$id=FALSE;
		$this->load->model('reclamos_model','rm');
		$post=$_POST;
		$post['login']=$this->session->userdata('usuario');
		$post['fecha']=date('Y-m-d H:i');
		$id=$this->rm->addReclamo($post);
		
		if ($id){
			echo mensajeGrowl("Su reclamo fue procesado");
		}else{
			echo mensajeGrowl("Hubo un error en el proceso de registro de su eclamo / sugerencia",'error');
		}
		
	}
	
	function procSugerencia(){
		$this->load->model('reclamos_model','rm');
		$res=$this->rm->addSugerencia($_POST);
		if ($res){
			echo mensajeGrowl("Su sugerencia fue registrada");
		}else{
			echo mensajeGrowl("La sugerencia no pudo ser guardada",'error');
		}
		
	}
	
	function _header(){
		$data['title']="Atenci&oacute;n Integral al Trabajador";
		$this->load->view('main-view',$data);
		$deta['permisos']=$this->session->userdata('permisos');
		$this->load->view('comun/menu2',$deta);
	}
}