<?php
class imprimirplanilla extends Controller{
	function __construct(){
		parent::Controller();
		
		$this->load->library('pdf');
		$this->pdf->setPrintFooter(FALSE);
		$this->load->model('solicitud_model','sm');
		$this->load->model('admin_model','am');
		$this->load->model('imprimirplanilla_model','im');
		//$this->load->config('atenciontrabajador');
		$this->config_model->setConfig();
	}
	
	function confirma(){
		$idSolicitud=$this->uri->segment(3);
		$arrSolicitud=$this->sm->getSolicitud($idSolicitud);
		$data['idSolicitud']=$idSolicitud;
		if ($arrSolicitud['nombre']!=""){
			$data['usuarioImpresion']=$arrSolicitud['nombre'];
		}else{
			$data['usuarioImpresion']="el usuario";
		}
		
		$this->_header();
		$this->load->view('analista/confirmacion-view',$data);
		 
	}
	
	function _header(){
		$data['title']="Atenci&oacute;n Integral al Trabajador";
		$this->load->view('main-view',$data);
		$deta['permisos']=$this->session->userdata('permisos');
		$this->load->view('comun/menu2',$deta);	
		
	}
	
	function index(){
		$tipoconstancia=1;
		$idSolicitud=$this->uri->segment(3);
		$arrSolicitud=$this->sm->getSolicitud($idSolicitud);
		
		if ($this->uri->segment(4)!='OK'){
			if ($arrSolicitud['estatus']=='I'){
				redirect(base_url().'index.php/imprimirplanilla/confirma/'.$idSolicitud."");
			}
		}
		
		$arrParametros=$this->sm->getParametrosSolicitud($idSolicitud);
		$this->load->model('personal_model','pm');
				
		$telefono="N/A";
		$telefonohab="N/A";
		$telefonocel="N/A";
		$extension="N/A";
		$nivelAcademicoTxt="";
		$nombresConyuge="N/A";
		$apellidosConyuge="N/A";
		$cedulaConyuge="N/A";
		$telefonoConyuge="N/A";
		
		
		foreach($arrParametros['detalle'] as $value){
			if ($value['parametro']=="nivelacademico"){
				$nivelAcademico=$value['valords'];
				$nivelacad=$this->im->getNivelAcademico($nivelAcademico);
				$nivelAcademicoTxt=$nivelacad->descripcion;
			}
			
			
			if ($value['parametro']=="telefono"){
				$telefono=$value['valords'];
			}
			
			
			if ($value['parametro']=="telefonohab"){
				$telefonohab=$value['valords'];
			}
			
			if ($value['parametro']=="telefonocel"){
				$telefonocel=$value['valords'];
			}
			
			if ($value['parametro']=="extension"){
				$extension=$value['valords'];
			}
			
			if ($value['parametro']=="montoprestaciones"){
				$montoPrestaciones=$value['valords'];
			}
			
			if ($value['parametro']=="nombres_conyuge"){
				$nombresConyuge=$value['valords'];
			}
			
			if ($value['parametro']=="apellidos_conyuge"){
				$apellidosConyuge=$value['valords'];
			}
			
			if ($value['parametro']=="cedula_conyuge"){
				$cedulaConyuge=$value['valords'];
			}else{
				$cedulaConyuge=0;
			}
			
			if ($value['parametro']=="telefono_conyuge"){
				$telefonoConyuge=$value['valords'];
			}
			
			if ($value['parametro']=="tipoconstancia"){
				$tipoconstancia=$value['valords'];
			}
			
			$arr[$value['parametro']]=$value['valords'];
			
			
	
		}
		$meses=array('','enero','febrero','marzo','abril','mayo','junio','julio','agosto','septiembre','octubre','noviembre','diciembre');
		$this->load->library('CNumeroaLetra');
		$this->load->library('numerosaletras');
		
		$montoCestaTicket=$this->config->item('cestaticket');
		$cestaTicketLetras=new CNumeroaLetra();
		$cestaTicketLetras->setNumero($montoCestaTicket);
		
		$this->load->library('persona');
		$cedula=$arrSolicitud['cedula'];
		$persona=$this->persona->getPersona($cedula);
		if (!$persona){
			$this->_header();
			die('La persona no existe como Activo');
		}
		
		$numero = new CNumeroaLetra();
		#die('Valor de Numero:'.$numero);
		$numero->setNumero($persona['sueldo']);
		$monto=number_format($persona['sueldo'],2,',','.');
		$arr['cedula']=$cedula;
		$arr['nombres']=$this->persona->getNombres();
		$arr['apellidos']=$this->persona->getApellidos();
		$arr['nombre']=$this->persona->getNombre();
		$arr['adscripcion']=$this->persona->getAdscripcion();
		$arr['tipopersonal']=$this->persona->getTipoPersonal();
		$arr['cargo']=$this->persona->getCargo();
		$arr['codigonomina']=$this->persona->getCodigoNomina();
		$arr['fechasistema']="Caracas, ".date('d'). " de ". $meses[intval(date('m'))]. " de ".date('Y');
		$arr['fechasimplesistema']=date('d'). "/". date('m'). "/".date('Y');
		$arr['fechaingreso']=pgDate($this->persona->getFechaIngreso());
		
		if ($this->session->userdata('idusuario')>0){
			$this->sm->setPlanillaImpresa($idSolicitud);
		}

		switch ($arrSolicitud['cod_tramite']){
			case 'PRI002':
				
				if ($this->persona->getTelefonoCelular()=="0"){
					$arr['celular']=$telefonocel;
				}else{
					$arr['celular']=$this->persona->getTelefonoCelular();
				}
				
				$arr['nivelacademico']=$nivelAcademicoTxt;
				$arr['telefono']=$telefono;
				$arr['telefonohab']=$telefonohab;
				$arr['telefonocel']=$telefonocel;
				$arr['extension']=$extension;
				$this->_PRI002($arr);
				break;
				
			case 'SOL002':
				
				$mont=new CNumeroaLetra();
				$mont->setNumero($montoPrestaciones);
				$montoTxt=$mont->letra();
				
				$arr['montoprestaciones']=number_format($montoPrestaciones,2,",",".");				
				$arr['nombres_conyuge']=$nombresConyuge;
				$arr['apellidos_conyuge']=$apellidosConyuge;
				$arr['cedula_conyuge']=$cedulaConyuge;
				$arr['telefono_conyuge']=$telefonoConyuge;
				$arr['montotxt']=$montoTxt;
				$this->_SOL002($arr);
				break;
				
			case 'SOL004':
				
				$arr['telefono']=$telefono;
				$arr['telefonohab']=$telefonohab;
				$arr['telefonocel']=$telefonocel;
				$arr['extension']=$extension;
				
				$arr['hijos'][]=array('nombres'=>$arr['nombreh1'], 'fechanac'=>$arr['fechanach1']);
				$arr['hijos'][]=array('nombres'=>$arr['nombreh2'], 'fechanac'=>$arr['fechanach2']);
				$arr['hijos'][]=array('nombres'=>$arr['nombreh3'], 'fechanac'=>$arr['fechanach3']);
				$arr['hijos'][]=array('nombres'=>$arr['nombreh4'], 'fechanac'=>$arr['fechanach4']);
				$arr['hijos'][]=array('nombres'=>$arr['nombreh5'], 'fechanac'=>$arr['fechanach5']);
				
				$this->_SOL004($arr);
				
				break;

			case 'SOL005':
				$this->_SOL005($arr);
				break;
				
			case 'SOL006':
				
				
				$this->_SOL006($arr);
				break;
			
			case 'SOL008':
				$arrDia=array(1=>'un',21=>'ventiún',31=>'treinta y un');		
				
				$dialetra=new CNumeroaLetra();
				$dialetra->setNoMoneda(TRUE);
				$dia=intval(date('d'));
				if ($dia==1 or $dia==21 or $dia==31){
					$dialetraStr=utf8_decode($arrDia[$dia]);
				}else{
					$dialetra->setNumero(intval(date('d')));
					$dialetraStr=$dialetra->letra();
				}
				
				$this->load->model('admin_model','am');
				$idTipoPersonal=$this->persona->getIdTipoPersonal();
				$idTrabajador=$this->persona->getIdTrabajador();
				$paqueteSueldo=$this->pm->getSueldoAnual($idTrabajador);

				
				$val=$this->am->getFormatoSolicitud($arrSolicitud['id_tramite'],$idTipoPersonal);
				
				
				$fechaIngreso=$persona['fechaingreso'];
				
				if($this->persona->getSexo()=="M"){
					$genero="el ciudadano";
					$contratado="contratado";
					$adscrito="adscrito";
					$ao="o";
				}else{
					$genero="la ciudadana";
					$contratado="contratada";
					$adscrito="adscrita";
					$ao="a";
				}
				
				
				if ($tipoconstancia==2 or $tipoconstancia==3){
					$numero2=new CNumeroaLetra();
					$bfaLetras=new CNumeroaLetra();
					$bfaLetras->setNumero($paqueteSueldo->aguinaldo);
					$sueldoAnualLetras=new CNumeroaLetra();
					$sueldoAnualLetras->setNumero($paqueteSueldo->asignacion*12);
					$sueldoAnual=$paqueteSueldo->asignacion*12;
					$bfaJubiPen=$paqueteSueldo->asignacion*7.5+0.01;
					$bfaJubiPenLetras=new CNumeroaLetra();
					$bfaJubiPenLetras->setNumero($bfaJubiPen);
					$totalAnual=$paqueteSueldo->aguinaldo+$paqueteSueldo->vacacional+($paqueteSueldo->asignacion*12);
					$numero2->setNumero($totalAnual);
					
					////////////////////////// conversión números a letras parte décimal del sueldo anual //////////// 
					
					$centimos_letras_sueldo_anual = new numerosaletras();				
				    $pos2 = stripos($totalAnual, '.');
				    $centimos2 = substr($totalAnual,$pos2+1,2) ;		
				    $centimos_sueldo_anual = strtoupper($centimos_letras_sueldo_anual->convertir($centimos2));	
					/*
					echo "SUELDO ANUAL: $numero2</br>";			
				    echo "Monto: $totalAnual</br>";
				    echo "Posicion Digito: $pos2</br>";
				    echo "Centimos: $centimos2</br>";
				    echo "Centimos en Letras: $centimos_sueldo_anual</br>";
				    die();
				    */
				    //////////////// conversión números a letras parte décimal del sueldo anual jubilados ////////////
				    
					$centimos_letras_sueldo_anual_jubipen = new numerosaletras();				
				    $pos3 = stripos($sueldoAnual, '.');
				    $centimos3 = substr($sueldoAnual,$pos3+1,2) ;		
				    $centimos_sueldo_anual_jubipen = strtoupper($centimos_letras_sueldo_anual_jubipen->convertir($centimos3));	
					/*
					echo "BFA JUBIPEN: $numero2</br>";			
				    echo "Monto: $sueldoAnual</br>";
				    echo "Posicion Digito: $pos3</br>";
				    echo "Centimos: $centimos3</br>";
				    echo "Centimos en Letras: $centimos_sueldo_anual_jubipen</br>";
				    //die();
				    */					
				    //////////////// conversión números a letras parte décimal del bono fin de año jubilados ////////////
				    
				    $centimos_letras_bfa_jubipen = new numerosaletras();				
				    $pos4 = stripos($bfaJubiPen, '.');
				    $centimos4 = substr($bfaJubiPen,$pos4+1,2) ;		
				    $centimos_bfa_jubipen = strtoupper($centimos_letras_bfa_jubipen->convertir($centimos4));	
					/*
					echo "BFA JUBIPEN: $numero2</br>";			
				    echo "Monto: $bfaJubiPen</br>";
				    echo "Posicion Digito: $pos4</br>";
				    echo "Centimos: $centimos4</br>";
				    echo "Centimos en Letras: $centimos_bfa_jubipen</br>";
				    die();
				    */
				    ////////////////////////////////////////////////////////////////////////////////////
				    
					$val2=$this->am->getFormatoSolicitud($arrSolicitud['id_tramite'],$idTipoPersonal,2);
					$val2['formato']=str_replace("{contratado}",$contratado,$val2['formato']);
					$val2['formato']=str_replace("{ao}",$ao,$val2['formato']);
					$val2['formato']=str_replace("{adscrito}",$adscrito,$val2['formato']);
					$val2['formato']=str_replace("{cedula}",number_format($cedula,0,",","."),$val2['formato']);
					$val2['formato']=str_replace("{fechaingreso}",pgDate($fechaIngreso),$val2['formato']);
					//$val2['formato']=str_replace("{bonoauxilio}",number_format($paqueteSueldo->aguinaldo/2,2,",","."),$val2['formato']);
					$val2['formato']=str_replace("{bfa}",number_format($paqueteSueldo->aguinaldo,2,",","."),$val2['formato']);
					$val2['formato']=str_replace("{bfaletras}",utf8_decode($bfaLetras->letra()),$val2['formato']);
					
					$val2['formato']=str_replace("{bfajubipen}",number_format($bfaJubiPen,2,",","."),$val2['formato']);
					$val2['formato']=str_replace("{bfajubipenletras}",utf8_decode($bfaJubiPenLetras->letra().' '.$centimos_bfa_jubipen),$val2['formato']);
					
					
					$val2['formato']=str_replace("{bonovacacional}",number_format($paqueteSueldo->vacacional,2,",","."),$val2['formato']);
					$val2['formato']=str_replace("{totalanual}",number_format($totalAnual,2,',','.'),$val2['formato']);
					$val2['formato']=str_replace("{totalanualletras}",utf8_decode($numero2->letra().' '.$centimos_sueldo_anual),$val2['formato']);
					$val2['formato']=str_replace("{genero}",$genero,$val2['formato']);
					$val2['formato']=str_replace("{mes}",$meses[intval(date('m'))],$val2['formato']);
					$val2['formato']=str_replace("{dia}",date('d'),$val2['formato']);
					$val2['formato']=str_replace("{dialetra}",$dialetraStr,$val2['formato']);
					$val2['formato']=str_replace("{sueldoanual}",number_format($sueldoAnual,2,",","."),$val2['formato']);
					$val2['formato']=str_replace("{sueldoanualletras}",utf8_decode($sueldoAnualLetras->letra().' '.$centimos_sueldo_anual_jubipen),$val2['formato']);
					$val2['formato']=str_replace("{cestaticket}",number_format($montoCestaTicket,2,",","."),$val2['formato']);
					$val2['formato']=str_replace("{cestaticketletras}",utf8_decode($cestaTicketLetras->letra()),$val2['formato']);
					$val2['md5']=md5($cedula.$paqueteSueldo->aguinaldo.$paqueteSueldo->vacacional.$paqueteSueldo->asignacion);
					
					foreach($persona as $campo=>$valor){
						$val2['formato']=str_replace("{".$campo."}",$valor,$val2['formato']);
					}
					
					$arr['formato2']=$val2['formato'];
					$arr['firma2']=$val2['firma'];
					$arr['md52']=$val2['md5'];
				}

				////////////////////////// conversión números a letras parte décimal sueldo anual ////////////
				//$monto = '4.629,00';
				$pullas_letras = new numerosaletras();				
				$pos = stripos($monto, ',');
				$pullas = substr($monto,$pos+1,2) ;		
				$centimos = strtoupper($pullas_letras->convertir($pullas));		
				/*
				echo "Monto: $monto</br>";
				echo "Pos: $pos</br>";
				echo "Pullas: $pullas</br>";
				echo "Pullas en Letras: $centimos</br>";
				die();
			    */
				///////////////////////////////////////////////////////////////////////////////////
				
				$val['formato']=str_replace("{year}",'2012',$val['formato']);
				$val['formato']=str_replace("{contratado}",$contratado,$val['formato']);
				$val['formato']=str_replace("{adscrito}",$adscrito,$val['formato']);
				$val['formato']=str_replace("{ao}",$ao,$val['formato']);
				$val['formato']=str_replace("{cedula}",number_format($cedula,0,",","."),$val['formato']);
				$val['formato']=str_replace("{fechaingreso}",pgDate($fechaIngreso),$val['formato']);
				$val['formato']=str_replace("{montoletras}",utf8_decode($numero->letra()),$val['formato']);
				$val['formato']=str_replace("{centimos}",utf8_decode($centimos),$val['formato']);
				$val['formato']=str_replace("{monto}",$monto,$val['formato']);
				$val['formato']=str_replace("{dia}",date('d'),$val['formato']);
				$val['formato']=str_replace("{mes}",$meses[intval(date('m'))],$val['formato']);
				$val['formato']=str_replace("{dialetra}",$dialetraStr,$val['formato']);
				$val['formato']=str_replace("{genero}",$genero,$val['formato']);
				$val['formato']=str_replace("{cestaticket}",number_format($montoCestaTicket,2,",","."),$val['formato']);
				$val['formato']=str_replace("{cestaticketletras}",utf8_decode($cestaTicketLetras->letra()),$val['formato']);
				
				$html="<span style='margin:0px 0px 10px 1cm;width:680px;text-align:justify;font-family:Arial, Verdana, sans-serif;'>".($val['formato'])."</span>";

				foreach($persona as $campo=>$valor){
					$val['formato']=str_replace("{".$campo."}",$valor,$val['formato']);
				}				
				
				$arr['formato']=$val['formato'];
				$arr['firma']=$val['firma'];
				$cargo=$persona['cargo'];
				$arr['md5']=$idSolicitud.$monto.$cedula.$arr['nombres'].$arr['apellidos'].$cargo;

				$arr['cedula']=$persona['cedula'];
				$arr['tipoconstancia']=$tipoconstancia;
				//var_dump($arr);
				
				$this->_SOL008($arr);
				break;
			
			case 'SOL009':
				$this->_SOL009($arr);
				break;
			case 'SOL003':
				$arr['telefono']=$telefono;
				$arr['telefonohab']=$telefonohab;
				$arr['telefonocel']=$telefonocel;
				$arr['extension']=$extension;
				$this->_SOL003($arr);
				
				break;
			case 'SOL001':
				$this->_SOL001($arr);
				break;
			case 'AUT001':
				$arr['cedula']=$cedula;
				$arr['nombres']=$this->persona->getNombres();
				$arr['apellidos']=$this->persona->getApellidos();
				$arr['nombre']=$this->persona->getNombre();
				$arr['tipopersonal']=$this->persona->getTipoPersonal();
				
				$this->_AUT001($arr);
				break;
			case 'SOL007':
				$arr['telefono']=$telefono;
				$arr['telefonohab']=$telefonohab;
				$arr['telefonocel']=$telefonocel;
				$arr['extension']=$extension;
				$arr['hijos'][]=array('nombres'=>$arr['nombreh1'], 'fechanac'=>$arr['fechanach1']);
				$arr['hijos'][]=array('nombres'=>$arr['nombreh2'], 'fechanac'=>$arr['fechanach2']);
				$this->_SOL007($arr);
				break;
			case 'SOL011':
				$arr['telefono']=$telefono;
				$arr['telefonohab']=$telefonohab;
				$arr['telefonocel']=$telefonocel;
				$arr['extension']=$extension;
				$arr['hijos'][]=array('nombres'=>$arr['nombreh1'], 'fechanac'=>$arr['fechanach1']);
				$arr['hijos'][]=array('nombres'=>$arr['nombreh2'], 'fechanac'=>$arr['fechanach2']);
				$this->_SOL011($arr);
				break;
			default:
				echo "Error... No hay reporte definido";
				break;
		}
	}
	
	function _SOL006($arr){
		$this->pdf->AddPage();
		$this->pdf->SetFont('helvetica', '', 8);
		$this->pdf->Cell(160,2,"Fecha de Solicitud: ".$arr['fechasimplesistema'],"",0,"R");
		$this->pdf->SetFont('helvetica', 'B', 10);
		
        $this->pdf->ln(8);
        $this->pdf->writeHTML("<span style=\"text-align:center;\">Solicitud de Vacaciones</span>", true, 0, true, true);
        
		$this->pdf->ln(8);		
		$this->pdf->SetFont('helvetica', '', 10);
        $this->pdf->Cell(30,2,"Cédula:","",0,"L");
        $this->pdf->Cell(140,2,"Nombres y Apellidos:","",0,"L");
        $this->pdf->ln();
        $this->pdf->Cell(30,2,number_format($arr['cedula'],0,",","."),"",0,"L");
        $this->pdf->Cell(140,2,$arr['nombres']." ".$arr['apellidos'],"",0,"L");
        $this->pdf->ln();
        $this->pdf->ln();
        $this->pdf->Cell(70,2,"Cargo:","",0,"L");
        $this->pdf->Cell(70,2,"Adscrito a:","",0,"L");
        
        $this->pdf->ln();
        $this->pdf->Cell(70,2,$arr['cargo'],"",0,"L");
        $this->pdf->Cell(70,2,$arr['adscripcion'],"",0,"L");
        
        $this->pdf->ln();
        $this->pdf->ln();
        $this->pdf->Cell(70,2,"Fecha de Ingreso:","",0,"L");
        
        
        $this->pdf->ln();
        $this->pdf->Cell(70,2,$arr['fechaingreso'],"",0,"L");
        $this->pdf->ln();        
        $this->pdf->ln();
        
        $this->pdf->Cell(70,2,"Nro de días solicitados:____________________","",0,"L");
        
        
        $this->pdf->ln();
        $this->pdf->ln();
        $this->pdf->Cell(70,2,"Periodos:_____________________","",0,"L");
        $this->pdf->Cell(70,2,"Antigüedad A.P.N:_____________________","",0,"L");
        
        $this->pdf->ln();
        $this->pdf->ln();
        $this->pdf->ln();
        $this->pdf->ln();
        $this->pdf->ln();
        $this->pdf->ln();
        $this->pdf->Cell(140,2,"__________________________________","",0,"C");
        $this->pdf->ln();
        $this->pdf->Cell(140,2,"Firma del Solicitante","",0,"C");
        
        $this->pdf->ln();
        $this->pdf->Cell(140,2,"***********************************************************************************************************************************************","",0,"C");
        $this->pdf->ln();
        $this->pdf->ln();
        $this->pdf->Cell(140,2,"APROBACIÓN","",0,"C");
        
        $this->pdf->ln();
        $this->pdf->ln();
        $this->pdf->ln();
        $this->pdf->ln();
        $this->pdf->Cell(60,2,"________________________________","",0,"C");
        $this->pdf->Cell(20,2,"","",0,"C");
        $this->pdf->Cell(60,2,"________________________________","",0,"C");
        
        $this->pdf->ln();
        $this->pdf->Cell(60,2,$arr['jefeinmediato'],"",0,"C");
        $this->pdf->Cell(20,2,"","",0,"C");
        $this->pdf->Cell(60,2,$arr['director'],"",0,"C");
        $this->pdf->ln();
        $this->pdf->Cell(60,2,"Jefe Inmediato","",0,"C");
        $this->pdf->Cell(20,2,"","",0,"C");
        $this->pdf->Cell(60,2,"Director","",0,"C");
        $this->pdf->ln();
		$this->pdf->Cell(140,2,"***********************************************************************************************************************************************","",0,"C");
        
		$this->pdf->ln();
        $this->pdf->ln();
        $this->pdf->Cell(140,2,"SOLO PARA SER LLENADO POR LA DIVISIÓN DE REGISTRO Y CONTROL","",0,"C");
        
        $this->pdf->ln();
        $this->pdf->ln();
        $this->pdf->Cell(60,2,"Días otorgados:_________","",0,"L");
        
        $this->pdf->Cell(30,2,"Continuos____","",0,"L");
        $this->pdf->Cell(30,2,"Hábiles____","",0,"L");
        
        $this->pdf->ln();
        $this->pdf->ln();
        $this->pdf->Cell(140,2,"Antigüedad en la A.P.N.:________________________  Fecha de Inicio:________________________","",0,"L");
        $this->pdf->ln();
        $this->pdf->ln();
        $this->pdf->Cell(140,2,"Fecha de reincorporación:_____________________    Lapsos otorgados:________________________","",0,"L");
        $this->pdf->ln();
        $this->pdf->ln();
        $this->pdf->Cell(140,2,"Lapsos pendientes:_____________________    ","",0,"L");
        $this->pdf->ln();
        $this->pdf->ln();
        $this->pdf->Cell(160,2,"Firma del Analista:_____________________    ","",0,"R");
        $this->pdf->ln();
        $this->pdf->ln();
        $this->pdf->Cell(160,2,"Calculado por:_____________________    ","",0,"L");
        $this->pdf->ln();
        $this->pdf->Cell(160,2,"Revisado por:_____________________    ","",0,"L");
        $this->pdf->ln();
        $this->pdf->Cell(160,2,"Elaborado por:_____________________    ","",0,"L");
        
        $this->pdf->ln();
        $this->pdf->ln();
     
		$this->pdf->Output($arr['cedula'].'_SOL006.pdf', 'D');
		
	}
	
	function _SOL011($arr){
		$this->pdf->AddPage();
		$this->pdf->SetFont('helvetica', 'B', 12);
        $this->pdf->ln(2);
        $this->pdf->writeHTML("<span style=\"text-align:center;\">Solicitud de Bono Guardería</span>", true, 0, true, true);
        $this->pdf->ln();
        $this->pdf->SetFont('helvetica', 'N', 10);
        $this->pdf->Cell(100,2,"Fecha de recepción: ".$arr['fechasimplesistema']);
		$this->pdf->ln(8);
		$this->pdf->SetFont('helvetica', 'B', 10);
		$this->pdf->Cell(100,2,"DATOS DEL SOLICITANTE");
		$this->pdf->ln(8);		
		$this->pdf->SetFont('helvetica', '', 10);
        $this->pdf->Cell(30,2,"Cédula:","",0,"L");
        $this->pdf->Cell(140,2,"Nombres:","",0,"L");
        $this->pdf->ln();
        $this->pdf->Cell(30,2,number_format($arr['cedula'],0,",","."),"",0,"L");
        $this->pdf->Cell(140,2,$arr['nombre'],"",0,"L");
        $this->pdf->ln();
        $this->pdf->ln();
        
        $this->pdf->Cell(56.66,2,'Nº Nómina',"",0,"L");
        //$this->pdf->Cell(56.66,2,'Teléfono Hab.',"",0,"L");
        $this->pdf->Cell(56.66,2,'Celular',"",0,"L");
        $this->pdf->ln();
        $this->pdf->Cell(56.66,2,$arr['codigonomina'],"",0,"L");
        //$this->pdf->Cell(56.66,2,$arr['telefonohab'],"",0,"L");
        $this->pdf->Cell(56.66,2,$arr['telefonocel'],"",0,"L");
        $this->pdf->ln();
        $this->pdf->ln();
        
        $this->pdf->Cell(150,2,"Dirección de Adscripción");
		$this->pdf->ln();
		$this->pdf->Cell(150,2,$arr['adscripcion']);
	  	$this->pdf->ln();
        $this->pdf->ln();
        
        $this->pdf->Cell(56.66,2,'Teléfono Ofc.',"",0,"L");
        $this->pdf->Cell(56.66,2,'Extensión',"",0,"L");
        
        $this->pdf->ln();
        $this->pdf->Cell(56.66,2,$arr['telefono'],"",0,"L");
        $this->pdf->Cell(56.66,2,$arr['extension'],"",0,"L");        
        $this->pdf->ln();
        $this->pdf->ln();
       
		$this->pdf->Cell(50,2,"Tipo de Trabajador");
		$this->pdf->ln();
		$this->pdf->Cell(50,2,$arr['tipopersonal']);
		
		$this->pdf->ln();
        $this->pdf->ln();
		
		$this->pdf->SetFont('helvetica', 'B', 10);      
		$this->pdf->Cell(50,2,"DATOS DE(LOS) REPRESENTADO(S)");
		$this->pdf->SetFont('helvetica', '', 10);
		
		$this->pdf->ln();
		$this->pdf->Cell(90,2,"Nombres y apellidos");
		$this->pdf->Cell(50,2,"Fecha de nacimiento");
		$this->pdf->ln();
		$this->pdf->Cell(90,2,$arr['nombreh1']);
		$this->pdf->Cell(50,2,$arr['fechanach1']);
		$this->pdf->ln();
		$this->pdf->Cell(90,2,$arr['nombreh2']);
		$this->pdf->Cell(50,2,$arr['fechanach2']);
		
		 $this->pdf->ln(8);
		$this->pdf->SetFont('helvetica', 'B', 10);
		$this->pdf->Cell(100,2,"DATOS DEL CENTRO DE EDUCACIÓN INICIAL");
		$this->pdf->ln(8);
		$this->pdf->SetFont('helvetica', '', 10);
        $this->pdf->Cell(100,2,"Nombre del Instituto:","",0,"L");
        $this->pdf->Cell(40,2,"Teléfono:","",0,"L");
        $this->pdf->ln();
        $this->pdf->Cell(100,2,$arr['nombrecolegio'],"",0,"L");
        $this->pdf->Cell(40,2,$arr['telefonocolegio'],"",0,"L");
        $this->pdf->ln();
        $this->pdf->ln();

        $this->pdf->Cell(100,2,"Nro Registro en MPPPE:","",0,"L");
        $this->pdf->Cell(40,2,"Cuenta Bancaria del Instituto educativo:","",0,"L");
        $this->pdf->ln();
        $this->pdf->Cell(100,2,$arr['mpppe'],"",0,"L");
        $this->pdf->Cell(40,2,$arr['cuentabancaria'],"",0,"L");
        $this->pdf->ln();
        $this->pdf->ln();
		
		
		$this->pdf->ln(20);
		$this->pdf->Cell(40,2,'RECAUDOS SOLICITADOS:');
		$this->pdf->ln();
		$this->pdf->SetFont('helvetica', '', 10);

		$html=<<<EOD
<ul>
	<li>___ Constacia de Inscripción</li>
	<li>___ Factura de Mensualidad Original</li>
	<li>___ Cédula de Identidad del Funcionario(a)</li>
	<li>___ Carnet del Funcionario</li>
	<li>___ Resolución o Contrato (Nuevo Ingreso)</li>
	<li>___ Partida o Constancia de Nacimiento</li>
	<li>___ Registro Mercantil del Plantel</li>
	<li>___ Constancia de Inscripción del Plantel en el MPPPE</li>
</ul>
EOD;
		$this->pdf->writeHTML($html, true, false, true, false, '');
		$this->pdf->ln(12);
		$this->pdf->Cell(40,2,"Observaciones:","",0,"L");
        $this->pdf->ln();
        $this->pdf->MultiCell(160,2,$arr['observaciones'],"","L",FALSE,0);
		
        $this->pdf->Output($arr['cedula'].'_SOL011.pdf', 'D');
	}
	
	function _SOL007($arr){
		$this->pdf->AddPage();
		$this->pdf->SetFont('helvetica', 'B', 12);
        $this->pdf->ln(2);
        $this->pdf->writeHTML("<span style=\"text-align:center;\">Solicitud de Bono Especial Hijos Excepcionales</span>", true, 0, true, true);
        
        $this->pdf->ln(8);
		$this->pdf->SetFont('helvetica', 'B', 10);
		$this->pdf->Cell(100,2,"DATOS DEL BENEFICIARIO(A)");
		$this->pdf->ln(8);
		$this->pdf->SetFont('helvetica', '', 10);
        $this->pdf->Cell(30,2,"Cédula:","",0,"L");
        $this->pdf->Cell(140,2,"Nombres:","",0,"L");
        $this->pdf->ln();
        $this->pdf->Cell(30,2,number_format($arr['cedula'],0,",","."),"",0,"L");
        $this->pdf->Cell(140,2,$arr['nombre'],"",0,"L");
        $this->pdf->ln();
        $this->pdf->ln();
        
        $this->pdf->Cell(56.66,2,'Nº Nómina',"",0,"L");
        //$this->pdf->Cell(56.66,2,'Teléfono Hab.',"",0,"L");
        $this->pdf->Cell(56.66,2,'Celular',"",0,"L");
        $this->pdf->ln();
        $this->pdf->Cell(56.66,2,$arr['codigonomina'],"",0,"L");
        //$this->pdf->Cell(56.66,2,$arr['telefonohab'],"",0,"L");
        $this->pdf->Cell(56.66,2,$arr['telefonocel'],"",0,"L");
        $this->pdf->ln();
        $this->pdf->ln();
        
        $this->pdf->Cell(150,2,"Dirección de Adscripción");
		$this->pdf->ln();
		$this->pdf->Cell(150,2,$arr['adscripcion']);
	  	$this->pdf->ln();
        $this->pdf->ln();
        
        $this->pdf->Cell(56.66,2,'Teléfono Ofc.',"",0,"L");
        $this->pdf->Cell(56.66,2,'Extensión',"",0,"L");
        
        $this->pdf->ln();
        $this->pdf->Cell(56.66,2,$arr['telefono'],"",0,"L");
        $this->pdf->Cell(56.66,2,$arr['extension'],"",0,"L");        
        $this->pdf->ln();
        $this->pdf->ln();
       
		$this->pdf->Cell(50,2,"Tipo de Trabajador");
		$this->pdf->ln();
		$this->pdf->Cell(50,2,$arr['tipopersonal']);
		
		$this->pdf->ln();
        $this->pdf->ln();
        
		
		$this->pdf->SetFont('helvetica', 'B', 10);      
		$this->pdf->Cell(50,2,"DATOS DEL MENOR");
		$this->pdf->SetFont('helvetica', '', 10);
		
		$this->pdf->ln();
		$this->pdf->Cell(90,2,"Nombres y apellidos");
		$this->pdf->Cell(50,2,"Fecha de nacimiento");
		$this->pdf->ln();
		$this->pdf->Cell(90,2,$arr['nombreh1']);
		$this->pdf->Cell(50,2,$arr['fechanach1']);
		$this->pdf->ln();
		$this->pdf->Cell(90,2,$arr['nombreh2']);
		$this->pdf->Cell(50,2,$arr['fechanach2']);
		
		
		$this->pdf->SetFont('helvetica', 'B', 10);
        $this->pdf->ln(30);
		$this->pdf->Cell(40,2,'Firma del Funcionario:');
		$this->pdf->Cell(40,2,'','B');
		$this->pdf->ln(20);
		$this->pdf->Cell(40,2,'REQUISITOS:');
		$this->pdf->ln();
		$this->pdf->SetFont('helvetica', '', 10);
		
		


		$html=<<<EOD
<ul>
	<li>INFORME MÉDICO DETALLADO</li>
	<li>FOTOCOPIA DEL CARNET DEL TRABAJADOR</li>
	<li>FOTOCOPIA DE LA CÉDULA DE IDENTIDAD</li>
	<li>COPIA DE LA PARTIDA DE NACIMIENTO DEL HIJO(A)</li>
	<li>COPIA DEL CONTRATO DE TRABAJO (EN CASO DE SER PERSONAL CONTRATADO)</li>
</ul>
EOD;
		$this->pdf->writeHTML($html, true, false, true, false, '');
		$this->pdf->ln();
		
		
		
		
		/*$this->pdf->Cell(150,2,'.- FOTOCOPIA DEL CARNET Y CÉDULA DE IDENTIDAD DEL TRABAJADOR');
		$this->pdf->ln();
		$this->pdf->Cell(150,2,'.- FOTOCOPIA DE LA  PARTIDA  DE NACIMIENTO DEL HIJO (A)');
		$this->pdf->ln();
		$this->pdf->Cell(150,2,'.- COPIA  DEL CONTRATO DE TRABAJO ( SI ES PERSONAL CONTRATADO)');
		*/
		$this->pdf->ln();
		$this->pdf->ln();
		$this->pdf->ln(14);
		$this->pdf->SetFont('helvetica', '', 10);
		$this->pdf->MultiCell(160,2,$arr['fechasistema'],"","L",FALSE,0);
		
		
        $this->pdf->Output($arr['cedula'].'_SOL007.pdf', 'D');
	}
	
	function _AUT001($arr){
		$this->pdf->SetMargins(30,25);
		$this->pdf->AddPage();
		$this->pdf->SetFont('helvetica', '', 12);
		$this->pdf->MultiCell(170,2,$arr['fechasistema'],"","R");		
		$this->pdf->SetFont('helvetica', 'B', 14);
		
		$this->pdf->ln(25);
    
        $this->pdf->MultiCell(170,2,"AUTORIZACIÓN","","C");
        $this->pdf->ln();
        $this->pdf->ln();
        $this->pdf->ln();

        $this->pdf->SetFont('helvetica', '', 12);
        switch ($arr['tipocuenta']){
        	case 1:
        		$cuenta="corriente";
        		break;
        	case 2:
        		$cuenta="de ahorros";
        		break;
        }
        $cuentafide=$arr['cuenta_fideicomiso'];
        $this->pdf->MultiCell(150, 2, "Yo ".$arr['nombre'].", mayor de edad, civilmente hábil, de este domicilio, titular de la Cédula de Identidad N° ".$arr['cedula'].", y personal ".$arr['tipopersonal'].", por medio de la presente autorizo al BANCO DE VENEZUELA S.A., BANCO UNIVERSAL, para que asocie mi cuenta $cuenta número: $cuentafide, al Fondo de Prestaciones  Sociales del Ministerio del Poder Popular para Relaciones Exteriores que se lleva por esa Institución. Esto a los fines de que me sean depositadas en dicha cuenta los aportes correspondientes al fideicomiso.\n", "", 'J', 0, 1, '', '', true);
        $this->pdf->ln(12);
        $this->pdf->MultiCell(150, 2, "Atentamente;", "", 'C', 0, 1, '', '', true);
        $this->pdf->ln();
        $this->pdf->ln();
        $this->pdf->MultiCell(150, 2, "Firma", "", 'C', 0, 1, '', '', true);
        $this->pdf->ln();
        $this->pdf->ln();
        $this->pdf->MultiCell(150, 2, "___________________________", "", 'C', 0, 1, '', '', true);
        $this->pdf->ln();
        $this->pdf->MultiCell(120, 2, "", "", 'L', 0, 0, '', '', true);
        $this->pdf->MultiCell(33, 33, 'Pulgar Derecho', 1, 'J', 0, 1, '', '', true, 0, false, true, 40, 'B');
        $this->pdf->Output($arr['cedula'].'_AUT001.pdf', 'D');
	}
	
	function _SOL001($arr){
		$this->pdf->SetMargins(30,25);
		$this->pdf->AddPage();
		$this->pdf->SetFont('helvetica', 'B', 12);
        $this->pdf->ln();
        $this->pdf->writeHTML("<span style=\"text-align:center;\">COMPUTO VACACIONAL</span>", true, 0, true, true);
        $this->pdf->SetFont('helvetica', '', 10);
        $this->pdf->ln();
        $this->pdf->Cell(160,2,$arr['fechasistema'],"",0,"R");
        $this->pdf->ln();$this->pdf->ln();$this->pdf->ln();$this->pdf->ln();
        $this->pdf->Cell(30,2,"Cédula:","",0,"L");
        $this->pdf->Cell(140,2,"Nombres:","",0,"L");
        $this->pdf->ln();
        $this->pdf->Cell(30,2,number_format($arr['cedula'],0,",","."),"",0,"L");
        $this->pdf->Cell(140,2,$arr['nombre'],"",0,"L");
        $this->pdf->ln();$this->pdf->ln();
        $this->pdf->Cell(140,2,"Cargo:","",0,"L");
        $this->pdf->ln();
        $this->pdf->Cell(140,2,"XXX cargo XXX","",0,"L");
        $this->pdf->ln();$this->pdf->ln();
        $this->pdf->Cell(120,2,"Adscripción:","",0,"L");
        $this->pdf->cell(30,2,"Extensión:","",0,"L");
        $this->pdf->ln();
        $this->pdf->Cell(120,2,"XXX Adscripción XXX","",0,"L");
        $this->pdf->cell(30,2,"xx ext xxx","",0,"L");
        $this->pdf->ln();$this->pdf->ln();
        $this->pdf->Cell(50,2,"Fecha de Ingreso:","",0,"L");
        $this->pdf->cell(50,2,"Fecha de Egreso:","",0,"L");
        $this->pdf->ln();
        $this->pdf->Cell(50,2,"##-##-####","",0,"L");
        $this->pdf->cell(50,2,"##-##-####","",0,"L");
        $this->pdf->ln();$this->pdf->ln();
        $this->pdf->Cell(150,2,"Antigüedad en la Administración Pública Nacional:","",0,"L");
        
        $this->pdf->Cell(150,2,"XX Años","",0,"L");
        $this->pdf->ln();$this->pdf->ln();$this->pdf->ln();$this->pdf->ln();
        $this->pdf->Cell(40,2,"","",0,"C");
        $this->pdf->Cell(70,2,"","B",0,"C");
        $this->pdf->ln();
        $this->pdf->Cell(40,2,"","",0,"C");
        $this->pdf->Cell(70,2,"Firma del Funcionario Solicitante","",0,"C");
        
        
        $this->pdf->Output($arr['cedula'].'_SOL001.pdf', 'D');
	}
	
	function _SOL003($arr){
		$this->pdf->AddPage();
		$this->pdf->SetFont('helvetica', 'B', 12);
        $this->pdf->ln(2);
        $this->pdf->writeHTML("<span style=\"text-align:center;\">Solicitud de Bonificación por Matrimonio</span>", true, 0, true, true);
        
        $this->pdf->ln(8);
		$this->pdf->SetFont('helvetica', 'B', 10);
		$this->pdf->Cell(100,2,"DATOS DEL BENEFICIARIO(A)");
		$this->pdf->ln(8);
		$this->pdf->SetFont('helvetica', '', 10);
        $this->pdf->Cell(30,2,"Cédula:","",0,"L");
        $this->pdf->Cell(140,2,"Nombres:","",0,"L");
        $this->pdf->ln();
        $this->pdf->Cell(30,2,number_format($arr['cedula'],0,",","."),"",0,"L");
        $this->pdf->Cell(140,2,$arr['nombre'],"",0,"L");
        $this->pdf->ln();
        $this->pdf->ln();
        
        $this->pdf->Cell(56.66,2,'Nº Nómina',"",0,"L");
        //$this->pdf->Cell(56.66,2,'Teléfono Hab.',"",0,"L");
        $this->pdf->Cell(56.66,2,'Celular',"",0,"L");
        $this->pdf->ln();
        $this->pdf->Cell(56.66,2,$arr['codigonomina'],"",0,"L");
        //$this->pdf->Cell(56.66,2,$arr['telefonohab'],"",0,"L");
        $this->pdf->Cell(56.66,2,$arr['telefonocel'],"",0,"L");
        $this->pdf->ln();
        $this->pdf->ln();
        
        $this->pdf->Cell(150,2,"Dirección de Adscripción");
		$this->pdf->ln();
		$this->pdf->Cell(150,2,$arr['adscripcion']);
	  	$this->pdf->ln();
        $this->pdf->ln();
        
        $this->pdf->Cell(56.66,2,'Teléfono Ofc.',"",0,"L");
        $this->pdf->Cell(56.66,2,'Extensión',"",0,"L");
        
        $this->pdf->ln();
        $this->pdf->Cell(56.66,2,$arr['telefono'],"",0,"L");
        $this->pdf->Cell(56.66,2,$arr['extension'],"",0,"L");        
        $this->pdf->ln();
        $this->pdf->ln();
       
        $this->pdf->Cell(50,2,"Nro de Cuenta");
		$this->pdf->Cell(50,2,"Tipo de Trabajador");
		$this->pdf->ln();
		$this->pdf->Cell(50,2,$arr['cuentabancaria']);
		$this->pdf->Cell(50,2,$arr['tipopersonal']);		
		
		
		$this->pdf->ln();
        $this->pdf->ln();
        $this->pdf->Cell(56.66,2,"Fecha de Matrimonio","",0,"L");
        $this->pdf->ln();
        $this->pdf->Cell(56.66,2,$arr['fechamatrimonio'],"",0,"L");
        
        $this->pdf->ln();
        $this->pdf->ln();
        $this->pdf->SetFont('helvetica', 'B', 10);
        $this->pdf->Cell(170,2,"Datos para el Pasaje Aereo");
		$this->pdf->ln();$this->pdf->ln();
		$this->pdf->SetFont('helvetica', '', 10);
		$this->pdf->Cell(170,2,"Apellidos y nombres del Conyuge:");
		$this->pdf->ln();
		$this->pdf->Cell(170,2,$arr['nombres_conyuge']." ".$arr['apellidos_conyuge']);
		$this->pdf->ln();
        $this->pdf->ln();
        $this->pdf->Cell(80,2,"Destino (Ruta)");
        $this->pdf->Cell(35,2,"Fecha de Salida");
        $this->pdf->Cell(35,2,"Fecha de Retorno");
		$this->pdf->ln();
		$this->pdf->Cell(80,2,$arr['destinoruta']);
        $this->pdf->Cell(35,2,$arr['fechasalida']);
        $this->pdf->Cell(35,2,$arr['fecharetorno']);
        
     	$this->pdf->SetFont('helvetica', 'B', 10);
        $this->pdf->ln(30);
		$this->pdf->Cell(40,2,'Firma del Funcionario:');		
		$this->pdf->Cell(60,2,'','B');
		$this->pdf->ln();$this->pdf->ln();
		$this->pdf->Cell(80,2,$arr['fechasistema']);
		
		$this->pdf->ln(20);
		$this->pdf->Cell(40,2,'REQUISITOS:');
		$this->pdf->ln();
		$this->pdf->SetFont('helvetica', '', 10);
				$html=<<<EOD
<ul>
	<li>FOTOCOPIA DEL CARNET DEL TRABAJADOR</li>
	<li>FOTOCOPIA DE CÉDULA DE IDENTIDAD DEL TRABAJADOR Y CONYUGE</li>
	<li>COPIA DEL ACTA DE MATRIMONIO</li>
	<li>CARTA DE SOLICITUD DEL BENEFICIO INDICANDO DESTINO Y FECHA DE SALIDA Y RETORNO</li>
	<li>COPIA  DEL CONTRATO DE TRABAJO ( SI ES PERSONAL CONTRATADO)</li>
</ul>
EOD;
		$this->pdf->writeHTML($html, true, false, true, false, '');
		
		$this->pdf->SetFont('helvetica', 'B', 8);
		$str='NOTA: Deberá retirar su pago por la Coordinación de Habilitado. Se recomienda esperar 30 días continuos para acudir a la unidad mencionada. En el caso de los pasajes aéreos, será llamado pertinentemente por la Coordinación de Bienestar Social para que retire los mismos.';
		$this->pdf->MultiCell(160,2,$str,"","L",FALSE,0);
        
        $this->pdf->Output($arr['cedula'].'_SOL003.pdf', 'D');
        
	}
	
	function _SOL009($arr){
		$this->pdf->AddPage();
		$this->pdf->SetFont('helvetica', 'B', 12);
        $this->pdf->ln(2);
        $this->pdf->writeHTML("<span style=\"text-align:center;\">Solicitud de Prima por Hijos</span>", true, 0, true, true);
        
        $this->pdf->ln(8);
		$this->pdf->SetFont('helvetica', 'B', 10);
		$this->pdf->Cell(100,2,"DATOS DEL BENEFICIARIO(A)");
		$this->pdf->ln(8);
		$this->pdf->SetFont('helvetica', '', 10);
        $this->pdf->Cell(30,2,"Cédula:","",0,"L");
        $this->pdf->Cell(140,2,"Nombres:","",0,"L");
        $this->pdf->ln();
        $this->pdf->Cell(30,2,number_format($arr['cedula'],0,",","."),"",0,"L");
        $this->pdf->Cell(140,2,$arr['nombre'],"",0,"L");
        $this->pdf->ln();
        $this->pdf->ln();
        
        $this->pdf->Cell(56.66,2,'Nº Nómina',"",0,"L");
        //$this->pdf->Cell(56.66,2,'Teléfono Hab.',"",0,"L");
        $this->pdf->Cell(56.66,2,'Celular',"",0,"L");
        $this->pdf->ln();
        $this->pdf->Cell(56.66,2,$arr['codigonomina'],"",0,"L");
        //$this->pdf->Cell(56.66,2,$arr['telefonohab'],"",0,"L");
        $this->pdf->Cell(56.66,2,$arr['telefonocel'],"",0,"L");
        $this->pdf->ln();
        $this->pdf->ln();
        
        $this->pdf->Cell(150,2,"Dirección de Adscripción");
		$this->pdf->ln();
		$this->pdf->Cell(150,2,$arr['adscripcion']);
	  	$this->pdf->ln();
        $this->pdf->ln();
        
        $this->pdf->Cell(56.66,2,'Teléfono Ofc.',"",0,"L");
        $this->pdf->Cell(56.66,2,'Extensión',"",0,"L");
        
        $this->pdf->ln();
        $this->pdf->Cell(56.66,2,$arr['telefono'],"",0,"L");
        $this->pdf->Cell(56.66,2,$arr['extension'],"",0,"L");        
        $this->pdf->ln();
        $this->pdf->ln();
       
		$this->pdf->Cell(50,2,"Tipo de Trabajador");
		$this->pdf->ln();
		$this->pdf->Cell(50,2,$arr['tipopersonal']);
		
		$this->pdf->ln();
        $this->pdf->ln();

        $this->pdf->SetFont('helvetica', 'B', 10);      
		$this->pdf->Cell(50,2,"DATOS DEL CÓNYUGE");
		$this->pdf->SetFont('helvetica', '', 10);
		$this->pdf->ln();
		$this->pdf->Cell(30,2,"Cédula");
		$this->pdf->Cell(140,2,"Nombres y apellidos");
		$this->pdf->ln();
		$this->pdf->Cell(30,2,number_format($arr['cedula_conyuge'],0,",","."));
		$this->pdf->Cell(140,2,$arr['nombres_conyuge']." ".$arr['apellidos_conyuge']);
		
		$this->pdf->ln();$this->pdf->ln();
		
		$this->pdf->Cell(50,2,"Lugar donde trabaja");
		$this->pdf->ln();
		
		$this->pdf->Cell(50,2,$arr['lugartrabajo']);
		$this->pdf->ln();$this->pdf->ln();
		
		$this->pdf->SetFont('helvetica', 'B', 10);      
		$this->pdf->Cell(50,2,"DATOS DEL MENOR");
		$this->pdf->SetFont('helvetica', '', 10);
		
		$this->pdf->ln();
		$this->pdf->Cell(90,2,"Nombres y apellidos");
		$this->pdf->Cell(50,2,"Fecha de nacimiento");
		$this->pdf->ln();
		$this->pdf->Cell(90,2,$arr['nombreh1']);
		$this->pdf->Cell(50,2,$arr['fechanach1']);
		$this->pdf->ln();
		$this->pdf->Cell(90,2,$arr['nombreh2']);
		$this->pdf->Cell(50,2,$arr['fechanach2']);
		
		
		$this->pdf->SetFont('helvetica', 'B', 10);
        $this->pdf->ln(30);
		$this->pdf->Cell(40,2,'Firma del Funcionario:');
		$this->pdf->Cell(40,2,'','B');
		$this->pdf->ln(20);
		$this->pdf->Cell(40,2,'REQUISITOS:');
		$this->pdf->ln();
		$this->pdf->SetFont('helvetica', '', 10);
		
		


		$html=<<<EOD
<ul>
	<li>FOTOCOPIA DEL CARNET Y CÉDULA DE IDENTIDAD DEL TRABAJADOR</li>
	<li>FOTOCOPIA DE LA  PARTIDA  DE NACIMIENTO DEL HIJO (A)</li>
	<li>COPIA  DEL CONTRATO DE TRABAJO ( SI ES PERSONAL CONTRATADO)</li>
	
</ul>
EOD;
		$this->pdf->writeHTML($html, true, false, true, false, '');

		
		
		
		/*$this->pdf->Cell(150,2,'.- FOTOCOPIA DEL CARNET Y CÉDULA DE IDENTIDAD DEL TRABAJADOR');
		$this->pdf->ln();
		$this->pdf->Cell(150,2,'.- FOTOCOPIA DE LA  PARTIDA  DE NACIMIENTO DEL HIJO (A)');
		$this->pdf->ln();
		$this->pdf->Cell(150,2,'.- COPIA  DEL CONTRATO DE TRABAJO ( SI ES PERSONAL CONTRATADO)');
		*/
		$this->pdf->ln();
		$this->pdf->ln();
		$this->pdf->SetFont('helvetica', 'B', 10);
		$str='NOTA: Deberá retirar su pago por la Coordinación de Habilitado. Se recomienda esperar 30 días continuos para acudir a la unidad mencionada.';
		$this->pdf->MultiCell(160,2,$str,"","L",FALSE,0);
		
		
        $this->pdf->Output($arr['cedula'].'_SOL004.pdf', 'D');
	}
	
	function _SOL005($arr){
		$this->pdf->SetMargins(30,25);
		$this->pdf->AddPage();
		
		$this->pdf->SetFont('helvetica', 'B', 24);
		$this->pdf->Cell(130,2,"",0,0,"C");
		$this->pdf->Cell(30,2,"FP-023",0,0,"C");		
		
		$this->pdf->SetFont('helvetica', 'B', 12);
        $this->pdf->ln();
        
        $this->pdf->Cell(160,2,"Solicitud de Antecedentes de Servicio","TLRB",0,"C");
        $this->pdf->SetFont('helvetica', '', 10);
        $this->pdf->ln();        
        $this->pdf->Cell(120,2,"Nombres y Apellidos:","TLR",0,"L");
        $this->pdf->Cell(40,2,"Cédula:","TLR",0,"L");
        $this->pdf->ln();
        $this->pdf->Cell(120,2,$arr['nombre'],"BLR",0,"L");
        $this->pdf->Cell(40,2,$arr['cedula'],"BLR",0,"L");
        $this->pdf->ln();        
        $this->pdf->Cell(80,2,"Última dirección de adscripción:","TLR",0,"L");
        $this->pdf->Cell(80,2,"Último cargo:","TLR",0,"L");
        $this->pdf->ln();
        $this->pdf->Cell(80,2,$arr['uadscripcion'],"BLR",0,"L")     ;  
        $this->pdf->Cell(80,2,$arr['ucargo'],"BLR",0,"L")     ;
        
        $this->pdf->ln();        
        $this->pdf->Cell(50,2,"Fecha de Ingreso:","TLR",0,"L");
        $this->pdf->Cell(50,2,"Fecha de Egreso:","TLR",0,"L");
        $this->pdf->Cell(60,2,"Último Sueldo:","TLR",0,"L");
        $this->pdf->ln();
        $this->pdf->Cell(50,2,$arr['fechaingreso'],"BLR",0,"L")     ;  
        $this->pdf->Cell(50,2,$arr['fechaegreso'],"BLR",0,"L")     ;
        $this->pdf->Cell(60,2,$arr['ultimosueldo'],"BLR",0,"L")     ;
        
        $this->pdf->ln();        
        $this->pdf->Cell(50,2,"Otras Asignaciones:","TLR",0,"L");
        $this->pdf->Cell(110,2,"FP-023 será presentado ante:","TLR",0,"L");
        $this->pdf->ln();
        $this->pdf->Cell(50,2,$arr['otrasasignaciones'],"BLR",0,"L")     ;  
        $this->pdf->Cell(110,2,$arr['fp023ante'],"BLR",0,"L")     ;
        
        $this->pdf->ln();
        $this->pdf->Cell(160,2,"","TLR",0,"L");
        $this->pdf->ln();
        $this->pdf->Cell(160,2,"","RL",0,"L");
        $this->pdf->SetFont('helvetica', 'B', 18);
        $this->pdf->ln();
        $this->pdf->SetFont('helvetica', 'N', 8);
        $this->pdf->Cell(50,2,"","L",0,"L");
        $this->pdf->Cell(60,2,"","B",0,"L");
        $this->pdf->Cell(50,2,"","R",0,"L");
        
        $this->pdf->ln();
        $this->pdf->Cell(160,2,"Firma del Solicitante","RL",0,"C");
        $this->pdf->ln();
        $this->pdf->Cell(160,2,"Anexar: Copia de la Cédula de Identidad","BLR",0,"L");
        $this->pdf->SetFont('helvetica', 'B', 8);
        $this->pdf->ln();
        $this->pdf->Cell(160,2,"SOLO PARA SER LLENADO POR LA DIVISION DE REGISTRO Y CONTROL","BLR",0,"C");
        $this->pdf->ln();
        $this->pdf->SetFont('helvetica', 'N', 8);
        $this->pdf->Cell(80,2,"1. Nombres y Apellidos:","TLR",0,"L");
        $this->pdf->Cell(80,2,"2. Cédula de Identidad:","TLR",0,"L");
        $this->pdf->ln();
        $this->pdf->Cell(80,2,"","BLR",0,"L");
        $this->pdf->Cell(80,2,"","BLR",0,"L");
        $this->pdf->ln();        
        $this->pdf->Cell(160,2,"3. Tipos de Servicios Prestados","TRL",0,"L");
        $this->pdf->ln();
        $this->pdf->Cell(160,2,"","RL",0,"C");
        $this->pdf->ln();
        $this->pdf->Cell(160,2,"Contratado_____________ Suplente_____________ Servicios Prestados____________ Serv. Militar_____________","RL",0,"L");
        $this->pdf->ln();
        $this->pdf->Cell(160,2,"","BRL",0,"L");
        $this->pdf->ln();
        
        $this->pdf->SetFont('helvetica', 'B', 12);
        $this->pdf->Cell(80,6,"INGRESO","TLR",0,"C");
        $this->pdf->Cell(80,6,"EGRESO","TLR",0,"C");
        $this->pdf->SetFont('helvetica', 'N', 8);
        $this->pdf->ln();
        $this->pdf->Cell(80,6,"4. Fecha:_____________________________","LR",0,"L");
        $this->pdf->Cell(80,6,"10. Fecha:_____________________________","LR",0,"L");
        $this->pdf->ln();
        $this->pdf->Cell(80,6,"5. Cargo:_____________________________","LR",0,"L");
        $this->pdf->Cell(80,6,"11. Cargo:_____________________________","LR",0,"L");
        $this->pdf->ln();
        $this->pdf->Cell(80,6,"6. Grado:_____________________________","LR",0,"L");
        $this->pdf->Cell(80,6,"12. Grado:_____________________________","LR",0,"L");
        $this->pdf->ln();
        $this->pdf->Cell(80,6,"7. N° RAC:___________________________","LR",0,"L");
        $this->pdf->Cell(80,6,"13. N° RAC:___________________________","LR",0,"L");
        $this->pdf->ln();
        $this->pdf->Cell(80,6,"8. Horario de Trabajo:___________________","LR",0,"L");
        $this->pdf->Cell(80,6,"14. Horario de Trabajo:___________________","LR",0,"L");
        $this->pdf->ln();
        $this->pdf->Cell(80,6,"9. Horas Mensuales:____________________","LR",0,"L");
        $this->pdf->Cell(80,6,"15. Horas Mensuales:____________________","LR",0,"L");
        $this->pdf->ln();
        $this->pdf->Cell(80,6,"","BLR",0,"L");
        $this->pdf->Cell(80,6,"","BLR",0,"L");
        $this->pdf->ln();        
        
        $this->pdf->Cell(80,6,"16. Remuneraciones:","LR",0,"L");
        $this->pdf->Cell(80,6,"17. Remuneraciones:","LR",0,"L");
        $this->pdf->ln();
        $this->pdf->Cell(80,6,"Sueldo Básico:","LR",0,"L");
        $this->pdf->Cell(80,6,"Sueldo Básico:","LR",0,"L");
        $this->pdf->ln();
        $this->pdf->Cell(80,6,"Compensaciones:","LR",0,"L");
        $this->pdf->Cell(80,6,"Compensaciones:","LR",0,"L");
        $this->pdf->ln();
        $this->pdf->Cell(80,6,"Primas:","LR",0,"L");
        $this->pdf->Cell(80,6,"Primas:","LR",0,"L");
        $this->pdf->ln();
        $this->pdf->Cell(80,6,"Total:","LR",0,"L");
        $this->pdf->Cell(80,6,"Total:","LR",0,"L");
        $this->pdf->ln();
        
        $this->pdf->Cell(160,6,"18. Tipo de egreso","TRL",0,"L");
        $this->pdf->ln();
        $this->pdf->Cell(160,6,"","RL",0,"L");
        $this->pdf->ln();
        $this->pdf->Cell(80,6,"19. Fundamento Legal","TRL",0,"L");
        $this->pdf->Cell(80,6,"20. Pago de prestaciones sociales","TRL",0,"L");
        $this->pdf->ln();
        $this->pdf->Cell(80,6,"","RL",0,"L");
        $this->pdf->Cell(80,6,"","RL",0,"L");
        $this->pdf->ln();
        $this->pdf->Cell(160,6,"21. Observaciones","TRL",0,"L");
        $this->pdf->ln();
        $this->pdf->Cell(160,6,"","RL",0,"L");
        $this->pdf->ln();
        $this->pdf->Cell(80,6,"22. Firma:","TRL",0,"L");
        $this->pdf->Cell(80,6,"23. Fecha","TRL",0,"L");
        $this->pdf->ln();
        $this->pdf->Cell(80,6,"","RL",0,"L");
        $this->pdf->Cell(80,6,"","RL",0,"L");
        $this->pdf->ln();
        $this->pdf->Cell(80,6,"Cargo","BRL",0,"L");
        $this->pdf->Cell(80,6,"","BRL",0,"L");
        
        $this->pdf->Output($arr['cedula'].'_SOL005.pdf', 'D');
	}
	
	function _SOL004($arr){
		$this->pdf->AddPage();
		$this->pdf->SetFont('helvetica', 'B', 12);
        $this->pdf->ln(2);
        $this->pdf->writeHTML("<span style=\"text-align:center;\">Solicitud de Prima por Hijos</span>", true, 0, true, true);
        
        $this->pdf->ln(8);
		$this->pdf->SetFont('helvetica', 'B', 10);
		$this->pdf->Cell(100,2,"DATOS DEL BENEFICIARIO(A)");
		$this->pdf->ln(8);
		$this->pdf->SetFont('helvetica', '', 10);
        $this->pdf->Cell(30,2,"Cédula:","",0,"L");
        $this->pdf->Cell(140,2,"Nombres:","",0,"L");
        $this->pdf->ln();
        $this->pdf->Cell(30,2,number_format($arr['cedula'],0,",","."),"",0,"L");
        $this->pdf->Cell(140,2,$arr['nombre'],"",0,"L");
        $this->pdf->ln();
        $this->pdf->ln();
        
        $this->pdf->Cell(56.66,2,'Nº Nómina',"",0,"L");
        $this->pdf->Cell(56.66,2,'Teléfono Hab.',"",0,"L");
        $this->pdf->Cell(56.66,2,'Celular',"",0,"L");
        $this->pdf->ln();
        $this->pdf->Cell(56.66,2,$arr['codigonomina'],"",0,"L");
        $this->pdf->Cell(56.66,2,$arr['telefonohab'],"",0,"L");
        $this->pdf->Cell(56.66,2,$arr['telefonocel'],"",0,"L");
        $this->pdf->ln();
        $this->pdf->ln();
        
        $this->pdf->Cell(150,2,"Dirección de Adscripción");
		$this->pdf->ln();
		$this->pdf->Cell(150,2,$arr['adscripcion']);
	  	$this->pdf->ln();
        $this->pdf->ln();
        
        $this->pdf->Cell(56.66,2,'Teléfono Ofc.',"",0,"L");
        $this->pdf->Cell(56.66,2,'Extensión',"",0,"L");
        
        $this->pdf->ln();
        $this->pdf->Cell(56.66,2,$arr['telefono'],"",0,"L");
        $this->pdf->Cell(56.66,2,$arr['extension'],"",0,"L");        
        $this->pdf->ln();
        $this->pdf->ln();
        
        $this->pdf->SetFont('helvetica', 'B', 10);
		$this->pdf->Cell(50,2,"Tipo de Trabajador");
		$this->pdf->SetFont('helvetica', '', 10);
		$this->pdf->ln();
		$this->pdf->Cell(50,2,$arr['tipopersonal']);
		
		$this->pdf->ln();
        $this->pdf->ln();
        $this->pdf->SetFont('helvetica', 'B', 10);
		$this->pdf->Cell(50,2,"DATOS DE HIJOS");
		$this->pdf->ln();
		$this->pdf->Cell(10,2,'Nº','TLRB',0,'C');
        $this->pdf->Cell(80,2,'Nombres','TLRB',0,'C');
        $this->pdf->Cell(40,2,'Fecha Nacimiento','TLRB',0,'C');
        $this->pdf->ln();
        $this->pdf->SetFont('helvetica', '', 10);
        $i=1;
		foreach($arr['hijos'] as $valor){
			if ($valor['nombres']!=''){
				$this->pdf->Cell(10,6,$i++,'LRB',0,'C');
				$this->pdf->Cell(80,6,$valor['nombres'],'LRB');
	        	$this->pdf->Cell(40,6,$valor['fechanac'],'LRB',0,'C');
	        	$this->pdf->ln();
			}
			
		}        
		
		
        $this->pdf->ln(30);
		$this->pdf->Cell(40,2,'Firma del Funcionario');
		$this->pdf->Cell(40,2,'','B');
		$this->pdf->ln(20);
		$this->pdf->Cell(40,2,'REQUISITOS:');
		$this->pdf->ln();
		$this->pdf->Cell(150,2,'.- FOTOCOPIA DEL CARNET DEL TRABAJADOR');
		$this->pdf->ln();
		$this->pdf->Cell(150,2,'.- FOTOCOPIA DE LA CÉDULA DE IDENTIDAD');
		$this->pdf->ln();
		$this->pdf->Cell(150,2,'.- COPIA DE LA PARTIDA  DE NACIMIENTO DEL HIJO (A)');
		$this->pdf->ln();
		$this->pdf->Cell(150,2,'.- COPIA DEL CONTRATO DE TRABAJO ( SI ES PERSONAL CONTRATADO)');
		$this->pdf->ln();
		$this->pdf->SetFont('helvetica', 'B', 10);
		$this->pdf->Cell(150,2,'.- Ultimo recibo de pago');		
		
        $this->pdf->Output($arr['cedula'].'_SOL004.pdf', 'D');
        
	}
	
	function _SOL002($arr){
		$monto=$arr['montoprestaciones'];
		$montoTxt=$arr['montotxt'];
		$this->pdf->SetMargins(30,25);
		$this->pdf->AddPage();
		
		$this->pdf->SetFont('helvetica', 'B', 12);
        $this->pdf->ln(2);
        $this->pdf->writeHTML("<span style=\"text-align:center;\">Solicitud de Prestaciones de Antigüedad</span>", true, 0, true, true);
        $this->pdf->ln();
        $this->pdf->SetFont('helvetica', 'B', 10);
        
        $this->pdf->ln(2);
        
        $this->pdf->Cell(85,2,"Datos personales","TLRB",0,"C");
        $this->pdf->Cell(85,2,"Datos del Cónyuge","TLRb",0,"C");
        $this->pdf->SetFont('helvetica', '', 10);
        $this->pdf->ln();
        $this->pdf->Cell(85,2,"Apellidos","TLR",0,"L");
        $this->pdf->Cell(85,2,"Apellidos","TLR",0,"L");
        $this->pdf->ln();
        
        $this->pdf->Cell(85,2,$arr['apellidos'],"LRB",0,"L");
        $this->pdf->Cell(85,2,$arr['apellidos_conyuge'],"LRB",0,"L");
        $this->pdf->ln();
        $this->pdf->Cell(85,2,"Nombres","TLR",0,"L");
        $this->pdf->Cell(85,2,"Nombres","TLR",0,"L");
        $this->pdf->ln();
        $this->pdf->Cell(85,2,$arr['nombres'],"LRB",0,"L");
        $this->pdf->Cell(85,2,$arr['nombres_conyuge'],"LRB",0,"L");
        
        $this->pdf->ln();
        $this->pdf->Cell(42.5,2,"Cédula","L",0,"L");
        $this->pdf->Cell(42.5,2,"Ext","R",0,"L");
        $this->pdf->Cell(42.5,2,"Cédula","L",0,"L");
        $this->pdf->Cell(42.5,2,"Teléfono","R",0,"L");
        $this->pdf->ln(); 
        $this->pdf->Cell(42.5,2,number_format($arr['cedula'],0,",","."),"LB",0,"L");
        $this->pdf->Cell(42.5,2,"3641","RB",0,"L");
        $this->pdf->Cell(42.5,2,number_format($arr['cedula_conyuge'],0,",","."),"LB",0,"L");
        $this->pdf->Cell(42.5,2,$arr['telefono_conyuge'],"RB",0,"L");
        $this->pdf->ln();
        $this->pdf->SetFont('helvetica', 'B', 10);
        $this->pdf->Cell(85,2,"SOLICITUD","LRB",0,"C");
        $this->pdf->Cell(85,2,"AUTORIZACIÓN DEL CÓNYUGE","LRB",0,"C");
        $this->pdf->SetFont('helvetica', '', 10);
        $this->pdf->ln();
       	$str=<<<EOD
Por medio de la presente me dirijo a Ud. Con la finalidad de solicitar la cantidad de: $montoTxt (BsF.$monto) de mis prestaciones de antigüedad depositadas en el Fideicomiso.




Firma y Fecha.                
EOD;
        $this->pdf->MultiCell(85,2,$str,"LRB","J",FALSE,0);
      	$str=<<<EOD
Sirva la presente para hacer de su conocimiento que: doy mi consentimiento para que mi cónyuge realice las gestiones relacionadas con la solicitud de un anticipo de las prestaciones de antigüedad.






Firma y Fecha.                
EOD;
        $this->pdf->MultiCell(85,2,$str,"LRB","J",FALSE,0);
        $this->pdf->ln();
        $this->pdf->MultiCell(170,2,"Declaro: En mi poder descansan los Documentos Originales solicitados por la Oficina de Recursos Humanos del Ministerio del Poder Popular para Relaciones Exteriores que a continuación se detallan:","LBR","L");
        $this->pdf->SetFont('helvetica', '', 8);
        $this->pdf->Cell(38.5,8,"Construcción de vivienda","LB",0,"L");
        $this->pdf->Cell(4,8,key_exists('construccionvivienda',$arr)? $arr['construccionvivienda']:"","LBR",0,"C");
        $this->pdf->Cell(38.5,8,"Adquisición de vivienda","LB",0,"L");
        $this->pdf->Cell(4,8,key_exists('adquisicionvivienda',$arr)? $arr['adquisicionvivienda']:"","LBR",0,"C");
        $this->pdf->Cell(60,8,"Mejora o Reparación de Vivienda","LB",0,"L");
        $this->pdf->Cell(4,8,key_exists('presupuesto',$arr)? $arr['presupuesto']:"","LBR",0,"C");
        $this->pdf->Cell(17,8,"","LB",0,"L");
        $this->pdf->Cell(4,8,key_exists('planillainscripcion',$arr)? $arr['planillainscripcion']:"","LBR",0,"C");
        $this->pdf->ln();
        
        $this->pdf->Cell(38.5,8,"Liberación de Hipoteca","LB",0,"L");
        $this->pdf->Cell(4,8,key_exists('liberacionhipoteca',$arr)? $arr['liberacionhipoteca']:"","LBR",0,"C");
        $this->pdf->Cell(38.5,8,"Pensiones Escolares","LB",0,"L");
        $this->pdf->Cell(4,8,key_exists('informemedico',$arr)? $arr['informemedico']:"","LBR",0,"C");
        $this->pdf->Cell(60,8,"Gastos Médicos y/u Hospitalarios","LB",0,"L");
        $this->pdf->Cell(4,8,key_exists('permisoconstruccion',$arr)? $arr['permisoconstruccion']:"","LBR",0,"C");
        $this->pdf->Cell(17,8,"","LB",0,"L");
        $this->pdf->Cell(4,8,key_exists('otrosdocumentos',$arr)? $arr['otrosdocumentos']:"","LBR",0,"C");
        $this->pdf->ln();
        $this->pdf->SetFont('helvetica', '', 10);
        $this->pdf->Cell(4,8,"","",0,"C");
        $this->pdf->ln();
        $this->pdf->MultiCell(170,10,"Firma: ___________________ Cédula:______________ Fecha:______________","","C","","","","","","",FALSE,"","","B");
        $this->pdf->ln();
        $this->pdf->SetFont('helvetica', '', 16);
        $this->pdf->Cell(170,60,"COLOCAR FOTOCOPIA DE LA CÉDULA EN ESTE ESPACIO","T",0,"C");
        $this->pdf->SetFont('helvetica', '', 10);
        $this->pdf->ln();
        $this->pdf->Cell(85,6,"Copia de la C.I. del Solicitante","B",0,"C");
        $this->pdf->Cell(85,6,"Copia de la C.I. del Cónyuge","B",0,"C");
        $this->pdf->ln();
        
        $this->pdf->SetFont('helvetica', '', 8);
        $this->pdf->Cell(56.66,1,"Elaborado Por","TLR",0,"L");
        $this->pdf->Cell(56.66,1,"Revisado Por","TLR",0,"L");
        $this->pdf->Cell(56.66,1,"Aprobado Por","TLR",0,"L");
        $this->pdf->ln();
        $this->pdf->Cell(56.66,19,"","BLR",0,"L");
        $this->pdf->Cell(56.66,19,"","BLR",0,"L");
        $this->pdf->Cell(56.66,19,"","BLR",0,"L");
        
        $this->pdf->Output($arr['cedula'].'_SOL002.pdf', 'D');
	}
	
	function _PRI002($arr){
		$this->pdf->AddPage();
		
		$html='';
		        
        $this->pdf->SetFont('helvetica', 'B', 12);
        $this->pdf->ln(8);
        $this->pdf->writeHTML("<span style=\"text-align:center;\">Solicitud de Prima por Profesionalización</span>", true, 0, true, true);
        $this->pdf->ln();
        $this->pdf->SetFont('helvetica', '', 10);
        //$html="<span style=\"text-align:justify;line-height:1.5em;\">asdasd</span>";
		$this->pdf->writeHTML($html, true, 0, true, true);
		$this->pdf->ln(8);
		$this->pdf->SetFont('helvetica', 'B', 10);
		$this->pdf->Cell(100,2,"DATOS DEL BENEFICIARIO(A)");
		$this->pdf->ln(8);
		$this->pdf->SetFont('helvetica', '', 10);
		$this->pdf->Cell(30,2,"Cédula");
		$this->pdf->Cell(100,2,"Nombres y Apellidos");
		
		$this->pdf->ln();
		$this->pdf->Cell(30,2,number_format($arr['cedula'],0,",","."));
		
		$this->pdf->Cell(100,2,utf8_encode($arr['nombre']));
		
		$this->pdf->ln(8);
		
		$this->pdf->Cell(30,2,"Nº de Nómina");
		$this->pdf->Cell(30,2,"Teléfono");
		$this->pdf->ln();
		
		
		$this->pdf->Cell(30,2,$arr['codigonomina']);
		$this->pdf->Cell(30,2,$arr['telefonohab']);
		
		$this->pdf->ln(8);
		
		$this->pdf->Cell(150,2,"Dirección de Adscripción");
		$this->pdf->ln();
		$this->pdf->Cell(150,2,$arr['adscripcion']);
		
		$this->pdf->ln(8);
		
		$this->pdf->Cell(30,2,"Teléfono Oficina");
		$this->pdf->Cell(30,2,"Ext");
		$this->pdf->Cell(30,2,"Celular");
		$this->pdf->ln();
		
		$this->pdf->Cell(30,2,$arr['telefono']);
		$this->pdf->Cell(30,2,$arr['extension']);
		$this->pdf->Cell(30,2,$arr['celular']);
		
		$this->pdf->ln(8);
		
		$this->pdf->SetFont('helvetica', 'B', 10);
		$this->pdf->Cell(50,2,"Tipo de Trabajador");
		$this->pdf->SetFont('helvetica', '', 10);
		$this->pdf->ln();
		$this->pdf->Cell(50,2,$arr['tipopersonal']);
		
		$this->pdf->ln(8);
		
		$this->pdf->SetFont('helvetica', 'B', 10);
		$this->pdf->Cell(50,2,"Nivel de Estudio");
		$this->pdf->SetFont('helvetica', '', 10);
		$this->pdf->ln();
		$this->pdf->Cell(170,2,$arr['nivelacademico'],0,"L");
		
		$this->pdf->ln(20);
		
		$this->pdf->SetFont('helvetica', 'B', 10);
		$this->pdf->Cell(40,2,"Firma del Funcionario:");
		$this->pdf->Cell(50,2,"","B");
		
		$this->pdf->ln(20);
		$this->pdf->SetFont('helvetica', 'B', 10);
		$this->pdf->Cell(50,2,"REQUISITOS","",1);
		$this->pdf->SetFont('helvetica', '', 10);
		$this->pdf->Cell(200,2,"-. Fotocopia de la Copia certificada del título a presentar (con vista del original)","",1);
		
		$this->pdf->Output($arr['cedula'].'_PRI002.pdf', 'D');
		
	}
	
	function _SOL008($arr){
		$this->pdf->setPrintFooter(TRUE);
		
		
		
        if ($arr['tipoconstancia']==1 or $arr['tipoconstancia']==3){
        	$this->pdf->AddPage();
        	$html=$arr['formato'];
		        
	       	$this->pdf->SetFont('helvetica', 'B', 14);
	        $this->pdf->ln(18);
	        $this->pdf->writeHTML("<span style=\"text-align:center;margin-top:50px;\">CONSTANCIA</span>", true, 0, true, true);
	        $this->pdf->ln();
	        $this->pdf->SetFont('helvetica', '', 11);
	        $html="<span style=\"text-align:justify;line-height:1.5em;\">".utf8_encode($html)."</span>";
			$this->pdf->writeHTML($html, true, 0, true, true);
			
			$this->pdf->ln(20);
			
			//$html="<span style=\"text-align:center;\"><h4>Walton Valencia Diaz</h4><br><h5>Director de Administración de Personal (E)<br>Según Resolución DM/ N° 013-A de fecha 21 de enero de 2009<br>publicada en Gaceta Oficial N° 39.106 de fecha 26 de enero de 2009</h4>";
			$html="<span style=\"text-align:center;\">".($arr['firma'])."</span>";
			$this->pdf->writeHTML($html, true, 0, true, true);
			$md5=md5($arr['md5']);
			$this->pdf->setXY(87,247);
			$this->pdf->SetFont('helvetica', '', 6);
			$this->pdf->Cell(2,2,strtoupper($md5),"C",1);
        }
        
		if ($arr['tipoconstancia']==2 or $arr['tipoconstancia']==3){
			$this->pdf->AddPage();
			$html=$arr['formato2'];
		        
	       	$this->pdf->SetFont('helvetica', 'B', 14);
	        $this->pdf->ln(10);
	        $this->pdf->writeHTML("<span style=\"text-align:center;margin-top:50px;\">CONSTANCIA</span>", true, 0, true, true);
	        $this->pdf->ln();
	        $this->pdf->SetFont('helvetica', '', 11);
	        $html="<span style=\"text-align:justify;line-height:1.5em;\">".utf8_encode($html)."</span>";
			$this->pdf->writeHTML($html, true, 0, true, true);
			
			$this->pdf->ln(12);
			
			//$html="<span style=\"text-align:center;\"><h4>Walton Valencia Diaz</h4><br><h5>Director de Administración de Personal (E)<br>Según Resolución DM/ N° 013-A de fecha 21 de enero de 2009<br>publicada en Gaceta Oficial N° 39.106 de fecha 26 de enero de 2009</h4>";
			$html="<span style=\"text-align:center;\">".($arr['firma2'])."</span>";
			$this->pdf->writeHTML($html, true, 0, true, true);
			$md5=md5($arr['md52']);
			$this->pdf->setXY(87,247);
			$this->pdf->SetFont('helvetica', '', 6);
			$this->pdf->Cell(2,2,strtoupper($md5),"C",1);
        }
        
		$this->pdf->Output($arr['cedula'].'_constancia.pdf', 'I');
	}
	
	
}
