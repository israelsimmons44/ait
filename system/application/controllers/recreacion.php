<?php
class Recreacion extends Controller{
	function __construct(){
		
		parent::Controller();
		
		if (!$this->session->userdata('nombre')){
			redirect(base_url()."index.php/comun/login");
		}
		
		$this->load->model('recreacion_model','rem');
		$this->load->library('parser');
		$this->load->library('pdf');
		$this->load->helper('funciones');
		
	}

////////////////////////////////////////////// FUNCIÓN QUE CARGA EL BANER Y MENÚ /////////////////////////	
	
	function _header(){
		$data['title']="Atenci&oacute;n Integral al Trabajador";
		$this->load->view('main-view',$data);
		$deta['permisos']=$this->session->userdata('permisos');
		$this->load->view('comun/menu2',$deta);		
	}
	
////////////////////////////////////////////// FUNCIÓN QUE CARGA FORMULARIO DE CONSULTA DE PERSONAL POR CÉDULA /////////////////////////	
	
	function personal(){
		$this->_header();
		$data=array();
		$this->load->library('formulario');
		$this->formulario->setAction(base_url()."index.php/recreacion/consultaHijosAnalista");
		$this->formulario->addInput('cedula','C&eacute;dula Trabajador','','required:true');
		$this->formulario->addButton('btnEnviar','Consultar','enviar');
		
		$data['formulario']=$this->formulario->outputHTML();
		
		$this->parser->parse('recreacion/consultahijos-view',$data);
	}	

//////////////////////// FUNCIÓN PARA MOSTRAR DATOS DE CONSULTA DE HIJOS (OPCIÓN PARA ANALISTAS DE AIT)/////////////////////////	
	
	function consultaHijosAnalista(){

		$data['cedula']="";
		$data['nombres']="";
		$data['apellidos']="";
		$data['dependencia']="";
		$data['cargo']="";
		$data['direccion_residencia']="";
		$data['telefono_celular']="";
		$data['telefono_oficina']="";
		$data['telefono_residencia']="";
		$data['email']="";
		$data['hijostrabajador']="";
		
		$cedula=$this->input->post('cedula');
		
		$resPersonal=$this->rem->getPersonal($cedula);
		//$resPersonal=$this->rem->getPersonal($this->session->userdata('cedula'));
		$arrPersonal=$resPersonal['personal'];
		if ($arrPersonal){
			$data['cedula']=$arrPersonal->cedula;
			$data['nombres']=utf8_encode($arrPersonal->primer_nombre." ".$arrPersonal->segundo_nombre);
			$data['apellidos']=utf8_encode($arrPersonal->primer_apellido." ".$arrPersonal->segundo_apellido);
			$data['dependencia']=utf8_encode($arrPersonal->dependencia);
			$data['cargo']=utf8_encode($arrPersonal->cargo);
			$data['direccion_residencia']=utf8_encode($arrPersonal->direccion_residencia);
			$data['telefono_celular']=$arrPersonal->telefono_celular;
			$data['telefono_oficina']=$arrPersonal->telefono_oficina;
			$data['telefono_residencia']=$arrPersonal->telefono_residencia;
			$data['email']=utf8_encode($arrPersonal->email);
			$data['hijostrabajador']=$resPersonal['hijostrabajador'];
		}else{
			$data['hijostrabajador']=array();
			$data['nombres']="LA C&Eacute;DULA QUE INGRES&Oacute; NO ESTA REGISTRADA";
		}	
 
		
		$this->parser->parse('recreacion/detalleconsultahijos-view',$data);
		
	}	
	
////////////////////////////////////////////// FUNCIÓN PARA MOSTRAR DATOS DE CONSULTA DE HIJOS /////////////////////////	
	
	function consultaHijos(){
		
		$this->_header();

		$data['cedula']="";
		$data['nombres']="";
		$data['apellidos']="";
		$data['dependencia']="";
		$data['cargo']="";
		$data['direccion_residencia']="";
		$data['telefono_celular']="";
		$data['telefono_oficina']="";
		$data['telefono_residencia']="";
		$data['email']="";
		$data['hijostrabajador']="";
		
		//$resPersonal=$this->rem->getPersonal($cedula);
		$resPersonal=$this->rem->getPersonal($this->session->userdata('cedula'));
		$arrPersonal=$resPersonal['perso$nal'];
                $data['hijostrabajador']=$resPersonal['hijostrabajador'];
		if ($arrPersonal){
			$data['cedula']=$arrPersonal->cedula;
			$data['nombres']=utf8_encode($arrPersonal->primer_nombre." ".$arrPersonal->segundo_nombre);
			$data['apellidos']=utf8_encode($arrPersonal->primer_apellido." ".$arrPersonal->segundo_apellido);
			$data['dependencia']=utf8_encode($arrPersonal->dependencia);
			$data['cargo']=utf8_encode($arrPersonal->cargo);
			$data['direccion_residencia']=utf8_encode($arrPersonal->direccion_residencia);
			$data['telefono_celular']=$arrPersonal->telefono_celular;
			$data['telefono_oficina']=$arrPersonal->telefono_oficina;
			$data['telefono_residencia']=$arrPersonal->telefono_residencia;
			$data['email']=utf8_encode($arrPersonal->email);
			$data['hijostrabajador']=$resPersonal['hijostrabajador'];
		}else{
			$data['hijostrabajador']=array();
			$data['nombres']="LA C&Eacute;DULA QUE INGRES&Oacute; NO ESTA REGISTRADA";
		}	
 
		
		$this->parser->parse('recreacion/detalleconsultahijos-view',$data);
		
	}

//////////////////////// FUNCIÓN PARA DIRECCIONAR A FORMULARIO DE CARGA DE DATOS ADICIONALES DE HIJOS /////////////////////////	
	
	function AgregaDatosMedicosHijos () {
		$this->load->helper('form');
		$this->_header();
		$idFamiliar=$this->uri->segment(3);
		//die ($idFamiliar);
		$data = $this->rem->getHijo($idFamiliar);
		#var_dump($data);die();
		$this->jquery->output();
		$this->parser->parse('recreacion/datosmedicoshijos-view',$data);
	}
	
////////////////////// FUNCIÓN PARA INGRESAR DATOS ADICIONALES EN BD Y GENERAR PLANILLA DE INSCRIPCIÓN /////////////////////////	
	
	function ProcAgregaDatosMedicosHijos () {
		
		//var_dump( $this->input->post ('cedula'));
		$data['id_familiar']=$this->input->post ('id_familiar');
		$data['cedula_familiar']=$this->input->post ('cedula_familiar');
		$data['talla_franela']=$this->input->post ('talla_franela');
		$data['talla_pantalon']=$this->input->post ('talla_pantalon');
		$data['talla_gorra']=$this->input->post ('talla_gorra');
		$data['direccion_hijo']=$this->input->post ('direccion_hijo');
		$data['enfermedad']=$this->input->post ('enfermedad');
		$data['medicamento']=$this->input->post ('medicamento');
		$data['alergico']=$this->input->post ('alergico');
		$data['medicotratante']=$this->input->post ('medicotratante');
		$data['telefono_medico']=$this->input->post ('telefono_medico');
		$data['cedula_personaretira']=$this->input->post ('cedula_personaretira');
        if ($data['cedula_personaretira']== null) {
            $data['cedula_personaretira']=0;
        }else{ 
        	$data['cedula_personaretira']=$this->input->post ('cedula_personaretira');
        }
		$data['nombre_personaretira']=$this->input->post ('nombre_personaretira');
		$data['parentesco']=$this->input->post ('parentesco');
		$data['direccion_personaretira']=$this->input->post ('direccion_personaretira');
		$data['telefono_personaretira']=$this->input->post ('telefono_personaretira');
		$data['celular_personaretira']=$this->input->post ('celular_personaretira');
		
		$this->rem->guardaDatosMedicosHijos($data);
		$this->_header();
	    $idFamiliar=$this->uri->segment(3);
		$this->parser->parse('recreacion/datosadicionalesguardados-view',$data);

	}

///////////////////// FUNCIÓN PARA GENERAR PLANILLA DE INSCRIPCIÓN, AUTORIZACIÓN Y PERMISO DE VIAJE /////////////////////////	
	
	function GenerarPlanilla () {
		
	    $idFamiliar=$this->uri->segment(3);
		//$PapaHijo= $this->rem->getPadreHijo($idFamiliar);

########################################### VARIABLES GENERALES ######################################################
            $fecha = date('d/m/Y');
            $anio=date("Y");
            $dia = date('d');
            $numMes = date('m');             
            $mes = getMesES($numMes);               
            $anio = date('Y');	    
	    $titulo1="INCRIPCIÓN PERÍODO VACACIONAL"." ".$anio;
	    $titulo2="*** PLAN VACACIONAL ***";
	    $titulo3="*** CAMPAMENTO ***";
	    $titulo4="TITULAR DEL GRUPO FAMILIAR";
	    $titulo5="DATOS DEL NIÑO(A)";
	    $titulo6="DATOS DE OTRA PERSONA QUE PUEDA RETIRAR AL NIÑO(A) - Obviar si es el mismo titular";


######################################### VARIABLES OBTENIDAS DEL MODELO getPadreHijo ###################################

                $padreHijo = $this->rem->getPadreHijo($idFamiliar);
                $nombrePadre = utf8_encode($padreHijo->nombre);
                $cedulaPadre = $padreHijo->cedula;
                $dependencia = utf8_encode($padreHijo->dependencia);
                $cargo = utf8_encode($padreHijo->cargo);
                $sexo = getSexo($padreHijo->sexo);
                $nombreHijo = utf8_encode($padreHijo->nombre_fam);
                $cedulaHijo = $padreHijo->cedula_familiar;
                $edad = $padreHijo->edad;
                $fechaNacimientoHijo = pgDate($padreHijo->fecha_nacimiento); 
                $direccionPadre = utf8_encode($padreHijo->direccion_residencia);
                $telefonoPadre = $padreHijo->telefono_oficina;
                $celularPadre = $padreHijo->telefono_celular;
                $email = utf8_encode($padreHijo->email);

###################################### VARIABLES OBTENIDAS DEL MODELO getDatosHijoAdicionales #############################

                $DatosHijo= $this->rem->getDatosHijoAdicionales($idFamiliar);
                $tallaFranela = $DatosHijo->talla_franela;
                $tallaGorra = $DatosHijo->talla_gorra;
                $tallaPantalon = $DatosHijo->talla_pantalon;
                $direccionHijo = $DatosHijo->direccion_hijo;
                $enfermedad = $DatosHijo->enfermedad;
                $medicamento = $DatosHijo->medicamento;
                $medicoTratante = $DatosHijo->medicotratante;
                $telefonoMedico = $DatosHijo->telefono_medico;
                $alergico = $DatosHijo->alergico;
                $cedulaPersonaretira = $DatosHijo->cedula_personaretira;
                $nombrePersonaretira = $DatosHijo->nombre_personaretira;
                $parentesco = $DatosHijo->parentesco;
                $direccionPersonaretira = $DatosHijo->direccion_personaretira;
                $telefonoPersonaretira = $DatosHijo->telefono_personaretira;
                $celularPersonaretira = $DatosHijo->celular_personaretira;

########################################### GENERAR PLANILLA DE INSCRIPCION #################################################  	    
 
        $this->pdf->SetMargins(20, 25 , 30);     
                                       
         if ($edad>=13)
                    $titulo='NIÑO(A) PARA CAMPAMENTO';
                    
                else
                    $titulo='NIÑO(A) PARA PLAN VACACIONAL';
               
                $htmlHijo = "
                    <p align=\"center\"><b>INSCRIPCIÓN PERIODO VACACIONAL $anio </b></p>
                    </br>
                    <div align=\"center\"><b>$titulo</b></div>
                    </br>
                    <p></p>
                    <div align=\"justify\">
                    <b>DATOS DEL NIÑO(A)</b>
                    <hr />
                    <div>
                   <table width=\"2000\" border=\"0\">
					  <tr>
					    <td width=\"600\"><b>Apellidos y Nombres:</b></td>
					    <td width=\"1000\">$nombreHijo</td>
					  </tr>
					  <tr>
					    <td width=\"600\"><b>Cédula:</b></td>
					    <td width=\"600\">$cedulaHijo</td>
					  </tr>
					  <tr>
					    <td width=\"600\"><b>Sexo:</b></td>
					    <td width=\"600\">$sexo</td>
					  </tr>
					  <tr>
					    <td width=\"600\"><b>Cédula:</b></td>
					    <td width=\"600\">$cedulaHijo</td>
					  </tr>
					  <tr>
					    <td width=\"600\"><b>Fecha Nacimiento:</b></td>
					    <td width=\"600\">$fechaNacimientoHijo</td>
					  </tr>
					  <tr>
					    <td width=\"600\"><b>Talla Franela:</b></td>
					    <td width=\"600\">$tallaFranela</td>
					  </tr>
					  <tr>
					    <td width=\"600\"><b>Talla Gorra:</b></td>
					    <td width=\"600\">$tallaGorra</td>
					  </tr>
					  <tr>
					    <td width=\"600\"><b>Talla Pantalón:</b></td>
					    <td width=\"600\">$tallaPantalon</td>
					  </tr>
					  <tr>
					    <td width=\"600\"><b>Dirección Habitación.:</b></td>
					    <td width=\"1200\">$direccionHijo</td>
					  </tr>
					  <tr>
					    <td width=\"1300\"><b>Enfermedad o limitación física que requiera tratamiento:</b></td>
					    <td width=\"700\">$enfermedad</td>
					  </tr>
					  <tr>
					    <td width=\"1000\"><b>Medicamento indicado (nombre y dosis):</b></td>
					    <td width=\"1000\">$medicamento</td>
					  </tr>
					  <tr>
					    <td width=\"600\"><b>Nombre del médico tratante:</b></td>
					    <td width=\"600\">$medicoTratante</td>
					  </tr>
					  <tr>
					    <td width=\"600\"><b>Teléfono Médico:</b></td>
					    <td width=\"600\">$telefonoMedico</td>
					  </tr>
					  <tr>
					    <td width=\"1000\"><b>Alérgico a (alimentos,medicamentos u otros):</b></td>
					    <td width=\"1000\">$alergico</td>
					  </tr>
					</table>
					<p></p>                  
                    <b>DATOS DEL TRABAJADOR</b>
                    <hr />
                    <table width=\"1700\" border=\"0\">
					  <tr>
					    <td width=\"600\"><b>Cédula:</b></td>
					    <td width=\"600\">$cedulaPadre</td>
					  </tr>
					  <tr>
					    <td width=\"600\"><b>Apellidos y Nombres:</b></td>
					    <td width=\"1200\">$nombrePadre</td>
					  </tr>
					  <tr>
					    <td width=\"600\"><b>Dependencia:</b></td>
					    <td width=\"1200\">$dependencia</td>
					  </tr>
					  <tr>
					    <td width=\"600\"><b>Cargo:</b></td>
					    <td width=\"1200\">$cargo</td>
					  </tr>
					  <tr>
					    <td width=\"600\"><b>Dirección Hab.:</b></td>
					    <td width=\"1200\">$direccionPadre</td>
					  </tr>
					  <tr>
					    <td width=\"600\"><b>Teléfono Hab.:</b></td>
					    <td width=\"600\">$telefonoPadre</td>
					  </tr>
					  <tr>
					    <td width=\"600\"><b>Celular:</b></td>
					    <td width=\"600\">$celularPadre</td>
					  </tr>
					  <tr>
					    <td width=\"600\"><b>Correo Electrónico:</b></td>
					    <td width=\"800\">$email</td>
					  </tr>
                    </table>
                    <p></p>
                    <b>DATOS PERSONA QUE RETIRA AL NIÑO(A)</b> (obviar mismo titular)
                    <hr />
                    <table width=\"1200\" border=\"0\">
					  <tr>
					    <td width=\"600\"><b>Cédula:</b></td>
					    <td width=\"600\">$cedulaPersonaretira</td>
					  </tr>
					  <tr>
					    <td width=\"600\"><b>Apellidos y Nombres:</b></td>
					    <td width=\"1200\">$nombrePersonaretira</td>
					  </tr>
					  <tr>
					    <td width=\"600\"><b>Nexo con el niño(a):</b></td>
					    <td width=\"600\">$parentesco</td>
					  </tr>
					  <tr>
					    <td width=\"600\"><b>Dirección Hab.:</b></td>
					    <td width=\"1200\">$direccionPersonaretira</td>
					  </tr>
					  <tr>
					    <td width=\"600\"><b>Teléfono Hab.:</b></td>
					    <td width=\"600\">$telefonoPersonaretira</td>
					  </tr>
					  <tr>
					    <td width=\"600\"><b>Celular:</b></td>
					    <td width=\"600\">$celularPersonaretira</td>
					  </tr>
                    </div>
                ";
              // echo "$htmlHijo";
               
                $this->pdf->SetFontSize(10);
                $this->pdf->AddPage('R', 'letter');
                $style = array('width' => 0.5, 'cap' => 'butt', 'join' => 'miter', 'dash' => '10,10', 'color' => array(0, 128, 0));
                $this->pdf->RoundedRect(155, 24, 35, 30, 3.50, '1111');               
                $this->pdf->writeHTML($htmlHijo, true, false, true, false, '');               
                $this->pdf->writeHTMLCell(20, 20,166,36,'<b>FOTO</b>' );

########################################### GENERAR AUTORIZACIÓN #################################################                
                
                $html = "                    
                    <p>                    
                    Se anexa AUTORIZACIÓN de la participación del niño(a) y constancia de aceptación de las condiciones del Plan Vacacional y Campamento    </p>
                    <p align=\"center\"><strong>PLAN VACACIONAL /  CAMPAMENTO MPPRE 2013</strong></p>
                    <div align=\"justify\">
                    <p><strong>AUTORIZACION</strong></p>
                    Yo, <b>$nombrePadre</b>, titular de la cédula de identidad Nº. <b>$cedulaPadre</b>, mayor de edad y de este domicilio, me dirijo a Ustedes en mi condición de representante legal del niño(a) <b>$nombreHijo</b>, nacido(a) en fecha <b>$fechaNacimientoHijo</b>, titular de la cédula de identidad Nº. <b>$cedulaHijo</b>, para autorizar formal y voluntariamente la participación de mi representado(a) en el <b>PLAN VACACIONAL O CAMPAMENTO DEL MPPRE $anio</b>, a efectuarse en el mes de agosto.
                    En este mismo acto, dejo constancia de lo siguiente:
                    <p><b>PRIMERO:</b> Declaro que mi representado(a) se encuentra en buenas condiciones de salud, y que no padece de alteraciones físicas y mentales que le impidan desarrollar las actividades programadas en este Plan Vacacional o Campamento.</p>
                    <p><b>SEGUNDO:</b> Acepto que mi representado(a) sea trasladado por los representantes del MPPRE a los lugares que se haya previsto visitar para PLAN VACACIONAL O CAMPAMETO MPPRE 2013 y me comprometo acatar y respetar las instrucciones y los requisitos que se establezcan para el traslado, así como las normas establecidas por el comité organizador que incluye la decisión sobre la ubicación de mi representado(a) en las patrullas.
                    </p>
                    <p><b>TERCERO:</b> Me comprometo a trasladar y retirar puntualmente a mi representado(a) de todas las actividades previstas en este evento, y a respetar los horarios que se establezcan.</p>
                    <p><b>CUARTO:</b> Aseguro que mi representado tendrá un comportamiento adecuado, integrándose al grupo con educación, entusiasmo y espíritu de compañerismo. Aceptando que de lo contrario no podrá ser inscrito el año siguiente en el evento.</p>
                    <p><b>Observación</b></p>
                    <p>En caso de que el adolescente llegue después de la hora y se haya retirado el transporte, el MPPRE no habilitará otro vehículo para su traslado, ni será recibido en el lugar.</p>
                    <p> Yo, <b>$nombrePadre</b>, portador(a) de la C.I. Nº. <b>$cedulaPadre</b>, declaro que los datos aquí suministrados son fidedignos, eximiendo de toda responsabilidad al MPPRE por cualquier discrepancia en los mismos.</p>
                    <p>Así lo suscribo en Caracas a los $dia días del mes de $mes de $anio.                  </p>
                    <p><b>NOMBRE DEL REPRESENTANTE:</b> </p>
                    $nombrePadre, FIRMA____________________FECHA:  $fecha
                    </div> " ;
                $this->pdf->SetFontSize(10);
                //$this->pdf->SetMargins(10, 25 , 30);
                $this->pdf->AddPage('R', 'letter');
                $this->pdf->writeHTML($html, true, false, true, false, '');

########################################### GENERAR PERMISO DE VIAJE #################################################                
                
                if ($edad >= 13){
                     $this->pdf->AddPage('R', 'letter');
                     $htmlViaje ="
                     <span>
                        <p align=\"center\"><b>CAMPAMENTO JUVENIL $anio </b></p></br>
                        <p align=\"center\"><b>    <u>Permiso de Viaje</u></b></p>
                        <p align=\"justify\"> Yo <b>$nombrePadre</b>, portador(a) de la cédula de identidad Nº <b>$cedulaPadre</b>, autorizo a mi representado(a) <b>$nombreHijo</b> cédula de identidad Nº <b>$cedulaHijo</b>, para que asista al Campamento Juvenil $anio del Ministerio del Poder Popular para Relaciones Exteriores, que se realizará dentro del territorio nacional en el mes de agosto del presente año. En compañía de los ciudadanos Keiver Coronel C.I. 18.933.807, Domingo Cautela C.I. 6.308.777 y las ciudadanas: Denise Vargas, C.I. 10.111.785 y Yaneth Vaamondez C.I. 10.116.901 ,  todos venezolanos y venezolanas mayores de edad, adscritos a el Área de Bienestar Social de la Oficina de Recursos Humanos / Dirección de Administración de Personal del mencionado ente Ministerial.    </p>
                        <p>Nombre y Apellido del Representante: <b>$nombrePadre</b></p></br>
                        <p> Firma: _________________________________    </p> </br>                       
                        <span align=\"justify\">
                         Anexo: (en el mismo orden y engrapado)</br>
                        <ul>
                        <p>- Fotocopia de la cédula identidad del representante. </p>
                        <p>- Fotocopia de la c&eacute;dula de identidad del representante.   </p>
                        <p>- Fotocopia de la cédula de identidad ampliada del adolescente.    </p>
                        <p>- Fotocopia de la partida de nacimiento del adolescente.</p>
                        <p>- Una (1) foto tipo carnet.    </p>
                        <p>- Constancia de buena salud.    </p>
                        <p>- Permiso de Viaje emitido por el Consejo de Protecci&oacute;n de Ni&ntilde;os, Ni&ntilde;as y Adolescentes. (Obligatorio)    </p>
                        <p>- Para los nuevos ingresos (contratados), debe presentar copia del documento que demuestre el v&iacute;nculo laboral con fecha de inicio m&aacute;xima al 30/04/2013  </p>
                        </ul>
                        </span>
                        </span>";
                     $this->pdf->writeHTML($htmlViaje, true, false, true, false, '');
                }
                    
########################################### IMPRESIÓN DE PDF'S #################################################
                
			$this->pdf->output('Planilla Inscripcion.pdf', 'D');
     }
     
     
}
