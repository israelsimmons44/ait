<?php
class gestion extends Controller{
	function __construct(){
		parent::Controller();
		if (!$this->session->userdata('nombre')){
			redirect(base_url()."index.php/comun/login");
		}
		$this->load->library('parser');
		$this->load->model('gestion_model','gm');
		$this->output->enable_profiler(FALSE);
		$this->load->helper('date');
		$this->load->helper('funciones');
		
	}
	
	function _header(){
		$data['title']="Atenci&oacute;n Integral al Trabajador";
		$this->load->view('main-view',$data);
		$deta['permisos']=$this->session->userdata('permisos');
		$this->load->view('comun/menu2',$deta);		
	}
	
	function index(){
		$this->load->library('formulario');
		$this->formulario->setNombreForm('formpersona');
		$this->formulario->setAction(base_url()."index.php/analista/buscaPersona");
		$data['cedula']=$this->formulario->addInput('frmcedula', 'C&eacute;dula','','required:true');
		$data['btn']=$this->formulario->addButton('btnbuscar','Buscar','enviar1');
		$data['seltramites']=$this->formulario->addSelect('seltramite','Tramites',$this->gm->getTramites());
		$data['selanalista']=$this->formulario->addSelect('selanalista','Analista',$this->gm->getAnalistas());
		
		$data['fechatramite']=$this->formulario->addDatePicker('fechatramite','Fecha');
		$data['fechaenviado']=$this->formulario->addDatePicker('fechaenviado','Fecha');
		$data['fechafirmado']=$this->formulario->addDatePicker('fechafirmado','Fecha');
		
		$contenedor=$this->formulario->output();		
		$data['javascript']=$contenedor->javascript;
		
		$this->_header();
		$mes=intval(date('m'));
		$anho=date('Y');
		
		$data1['resultado']=$this->gm->vistaSeguimiento($anho,$mes);
		
		$data['resultado']=$this->parser->parse('gestion/vista-gestion-view',$data1,TRUE);

		$this->parser->parse('gestion/index-view',$data);
	}
	
	function registra(){
		$data['cedula']=$this->input->post('frmcedula');
		$data['id_tramite']=$this->input->post('seltramite');
		$data['id_usuario']=$this->input->post('selanalista');
		$data['fecha_solicitud']=$this->input->post('fechatramite');
		if ($this->input->post('fechaenviado')){
			$data['fecha_enviada']=$this->input->post('fechaenviado');
		}
		
		if ($this->input->post('fechafirmado')){
			$data['fecha_recibida_firmada']=$this->input->post('fechafirmado');
		}
		
		
		$this->gm->insert($data);
		echo mensajeGrowl("El registro de seguimiento se guard&oacute; correctamente",'ok',3000);
		
	}
	
	function consulta(){
		$this->_header();
		
		$mes=$this->input->post('mes');
		$anho=$this->input->post('anho');
		
		if ($mes==""){
			$mes=intval( date("m"));
			$anho=date("Y");
		}
		
		$this->load->library('formulario');
		//$sql="select distinct date_part('Y',fecha_solicitud) as valor, date_part('Y',fecha_solicitud) as descripcion from seguimiento";
		//$data['fldanho']=$this->formulario->addSelect('anho','A&ntilde;o',$sql);
		$arrMes[1]=array('valor'=>1,'descripcion'=>'Enero');
		$arrMes[2]=array('valor'=>2,'descripcion'=>'Febrero');
		$arrMes[3]=array('valor'=>3,'descripcion'=>'Marzo');
		$arrMes[4]=array('valor'=>4,'descripcion'=>'Abril');
		$arrMes[5]=array('valor'=>5,'descripcion'=>'Mayo');
		$arrMes[6]=array('valor'=>6,'descripcion'=>'Junio');
		$arrMes[7]=array('valor'=>7,'descripcion'=>'Julio');
		$arrMes[8]=array('valor'=>8,'descripcion'=>'Agosto');
		$arrMes[9]=array('valor'=>9,'descripcion'=>'Septiembre');
		$arrMes[10]=array('valor'=>10,'descripcion'=>'Octubre');
		$arrMes[11]=array('valor'=>11,'descripcion'=>'Noviembre');
		$arrMes[12]=array('valor'=>12,'descripcion'=>'Diciembre');
		//$data['fldmes']=$this->formulario->addSelect('mes','Mes',$arrMes);
		$data['strmes']=$arrMes[$mes]['descripcion'];
		$data['stranho']=$anho;
		
		$arrVistaSeguimiento=$this->gm->vistaSeguimiento($anho,$mes);
		$arrNombres=$this->gm->getNombresPersonalSeguimiento();
		//var_dump($arrNombres);
		foreach ($arrVistaSeguimiento as $key=>$value) {
			if (key_exists($value['cedula'], $arrNombres)){
				$arrVistaSeguimiento[$key]['nombre1']=utf8_encode($arrNombres[$value['cedula']]);
				$arrVistaSeguimiento[$key]['fecha_solicitud']=pgDate($arrVistaSeguimiento[$key]['fecha_solicitud']);
			}else{
				$arrVistaSeguimiento[$key]['nombre1']="";
			}
			
			
			//var_dump($arrVistaSeguimiento[$key]);
			//echo "<br><br>";
		}
		
		//$data1['resultado']=$arrVistaSeguimiento;
		
		//$data['resultado']=$this->parser->parse('gestion/vista-gestion-view',$data1,TRUE);
		//$this->load->view('gestion/index-view');
		$this->parser->parse('gestion/consulta-view',$data);
	}
	
	function buscaPersona(){
		$permisos=$this->session->userdata('permisos');
		
		$cedula=$this->input->post('cedula');
		$this->load->library('persona');
		$persona=$this->persona->getPersona($cedula);
		$nombre=utf8_encode($this->persona->getNombre());
		if (trim($cedula)!=""){
			$arrTramites=$this->gm->getTramitesPersona($cedula);
			
			$html="<h3>$nombre</h3><table cellspacing='0' class='display' id='tablamod'><thead>";
			$html.="<tr><th>Tr&aacute;mite</th><th>Registro</th><th>Enviado</th><th>Recibido Firmado</th><th>Entregado</th><th>Analista</th></tr></thead>";
			if ($arrTramites){
				foreach ($arrTramites as $value) {
					$idSeguimiento=$value['id_seguimiento'];
					$nombre=$value['nombre'];
					$fr=pgDate($value['fecha_solicitud']);
					$fe=pgDate($value['fecha_enviada']);
					$frf=pgDate($value['fecha_recibida_firmada']);
					$fen=pgDate($value['fecha_entregada']);
					$analista=$value['analista'];
					
					if($permisos['seguimiento']=='t'){
						$html.="<tr><td><a href='#' onclick='javascript:muestraDialog($idSeguimiento)'>$nombre</a></td><td>$fr</td><td>$fe</td><td>$frf</td><td>$fen</td><td>$analista</td></tr>";
					}else{
						$html.="<tr><td>$nombre</td><td>$fr</td><td>$fe</td><td>$frf</td><td>$fen</td><td>$analista</td></tr>";
					}					
				}
				
			}
			
			$html.="</table>";
			echo $html;
			
		}else{
			echo mensajeGrowl("No se encontr&oacute;",'error',3000);
		}
		
		
	}
	
	function getDetalleSeguimiento(){
		$idSeguimiento=$this->input->post('id_seguimiento');
		$arrSeguimiento=$this->gm->getDetalleSeguimiento($idSeguimiento);
		
		$cedula=$arrSeguimiento['cedula'];
		$this->load->library('persona');
		$persona=$this->persona->getPersona($cedula);
		$arrSeguimiento['nombre']=utf8_encode($this->persona->getNombre());
		
		$arrSeguimiento['fecha_solicitud']=pgDate($arrSeguimiento['fecha_solicitud']);
		$arrSeguimiento['fecha_enviada']=pgDate($arrSeguimiento['fecha_enviada']);
		if (!is_null($arrSeguimiento['fecha_recibida_firmada'])){
			$arrSeguimiento['fecha_recibida_firmada']=pgDate($arrSeguimiento['fecha_recibida_firmada']);
		}
		
		if (!is_null($arrSeguimiento['fecha_entregada'])){
			$arrSeguimiento['fecha_entregada']=pgDate($arrSeguimiento['fecha_entregada']);
		}		
		
		echo json_encode($arrSeguimiento);
	}
	
	function actualizaSeguimiento(){
		$idSeguimiento=$this->input->post('id_seguimiento');
		$post=FALSE;
		$data['mensaje']=mensajeGrowl("No hubo cambios",'ok',3000);
		if ($this->input->post('fecha_recibida_firmada')!=""){
			$post['fecha_recibida_firmada']=datePg($this->input->post('fecha_recibida_firmada'));
			$this->_enviarCorreo($idSeguimiento);
		}
		
		
		if ($this->input->post('fecha_enviada')!=""){
			$post['fecha_enviada']=datePg($this->input->post('fecha_enviada'));
		}
		
		
		
		
		if ($this->input->post('fecha_entregada')!=""){
			$post['fecha_entregada']=datePg($this->input->post('fecha_entregada'));
			
			//$this->_enviarCorreo($idSeguimiento);
		}
		
		
		if ($post){
			if ($this->gm->actualizar($idSeguimiento,$post)){
				$data['mensaje']=mensajeGrowl("Los datos del seguimiento fueron guardados",'ok',3000);
			}else{
				$data['mensaje']=mensajeGrowl("Hubo un error actualizando el seguimiento",'error',3000);
			}
		}
		
		
		echo json_encode($data);
	}
	
	function _enviarCorreo($idSeguimiento){
		$this->load->library('email');
		
		$arrDetalleSeguimiento=$this->gm->getDetalleSeguimiento($idSeguimiento);
		$cedula=$arrDetalleSeguimiento['cedula'];
		$tramite=$arrDetalleSeguimiento['nombre'];
		//var_dump($arrDetalleSeguimiento);
		$arrUsers=$this->adldap->buscar(FALSE,"description=$cedula",TRUE);
		$mail=$arrUsers[0]['mail'][0];
		//return $mail;
		
		$this->email->to($mail); 
		$this->email->from('atencion.altrabajador@mppre.gob.ve');
		
		$msg="Se le agradece pasar por la Oficina de Atencion al Trabajador, para que se sirva retirar su $tramite. \n \n SALUDOS CORDIALES, \n Oficina de Atención Integral al Trabajador";
		//$msg="Su $tramite ha sido procesado, favor diríjase a la Oficina de Atención al Trabajador para retirarlo";
		$this->email->subject('Trámite listo');
		$this->email->message($msg);
		
		$this->email->send();
	}
	
	function reportes(){
		
		$this->load->library('table');
		
		$fecha='2011-09-01';
		$fechaFin='2011-09-21';
		$resultado=$this->gm->getSeguimientoCuenta($fecha,$fechaFin);
		$data['tabla']="";
		$data['solicitudes']="";
		$data['entregados']="";
		if ($resultado){
			$data['resultado']=$resultado;
		}
		
		//$data['tabla']=$this->table->generate(array(array('algo'=>$resultado->nroSolicitudes,'again'=>$resultado->nroEntregados),array('otro'=>5,'apele'=>66)));
		$this->_header();
		
		$this->parser->parse('gestion/reportes-view',$data);
	}
	
	function data(){
		$array[]=array('firefox',4);
		$array[]=array('IE',5);
		
		echo json_encode($array,512);
	}
	
	function probando() {
            echo 'estoy molesta, pero habrá gozo!';
        }
	
}