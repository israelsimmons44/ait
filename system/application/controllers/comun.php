<?php
class comun extends Controller{
	function __construct(){
		parent::Controller();
		$this->load->helper('form');
	}
	
	function index(){
		
	}
	
	function _header(){
		$data['title']="Atenci&oacute;n Integral al Trabajador";
		$this->load->view('main-view',$data);
		$deta['permisos']=$this->session->userdata('permisos');
		$this->load->view('comun/menu2',$deta);
		
		//$this->jquery->add_script();
		
	}
	
	function ajaxLogin(){
		$login=$this->input->post('login');
		$password=$this->input->post('password');
		$this->load->model('comun_model','cm');
		$this->session->sess_destroy();
		
		$this->cm->cargaUsuarioSesion($login,$password);
		
		redirect(base_url());
		//echo $login;
	}
	
	function login(){
		$this->session->sess_destroy();
		$this->_header();
		$script = <<<EOD
$(document).ready(function(){
	$("#resultado-ajax").hide();
});		
EOD;
		$this->jquery->add_script($script);
		
		
		$this->load->helper('funciones');
		$frmLogin['open']=form_open(base_url().'index.php/comun/ajaxLogin',array('name'=>'frmlogin','id'=>'frmlogin'));
		$frmLogin['label'][]=form_label("Usuario","login");
		$frmLogin['input'][]=form_input('login','','size="25"');
		$frmLogin['label'][]=form_label("Contraseña","password");
		$frmLogin['input'][]=form_password('password','','size="25"');
		$frmLogin['submit']=form_submit('','Iniciar Sesión');
		$frmLogin['close']=form_close();
		$arr['form']="frmlogin";
		$arr['url']=base_url()."index.php/comun/ajaxLogin";
		
		$data['frmLogin']=$frmLogin;
		$this->jquery->add_script(ajaxifica($arr));
		
		$this->jquery->output();
		$this->load->view('comun/login-view',$data);
		
		
	}
	
	function logout(){
		$this->session->sess_destroy();
		redirect(base_url());
	}
	
}