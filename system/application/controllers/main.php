<?php
class main extends Controller{
	function __construct(){
		parent::Controller();	
		$this->load->library('parser');
		if (!$this->session->userdata('nombre')){
			redirect(base_url()."index.php/comun/login");
		}	
		
		$this->load->helper('form');
	}
	
	function index(){
		$this->_header();
	}
	
	function _header(){
		$data['title']="Atenci&oacute;n Integral al Trabajador";
		$this->load->view('main-view',$data);
		$deta['permisos']=$this->session->userdata('permisos');
		$this->load->view('comun/menu2',$deta);
	}
	
	function solicitud(){
		$this->_header();
		$this->load->library('table');
		$this->load->helper('funciones');
		
		$cedula=$this->session->userdata('cedula');
		$nombre=$this->session->userdata('nombre');
		$tipo=$this->session->userdata('tipo');
		$usuario=$this->session->userdata('usuario');
		
		$this->load->model('solicitud_model',"sm");
		$data['recaudos']="";
		$data['tramites']="";
		$data['tituloRecaudos']="";
		$data['form']="";
		$data['cedula']=$cedula;
		$data['usuario']=$usuario;
		
		$accion=$this->uri->segment(3);
		if ($accion!=""){
			 $resultado=$this->sm->getRecaudos($accion);
			 $data['recaudos2']=$this->sm->getRecaudosForm($accion);
			 $data['ajaxfrmrecaudos']=ajaxifica(array('form'=>'frmsolicitud', 'url'=>base_url()."index.php/main/grabaSolicitud"),'enviafrmsolicitud');
			 
			 $data['recaudos']=htmlRecaudos($resultado,TRUE);
			 $data['tituloRecaudos']=$resultado['titulo'];
			 //$data['recaudos']=$this->table->generate($resultado['detallado']);
		}		
		
		$data['listaSolicitud']=htmlTramites($this->sm->getTramites());
$strPersona=<<<EOF
<h4 style='margin-bottom:5px;'>Datos Personales</h4>
<table>
<tr><td>Cédula</td><td>$cedula</td></tr>
<tr><td>Nombre</td><td>$nombre</td></tr>
<tr><td>Tipo</td><td>$tipo</td></tr>
<tr><td>Usuario</td><td>$usuario</td></tr>
</table>
EOF;

$strPersona.=$this->sm->getSolicitudesPersona($cedula);
		
		$data['persona']=$strPersona;
		$arr['form']="frmCaso";
		$arr['url']=base_url()."index.php/admin/procesa/ingresacaso";
		$data['scriptAjax']=ajaxifica($arr);
		
		$this->parser->parse('solicitud-view',$data);
	}
	
	function grabaSolicitud(){
		$this->load->model('solicitud_model',"sm");
		$this->sm->setSolicitud($_POST);
	}
	
	function pruebaform(){
		$this->_header();
		$this->load->library('parser');
		$this->load->library('formulario');
		
		$this->formulario->setAction(base_url()."/index.php/main/consultatramite");
		$this->formulario->addInput("nombre","Israel Joshua","required");
		$this->formulario->addButton("btnEnvio","Enviar", "enviar");
		
		$formulario= $this->formulario->output();
		
		$data['fldnombre']=$formulario->campos['nombre'];
		$data['btnenviar']=$formulario->campos['btnEnvio'];
		$data['escript']=$formulario->javascript;
		
		$this->parser->parse('pruebaforma-view',$data);
		
	}
	
	function consultatramite(){
		$this->_header();
		$this->load->model('solicitud_model','sm');
		$this->load->helper('funciones');
		$cedula=$this->session->userdata('cedula');
		$this->load->library('persona');
		$this->persona->getPersona($cedula);
		$data['cedula']=number_format($cedula,0,",",".");
		$data['nombre']=$this->persona->getNombre();
		$data['tipopersonal']=$this->persona->getTipoPersonal();
		$data['fechaingreso']=pgDate($this->persona->getFechaIngreso());
		$data['adscripcion']=$this->persona->getAdscripcion();
		$data['cargo']=$this->persona->getCargo();
		$arr=$this->sm->getSolicitudesPersona($cedula,TRUE);
		//var_dump($arr);
		
		if ($arr){
			$html="<table id='solicitudes' class='tabla' border='0' cellspacing='0' width='100%'><thead><tr><th>Fecha</th><th># Sol.</th><th>Descripción</th><th>&nbsp;</th><th>&nbsp;</th></tr></thead><tbody>";
			foreach ($arr as $valor){
				if ($valor['cod_tramite']=='SOL008'){
					$html.="<tr id=".$valor['id_solicitud']."><td>".$valor['fecha']."</td><td align='center' >".$valor['id_solicitud']."</td><td>".$valor['nombre']."</td><td>&nbsp;</td><td><a href='#' onclick='javascript:eliminaSolicitud(".$valor['id_solicitud'].")'><img src='".base_url()."/images/delete.png'/></a></td></tr>";
				}else{
					$html.="<tr id=".$valor['id_solicitud']."><td>".$valor['fecha']."</td><td align='center' >".$valor['id_solicitud']."</td><td>".$valor['nombre']."</td><td><a href='".base_url()."index.php/imprimirplanilla/index/". $valor['id_solicitud']."'><img src='".base_url()."/images/printer_on.png'/></a></td><td><a href='#' onclick='javascript:eliminaSolicitud(".$valor['id_solicitud'].")'><img src='".base_url()."/images/delete.png'/></a></td></tr>";
				}
				
			}
			$html.="</tbody></table>";
		}else{
			$html="No Hay Solicitudes";
		}
		
		$data['solicitudes']=$html;
		
		$this->parser->parse('consultatramite-view',$data);
	}
	
	function eliminaSolicitud(){
		$idSolicitud=$this->uri->segment(3);
		$this->load->model('solicitud_model',"sm");
		$this->sm->deleteSolicitud($idSolicitud);
	}

	
	function solicitud2(){
		$this->_header();
		$this->load->model('solicitud_model',"sm");
		$this->load->library('parser');
		$this->session->set_userdata('cedulatramite','');
		$cedula=$this->session->userdata('cedula');
		$nombre=$this->session->userdata('nombre');
		$tipo=$this->session->userdata('tipo');
		$usuario=$this->session->userdata('usuario');
		
		
		$strPersona=<<<asd
<h4 style='margin-bottom:5px;'>Datos Personales</h4>
<table>
<tr><td>Cédula</td><td>$cedula</td></tr>
<tr><td>Nombre</td><td>$nombre</td></tr>
<tr><td>Tipo</td><td>$tipo</td></tr>
<tr><td>Usuario</td><td>$usuario</td></tr>
</table>
asd;

		$strPersona.=$this->sm->getSolicitudesPersona($cedula);
		$data['persona']=$strPersona;
		
		
		$data['lista']=$this->sm->getTramites();
		
		$this->parser->parse('solicitud2-view',$data);
	}
	
}