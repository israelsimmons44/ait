<?php
class Misiones extends Controller{
	function __construct(){
		
		parent::Controller();
		
		if (!$this->session->userdata('nombre')){
			redirect(base_url()."index.php/comun/login");
		}
		
		$this->load->model('misiones_model','mm');
		$this->load->library('parser');
		//$this->load->library('pdf');
		$this->load->helper('funciones');
		
	}

////////////////////////////////////////////// FUNCIÓN QUE CARGA EL BANER Y MENÚ /////////////////////////	
	
	function _header(){
		$data['title']="Atenci&oacute;n Integral al Trabajador";
		$this->load->view('main-view',$data);
		$deta['permisos']=$this->session->userdata('permisos');
		$this->load->view('comun/menu2',$deta);		
	}	
	
	function detallePersonalMision(){
		//$this->_header();
		//$idDependencia=$this->input->post('idDependencia');
		$idDependencia = $this->uri->segment(3);
		//$idDependencia=1219;
		$mision = $this->mm->getDependencia($idDependencia);		
		$data['dependencia']= $mision; 	 
		$data['personalactivo']=$this->mm->getPersonalActivo($idDependencia);
		$data['title']="Personal Servicio Exterior ";
		//$this->load->view('menu');
		//$this->load->view('main-view',$data);
		//$this->jquery->output();		
		//$this->parser->parse('misiones/personalactivo-view',$data);
		$this->parser->parse('misiones/personalactivo-view',$data);
	}
	
}