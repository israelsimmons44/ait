## Atención al trabajador (*Employee Service Department*)

This app is an extension of SIGEFIRRHH which is a Web Java Application created by the Venezuelan Ministry of Teasury and Finance for Payroll and Human Resourses Management in the public administration.

It was made on CodeIgniter a popular PHP framework by that time.

AIT connects directly to the SIGEFIRRHH database and get all data necesary in order to give access to the public administration's employees to his pay stubs, make requests like job letters