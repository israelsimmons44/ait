<?php
class CI_highchart {
	
	private $series=array();
	private $tipo="column";
	private $container="aquivaelgrafico";
	private $titulo="TITULO DEL GRAFICO";
	private $subTitulo="";
	private $xAxis="";
	private $tituloY="";
	
	
	function __construct(){
		log_message('debug', "Clase HighChart inicializada");
		
		$this->CI =& get_instance();
	}
	
	function render(){
		$render="<script type=\"text/javascript\">

		var chart1; 
$(function(){
	
	chart1=new Highcharts.Chart({
		'chart':{'renderTo':'$this->container','defaultSeriesType': '$this->tipo'},
		'title':{'text':'$this->titulo'},
		'plotOptions': {
			column: {
				allowPointSelect: true,
				cursor: 'pointer',
				dataLabels: {
					enabled: true
					
				}
			}
		},
		tooltip: {
			formatter: function() {
				return ''+
					this.x +': '+ this.y +' ';
			}
		},

		xAxis: {
			categories: $this->xAxis
		},
		yAxis: {
			min: 0,
			title: {
				text: '$this->tituloY'
			}
		},
		plotOptions: {
			column: {
				pointPadding: 0.1,
				borderWidth: 0
			}
		},
		credits:{enabled:false},

		'series':".$this->renderSeries()."
	})
})

		
	</script>
		";
		return $render;
	}
	
	function renderPie(){
		$render="<script type=\"text/javascript\">

		var chart1;
		$(function() {
			chart1 = new Highcharts.Chart({
				chart: {
					renderTo: 'aquivaelgrafico',
				},
				title: {
					text: 'Browser market shares at a specific website, 2010'
				},
				tooltip: {
					formatter: function() {
						return '<b>'+ this.point.name +'</b>: '+ this.y +' %';
					}
				},
				plotOptions: {
					pie: {
						allowPointSelect: true,
						cursor: 'pointer'
						
					}
				},
			    series: ".$this->renderSeries()."
			});
		});
	</script>";
	return $render;
	
	}
	
	function addSerie($arrSerie){
		if (key_exists('nombre', $arrSerie)){
			$nombre=$arrSerie['nombre'];
		}else{
			$nombre='serie'.count($this->series);
		}
		$this->series[]=array('name'=>$nombre,'data'=>$arrSerie['data']);
	}
	
	public function renderSeries(){
		return json_encode($this->series);
	}
	
	public function getSeries(){
		return $this->series;
	}
	
	public function setTipo($tipo='column'){
		$this->tipo=$tipo;
	}
	
	public function getTipo(){
		return $this->tipo;
	}
	
	public function setXAxis($arrXAxis){
		$this->xAxis=json_encode($arrXAxis);
	}
	
	public function getXAxis(){
		return $this->xAxis;
	}
	
	public function setTitulo($titulo){
		$this->titulo=$titulo;
	}
	
	public function setTituloY($tituloY){
		$this->tituloY=$tituloY;
	}
	
}