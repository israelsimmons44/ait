<?php

    class SMTPMAIL
    {
        var $host="";
        var $port=25;
        var $error;
        var $state;
        var $con=null;
        var $greets="";
        
        function SMTPMAIL()
        {
            $this->host=ini_get("SMTP");
            $this->port=25;
            $this->state="DISCONNECTED";
        }
        function set_host($host)
        {
            $this->host=$host;
        }
        function set_port($port=25)
        {
            $this->port=$port;
        }
        function error()
        {
            return $this->error;
        }
        function connect($host="",$port=25)
        {
            if(!empty($host))
                $this->host($host);
            $this->port=$port;
            if($this->state!="DISCONNECTED")
            {
                $this->error="Error : connection already open.";
                return false;
            }
            
            $this->con=@fsockopen($this->host,$this->port,$errno,$errstr);
            if(!$this->con)
            {
                $this->error="Error($errno):$errstr";
                return false;
            }
            $this->put_line("HELO smtp.atencion");
            $this->state="CONNECTED";
            $this->greets=$this->get_line();
            
            return true;
        }
        
        
        function sendmail($para, $subject, $mensaje, $de){
        	$newLine="\r\n";
        	$ret=$this->connect();
        	if (!$ret)	
        		return $ret;
        		
        	$this->put_line("MAIL FROM:<$de>");
        	$arrLogMail['MAIL FROM:']=$this->get_line();
        	
        	$this->put_line("RCPT TO:<$para>");
        	$arrLogMail['RCPT TO:']=$this->get_line();
        	
        	$this->put_line("DATA");
        	$arrLogMail['DATA:']=$this->get_line();
        	
        	$headers = "MIME-Version: 1.0" . $newLine;
			$headers .= "Content-type: text/html; charset=iso-8859-1" . $newLine;
			$headers .= "To: <$para>" . $newLine;
			$headers .= "From: <$de>" . $newLine;
			
			$this->put_line("To: $para\r\n From: $de\r\nSubject:$subject\r\n$headers\r\n\r\n$mensaje\r\n.\r\n ");
			$arrLogMail['headers']=$this->get_line();
			
			$this->put_line("QUIT" . $newLine);
			$arrLogMail['QUIT']=$this->get_line();
			
			return $arrLogMail;
        }
        
        
        function send_smtp_mail($to,$subject,$data,$cc="",$from='')
        {
            $ret=$this->connect();
            if(!$ret)
                return $ret;
            
            $this->put_line("MAIL FROM: $from");
            $response=$this->get_line();
            
            if(intval(strtok($response," "))!=250)
            {
                $this->error=strtok($response,"\r\n");
                return false;
            }
            $to_err=preg_split("/[,;]/",$to);
            foreach($to_err as $mailto)
            {
                $this->put_line("RCPT TO: $mailto");
                $response=$this->get_line();
                
                if(intval(strtok($response," "))!=250)
                {
                    $this->error=strtok($response,"\r\n");
                    return false;
                }
            }
            if(!empty($cc))
            {
                $to_err=preg_split("/[,;]/",$cc);
                foreach($to_err as $mailto)
                {
                    $this->put_line("RCPT TO: $mailto");
                    $response=$this->get_line();
                    
                    if(intval(strtok($response," "))!=250)
                    {
                        $this->error=strtok($response,"\r\n");
                        return false;
                    }
                }
            }
            $this->put_line("DATA");
            $response=$this->get_line();
            
            if(intval(strtok($response," "))!=354)
            {
                $this->error=strtok($response,"\r\n");
                echo $response."<br>";
                return false;
            }
            
            $this->put_line("SUBJECT: $subject");
            $this->put_line("");
            $this->put_line($data);
            $this->put_line(".");
            $this->put_line("QUIT");
            $response=$this->get_line();
            
            if(intval(strtok($response," "))!=250)
            {
            	echo "se quedo aqui";
                $this->error=strtok($response,"\r\n");
                return false;
            }
            $this->close();
            echo "llego aqui";
            return true;
        }
        // This function is used to get response line from server
        function get_line()
        {
        	$line="";
            while(!feof($this->con))
            {
                $line.=fgets($this->con);
                if(strlen($line)>=2 && substr($line,-2)=="\r\n")
                	//echo substr($line,0,-2)."<br>";
                    return(substr($line,0,-2));
            }
        }
        ////This functiuon is to retrive the full response message from server

        ////This functiuon is to send the command to server
        function put_line($msg="")
        {
            return @fputs($this->con,"$msg\r\n");
        }
        
        function close()        
        {
            @fclose($this->con);
            $this->con=null;
            $this->state="DISCONNECTED";
        }
    }