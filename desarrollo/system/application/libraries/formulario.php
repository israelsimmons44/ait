<?php
/**
 * CodeIgniter Formulario Class
 *
 * @package		CodeIgniter
 * @subpackage	Libraries
 * @category	Libraries
 * @author		Israel Simmons
 * @link		PENDIENTE
 */
class CI_Formulario{
	var	$campos			=array();
	var	$camposHid		=array();
	var $reglas			=array();
	var $nombreForm		="form";
	var $ajaxFunction	="";
	var $action			="";
	var $_funcion		="enviar";
	var $success		="";
	var $_scriptJquery	=array();
	var $_reglas		=array();
	var $_label			=array();
	var $_editores		=array();
	
	
	function __construct(){
		log_message('debug', "Clase Formulario inicializada");
		
		$this->CI =& get_instance();
	}
	/**
	 * 
	 * Añadir input tipo texto al formulario que se esta creando
	 * @param string $nombre 	--nombre que tendra el input en el formulario
	 * @param string $label 	--etiqueta de informacion para el campo
	 * @param string $valor 	-- valor predetarminado del campo
	 * @param string $reglas   	--reglas jquery validator
	 * @param array $extras		--extras para expandir los atributos del input ejm. array('class'=>'campoformulario', 'maxlength'=>40)	
	 */
	function addInput($nombre, $label,$valor="", $reglas="", $extras=array()){
		$ext="";
		if (count($extras)>0){
			foreach($extras as $key=>$value){
				$ext.="$key='$value'";
			}
		}
		
		$this->campos[$nombre]="<input class='' type='text' name='$nombre' id='$nombre' value='$valor' $ext/>";
		if ($reglas!=""){
			$this->_reglas[$nombre]=$reglas;
		}
		
		$this->_label[$nombre]=$label;
		return $this->campos[$nombre];
	}
	
	function addTextArea($nombre, $label,$valor="", $reglas="", $extras=array()){
		$ext="";
		if (count($extras)>0){
			foreach($extras as $key=>$value){
				$ext.="$key='$value'";
			}
		}
		
		$this->campos[$nombre]="<textarea name='$nombre' id='$nombre' $ext>$valor</textarea>";
		if ($reglas!=""){
			$this->_reglas[$nombre]=$reglas;
		}
		
		$this->_label[$nombre]=$label;
		return $this->campos[$nombre];
	}
	
	function addInput2($nombre, $label, $valor="", $reglas=""){
		$inpu=new Input_Field($nombre, $label, $valor, $reglas);
		$this->campos[$nombre] =  $inpu->campo;
		$this->_label[$nombre]=$label;
		return $inpu->campo;
	}
	
	function addButton($nombre, $texto, $funcion=""){
		if ($funcion==""){
			$funcion=$this->_funcion;
		}
		$this->campos[$nombre]="<button name='$nombre' id='$nombre' onclick='javascript:$funcion();return false;' >$texto</button>";
		$this->_funcion=$funcion;
		return $this->campos[$nombre];
	}
	
	function addDatePicker($nombre, $label,$reglas=""){
		$this->campos[$nombre]="<input class='ui-corner-all' name='$nombre' id='$nombre' />";
		$this->_label[$nombre]=$label;
		$this->_scriptJquery[]='$("#'.$nombre.'").datepicker({ dateFormat: "dd-mm-yy" }); ';
		if ($reglas!=""){
			$this->_reglas[$nombre]=$reglas;
		}		
		return $this->campos[$nombre];		
	}
	
	function addSelect($nombre, $label, $valores, $selected=0, $reglas="" ){
		$string="<select name='$nombre' id='$nombre'>%OPTIONS%</select>";
		
		$str="<option value=''>Seleccione una opción</option>";
		
		if (!is_array($valores)){
			$this->CI->load->database();
			$sql=$valores;
			$res=$this->CI->db->query($sql);
			if ($res->num_rows>0){
				foreach ($res->result_array() as $valor){
					$str.="<option value='".$valor['valor']."'>".$valor['descripcion']."</option>\n";
				}
			}
			
		}else{
			if (count($valores)>0){
				foreach ($valores as $valor){
					$str.="<option value='".$valor['valor']."'>".$valor['descripcion']."</option>\n";
				}
			}
		}
		
		
		if ($reglas!=""){
			$this->_reglas[$nombre]=$reglas;
		}		
		
		$this->campos[$nombre]=str_replace('%OPTIONS%',$str,$string);
		$this->_label[$nombre]=$label;
		return $this->campos[$nombre];		
	}
	
	function addCheckBox($nombre,$label, $checked=FALSE){
		if ($checked==TRUE){
			$this->campos[$nombre]="<input type='checkbox' checked='checked' name='$nombre' id='$nombre' value='$nombre'>";
		}else{
			$this->campos[$nombre]="<input type='checkbox' name='$nombre' id='$nombre' value='X'>";
		}
		
		$this->_label[$nombre]="<label for='$nombre'>$label</label>";
		return $this->campos[$nombre];
	}
	
	function addHidden($nombre, $valor=""){
		$this->camposHid[$nombre]="<input type='hidden' name='$nombre' id='$nombre' value='$valor'>";
		return $this->camposHid[$nombre];
	}
	
	function addAutoComplete($nombre, $nombreHidden='', $rutaDatos=''){
		$this->campos[$nombre]="<input class='ui-corner-all' type='text' name='$nombre' id='$nombre' />";
		$this->addHidden($nombreHidden,"");
		
		$str=<<<EOF
function seleccionado(codigo){
	$('#$nombreHidden').val(codigo);
}		
		
$('#$nombre').autocomplete({
	source: "$rutaDatos",
	minLength: 4,
	select: function(event, ui){
		seleccionado(ui.item ? (ui.item.codigo):"");			
	}})
EOF;

		$this->_scriptJquery[]=$str;
		return $this->campos[$nombre];
	
	}
	
	function addEditor($nombre, $label,$valor){
		$this->campos[$nombre]="<textarea id='$nombre' name='$nombre' style='text-indent:20px;'>$valor</textarea>";
		$this->_label[$nombre]="<label for='$nombre'>$label</label>";
		$this->_scriptJquery[]="var $nombre = CKEDITOR.replace( '$nombre',
		{
			toolbar : [ ['Format','-', 'Bold', 'Italic', 'Underline', '-','NumberedList','BulletedList','-','Outdent','Indent','-','Table' ] ]
		});";
		
		$this->_editores[$nombre]="var $nombre = CKEDITOR.instances.template_body.getData();";
		
	}
	
	function output(){
		$this->_setAjaxScript();
		
		$arr=$this->campos;
		if (count($this->camposHid)>0){
			$arr=array_merge($this->camposHid,$arr);
		}
		
		$form=new Formulario_colector($arr, $this->ajaxFunction);
		return $form;
	}
	
	function outputHTML(){
		$this->_setAjaxScript();
		$html="<form method='post' id='$this->nombreForm' name='$this->nombreForm'>";
		
		foreach ($this->camposHid as $key=>$row){
			$label="";
			if(key_exists($key,$this->_label)){
				$label=$this->_label[$key];
			}
			
			$html.= "$row";
		}
		
		
		$html.="<table width='100%'>";
		foreach ($this->campos as $key=>$row){
			$label="";
			if(key_exists($key,$this->_label)){
				$label=$this->_label[$key];
			}
			
			$html.= "<tr><td width='150'>$label</td><td>$row</td></tr>";
		}
		$html.="</table>";
		$html.="</form>";
		
		$html.="\n<script>$this->ajaxFunction</script>";
		
		return $html;
		
	}
	
	function _setAjaxScript(){
		$str=<<<EOF
$('button').button();		
function $this->_funcion (){
	if($('#$this->nombreForm').valid()){
		$('#resultado-ajax').hide();
		$('#cargando').show();
		
		$.ajax({
			url:'$this->action',
			type:'post',
			data:$('#$this->nombreForm').serialize(),
			success:function(data){ $('#resultado-ajax').html(data); $('#cargando').hide();$('#resultado-ajax').show(); }
		});
	}
	
}
EOF;

		$row1="";
		$row2="";
		
		
	foreach ($this->_scriptJquery as $row){
		$row1.=$row."\n";	
	}
	
	
	
	foreach ($this->_reglas as $key=>$row){
		$row2.= "$key : { $row }, ";
	}
	$row2=substr($row2,0,strlen($row2)-2);
	
	$reglas="$('#$this->nombreForm').validate({
		rules:{
			$row2 
		}}
	);\n";
	
	

	$str.="$(document).ready(function(){
			$row1;
			
			$reglas;
		})";
	
	
	
	$this->ajaxFunction=$str;
	return $this->ajaxFunction;
	}
	
	
	
	
/**
 * getters y setters
 */
	function setNombreForm($nombre){
		$this->nombreForm=$nombre;
	}
	
	function getNombreForm(){
		return $this->nombreForm;
	}
	
	function setAction($nombre){
		$this->action=$nombre;
	}
	
	function getAction(){
		return $this->action;
	}
	
	function getJavascript(){
		return $this->ajaxFunction;
	}
	
	
}

class Formulario_colector{
	var	$campos		=array();
	var $formHTML="";
	var $javascript="";

	function __construct($campos, $javascript){
		$this->campos=$campos;
		
		$this->javascript=$javascript;
	}
}

class Input_Field extends Campo{
	var $validacion		= "";
	var $valor			="";
	
	function __construct($tipo, $nombre, $valor, $validacion){
		
		$this->setValidacion($validacion);
		$this->setValor($valor);
		
		return parent::__construct($tipo, $nombre);
		
	}	
	
	/**
	 * @param $valor the $valor to set
	 */
	private function setValor($valor) {
		$this->valor = $valor;
	}

	/**
	 * @return the $valor
	 */
	private function getValor() {
		return $this->valor;
	}
	
	/**
	 * @param $validacion the $validacion to set
	 */
	private function setValidacion($validacion) {
		$this->validacion = $validacion;
	}

	/**
	 * @return the $validacion
	 */
	private function getValidacion() {
		return $this->validacion;
	}
		
}

class Select extends Campo{
	var $validacion		= "";
	var $valor			="";
		
	function __construct($nombre, $valores=array()){
		$this->setValor("");
		$this->setValidacion("");
		$res=parent::__construct('select',$nombre);
		
		$acc=$this->_setOptions($valores);
		return "xxx";
		
	}
	
	function _setOptions($arr){
		$str="<option value=''>Elija una opción</option>";
		if (count($arr)>0){
			foreach ($arr as $valor){
				$str.="<option value='".$valor['valor']."'>".$valor['descripcion']."</option>\n";
			}
		}
		return str_replace('%OPTIONS%',$str,$this->campo);
		
	}
	
/**
	 * @param $valor the $valor to set
	 */
	private function setValor($valor) {
		$this->valor = $valor;
	}

	/**
	 * @return the $valor
	 */
	private function getValor() {
		return $this->valor;
	}
	
	/**
	 * @param $validacion the $validacion to set
	 */
	private function setValidacion($validacion) {
		$this->validacion = $validacion;
	}

	/**
	 * @return the $validacion
	 */
	private function getValidacion() {
		return $this->validacion;
	}
	
}

class Campo {
	var $nombre="";
	var $tipo="";
	var $label="";
	var $campo="";
	
	function __construct($tipo, $nombre){		
		$this->setNombre($nombre);
		$this->setLabel($nombre);
		$this->setTipo($tipo);
		
		switch ($tipo){
			case "text":
				$string = "<input type='text' name='$nombre' id='$nombre' />";
				break;
			case "select":
				
				$string="<select name='$nombre' id='$nombre'>%OPTIONS%</select>";
				break;
		}
		
		$this->setCampo($string);
		return $this->getCampo();
	}	
	
	/**
	 * @param $campo the $campo to set
	 */
	private function setCampo($campo) {
		$this->campo = $campo;
	}

	/**
	 * @return the $campo
	 */
	private function getCampo() {
		return $this->campo;
	}
	
	
	/**
	 * @param $label the $label to set
	 */
	private function setLabel($label) {
		$this->label = $label;
	}

	/**
	 * @param $tipo the $tipo to set
	 */
	private function setTipo($tipo) {
		$this->tipo = $tipo;
	}

	/**
	 * @param $nombre the $nombre to set
	 */
	private function setNombre($nombre) {
		$this->nombre = $nombre;
	}

	/**
	 * @return the $label
	 */
	private function getLabel() {
		return $this->label;
	}

	/**
	 * @return the $tipo
	 */
	private function getTipo() {
		return $this->tipo;
	}

	/**
	 * @return the $nombre
	 */
	private function getNombre() {
		return $this->nombre;
	}

	
	
}



