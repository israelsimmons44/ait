<div class="ui-dialog ui-widget ui-widget-content ui-corner-all" style="float:left;position:relative;margin-right:20px;width:890px;">
   <div class="ui-dialog-titlebar ui-widget-header ui-corner-top ui-helper-clearfix">
      <span id="ui-dialog-title-dialog" class="ui-dialog-title">Tipos de Reclamo</span>
      <a class="ui-dialog-titlebar-close ui-corner-all" href="#"></a>
   </div>
   <div style="min-height: 109px; width: auto;overflow: hidden;" class="ui-dialog-content ui-widget-content" id="dialog">
   	<div style="float:left;">
		   {formulario}
	</div>
	<div style="float:right;width:400px;" >
		<table id="tiporeclamo" class="display" cellspacing="0">
			<thead><tr><th>Id</th><th>Descripci&oacute;n</th></tr></thead>
			{tiporeclamos}
				<tr><td>{id_tipo_reclamo}</td><td>{descripcion}</td></tr>
			{/tiporeclamos}
		</table>
	</div>
	  
	<div id='cargando' style="display:none;width:450px;float:right;"><img src="<?=base_url()?>images/ajax-loader.gif"/>GUARDANDO LOS DATOS</div>
    <div id='resultado-ajax' style="width:450px;float:right;"></div>
   </div>
   
<script type="text/javascript">
	$(function(){
		$('#tiporeclamo').dataTable({"aaSorting": [[ 0, "asc" ]],
			"bAutoWidth":false,
			"bJQueryUI": true,
			
			"iDisplayLength": 10,
			"oLanguage": {
						"sZeroRecords": "No se encontraron registros!!!"
												
			}
});
		$('.ui-corner-tl').hide();
	})
</script>
