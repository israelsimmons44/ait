<div class="ui-dialog ui-widget ui-widget-content ui-corner-all" style="float:left;position:relative;margin-right:20px;width:890px;">
   <div class="ui-dialog-titlebar ui-widget-header ui-corner-top ui-helper-clearfix">
      <span id="ui-dialog-title-dialog" class="ui-dialog-title">Sugerencias</span>
      <a class="ui-dialog-titlebar-close ui-corner-all" href="#"></a>
   </div>
   <div style="height:auto; width: auto;overflow: hidden;" class="ui-dialog-content ui-widget-content" id="dialog">
   	
			<table width="100" cellspacing="0" cellpadding="5" id="detalleSugerencia" class="display">
      		<thead>
      			<tr><th>Id</th><th>C&eacute;dula</th><th>Usuario</th><th>Sugerencia</th></tr>
      		</thead>
      		<tbody>
			{sugerencias}
				<tr><td align="center">{id_sugerencia}</td><td align="right">{cedula}</td><td>{usuario}</td><td width="600">{sugerencia}</td></tr>
			{/sugerencias}
			</tbody>
			</table>
      
   </div>
</div>

<script>
	$(function(){
		$('#detalleSugerencia').dataTable({"aaSorting": [[ 1, "asc" ]],
			"bAutoWidth":false,
			"bJQueryUI": true,
			"sPaginationType": "full_numbers",
			"iDisplayLength": 10,
			"oLanguage": {
						"sZeroRecords": "No se encontraron registros!!!"
												
			}


});
	})
</script>