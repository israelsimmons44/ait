<div class="ui-dialog ui-widget ui-widget-content ui-corner-all" style="width:895px;margin-top:15px;">
   <div class="ui-dialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix">
      <span id="ui-dialog-title-dialog" class="ui-dialog-title">Registro de Recaudos</span>
      
   </div>
   <div style=" min-height: 109px; width: auto;" class="ui-dialog-content ui-widget-content" id="dialog">
      <div>
      
		<form method="post" id="frmRecaudos">
			<table>
				<tr><td>Nombre</td><td><input type="text" name="nombre" id="nombre" class="required" size="50"/></td></tr>
				<tr><td>ruta de planilla</td><td><input type="text" name="ruta_planilla" id="ruta_planilla" size="50"/></td></tr>
				<tr><td colspan="2"><input type="submit" onclick="javascript:envia();return false;" id="submit" value="Guardar"/></td></tr>
			</table>
			
		</form>
		
	</div>
	
   </div>
   
</div>
<div class="ui-widget" id="anuncio">
	<div class="ui-state-highlight ui-corner-all" style="padding: 0pt 0.7em; margin-top: 10px;">
		<p>
		<span class="ui-icon ui-icon-info" style="float: left; margin-right: 0.3em;" ></span>
		<span id="anuncio-texto">pp</span>
		</p>
		
	</div>
</div>

<div class="ui-widget" id="anuncio-error">
	<div class="ui-state-error ui-corner-all" style="padding: 0pt 0.7em; margin-top: 10px;">
		<p>
		<span class="ui-icon ui-icon-alert" style="float: left; margin-right: 0.3em;" ></span>
		<span id="anuncio-texto-error">pp</span>
		</p>
		
	</div>
</div>

<script type="text/javascript">
<!--
	$(document).ready(function(){
		$('#anuncio').hide();
		$('#anuncio-error').hide();
		$('#frmRecaudos').validate({
			messages:{
				nombre:{required:"Este campo es requerido",minlength:"Debe ser de al menos 3 caracteres"},
				ruta_planilla:{required:"Este campo es requerido"}
			},
			rules:{
				nombre:{required:true,minlength:3},
				ruta_planilla:{required:true}
			}
		});
	})
	
	function envia(){
		if ($('#frmRecaudos').valid()==true){
			$.ajax({
				url:"<?=base_url()."index.php/admin/procesa/ingresarecaudo"?>",
				data:$('#frmRecaudos').serialize(),
				type:"POST",
				success:function(data){
						$('#anuncio-texto').html(data);
						$('#anuncio').fadeIn(500).delay(3000).fadeOut(1000);
				}
			});
		}else{
			$('#anuncio-texto-error').html("Hay errores en el formulario, por favor corr&iacute;jalos");
			$('#anuncio-error').fadeIn(500).delay(3000).fadeOut(1000);
		}
	}
	
//-->
</script>