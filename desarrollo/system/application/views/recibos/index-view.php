<div class="ui-dialog ui-widget ui-widget-content ui-corner-all" style="float:left;position:relative;margin-right:20px;width:890px;">
   <div class="ui-dialog-titlebar ui-widget-header ui-corner-top ui-helper-clearfix">
      <span id="ui-dialog-title-dialog" class="ui-dialog-title">Consulta de Recibos de Pago</span>
      <a class="ui-dialog-titlebar-close ui-corner-all" href="#"></a>
   </div>
   <div style="min-height: 109px; width: auto;overflow: hidden;" class="ui-dialog-content ui-widget-content" id="dialog">
   		<div>
   			<p>Informaci&oacute;n hist&oacute;rica a partir del a&ntilde;o 2010 </p><p>para recibos anteriores a este periodo consultar en <a href="http://recibos.mppre.gob.ve" style="color:#549152;text-decoration:underline;" target="_blank">http://recibos.mppre.gob.ve</a></p>
   		</div>
		   <table id="detallenominas" class='display' cellspacing='0';>
			<thead>
				<tr><th >Ref.</th><th>A&ntilde;o</th><th>Mes</th><th>Semana</th><th>Descripci&oacute;n</th><th>Acciones</th></tr>
			</thead>
			{recibos}
				<tr height='30px'><td>{id_historico_nomina}</td><td>{anio}</td><td>{mes}</td><td>{semana_quincena}</td><td>{descripcion}</td><td>{imprime}</td></tr>
			{/recibos}
			</table>	
		   
      </div>
   </div>

<span style='margin-left:15px;' id='resultado-ajax'></span>

<script>
	$(function(){
		$('#detallenominas').dataTable({"aaSorting": [[ 1, "desc" ],[ 2, "desc" ],[ 3, "desc" ]],
			"bAutoWidth":false,
			"bJQueryUI": true,
			"sPaginationType": "full_numbers",
			"iDisplayLength": 10,
			"aoColumns":[null,null,{"bVisible":false},{"bVisible":false},null,null],
			"oLanguage": {
						"sZeroRecords": "No se encontraron registros!!!"
												
			}
});
	});
</script>