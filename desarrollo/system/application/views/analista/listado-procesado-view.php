<div class="ui-dialog ui-widget ui-widget-content ui-corner-all" style="float:left;position:relative;margin-right:20px;width:890px;">
   <div class="ui-dialog-titlebar ui-widget-header ui-corner-top ui-helper-clearfix">
      <span id="ui-dialog-title-dialog" class="ui-dialog-title">Solicitudes Procesadas</span>
      <a class="ui-dialog-titlebar-close ui-corner-all" href="#"></a>
   </div>
   <div style="height:auto; width: auto;overflow: hidden;" class="ui-dialog-content ui-widget-content" id="dialog">
		<div>
			<form action="" method="post">
			<input type="hidden" name="altfecha1" id="altfecha1"/>
			<input type="hidden" name="altfecha2" id="altfecha2" />
			<table>
				
				<tr><td>Inicio</td><td><input class="fecha" name="fechainicio"  id="fechainicio"></td></tr>
				<tr><td>Fin</td><td><input class="fecha" name="fechafin" id="fechafin"></td></tr>
				<tr><td>&nbsp;</td><td><button>Consultar</button></td></tr>
			</table>
				
				
				
			</form>
		</div>
		
		<h4>{enunciado}</h4>
		<table class="display" cellspacing="0" cellpadding="5">
			<thead>
				<tr><th>#</th><th>Solicitud</th><th>C&eacute;dula</th><th>Correo</th><th>Oficina</th><th>Celular</th></tr>
			</thead>
			<tbody>
			{procesados}
				<tr><td align="right">{id_solicitud}</td><td>{nombre}</td><td align="right">{cedula}</td><td>{usuario}</td><td>{telhab}</td><td>{telcel}</td></tr>
			{/procesados}
			</tbody>
		</table>
   		
   </div>
   
   
</div>

<script>

$(function(){
	
$('.display').dataTable({"aaSorting": [[ 0, "asc" ]],
	"bJQueryUI": true,
	"sPaginationType": "full_numbers",
	"iDisplayLength": 10,
	"oLanguage": {
				"sZeroRecords": "No se encontraron registros!!!"
										
	}


});

$('#fechainicio').datepicker({ altFormat: 'yy-mm-dd',dateFormat:'dd-mm-yy', altField:'#altfecha1' });
$('#fechafin').datepicker({ altFormat: 'yy-mm-dd',dateFormat:'dd-mm-yy', altField:'#altfecha2' });
	
});


</script>