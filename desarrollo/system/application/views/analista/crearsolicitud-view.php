<div class="ui-dialog ui-widget ui-widget-content ui-corner-all" style="float:left;position:relative;margin-right:20px;width:890px;">
   <div class="ui-dialog-titlebar ui-widget-header ui-corner-top ui-helper-clearfix">
      <span id="ui-dialog-title-dialog" class="ui-dialog-title">Crear Solicitud</span>
      <a class="ui-dialog-titlebar-close ui-corner-all" href="#"></a>
   </div>
   <div style="min-height: 109px; width: auto;overflow: hidden;" class="ui-dialog-content ui-widget-content" id="dialog">
   	

      <div class="ui-dialog ui-widget ui-widget-content ui-corner-all" style="float:left;position:relative;margin-right:20px;width:100%">
		   <div style="min-height: 109px; width: auto;" class="ui-dialog-content ui-widget-content" id="dialog">
		   	<div id="divnombre">
		      <p id="formbusqueda">
		      	<form id="formpersona" name="formpersona">
		      		<table>
			      		<tr><td>C&eacute;dula</td><td>{cedula}{btn}</td></tr>
			      		<tr><td>Nombre</td><td><input id="formanombre" name="formanombre" READONLY="READONLY" type="text" size="40" /></td></tr>
		      			<tr><td>Tipo de Personal</td><td><input id="formatipopersonal" READONLY="READONLY" name="formatipopersonal" type="text" size="40" /></td></tr>
		      		</table>
		      	</form>
		      </p>
		      
		      <p id="formadatos">
				
		      </p>
		      
		      </div>
		   </div>
		</div>
      
      
      
      </div>
   </div>
   
   {tramites}


<span style='margin-left:15px;' id='resultado-ajax'></span>

<script>

$('button').button();		
function enviar1 (){
	if($('#formpersona').valid()){
		$.ajax({
			url:'<?=base_url()?>index.php/analista/buscaPersona',
			type:'post',
			data:$('#formpersona').serialize(),
			success:function(data){ $('#resultado-ajax').html(data) ;  }
		});
	}
	
}
function limpiar(){
	$('#cedula').val('');
	$('#formanombre').val('');
	$('#formatipopersonal').val('');
}

$(document).ready(function(){			
	$('#formpersona').validate({
		rules:{
			cedula : { required:true } 
		}}
	);

	
	
	$('.display').dataTable({ "bJQueryUI": true,
		"sPaginationType": "full_numbers",
		"oLanguage": {
					"sZeroRecords": "No se encontraron registros!!!"} });
	$('.dataTables_wrapper').hide();
})

</script>