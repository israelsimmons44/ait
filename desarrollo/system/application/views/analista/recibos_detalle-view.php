{persona}

<table id="recibos-table" width="100%" cellspacing="0">
<thead>
<tr><th>Ref.</th><th>A&ntilde;o</th><th>Mes</th><th>Semana</th><th width="500">Descripci&oacute;n</th><th>Acci&oacute;n</th></tr>
</thead>
<tbody>
{recibos}
<tr><td>{id_historico_nomina}</td><td>{anio}</td><td>{mes}</td><td>{semana_quincena}</td><td>{descripcion}</td><td>{imprime}</td></tr>
{/recibos}
</tbody>

</table>

<script>
	$(function(){
		$('#recibos-table').dataTable({"aaSorting": [[ 1, "desc" ],[ 2, "desc" ],[ 3, "desc" ]],
			"bAutoWidth":false,
			"bJQueryUI": true,
			"sPaginationType": "full_numbers",
			"iDisplayLength": 10,
			"aoColumns":[null,null,{"bVisible":false},{"bVisible":false},null,null],
			"oLanguage": {
						"sZeroRecords": "No se encontraron registros!!!"
												
			}
});
	})
</script>