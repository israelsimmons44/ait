<div class="ui-dialog ui-widget ui-widget-content ui-corner-all" style="float:left;position:relative;margin-right:20px;width:890px;">
   <div class="ui-dialog-titlebar ui-widget-header ui-corner-top ui-helper-clearfix">
      <span id="ui-dialog-title-dialog" class="ui-dialog-title">Reportes</span>
      <a class="ui-dialog-titlebar-close ui-corner-all" href="#"></a>
   </div>
   <div style="height:auto; width: auto;overflow: hidden;" class="ui-dialog-content ui-widget-content" id="dialog">
      
      <div id="aquivaelgrafico" style="width:600px;float:right;"></div>
      <table id="datatable">
			<thead>
				<tr>
					<th></th>
					<th>Jane</th>
					<th>John</th>

				</tr>
			</thead>
			<tbody>
				<tr>
					<th>Apples</th>
					<td>7</td>
					<td>4</td>

				</tr>
				<tr>
					<th>Pears</th>
					<td>2</td>
					<td>0</td>
				</tr>
				<tr>

					<th>Plums</th>
					<td>5</td>
					<td>11</td>
				</tr>
				<tr>
					<th>Bananas</th>
					<td>1</td>

					<td>1</td>
				</tr>
				<tr>
					<th>Oranges</th>
					<td>2</td>
					<td>4</td>
				</tr>

			</tbody>
		</table>
      
      
      
      
      
      
   </div>
   
</div>

<span style='margin-left:15px;' id='resultado-ajax'></span>


<script>
Highcharts.visualize = function(table, options) {
	// the categories
	options.xAxis.categories = [];
	$('tbody th', table).each( function(i) {
		options.xAxis.categories.push(this.innerHTML);
	});
	
	// the data series
	options.series = [];
	$('tr', table).each( function(i) {
		var tr = this;
		$('th, td', tr).each( function(j) {
			if (j > 0) { // skip first column
				if (i == 0) { // get the name and init the series
					options.series[j - 1] = { 
						name: this.innerHTML,
						data: []
					};
				} else { // add values
					options.series[j - 1].data.push(parseFloat(this.innerHTML));
				}
			}
		});
	});
	
	var chart = new Highcharts.Chart(options);
}
	
// On document ready, call visualize on the datatable.
$(document).ready(function() {			
	var table = document.getElementById('datatable'),
	options = {
		   chart: {
		      renderTo: 'aquivaelgrafico',
		      defaultSeriesType: 'column'
		   },
		   title: {
		      text: 'Data extracted from a HTML table in the page'
		   },
		   xAxis: {
		   },
		   yAxis: {
		      title: {
		         text: 'UNIDADES'
		      }
		   },
		   tooltip: {
		      formatter: function() {
		         return '<b>'+ this.series.name +'</b><br/>'+
		            this.y +' '+ this.x.toLowerCase();
		      }
		   }
		};
	
      					
	Highcharts.visualize(table, options);
});


</script>