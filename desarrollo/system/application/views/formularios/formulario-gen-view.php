<div class="ui-dialog ui-widget ui-widget-content ui-corner-all" style="width:565px;">
   <div class="ui-dialog-titlebar ui-widget-header ui-corner-top ui-helper-clearfix">
      <span id="ui-dialog-title-dialog" class="ui-dialog-title">Formulario</span>
      <a class="ui-dialog-titlebar-close ui-corner-all" href="#"></a>
   </div>
   <div style="min-height: 109px; width: auto;" class="ui-dialog-content ui-widget-content" id="dialog">
   	<h3>{titulo}</h3>
   	<h4>{recaudosTitulo}</h4>
   	<p>
   		<ul>
	   		{recaudos}
	   			<li>{descripcion}</li>
	   		{/recaudos}
   		</ul>
   	</p>
      <p>
      	{formulario}
      </p>
   </div>
</div>




<span style='margin-left:15px;' id='resultado-ajax'></span>