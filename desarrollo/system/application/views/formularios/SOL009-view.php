<div class="ui-dialog ui-widget ui-widget-content ui-corner-all" style="width:565px;">
   <div class="ui-dialog-titlebar ui-widget-header ui-corner-top ui-helper-clearfix">
      <span id="ui-dialog-title-dialog" class="ui-dialog-title">Formulario</span>
      <a class="ui-dialog-titlebar-close ui-corner-all" href="#"></a>
   </div>
   <div style="min-height: 109px; width: auto;" class="ui-dialog-content ui-widget-content" id="dialog">
   	<h3>{titulo}</h3>
      
      	<form id="form1" name="form1">
      	{cedula}
      	{codtramite}
      	
      	<table width="100%">
      		<tr><td colspan="2"><strong>DATOS DEL SOLICITANTE:</strong></td></tr>
      		<tr><td width="100">Teléfono ofc.</td><td>{telefono}</td></tr>
      		<tr><td width="100">Extensión</td><td>{extension}</td></tr>
      		<tr><td>Celular</td><td>{telefonocel}</td></tr>
      	</table>
      	
      	<table width="100%">
      		<tr><td colspan="2"><strong>DATOS DEL CÓNYUGE:</strong></td></tr>
      		<tr><td width="100">Cédula</td><td>{cedula_conyuge}</td></tr>
      		<tr><td width="100">Nombres</td><td>{nombres_conyuge}</td></tr>
      		<tr><td width="100">Apellidos</td><td>{apellidos_conyuge}</td></tr>
      		
      		<tr><td>Lugar donde Trabaja</td><td>{lugartrabajo}</td></tr>
      	</table>
      	
      	<table width="100%">
      	
      		<tr><td colspan="2"><strong>DATOS DE HIJOS:</strong></td></tr>
      		<tr><td>Nombres y Apellidos</td><td>Fecha de Nacimiento</td></tr>
      		
      		<tr><td>{nombreh1}</td><td>{fechanach1}</td></tr>
      		<tr><td>{nombreh2}</td><td>{fechanach2}</td></tr>
      	</table>
      	{btn}
      	</form>
      
   </div>
</div>

<span style='margin-left:15px;' id='resultado-ajax'></span>

<script type="text/javascript">
<!--
{javascript}
//-->
</script>