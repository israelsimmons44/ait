<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if (! function_exists('pgDate')){
	 function pgDate($fechapg){
	 	if ($fechapg!=""){
		 	$arrFecha=explode("-",$fechapg);
		 	
		 	$anho=$arrFecha[0];
		 	$mes=$arrFecha[1];
		 	$dia=$arrFecha[2];
		 	$fecha=$dia."-".$mes."-".$anho;
		 	return $fecha;
	 	}else{
	 		return "";
	 	}
	 	
	 }
}

if (! function_exists('isleapyear')){
	function isleapyear($year = '') {
		if (empty($year)) {
			$year = date('Y');
		}
		$year = (int) $year;
		if ($year % 4 == 0) {
			if ($year % 100 == 0) {
				return ($year % 400 == 0);
			} else {
				return true;
			}
		} else {
			return false;
		}
	}	
}


if (! function_exists('quincenas')){
	 function quincenas($anio, $mes, $quincena=1){
	 	$meses=array(1=>"Enero",2=>"Febrero",3=>"Marzo",4=>"Abril",5=>"Mayo",6=>"Junio",7=>"Julio",8=>"Agosto",9=>"Septiembre",10=>"Octubre",11=>"Noviembre",12=>"Diciembre");
	 	$bisiesto=isleapyear($anio);
	 	if ($quincena==1){
	 		$quincena="1ra Quincena de ";
	 	}else{
	 		$quincena="2da Quincena de ";
	 	}
	 	
	 	$quincena=$quincena.$meses[intval($mes)];
	 	if ($bisiesto==TRUE){
	 		$ultimoDia=array(1=>31,2=>29,3=>31,4=>30,5=>31,6=>30,7=>31,8=>31,9=>30,10=>31,11=>30,12=>31);
	 	}else{
	 		$ultimoDia=array(1=>31,2=>28,3=>31,4=>30,5=>31,6=>30,7=>31,8=>31,9=>30,10=>31,11=>30,12=>31);
	 	}
	 	
	 	$primeraQuincena="15/".$mes."/".$anio;
	 	$segundaQuincena=$ultimoDia[intval($mes)]."/".$mes."/".$anio;
	 	return array($primeraQuincena,$segundaQuincena,$quincena);
	 }
}

if (! function_exists('mes')){
	 function mes($mes){
	 	$meses=array(1=>"Enero",2=>"Febrero",3=>"Marzo",4=>"Abril",5=>"Mayo",6=>"Junio",7=>"Julio",8=>"Agosto",9=>"Septiembre",10=>"Octubre",11=>"Noviembre",12=>"Diciembre");
	 	
	 	return $meses[$mes];
	 }
}

if(!function_exists('htmlSelect')){
	function htmlSelect($arr, $nombre){
		$html="<select name='$nombre'>";
		foreach ($arr as $valor){
			$value=$valor['valor'];
			$nombre=utf8_encode($valor['texto']);
			$html.="<option value='$value'>$nombre</option>"	;
		}
		$html.="</select>";
		
		return $html;
	}
}

if(!function_exists('htmlTramites')){
	function htmlTramites($arr){
		$html="<table>";
		$i=0;
		foreach ($arr as $valor){
			$value=$valor['id_tramite'];
			$codigo=$valor['cod_tramite'];
			$nombre=$valor['nombre'];
			$i++;
			
			$html.="<tr><td>$i.-</td><td><a id='$codigo' href='$codigo'>$nombre</a></td></tr>"	;
		}
		$html.="</table>";
		
		return $html;
	}
}

if(!function_exists('htmlUlTramites')){
	function htmlUlTramites($arr){
		$html="<ol id='selectable'>";
		$i=0;
		foreach ($arr as $valor){
			$codigo=$valor['cod_tramite'];
			$nombre=$valor['nombre'];			
			$html.="<li value='$codigo' class=\"ui-widget-content\">$nombre</li>"	;
		}
		$html.="</ol>";
		
		return $html;
	}
}



if(!function_exists('htmlRecaudos')){
	function htmlRecaudos($arr){	
		$html="";	
		if($arr['detalle']!=FALSE){
			
			
			//var_dump($arr['detalle'][0]);
			$html="<table>";
			foreach ($arr['detalle'] as $valor){
				$value=$valor['id_recaudo'];
				$nombre=$valor['nombre_recaudo'];
				$ruta=$valor['ruta_planilla'];
				$html.="<tr><td><a href='$ruta'>$nombre</a></td></tr>"	;
			}
			$html.="</table>";
		}
		
		
		return $html;
	}
}

/**
 * ajaxifica sirve para generar el script que envia los datos de un formulario via ajax
 * 
 * @name ajaxifica
 * @param arr
 */
if(!function_exists('ajaxifica')){
	function ajaxifica($arr, $funcion='enviar'){
		$form=$arr['form'];
		$action=$arr['url'];
		
		$script=<<<EOF
function $funcion(){
	if ($('#$form').valid()){
		$.ajax({
			url:'$action' ,
			data:$('#$form').serialize(),
			type:'POST',
			success:function(data){ $('#resultado-ajax').html(data)} 
		})
	}
	
}
EOF;
		
		return $script;		
	}
}

if(!function_exists('mensajeGrowl')){
	function mensajeGrowl($mensaje, $tipo='ok'){
		$str=<<<EOF
<script>
$.Growl.show({
	'title'  : "Mensaje del Sistema",
	'message': "$mensaje",
	'icon'   : "$tipo",
	'timeout': false
});
</script>
EOF;
		return $str;		
	}
}





