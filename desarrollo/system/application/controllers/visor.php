<?php
class visor extends Controller{
	function __construct(){
		parent::Controller();
		$this->load->model('personal_model','pm');
		$this->load->helper('funciones');
		$this->load->library('parser');
	}
	
	function trabajador(){
		$persona=$this->pm->getPersonal(11738998);
		$personal=$persona['personal'];
		$data['nombre']=$personal->primer_nombre." ".$personal->primer_apellido;
		$this->parser->parse('visor/trabajador-view',$data);
	}
}