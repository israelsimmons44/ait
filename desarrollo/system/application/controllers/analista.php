<?php
class Analista extends Controller{
	function __construct(){
		parent::Controller();
		if (!$this->session->userdata('nombre')){
			redirect(base_url()."index.php/comun/login");
		}
		$this->load->library('parser');
		$this->load->model('solicitud_model',"sm");
		$this->load->helper('funciones');
		$this->load->plugin('to_pdf');		
		$this->output->enable_profiler(FALSE);
	}
	
	function _header(){
		$data['title']="Atenci&oacute;n Integral al Trabajador";
		$this->load->view('main-view',$data);
		$deta['permisos']=$this->session->userdata('permisos');
		$this->load->view('comun/menu2',$deta);		
	}
	
	function index(){
		$this->_header();
		$this->load->view('analista/index-view');
	}
	
	function crearsolicitud(){
		$this->_header();
		$this->load->library('formulario');	
		$this->load->helper('ui');	
		
		$this->load->library('formulario');
		
		$this->formulario->setNombreForm('formpersona');
		$this->formulario->setAction(base_url()."index.php/analista/buscaPersona");
		$data['cedula']=$this->formulario->addInput('frmcedula', 'C&eacute;dula','','required:true');
		$data['btn']=$this->formulario->addButton('btnbuscar','Buscar','enviar1');
		
		$contenedor=$this->formulario->output();		
		$data['javascript']=$contenedor->javascript;
		
		
		$tramites=$this->sm->getTramites();
		$this->load->library('table');
		
		$this->table->set_template(array (
						'table_open' 			=> '<table class="display" border="0" cellpadding="4" cellspacing="0">',

						'heading_row_start' 	=> '<thead><tr>',
						'heading_row_end' 		=> '</tr></thead>',
						'heading_cell_start'	=> '<th>',
						'heading_cell_end'		=> '</th>',

						'row_start' 			=> '<tr>',
						'row_end' 				=> '</tr>',
						'cell_start'			=> '<td>',
						'cell_end'				=> '</td>',

						'row_alt_start' 		=> '<tr>',
						'row_alt_end' 			=> '</tr>',
						'cell_alt_start'		=> '<td>',
						'cell_alt_end'			=> '</td>',

						'table_close' 			=> '</table>'
					));
		
		
		//$this->table->set_heading(array('id','codigo','tramite'));
		//$tabla=$this->table->generate($tramites);
		
		
		//$data['tramites']=$tabla;
		
		$this->load->model('solicitud_model',"sm");
		$dita['lista']=$this->sm->getTramites();
		
		$data['tramites']=$this->parser->parse('analista/solicitud2-view',$dita,TRUE);
					
		$this->parser->parse('analista/crearsolicitud-view', $data);
		
	}
	
	function buscaPersona(){
		$cedula=$this->input->post('frmcedula');
		$this->load->library('persona');
		$persona=$this->persona->getPersona($cedula);
		
		if ($persona){
			$this->session->set_userdata('cedulatramite',$cedula);
			$nombre=utf8_encode($this->persona->getNombre());
			$tipo=utf8_encode($this->persona->getTipoPersonal());
			
			$str=<<<EOF
<script>
$('#formanombre').val('$nombre');
$('#formatipopersonal').val('$tipo');
$('.dataTables_wrapper').show();
</script>
EOF;
		
			echo $str;
		}else{
			echo mensajeGrowl("La persona no existe en la base de datos","error");
			echo "<script>$('#formanombre').val('');$('#formatipopersonal').val('') ;   $('.dataTables_wrapper').hide();</script>";
		}
		
	}
	
	function solicitudespendientes(){
		$this->_header();
			
		$data['titulo']="Relación de solicitudes pendientes";
		$data['solicitudes']=$this->sm->getSolicitudes();;
		
		$this->parser->parse('analista/solicitudes-view',$data);
	}
	
	function detalleSolicitud(){
		$idSolicitud=$this->uri->segment(3);
		$res=$this->sm->getDetalleSolicitud($idSolicitud);
		echo $res;
	}
	
	function detalleSolicitud2(){
		$meses=array('','enero','febrero','marzo','abril','mayo','junio','julio','agosto','septiembre','octubre','noviembre','diciembre');
		$idSolicitud=$this->uri->segment(3);
		$pdf=$this->uri->segment(4);
		$resultado=$this->sm->getParametrosSolicitud($idSolicitud);
		//var_dump($resultado);
		$cedula=$resultado['cedula'];
		
		$this->load->library('CNumeroaLetra');
		$this->load->library('persona');
		
		$numero=new CNumeroaLetra();
		$dialetra=new CNumeroaLetra();
		$dialetra->setNoMoneda(TRUE);
		$dialetra->setNumero(intval(date('d')));
		
		$persona=$this->persona->getPersona($cedula);
		
		$numero->setNumero($this->persona->getSueldo());
		$idTipoPersonal=$this->persona->getIdTipoPersonal();
		$monto=number_format($persona['sueldo'],2,',','.');
		if($this->persona->getSexo()=="M"){
			$genero="el ciudadano";
		}else{
			$genero="la ciudadana";
		}
		
		$cedula=number_format($persona['cedula'],0,',','.');
		$fechaIngreso=pgDate($persona['fechaingreso']);
		
		$this->load->model('admin_model',"am");
		$val=$this->am->getFormatoSolicitud(20,$idTipoPersonal);
		if ($resultado['codTramite']=='SOL008'){
			
			$val['formato']=str_replace("{cedula}",$cedula,$val['formato']);
			$val['formato']=str_replace("{fechaingreso}",$fechaIngreso,$val['formato']);
			$val['formato']=str_replace("{montoletras}",$numero->letra(),$val['formato']);
			$val['formato']=str_replace("{monto}",$monto,$val['formato']);
			$val['formato']=str_replace("{dia}",date('d'),$val['formato']);
			$val['formato']=str_replace("{mes}",$meses[intval(date('m'))],$val['formato']);
			$val['formato']=str_replace("{dialetra}",$dialetra->letra(),$val['formato']);
			
			foreach($persona as $campo=>$valor){
				$val['formato']=str_replace("{".$campo."}",$valor,$val['formato']);
			}
			
			$val['formato']=str_replace("{genero}",$genero,$val['formato']);
			if (!$pdf){
				echo "<p>$idTipoPersonal</p>";
				echo $val['formato'];
			}else{
				
				//$html=$this->_encabezado();
				
				
				$html="<span style='margin:0px 0px 10px 2.5cm;width:620px;text-align:justify;font-family:Arial, Verdana, sans-serif;'>".utf8_decode($val['formato'])."</span>";
				
				
//				$script=$this->_script("Atencion al trabajador");
				
				//$html = str_replace('<body>', '<body>'.$script, $html);
				
				$this->load->library('pdf');

		        
		        // set font
		        
		        
		        // add a page
		        
		        $this->pdf->AddPage();
		        
		        $this->pdf->SetFont('helvetica', 'B', 14);
		        $this->pdf->ln(18);
		        $this->pdf->writeHTML("<span style=\"text-align:center;margin-top:50px;\">CONSTANCIA</span>", true, 0, true, true);
		        $this->pdf->ln();
		        $this->pdf->SetFont('helvetica', '', 12);
		        $html="<span style=\"text-align:justify;line-height:1.5em;\">".$val['formato']."</span>";
				$this->pdf->writeHTML($html, true, 0, true, true);
				
				$this->pdf->ln(20);
				
				$html="<span style=\"text-align:center;\"><h4>Walton Valencia Diaz</h4><br><h5>Director de Administración de Personal (E)<br>Según Resolución DM/ N° 013-A de fecha 21 de enero de 2009<br>publicada en Gaceta Oficial N° 39.106 de fecha 26 de enero de 2009</h5></span>";
				$this->pdf->writeHTML($html, true, 0, true, true);
				
				$this->pdf->Output($persona['cedula'].'_constancia.pdf', 'D');
				
				//pdf_create($html, 'constancia_'.$persona['cedula']);
				
			//	echo $html;
			}

		}
		
	}
	
	function _encabezado($memonro=""){
		$html="<html><head></head><body><div class='pdf' id='circular' style='margin:120px 0px 80px 90px;font-size:1em;>";
		$html.="<div style='width:500px;height:0px;'></div>";
		$html.="<div class='nromemo' style='text-align:center;'>$memonro </div>";
		return $html;
	}
	
	function _script($oficina, $iniciales="", $anexo=""){
		$str='<script type="text/php">
						if ( isset($pdf) ) {
						$font = Font_Metrics::get_font("verdana", "bold");
						$pdf->page_text(520, 760, "{PAGE_NUM}/{PAGE_COUNT}", $font,8, array(0,0,0));
						
						
						 $footer = $pdf->open_object();
	
						  $text_height = 7; 
						  
						  $w = $pdf->get_width();
						  $h = $pdf->get_height();
						
						  // Draw a line along the bottom
						  $y = $h - 2 * $text_height - 24;
						  //$pdf->line(70, $y, $w - 70, $y, array(153,0,0), 2);
						  
						
						  // Add a logo
						  $img_w = 2 * 72; // 2 inches, in points
						  $img_h = 1 * 72; // 1 inch, in points -- change these as required
						  $pdf->image("'.base_url().'images/mppre-96.png","png", 50, 38 , 500, 39);
						  $pdf->page_text(57, 78, "'.utf8_decode($oficina).'", $font,7, array(0,0,0));
						  $pdf->page_text(70, 730, "'.$iniciales.'", $font,8, array(0,0,0));
						  $pdf->page_text(130, 730, "'.utf8_decode("Edificio del Ministerio del Poder Popular para Relaciones Exteriores, piso 10, ala B. Teléfono: 0212-80644.65").'", $font,8, array(0,0,0));
						  $pdf->page_text(270, 740, "'.utf8_decode("R.I.F- G-20000002-3.").'", $font,10, array(0,0,0));';
		
					if ($anexo!=""){
						$str.='$pdf->page_text(70, 730, "Anexo: '.utf8_encode($anexo).'", $font,8, array(0,0,0));';
					}
						  
		$str.='			 // Close the object (stop capture)
						  $pdf->close_object();
						
						  // Add the object to every page. You can
						  // also specify "odd" or "even"
						  $pdf->add_object($footer, "all");
											
						}
					</script>';
		return $str;
	}
	
	function verReclamos(){
		$this->load->model('reclamos_model','rm');
		$data['reclamos']=array();
		$res=$this->rm->getReclamos();
		$this->_header();
		if ($res){
			$data['reclamos']=$res;
		}
		
		$this->benchmark->mark('parseador_start');
		$this->parser->parse('analista/reclamos-view',$data);
		$this->benchmark->mark('parseador_end');
	}
	
	function verificadorRecibo(){
		$this->_header();
		$data=array();
		$this->load->library('formulario');
		
		$this->formulario->setAction(base_url()."index.php/analista/procVerificadorRecibo");
		$this->formulario->addInput('recibo','Nro. Recibo','','required:true');
		$this->formulario->addButton('btnenviar','Consultar','enviar');
		
		$data['formulario']=$this->formulario->outputHTML();
		
		$this->parser->parse('analista/verificadorRecibo-view',$data);
	}
	
	function procVerificadorRecibo(){
		$idHistoricoNomina=$this->input->post('recibo');
		$this->load->model('recibos_model','rm');
		$personaRecibo=$this->rm->getPersonaRecibo($idHistoricoNomina);
		$recibo=$this->rm->getRecibo($idHistoricoNomina);
		
		$totalAsigna=0;
		$totalDeduce=0;
		$total=0;
		
		if ($recibo){
			foreach ($recibo as $value) {
				$totalAsigna+=$value['monto_asigna'];
				$totalDeduce+=$value['monto_deduce'];
			}
			$total=$totalAsigna-$totalDeduce;
			
			$sha1=strtoupper(sha1('recibo '.$idHistoricoNomina." ".$personaRecibo->cedula.' '.$totalAsigna.' '.$totalDeduce));
			
			echo "<span style='font-weight:bold;'>$personaRecibo->cedula <br/> $personaRecibo->nombre1 $personaRecibo->nombre2 $personaRecibo->apellido1 $personaRecibo->apellido2";
			echo "<table width='50%'>";
			echo "<tr><td>Asignaciones</td><td align='right'>".number_format($totalAsigna,2,",",".")."</td></tr>";
			echo "<tr><td>Deducciones</td><td align='right'>".number_format($totalDeduce,2,",",".")."</td></tr>";
			echo "<tr><td>Total</td><td align='right'>".number_format($total,2,",",".")."</td></tr>";
			
			echo "</table>$sha1</span>";
			echo "<br/><a href='".base_url()."index.php/recibos/imprime/$idHistoricoNomina'>Imprimir Recibo: <img src='".base_url()."images/printer_on.png'/></a>";
		}else{
			echo "El n&uacute;mero de recibo que ingres&oacute; no existe";
		}
		
		
	}
	
	function verificadorConstancia(){
		$this->_header();
		$data=array();
		$this->parser->parse('analista/verificadorConstancia-view',$data);
	}
	
	function recibos(){
		$this->_header();
		$this->load->library('formulario');
		$this->formulario->setAction(base_url()."index.php/analista/procRecibos");
		$this->formulario->addInput('cedula','C&eacute;dula','','required:true');
		$this->formulario->addButton('btnEnviar','Enviar','enviar');

		$data['formulario']=$this->formulario->outputHTML();
		$this->parser->parse('analista/recibos_index-view',$data);
		
		
	}
	
	function procRecibos(){
		$cedula=$this->input->post('cedula');
		$this->load->library('persona');
		$this->persona->getPersona($cedula);
		$this->load->model('recibos_model','rm');
		$recibos=$this->rm->getRecibos($cedula);
		
		$data['persona']=utf8_encode($this->persona->getNombre());
		
		if ($recibos){
			foreach ($recibos as $key=>$value) {
				$recibos[$key]['fecha']=pgDate($value['fecha']);
				$recibos[$key]['imprime']="<a href='".base_url()."index.php/recibos/imprime/".$value['id_historico_nomina']."'><img src='".base_url()."images/printer_on.png' /></a>";
	
				//verifica si en descripcion esta vacio, si no es asi significa que es una nomina especial
				if ($recibos[$key]['descripcion']==""){
					$recibos[$key]['descripcion']=$recibos[$key]['quincenatexto'];
				}else{
					$recibos[$key]['descripcion']=utf8_encode($recibos[$key]['descripcion']);
				}
				
			}
			
			$data['recibos']=$recibos;
			$this->parser->parse('analista/recibos_detalle-view',$data);
		}else{
			echo "<h2>La c&eacute;dula que indic&oacute; no existe en la base de datos</h2>";
		}
	}
	
	function generaCaso(){
		$id_reclamo=$this->uri->segment(3);
		$this->_header();
		$this->load->library('formulario');
		$this->load->model('reclamos_model','rm');
		$data['reclamo']=$this->rm->getReclamo($id_reclamo);
		$this->formulario->setAction(base_url()."index.php/analista/procGeneraCaso");
		$this->formulario->addTextArea('observaciones','Observaciones','','required:true',array('cols'=>30,'rows'=>7));
		$this->formulario->addHidden('id_reclamo',$id_reclamo);
		$this->formulario->addButton('btnEnviar','Generar Caso','enviar');
		$data['formulario']=$this->formulario->outputHTML();
		
		$this->parser->parse('analista/generaCaso-view',$data);
	}
	
	function procGeneraCaso(){
		$this->load->model('caso_model','cm');
		
		$res=$this->cm->addCaso($_POST);
		if ($res){
			echo mensajeGrowl("El caso se generó exitosamente y fue registrado con el n&uacute;mero: ".$res);
		}else{
			echo mensajeGrowl("El caso no pudo ser registrado",'error');
		}
	}
	
	function consultaCaso(){
		$data=array();
		$idCaso=$this->uri->segment(3);
		$this->load->model('caso_model','cm');
		$resCaso=$this->cm->getCaso($idCaso);
		$data['detallecaso']=$resCaso;
		
		$this->load->library('formulario');
		$this->formulario->setAction(base_url()."index.php/analista/procAddDetalleReclamo/".$idCaso);
		$this->formulario->addTextArea('observaciones','Observaciones','','required:true',array('cols'=>30,'rows'=>7));
		$sql="select id_estatus_caso as valor, descripcion from estatuscaso";
		$this->formulario->addSelect('id_estatus_caso','Estatus',$sql,'','min:1');
		$this->formulario->addHidden('id_caso',$idCaso);
		$this->formulario->addButton('btnEnviar','Generar Caso','enviar');
		
		$data['formulario']=$this->formulario->outputHTML();
		
		$this->_header();
		$this->parser->parse('analista/consultaCaso-view',$data);
	}
	
	function procAddDetalleReclamo(){
		$idCaso=$this->uri->segment(3);
		$this->load->model('caso_model','cm');
		$res=$this->cm->addDetalleCaso($_POST);
		if ($res){
			echo mensajeGrowl("El detalle se ha agregado exitosamente");
		}else{
			echo mensajeGrowl("hubo un error guardando la actualizaci&oacute;n");
		}
		
		
	}
	
	function verCasos(){
		$data['casos']=array();
		$this->load->model('caso_model','cm');
		$this->_header();
		$resCasos=$this->cm->getCasos(1);
		if ($resCasos){
			$data['casos']=$resCasos;
		}
		
		$this->parser->parse('analista/casos-view',$data);
	}
	
	function personal(){
		$data=array();
		$this->_header();
		
		$this->load->library('formulario');
		$this->formulario->setAction(base_url()."index.php/analista/consultaPersonal");
		$this->formulario->addInput('cedula','C&eacute;dula','','required:true');
		$this->formulario->addButton('btnEnviar','Consultar','enviar');
		
		$data['formulario']=$this->formulario->outputHTML();
		
		$this->parser->parse('analista/consultaPersonal-view',$data);
	}
	
	function consultaPersonal(){
		$data['solicitudes']=array();
		$data['conceptosfijos']=array();
		$data['nombres']="";
		$data['apellidos']="";
		$data['sexo']="";
		$data['cedula']="";
		$data['fecha_nacimiento']="";
		$data['trabajador']="";
		
		$cedula=$this->input->post('cedula');
		$this->load->model('personal_model','pm');
		$this->load->model('solicitud_model','sm');
		$idTrabajador=$this->pm->getPersonalActivo($cedula);
		
		
		
		$resSolicitudes=$this->sm->getSolicitudesPersona($cedula,TRUE);
		if  ($resSolicitudes!=""){
			$data['solicitudes']=$resSolicitudes;
		}
		
		$resPersonal=$this->pm->getPersonal($cedula);
		$arrPersonal=$resPersonal['personal'];
		if ($arrPersonal){
			$data['nombres']=utf8_encode($arrPersonal->primer_nombre." ".$arrPersonal->segundo_nombre);
			$data['apellidos']=utf8_encode($arrPersonal->primer_apellido." ".$arrPersonal->segundo_apellido);
			$data['sexo']=$arrPersonal->sexo;
			$data['cedula']=$arrPersonal->cedula;
			$data['fecha_nacimiento']=pgDate($arrPersonal->fecha_nacimiento);
			$data['trabajador']=$resPersonal['trabajador'];
			if ($idTrabajador){
				$arrConceptos=$this->pm->getConceptosFijos($idTrabajador);
				if ($arrConceptos){
					foreach ($arrConceptos as $key=>$value) {
						$arrConceptos[$key]['monto']=number_format($value['monto'],2,",",".");
					}
					$data['conceptosfijos']=$arrConceptos;
				}else{
					$data['conceptosfijos']=array();
				}
				
			}else{
				$data['conceptosfijos']=array();
			}
		}else{
			$data['trabajador']=array();
			$data['nombres']="LA C&Eacute;DULA QUE INGRES&Oacute; NO ESTA REGISTRADA";
		}		
		
		$this->parser->parse('analista/detalleConsultaPersona-view',$data);
		
	}
	
	function verDatosProcesados(){
		$this->_header();
		$fecha1=$this->input->post('altfecha1');
		$fecha2=$this->input->post('altfecha2');
		
		if ($fecha1==""){
			$fecha1=date('Y-m-d');
		}
		
		if ($fecha2==""){
			$fecha2=date('Y-m-d');
		}
		echo $fecha1;
		$fecha1str=pgDate($fecha1);
		$fecha2str=pgDate($fecha2);
		
		$data['procesados']=$this->sm->getDatosSolicitud($fecha1,$fecha2);
		$data['enunciado']="Mostrando solicitudes del $fecha1str al $fecha2str";
		$this->parser->parse('analista/listado-procesado-view',$data);
	}
	
	function alarmas(){
		
	}
	
	
}