<?php
class gestion extends Controller{
	function __construct(){
		parent::Controller();
		if (!$this->session->userdata('nombre')){
			redirect(base_url()."index.php/comun/login");
		}
		$this->load->library('parser');
		$this->load->model('gestion_model','gm');
		$this->output->enable_profiler(FALSE);
		$this->load->helper('date');
	}
	
	function _header(){
		$data['title']="Atenci&oacute;n Integral al Trabajador";
		$this->load->view('main-view',$data);
		$deta['permisos']=$this->session->userdata('permisos');
		$this->load->view('comun/menu2',$deta);		
	}
	
	function index(){
		$this->load->library('formulario');
		$this->formulario->setNombreForm('formpersona');
		$this->formulario->setAction(base_url()."index.php/analista/buscaPersona");
		$data['cedula']=$this->formulario->addInput('frmcedula', 'C&eacute;dula','','required:true');
		$data['btn']=$this->formulario->addButton('btnbuscar','Buscar','enviar1');
		$data['seltramites']=$this->formulario->addSelect('seltramite','Tramites',$this->gm->getTramites());
		$data['selanalista']=$this->formulario->addSelect('selanalista','Analista',$this->gm->getAnalistas());
		
		$data['fechatramite']=$this->formulario->addDatePicker('fechatramite','Fecha');
		$data['fechaenviado']=$this->formulario->addDatePicker('fechaenviado','Fecha');
		$data['fechafirmado']=$this->formulario->addDatePicker('fechafirmado','Fecha');
		
		$contenedor=$this->formulario->output();		
		$data['javascript']=$contenedor->javascript;
		
		$this->_header();
		
		$data1['resultado']=$this->gm->vistaSeguimiento();
		$data['resultado']=$this->parser->parse('gestion/vista-gestion-view',$data1,TRUE);
		//$this->load->view('gestion/index-view');
		$this->parser->parse('gestion/index-view',$data);
	}
	
	function registra(){
		$data['cedula']=$this->input->post('frmcedula');
		$data['id_tramite']=$this->input->post('seltramite');
		$data['id_usuario']=$this->input->post('selanalista');
		$data['fecha_solicitud']=$this->input->post('fechatramite');
		$this->gm->insert($data);
	}
	
	
	
}