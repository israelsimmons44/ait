<?php
class Admin extends Controller{
	function __construct(){
		parent::Controller();
		if (!$this->session->userdata('nombre')){
			redirect(base_url()."index.php/comun/login");
		}		
		$this->load->model('admin_model','am');
		$this->load->library('parser');
		$this->load->helper('funciones');
	}
	
	function _header(){
		$data['title']="Administraci&oacute;n - Atenci&oacute;n Integral al Trabajador";
		$this->load->view('main-view',$data);
		$deta['permisos']=$this->session->userdata('permisos');
		$this->load->view('comun/menu2',$deta);
	}
	
	function index(){
		
	}
	
	function formatoConstancia(){		
		$this->_header();
		$nroFormato=$this->uri->segment(3);
		
		//$valorEditor=$this->am->getFormatoSolicitud('SOL008');
		switch ($nroFormato) {
			case 1:
				$tipoConstancia="MENSUAL";
			break;
			
			case 2:
				$tipoConstancia="ANUAL";
			break;
			
		}
		$data['tipoconstancia']=$tipoConstancia;
		$data['valor']="";
		$data['firma']="";
		$data['nroFormato']=$nroFormato;
		
		$this->parser->parse('admin/formatos/formato-constancia-view',$data);
	}
	
	function grabaFormatoConstancia(){
		$valor=$this->input->post('valor');
		$idTramite=$this->input->post('idtramite');
		$idTipoPersonal=$this->input->post('idtipopersonal');
		$nroFormato=$this->input->post('nro_formato');
		
		if ($this->am->setFormatoSolicitud($idTramite,$idTipoPersonal,$valor,'',$nroFormato)){
			echo mensajeGrowl("Los cambios en el formato han sido guardados");
		}else{
			echo mensajeGrowl("Ocurrió un error mientras se intentaban guardar los cambios");
		}
		
	}
	
	function obtenerFormato(){
		$idTramite=$this->input->post('idtramite');
		$idTipoPersonal=$this->input->post('idtipopersonal');
		$nroFormato=$this->input->post('nro_formato');
		
		$formato=$this->am->getFormatoSolicitud($idTramite, $idTipoPersonal,$nroFormato);
		$arr['formato']=$formato;
		$arr['firma']="firma";
		
		echo $formato['formato'];
		
	}
	
	function recaudos(){
		$this->_header();
		$this->load->view('admin/recaudos');
	}
	
	function solicitudes(){
		$this->_header();
	}
	
	function usuarios(){
		$this->_header();
		$this->load->view('admin/usuarios');
	}
	
	function verSugerencias(){
		$this->_header();
		$data['sugerencias']=$this->am->getSugerencias();
		$this->parser->parse('admin/sugerencias-view',$data);
	}
	
	function tipoReclamo(){
		$this->_header();
		$data['tiporeclamos']=$this->am->getTipoReclamos();
		$this->load->library('formulario');
		$this->formulario->setAction(base_url()."index.php/admin/procTipoReclamo");
		$this->formulario->addInput('descripcion','Descripci&oacute;n','','required:true');
		$this->formulario->addSelect('estatus','Estatus',array(array('valor'=>'A','descripcion'=>'ACTIVO'),array('valor'=>'S','descripcion'=>'SUSPENDIDO')));
		$this->formulario->addButton('guardar','Guardar','enviar');
		$data['formulario']=$this->formulario->outputHTML();
		$this->parser->parse('admin/tiporeclamo-view',$data);
	}
	
	function procTipoReclamo(){
		if ($this->am->addTipoReclamo($_POST)){
			$descripcion=$_POST['descripcion'];
			echo mensajeGrowl("El tipo de reclamo ha sido guardado");
			echo '<script>$(function(){$("#tiporeclamo tr:last").after("<tr><td>nuevo</td><td>'.$descripcion.'</td></tr>")})</script>';
		}
	}
	
	function procesa(){
		
		$accion=$this->uri->segment(3);
		
		switch ($accion){
			case "ingresarecaudo":
				echo "Recaudo guardado";
				break;
			case "usuarionuevo":
				echo "Usuario guardado";
				break;	
			case "ingresacaso":
				//ESTE CASO ES NADA MAS DE PRUEBA
				$this->load->model('solicitud_model','sm');
				
				$datos['nombre'] = $this->input->post('nombre');
				
				$this->sm->insertCaso($datos);
				
				
				//echo "Caso guardado";
				
				$str=<<<EOF
<script>
	 $.Growl.show({
          'title'  : "Mensaje del Sistema",
          'message': "La solicitud se ha efectuado correctamente",
          'icon'   : "ok",
          'timeout': false
        });
</script>
EOF;
echo $str;
				break;
		}		
		
	}
	
	function actualizaProcesado(){
		$idSolicitud=$this->uri->segment(3);
		$this->am->setSolicitudEstatus($idSolicitud);
		redirect(base_url()."index.php/analista/solicitudespendientes");
	}
	
}