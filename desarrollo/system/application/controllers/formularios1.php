<?php
class Formularios extends Controller{
	function __construct(){
		parent::Controller();
		$this->load->library('formulario');
		$this->load->library('parser');
		$this->load->model('solicitud_model',"sm");
		$this->load->helper('funciones');
		
	}
	
	function index(){
		$this->load->view('formularios/index-view');
		
	}
	
	function _header(){
		$data['title']="Atenci&oacute;n Integral al Trabajador";
		$this->load->view('main-view',$data);
		$deta['permisos']=$this->session->userdata('permisos');
		$this->load->view('comun/menu',$deta);		
	}
	
	/*
	 * Formulario de Adelanto de prestaciones
	 */
	function SOL001(){
		//$this->_header();
		$data['titulo']="Computo Vacacional";
		$data['formulario']="";
		
		$data['recaudosTitulo']="";
		$data['recaudos']=array();
		
		$this->formulario->setNombreForm('form1');
		$this->formulario->setAction(base_url()."index.php/formularios/procesaSOL001");
		$this->formulario->addHidden('cedula',$this->session->userdata('cedula'));
		$this->formulario->addHidden('codtramite','SOL001');
		$this->formulario->addButton('btn','Grabar Datos','enviar');
		$data['formulario']=$this->formulario->outputHTML();
		
		
		$this->parser->parse('formularios/formulario-gen-view',$data);
		
	}
	
	function procesaSOL001(){
		$porAnalista=FALSE;
		$this->load->helper('funciones');
		if ($this->session->userdata('cedulatramite')!=""){
			$_POST['cedula']=$this->session->userdata('cedulatramite');
			$porAnalista=TRUE;
		}
		$sol=$this->sm->setSolicitud($_POST);

		$this->load->library('smtpmail');
		
		$mail=new SMTPMAIL;
		$mail->set_host('zmail.mppre.gob.ve');
		$correo=$this->session->userdata('usuario')."@mppre.gob.ve";
		
		if ($porAnalista==FALSE){
			$msg="Hemos recibido su solicitud de Cómputo Vacacional, la misma está registrada bajo el número: ".$sol['id_solicitud']."<br> ".$this->session->userdata('usuario')." Pronto nos comunicaremos con usted por esta misma via para avisarle acerca de los avances.";
			$res=$mail->sendmail($correo, "Sistema de Solicitudes AIT",utf8_decode($msg),"atencion.altrabajador@mppre.gob.ve");
		}
	}
	
	/*
	 * Formulario de Adelanto de prestaciones
	 */
	function SOL002(){
		//$this->_header();
		$data['recaudosTitulo']="";
		$data['recaudos']=array();
		
		$data['titulo']="Adelanto de Prestaciones";
		$this->formulario->setNombreForm('form1');
		$this->formulario->setAction(base_url()."index.php/formularios/procesaSOL002");
		$this->formulario->addHidden('cedula',$this->session->userdata('cedula'));
		$this->formulario->addHidden('codtramite','SOL002');
		$this->formulario->addCheckBox('casado','Casado', TRUE);
		$reglaDependencia='required:{depends:function(element){return ($(\'#casado:checked\').val()==\'casado\')}}';
		$this->formulario->addInput('nombres_conyuge','Nombre del Conyuge','',$reglaDependencia, array('size'=>20));
		$this->formulario->addInput('apellidos_conyuge','Apellido del Conyuge','',$reglaDependencia, array('size'=>20));
		$this->formulario->addInput('cedula_conyuge','Cédula del Conyuge','',$reglaDependencia);
		$this->formulario->addInput('telefono_conyuge','Teléfono del Conyuge','',$reglaDependencia);
		$this->formulario->addInput('montoprestaciones','Monto a Solicitar','','required:true');
		$this->formulario->addCheckBox('construccionvivienda','Construcción de vivienda');
		$this->formulario->addCheckBox('adquisicionvivienda','Adquisición de vivienda');
		$this->formulario->addCheckBox('presupuesto','Presupuesto');
		$this->formulario->addCheckBox('planillainscripcion','Planilla de inscripción');
		$this->formulario->addCheckBox('liberacionhipoteca','Liberación de hipoteca');
		$this->formulario->addCheckBox('informemedico','Informe Médico');
		$this->formulario->addCheckBox('permisoconstruccion','Permiso de Construcción');
		$this->formulario->addCheckBox('otrosdocumentos','Otros');
		$this->formulario->addButton('btn','Grabar Datos','enviar');
		$data['formulario']=$this->formulario->outputHTML();
		
		$this->parser->parse('formularios/formulario-gen-view',$data);
		
	}
	
	function procesaSOL002(){
		$porAnalista=FALSE;
		$this->load->helper('funciones');
		if ($this->session->userdata('cedulatramite')!=""){
			$_POST['cedula']=$this->session->userdata('cedulatramite');
			$porAnalista=TRUE;
		}
		$sol=$this->sm->setSolicitud($_POST);

		$this->load->library('smtpmail');
		
		$mail=new SMTPMAIL;
		$mail->set_host('zmail.mppre.gob.ve');
		$correo=$this->session->userdata('usuario')."@mppre.gob.ve";
		
		if ($porAnalista==FALSE){
			$msg="Hemos recibido su solicitud de Adelanto de Prestaciones, la misma está registrada bajo el número: ".$sol['id_solicitud']."<br> ".$this->session->userdata('usuario')." Pronto nos comunicaremos con usted por esta misma via para avisarle acerca de los avances.";
			$res=$mail->sendmail($correo, "Sistema de Solicitudes AIT",utf8_decode($msg),"atencion.altrabajador@mppre.gob.ve");
		}
		
	}
	
	/*
	 * Formulario de Prima por profesionalizacion
	 */
	function PRI002(){
		//$this->_header();
		$data['titulo']="Prima por Profesionalización";
		
		$this->formulario->setNombreForm('form1');
		$this->formulario->setAction(base_url()."index.php/formularios/procesaPRI002");
		$this->formulario->addInput('telefono','Teléfono Oficina','','', array('size'=>10));
		$this->formulario->addInput('extension','Extensión','','', array('size'=>5));
		$this->formulario->addInput('telefonohab','Teléfono Habitación','','', array('size'=>10));
		$this->formulario->addInput('telefonocel','Teléfono Celular','','', array('size'=>10));
		$arr="select id_nivel_academico as valor, descripcion from nivelacademico";		
		$this->formulario->addSelect('nivelacademico','Nivel Académico',$arr,0,'required:true');
		$this->formulario->addHidden('cedula',$this->session->userdata('cedula'));
		$this->formulario->addHidden('codtramite','PRI002');
		$this->formulario->addButton('btn','Grabar Datos','enviar');
		$data['formulario']=$this->formulario->outputHTML();
		
		$data['recaudosTitulo']="Recaudos a Consignar";
		$data['recaudos']=array(
			array('descripcion'=>'Fotocopia de la C&eacute;dula de Identidad'),
			array('descripcion'=>'Fotocopia del t&iacute;tulo con vista del original')
		);
		
		$this->parser->parse('formularios/formulario-gen-view',$data);
		
	}	
	
	function procesaPRI002(){
		$porAnalista=FALSE;
		$this->load->helper('funciones');
		if ($this->session->userdata('cedulatramite')!=""){
			$_POST['cedula']=$this->session->userdata('cedulatramite');
			$porAnalista=TRUE;
		}
		$sol=$this->sm->setSolicitud($_POST);

		$this->load->library('smtpmail');
		
		$mail=new SMTPMAIL;
		$mail->set_host('zmail.mppre.gob.ve');
		$correo=$this->session->userdata('usuario')."@mppre.gob.ve";
		
		if ($porAnalista==FALSE){
			$msg="Hemos recibido su solicitud de Prima por Profesionalización, la misma está registrada bajo el número: ".$sol['id_solicitud']."<br> ".$this->session->userdata('usuario')." Pronto nos comunicaremos con usted por esta misma via para avisarle acerca de los avances.";
			$res=$mail->sendmail($correo, "Sistema de Solicitudes AIT",utf8_decode($msg),"atencion.altrabajador@mppre.gob.ve");
		}
	}
	
	/*
	 * Formulario de Ayuda por Matrimonio
	 */
	function SOL003(){
		//$this->_header();
		$data['titulo']="Ayuda por Matrimonio";
		$data['recaudosTitulo']="";
		$data['recaudos']=array();
		
		$this->formulario->setNombreForm('form1');
		$this->formulario->setAction(base_url()."index.php/formularios/procesaSOL003");
		$this->formulario->addHidden('cedula',$this->session->userdata('cedula'));
		$this->formulario->addHidden('codtramite','SOL003');
		$this->formulario->addInput('telefono','Teléfono Oficina','','', array('size'=>10));
		$this->formulario->addInput('extension','Extensión','','', array('size'=>5));
		$this->formulario->addInput('telefonohab','Teléfono Habitación','','', array('size'=>10));
		$this->formulario->addInput('telefonocel','Teléfono Celular','','', array('size'=>10));
		$this->formulario->addDatepicker('fechamatrimonio','Fecha de Matrimonio','required:true');
		$this->formulario->addInput('nombres_conyuge','Nombres del Conyuge','','required:true');
		$this->formulario->addInput('apellidos_conyuge','Apellidos del Conyuge','','required:true');
		$this->formulario->addInput('destinoruta','Destino (Ruta)');
		$this->formulario->addDatepicker('fechasalida','Fecha de Salida');
		$this->formulario->addDatepicker('fecharetorno','Fecha de Retorno');
		
		$data['btn']=$this->formulario->addButton('btn','Grabar Datos','enviar');		
		
		$data['formulario']=$this->formulario->outputHTML();
		
		$this->parser->parse('formularios/formulario-gen-view',$data);
		
	}
	
	function procesaSOL003(){
		$porAnalista=FALSE;
		$this->load->helper('funciones');
		if ($this->session->userdata('cedulatramite')!=""){
			$_POST['cedula']=$this->session->userdata('cedulatramite');
			$porAnalista=TRUE;
		}
		$sol=$this->sm->setSolicitud($_POST);

		$this->load->library('smtpmail');
		
		$mail=new SMTPMAIL;
		$mail->set_host('zmail.mppre.gob.ve');
		$correo=$this->session->userdata('usuario')."@mppre.gob.ve";
		
		if ($porAnalista==FALSE){
			$msg="Hemos recibido su solicitud de Ayuda por Matrimonio, la misma está registrada bajo el número: ".$sol['id_solicitud']."<br> ".$this->session->userdata('usuario')." Pronto nos comunicaremos con usted por esta misma via para avisarle acerca de los avances.";
			$res=$mail->sendmail($correo, "Sistema de Solicitudes AIT",utf8_decode($msg),"atencion.altrabajador@mppre.gob.ve");
		}
	}
	
	/*
	 * Formulario de Prima por Hijos
	 */
	function SOL004(){
		//$this->_header();
		$data['titulo']="Prima por Hijos";
		$data['recaudosTitulo']="Recaudos a Consignar";
		$data['recaudos']=array(
			array('descripcion'=>'Fotocopia de la C&eacute;dula de Identidad'),
			array('descripcion'=>'Partida de Nacimiento de el(los) niño(s)')
		);
		
		$this->formulario->setNombreForm('form1');
		$this->formulario->setAction(base_url()."index.php/formularios/procesaSOL004");
		$data['cedula']=$this->formulario->addHidden('cedula',$this->session->userdata('cedula'));
		$data['codtramite']=$this->formulario->addHidden('codtramite','SOL004');
		$data['telefono']=$this->formulario->addInput('telefono','Teléfono','','required:true', array('size'=>10));
		$data['extension']=$this->formulario->addInput('extension','Extension','','', array('size'=>5));
		$data['telefonohab']=$this->formulario->addInput('telefonohab','Telefono','','', array('size'=>10));
		$data['telefonocel']=$this->formulario->addInput('telefonocel','Telefono','','required:true', array('size'=>10));
		
		$data['nombreh1']=$this->formulario->addInput('nombreh1','Nombre','','required:true', array('size'=>30));
		$data['fechah1']=$this->formulario->addDatePicker('fechanach1','Fecha de Nac.');
		
		$data['nombreh2']=$this->formulario->addInput('nombreh2','Nombre','','', array('size'=>30));
		$data['fechah2']=$this->formulario->addDatePicker('fechanach2','Fecha de Nac.');
		
		$data['nombreh3']=$this->formulario->addInput('nombreh3','Nombre','','', array('size'=>30));
		$data['fechah3']=$this->formulario->addDatePicker('fechanach3','Fecha de Nac.');
		
		$data['nombreh4']=$this->formulario->addInput('nombreh4','Nombre','','', array('size'=>30));
		$data['fechah4']=$this->formulario->addDatePicker('fechanach4','Fecha de Nac.');
		
		$data['nombreh5']=$this->formulario->addInput('nombreh5','Nombre','','', array('size'=>30));
		$data['fechah5']=$this->formulario->addDatePicker('fechanach5','Fecha de Nac.');

		$data['btn']=$this->formulario->addButton('btn','Grabar Datos','enviar');
		
		$this->formulario->output();
		$data['javascript']=$this->formulario->getJavascript();
		
		$this->parser->parse('formularios/SOL004-view',$data);
	}	
	
	function procesaSOL004(){
		$porAnalista=FALSE;
		$this->load->helper('funciones');
		if ($this->session->userdata('cedulatramite')!=""){
			$_POST['cedula']=$this->session->userdata('cedulatramite');
			$porAnalista=TRUE;
		}
		$sol=$this->sm->setSolicitud($_POST);

		$this->load->library('smtpmail');
		
		$mail=new SMTPMAIL;
		$mail->set_host('zmail.mppre.gob.ve');
		$correo=$this->session->userdata('usuario')."@mppre.gob.ve";
		
		if ($porAnalista==FALSE){
			$msg="Hemos recibido su solicitud de Prima por Hijos, la misma está registrada bajo el número: ".$sol['id_solicitud']."<br> ".$this->session->userdata('usuario')." Pronto nos comunicaremos con usted por esta misma via para avisarle acerca de los avances.";
			$res=$mail->sendmail($correo, "Sistema de Solicitudes AIT",utf8_decode($msg),"atencion.altrabajador@mppre.gob.ve");
		}
	}
	
	
	/*
	 * Formulario de Antecedentes de Servicio
	 */
	function SOL005(){
		//$this->_header();
		$data['titulo']="Antecedentes de Servicio";
		$this->formulario->setNombreForm('form1');
		$this->formulario->setAction(base_url()."index.php/formularios/procesaSOL005");
		$this->formulario->addHidden('cedula',$this->session->userdata('cedula'));
		$this->formulario->addHidden('codtramite','SOL005');
		$this->formulario->addDatePicker('fechaingreso','Fecha de Ing.','required:true');
		$this->formulario->addDatePicker('fechaegreso','Fecha de Egr.');
		$this->formulario->addInput('adscripcion','Última Dirección de Adscripción','','required:true', array('size'=>30));
		$this->formulario->addInput('ultimosueldo','Último Sueldo Básico','','', array('size'=>10));
		$this->formulario->addInput('otrasasignaciones','Otra Asignaciones','','', array('size'=>10));

		$data['btn']=$this->formulario->addButton('btn','Grabar Datos','enviar');
		
		$data['formulario']=$this->formulario->outputHTML();
		
		$this->parser->parse('formularios/formulario-gen-view',$data);
		
	}
	
	function procesaSOL005(){
		$porAnalista=FALSE;
		$this->load->helper('funciones');
		if ($this->session->userdata('cedulatramite')!=""){
			$_POST['cedula']=$this->session->userdata('cedulatramite');
			$porAnalista=TRUE;
		}
		$sol=$this->sm->setSolicitud($_POST);

		$this->load->library('smtpmail');
		
		$mail=new SMTPMAIL;
		$mail->set_host('zmail.mppre.gob.ve');
		$correo=$this->session->userdata('usuario')."@mppre.gob.ve";
		
		if ($porAnalista==FALSE){
			$msg="Hemos recibido su solicitud de Antecedentes de Servicio, la misma está registrada bajo el número: ".$sol['id_solicitud']."<br> ".$this->session->userdata('usuario')." Pronto nos comunicaremos con usted por esta misma via para avisarle acerca de los avances.";
			$res=$mail->sendmail($correo, "Sistema de Solicitudes AIT",utf8_decode($msg),"atencion.altrabajador@mppre.gob.ve");
		}
	}
	
	/*
	 * Formulario de Vacaciones
	 */
	function SOL006(){
		//$this->_header();
		$data['titulo']="Vacaciones";
		$this->formulario->setNombreForm('form1');
		$this->formulario->setAction(base_url()."index.php/prueba/procesa");
		$this->formulario->addHidden('cedula',$this->session->userdata('cedula'));
		$this->formulario->addHidden('codtramite','SOL006');
		$data['jefeinmediato']=$this->formulario->addInput('jefeinmediato','Jefe Inmediato','','required:true, minlength:5', array('size'=>30));
		$data['director']=$this->formulario->addInput('director','Director','','required:true', array('size'=>30));

		$data['btn']=$this->formulario->addButton('btn','Grabar Datos','enviar');
		
		$data['formulario']=$this->formulario->outputHTML();
		
		$this->parser->parse('formularios/formulario-gen-view',$data);
		
	}
	
	/*
	 * Formulario de Bono hijos Excepcionales
	 */
	function SOL007(){
		//$this->_header();
		$data['titulo']="Bono Hijos Excepcionales";
		$this->formulario->setNombreForm('form1');
		$this->formulario->setAction(base_url()."index.php/prueba/procesa");
		$data['cedula']=$this->formulario->addHidden('cedula',$this->session->userdata('cedula'));
		$data['codtramite']=$this->formulario->addHidden('codtramite','SOL007');
		
		$data['nombreh1']=$this->formulario->addInput('nombreh1','Nombre','','required:true', array('size'=>30));
		$data['fechah1']=$this->formulario->addDatePicker('fechanach1','Fecha de Nac.');
		
		$data['nombreh2']=$this->formulario->addInput('nombreh2','Nombre','','', array('size'=>30));
		$data['fechah2']=$this->formulario->addDatePicker('fechanach2','Fecha de Nac.');
		

		$data['btn']=$this->formulario->addButton('btn','Grabar Datos','enviar');
		
		$this->formulario->output();
		$data['javascript']=$this->formulario->getJavascript();;
		
		$this->parser->parse('formularios/SOL007-view',$data);
		
	}
	
	/*
	 * Formulario de Constancia de Trabajo
	 */
	function SOL008(){
		//$this->_header();
		$data['recaudosTitulo']="";
		$data['recaudos']=array();
		$data['titulo']="Constancia de Trabajo";
		$this->formulario->setNombreForm('form1');
		$this->formulario->setAction(base_url()."/index.php/formularios/procesaConstancia");
		$this->formulario->addHidden('cedula',$this->session->userdata('cedula'));
		$this->formulario->addHidden('codtramite','SOL008');
		$this->formulario->addInput('telhab','Tel&eacute;fono Oficina','','', array('size'=>20));
		$this->formulario->addInput('telcel','Tel&eacute;fono Celular','','', array('size'=>20));
		$valores[]=array('valor'=>'1','descripcion'=>'MENSUAL');
		$valores[]=array('valor'=>'2','descripcion'=>'ANUAL');
		$valores[]=array('valor'=>'3','descripcion'=>'AMBAS');
		$this->formulario->addSelect('tipoconstancia','Tipo de Constancia',$valores,'','required:true');
		$data['btn']=$this->formulario->addButton('btn','Grabar Datos','enviar');
		$data['recaudos']=array();
		$data['formulario']=$this->formulario->outputHTML();
		
		$this->parser->parse('formularios/formulario-gen-view',$data);
		
	}
	
	function procesaConstancia(){
		$porAnalista=FALSE;
		$this->load->helper('funciones');
		if ($this->session->userdata('cedulatramite')!=""){
			$_POST['cedula']=$this->session->userdata('cedulatramite');
			$porAnalista=TRUE;
		}
		
		$sol=$this->sm->setSolicitud($_POST,FALSE);

		$this->load->library('smtpmail');
		
		$mail=new SMTPMAIL;
		$mail->set_host('zmail.mppre.gob.ve');
		$correo=$this->session->userdata('usuario')."@mppre.gob.ve";
		
		if ($porAnalista==FALSE){
			$msg="Hemos recibido su solicitud de Constancia de Trabajo, la cual está registrada bajo el número: ".$sol['id_solicitud']."<br> ".$this->session->userdata('usuario')." Pronto nos comunicaremos con usted por esta misma via para avisarle acerca de los avances.";
			$res=$mail->sendmail($correo, "Sistema de Solicitudes AIT",utf8_decode($msg),"atencion.altrabajador@mppre.gob.ve");
		}
	}
	
	
	/*
	 * Formulario de Ayuda por Nacimiento
	 */
	function SOL009(){
		//$this->_header();
		$data['titulo']="Ayuda por Nacimiento";
		$this->formulario->setNombreForm('form1');
		$this->formulario->setAction(base_url()."index.php/formularios/procesaSOL009");
		$data['cedula']=$this->formulario->addHidden('cedula',$this->session->userdata('cedula'));
		$data['codtramite']=$this->formulario->addHidden('codtramite','SOL009');
		$data['cedula_conyuge']=$this->formulario->addInput('cedula_conyuge','Cédula del Conyuge','','');
		$data['nombres_conyuge']=$this->formulario->addInput('nombres_conyuge','Nombre del Conyuge','','', array('size'=>20));
		$data['apellidos_conyuge']=$this->formulario->addInput('apellidos_conyuge','Apellido del Conyuge','','', array('size'=>20));
		$data['telefono_conyuge']=$this->formulario->addInput('telefono_conyuge','Teléfono del Conyuge','','');
		$data['lugartrabajo']=$this->formulario->addInput('lugartrabajo','Lugar de trabajo','','');
		$data['cuentabanco']=$this->formulario->addInput('cuentabanco','Cuenta Bancaria','','required:true, rangelength:[20,20]', array('size'=>20));
		$data['telefono']=$this->formulario->addInput('telefono','Telefono Oficina','','required:true', array('size'=>10));
		$data['extension']=$this->formulario->addInput('extension','Extensión','','required:true', array('size'=>5));
		$data['telefonocel']=$this->formulario->addInput('telefonocel','Telefono Celular','','', array('size'=>10));
		$data['nombreh1']=$this->formulario->addInput('nombreh1','Nombre y apellidos del menor','','required:true', array('size'=>20));
		$data['fechanach1']=$this->formulario->addDatePicker('fechanach1','Fecha de Nac.','required:true');
		$data['nombreh2']=$this->formulario->addInput('nombreh2','Nombre y apellidos del menor','','', array('size'=>20));
		$data['fechanach2']=$this->formulario->addDatePicker('fechanach2','Fecha de Nac.','');
		$data['btn']=$this->formulario->addButton('btn','Grabar Datos','enviar');
		
		//$data['formulario']=$this->formulario->outputHTML();
		$this->formulario->output();
		$data['javascript']=$this->formulario->getJavascript();
		
		$this->parser->parse('formularios/SOL009-view',$data);
		
	}
	
	function procesaSOL009(){
		$porAnalista=FALSE;
		$this->load->helper('funciones');
		if ($this->session->userdata('cedulatramite')!=""){
			$_POST['cedula']=$this->session->userdata('cedulatramite');
			$porAnalista=TRUE;
		}
		$sol=$this->sm->setSolicitud($_POST);

		$this->load->library('smtpmail');
		
		$mail=new SMTPMAIL;
		$mail->set_host('zmail.mppre.gob.ve');
		$correo=$this->session->userdata('usuario')."@mppre.gob.ve";
		
		if ($porAnalista==FALSE){
			$msg="Hemos recibido su solicitud de Ayuda por Nacimiento, la misma está registrada bajo el número: ".$sol['id_solicitud']."<br> ".$this->session->userdata('usuario')." Pronto nos comunicaremos con usted por esta misma via para avisarle acerca de los avances.";
			$res=$mail->sendmail($correo, "Sistema de Solicitudes AIT",utf8_decode($msg),"atencion.altrabajador@mppre.gob.ve");
		}
	}
	
	
	
}