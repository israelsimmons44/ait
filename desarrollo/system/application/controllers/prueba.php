<?php
class Prueba extends Controller{

	function __construct(){
		parent::Controller();
		if (!$this->session->userdata('nombre')){
			redirect(base_url()."index.php/comun/login");
		}
		$this->load->library('formulario');
		$this->load->library('parser');	
		
		$this->load->library('email');
		$config['protocol'] = 'smtp';
		$config['smtp_host']='zmail.mppre.gob.ve';
		$config['smtp_port']='25';
		$config['charset'] = 'utf-8';
		
		$this->email->initialize($config);
		
	}
	
	function index(){
		
		$this->_header();
		
		$this->formulario->setNombreForm('form1');
		$this->formulario->setAction(base_url()."/index/prueba/procesa");
		$this->formulario->addSelect('combobox','Usuarios',  'select id_usuario as valor, login as descripcion from usuario');
		$this->formulario->addButton('boton','Guardar Datos','enviar');
		
		$data['formulario']=$this->formulario->outputHTML();
		$data['javascript']=$this->formulario->getJavascript();
		
		//$data['formulario']=$this->formulario->addInput2('text', 'nombre','',"required:true");
		$this->parser->parse('prueba/prueba1-view',$data);
		
	}
	
	function prueba1(){
		$this->load->model('reclamos_model');
		$this->reclamos_model->getReclamos();
		
		
	}
	
	function prueba2(){
		$this->_header();
		
		$this->formulario->setNombreForm('form1');
		$this->formulario->setAction(base_url()."/index/prueba/procesa");
		$this->formulario->addInput("nombre","Nombre",'',"required:true, minlength:3 ");
		$this->formulario->addInput("apellido","Apellido",'',"required:true, minlength:5");
		$this->formulario->addDatePicker('fecha_nac',"Fecha de Nac.");
		$this->formulario->addButton('boton','Guardar Datos','enviar');
		
		$formulario=$this->formulario->output();
		
		$data['fldnombre']=$formulario->campos['nombre'];
		$data['fldapellido']=$formulario->campos['apellido'];
		$data['fecha_nac']=$formulario->campos['fecha_nac'];
		$data['boton']=$formulario->campos['boton'];
		$data['javascript']=$formulario->javascript;
		
		$this->parser->parse('prueba/prueba2-view',$data);
		
	}
	
	function prueba3(){
		$this->_header();
		
		$this->formulario->setNombreForm('form1');
		$this->formulario->setAction(base_url()."index/prueba/procesa");
		$data['fldnombre']=$this->formulario->addInput("nombre","Nombre",'',"required:true, minlength:3 ");
		$data['fldapellido']=$this->formulario->addInput("apellido","Apellido",'',"required:true, minlength:5");
		$data['hidcedula']=$this->formulario->addHidden("cedula",$this->session->userdata('cedula'));
		$data['fecha_nac']=$this->formulario->addDatePicker('fecha_nac',"Fecha de Nac.");
		$data['autocomplete']=$this->formulario->addAutoComplete('nivel','hidnivel', base_url()."index.php?c=prueba&m=motorJSON");
		$data['boton']=$this->formulario->addButton('boton','Guardar Datos','enviar');
		
		$fields=$this->formulario->output();
		
		$data['hidnivel']=$fields->campos['hidnivel'];
		
		$data['javascript']=$this->formulario->ajaxFunction;
		$this->parser->parse('prueba/prueba2-view',$data);
	}
	
	function prueba4(){
		$this->load->library('persona');
		
		$persona=$this->persona->getPersona(11738998);
		echo $persona['sueldo']['sueldo']."<br><br>";
		echo json_encode($persona['sueldo']['detalle']);
		//var_dump($res);
	}
	
	function prueba5(){
		$this->load->library('smtpmail');
		
		$mail=new SMTPMAIL;
		$mail->set_host('zmail.mppre.gob.ve');
		
		//$res=$mail->send_smtp_mail('<israel.simmons998@mppre.gob.ve>','probando chiquiri','PRUEBA PRUEBA PRUEBA','','<jesus.rodriguez937@mppre.gob.ve>');
		$res=$mail->sendmail($this->session->userdata('usuario')."@mppre.gob.ve","Solicitud Recibida","Ya recibimos tu solicitud en los proximos dias puedes venir a buscar todo","atencion.altrabajador@mppre.gob.ve");
		var_dump($res);
	}
	
	function prueba6(){
		
		$this->email->from('atencion.altrabajador@mppre.gob.ve','Atencion Al Trabajador');
		$this->email->to('israel.simmons998@mppre.gob.ve','Israel Simmons');
		$this->email->subject('probando la clase email');
		$this->email->message('PROBANDO LA CLASE EMAIL DE CODEIGNITER');
		
		$this->email->send();
		echo "listo";
		echo $this->email->print_debugger();

	}
	
	function motorJSON(){
		$this->load->model('prueba_model','pm');
		echo $this->pm->getNivelAcademico($this->input->get('term'));
	}
	
	function procesa(){
		$cedulatramite=$this->session->userdata('cedulatramite');
		$this->load->helper('funciones');
		echo mensajeGrowl("El formulario se ha procesado con éxito!!! para $cedulatramite");	
	}
	
	function _header(){
		$data['title']="Atenci&oacute;n Integral al Trabajador";
		$this->load->view('main-view',$data);
		$deta['permisos']=$this->session->userdata('permisos');
		$this->load->view('comun/menu2',$deta);		
	}
	
	function prueba7(){
		echo $this->jquery->output();
		echo "<div id='resultado-ajax'></div>";
		$this->formulario->setNombreForm('form1');
		$this->formulario->setAction(base_url()."index.php/prueba/procesa");
		$this->formulario->addInput('nombre','Nombre','','required:true');
		$this->formulario->addDatePicker('fecha1','Fecha Nac.');
		$this->formulario->addSelect('usuario','Usuario','select id_usuario as valor, login as descripcion from usuario');

		$this->formulario->addButton('btn','Grabar Datos','enviar');
		echo $this->formulario->outputHTML();
	}
	
	function pdf(){
		$this->load->plugin('to_pdf');
		pdf_create('<h1>Israel simmons</h1><p>Prueba 1 2 3</p>','israel.pdf');
		
	}
	
	function tcpdf(){
		$this->load->library('pdf');
		
		$this->pdf->SetSubject('TCPDF Tutorial');
        $this->pdf->SetKeywords('TCPDF, PDF, example, test, guide');
        
        // set font
        $this->pdf->SetFont('times', 'BI', 16);
        
        // add a page
        $this->pdf->AddPage();
        
        // print a line using Cell()
        $this->pdf->Cell(0, 16, 'Israel Simmons', 1, 1, 'C');
        $this->pdf->SetFont('helvetica', '', 10);
        $html="<h2>AAARRRGHHH!!!</h2>";
        $html.='<span style="text-align:justify;">Contando con todas las cosas que podemos contar Contando con todas las cosas que podemos contar Contando con todas las cosas que podemos contar Contando con todas las cosas que podemos contar Contando con todas las cosas que podemos contar Contando con todas las cosas que podemos contar Contando con todas las cosas que podemos contar Contando con todas las cosas que podemos contar Contando con todas las cosas que podemos contar Contando con todas las cosas que podemos contar Contando con todas las cosas que podemos contar</span>';
        $this->pdf->writeHTML($html, true, 0, true, true);
        
        $html = '<span style="text-align:justify;">a <u>abc</u> abcdefghijkl abcdef abcdefg <b>abcdefghi</b> a abc abcd abcdef abcdefg <b>abcdefghi</b> a abc abcd abcdef abcdefg <b>abcdefghi</b> a abc abcd abcdef abcdefg <b>abcdefghi</b> a <u>abc</u> abcd abcdef abcdefg <b>abcdefghi</b> a abc abcd abcdef abcdefg <b>abcdefghi</b> a abc abcd abcdef abcdefg <b>abcdefghi</b> a abc abcd abcdef abcdefg <b>abcdefghi</b> a abc abcd abcdef abcdefg <b>abcdefghi</b> a abc abcd abcdef abcdefg abcdefghi a abc abcd <a href="http://tcpdf.org">abcdef abcdefg</a> start a abc before <span style="background-color:yellow">yellow color</span> after a abc abcd abcdef abcdefg abcdefghi a abc abcd end abcdefg abcdefghi a abc abcd abcdef abcdefg abcdefghi a abc abcd abcdef abcdefg abcdefghi a abc abcd abcdef abcdefg abcdefghi a abc abcd abcdef abcdefg abcdefghi a abc abcd abcdef abcdefg abcdefghi a abc abcd abcdef abcdefg abcdefghi a abc abcd abcdef abcdefg abcdefghi<br />abcd abcdef abcdefg abcdefghi<br />abcd abcde abcdef</span>';
        $this->pdf->writeHTML($html, true, 0, true, true);
        //Close and output PDF document
        $this->pdf->Output('example_001.pdf', 'D');
		
	}
	
	function testy() {
		echo phpinfo();
	} 
}