<?php
class correo extends Controller{
	function __construct(){
		parent::Controller();
		$this->load->library('email');
		$this->load->library('formulario');
		$this->load->library('parser');
		$this->load->helper('funciones');
	}
	
	function _header(){
		$data['title']="Atenci&oacute;n Integral al Trabajador";
		$this->load->view('main-view',$data);
		$deta['permisos']=$this->session->userdata('permisos');
		$this->load->view('comun/menu2',$deta);		
	}
	
	function index(){
		$this->load->model('reclamos_model','rm');
		$id=$this->uri->segment(3);
		$reclamo=$this->rm->getReclamo($id);
		$data['descripcion']=$reclamo[0]['descripcion'];
		$data['cedula']=$reclamo[0]['cedula'];
		$this->_header();
		$this->formulario->setAction(base_url().'index.php/correo/enviar');
		$this->formulario->addSelect('estatus','Estatus',array(array('valor'=>1,'descripcion'=>'Estamos trabajando en eso')),'','required:true');
		$this->formulario->addTextArea('mensaje','Mensaje','','required:true',array('cols'=>60,'rows'=>7));
		$this->formulario->addHidden('email',$reclamo[0]['login']);
		$this->formulario->addHidden('cedula',$reclamo[0]['cedula']);
		$this->formulario->addButton('btnEnviar','Enviar','enviar');
		
		$data['formulario']=$this->formulario->outputHTML();
		$this->parser->parse('comun/correo-view',$data);
	}
	
	function enviar(){
		$correo=$this->input->post('email');
		$mensaje=$this->input->post('mensaje');
		$config['protocol']="smtp";
		$config['smtp_host']="zmail.mppre.gob.ve";
		$this->email->initialize($config);
		$this->email->clear();
		$this->email->from('atencion.altrabajador@mppre.gob.ve');
		//$this->email->to($this->input->post('email').'@mppre.gob.ve');
		$this->email->to($correo.'@mppre.gob.ve');
		$this->email->subject('Avance de estatus de reclamo');
		$this->email->message($mensaje);
		$this->email->send();
		echo mensajeGrowl("El mensaje ha sido enviado al usuario $correo");
	}
	
}