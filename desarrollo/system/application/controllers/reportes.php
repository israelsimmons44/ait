<?php
class reportes extends Controller{
	function __construct(){
		parent::Controller();
		$this->load->model('reportes_model','reportes');
		$this->load->library('highchart');
		$this->load->library('parser');
		$this->output->enable_profiler(FALSE);
		//$this->load->library('charts');
	}
	
	function _header(){
		$data['title']="Reportes - Atenci&oacute;n Integral al Trabajador";
		$this->load->view('main-view',$data);
		$deta['permisos']=$this->session->userdata('permisos');
		$this->load->view('comun/menu2',$deta);
	}
	
	function index(){
		$this->_header();
		$data['datos']=$this->reportes->getConstancias();
		$this->load->view('reportes/index2-view',$data);
	}
	
	function index2(){
		$this->_header();
		
		$serie1=array('nombre'=>'Impresos','data'=>array(1,5));

		$this->highchart->addSerie($serie1);

		$this->highchart->setTipo('column');
		$this->highchart->setTitulo('Relación de solicitudes');
		$this->highchart->setTituloY('Solicitudes 2');
		$this->highchart->setXAxis(array('ene','feb','mar','abr','may'));
		$data['grafico']=$this->highchart->render();
		
		$this->parser->parse('reportes/index3-view',$data);
	}
	
	function index3(){
		$res=$this->reportes->getTotalConstancias();
		$resP=$this->reportes->getTotalConstancias('P');
		$this->_header();
		
		
		foreach ($res as $value) {
			$arr[]=intval($value['total']);
		}
		
		foreach ($resP as $value) {
			$arrP[]=intval($value['total']);
		}
		
		$this->highchart->addSerie(array('nombre'=>'Solicitudes','data'=>$arr));
		$this->highchart->addSerie(array('nombre'=>'Procesadas','data'=>$arrP));
		
		$this->highchart->setTipo('column');
		$this->highchart->setTitulo('Relación de solicitudes');
		$this->highchart->setTituloY('Solicitudes 2');
		$this->highchart->setXAxis(array('feb','mar'));
		$data['grafico']=$this->highchart->render();
		
		$this->parser->parse('reportes/index3-view',$data);
		
	}
	
	function index4(){
		$res=$this->reportes->getDataSolicitudesPie();
		foreach ($res as $value) {
			$arr['data'][]=array($value['nombre'],intval($value['count']));
		}
		$arr['nombre']="Solicitudes";
		$data['datos']=json_encode($arr['data']);
		$this->_header();
		$this->parser->parse('reportes/index4-view',$data);
		
	}
	
	
}