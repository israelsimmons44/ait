<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$config['account_suffix'] = '';
$config['base_dn'] = 'ou=people,dc=mppre,dc=gob,dc=ve';
$config['domain_controllers'] = array ("10.11.11.55");
$config['ad_username'] = 'cn=config';
$config['ad_password'] = '12qwerty';
$config['real_primarygroup'] = true;
$config['use_ssl'] = false;
$config['use_tls'] = false;
$config['recursive_groups'] = true;


/* End of file adldap.php */
/* Location: ./system/application/config/adldap.php */
