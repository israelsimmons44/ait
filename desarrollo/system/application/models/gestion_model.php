<?php
class gestion_model extends Model{
	function __construct(){
		parent::Model();
		$this->load->database();
		
	}
	
	function getTramites(){
		$sql="select id_tramite as valor, nombre as descripcion from tramite where estatus='T'";
		$res=$this->db->query($sql);
		return $res->result_array();
	}
	
	function getAnalistas(){
		$sql="select id_usuario as valor, login as descripcion from usuario";
		$res=$this->db->query($sql);
		return $res->result_array();
	}
	
	function insert($data){
		$this->db->insert('seguimiento',$data);
	}
	
	function vistaSeguimiento(){
		$sql="select cedula, nombre, fecha_solicitud, fecha_enviada, fecha_recibida_firmada,fecha_entregada, \"login\" from seguimiento 
				inner join tramite using (id_tramite)
				left join usuario using (id_usuario)";
		$res=$this->db->query($sql);
		return $res->result_array();
	}
}