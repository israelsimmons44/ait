<?php
class Solicitud_Model extends Model{
	function __construct(){
		parent::Model();
		$this->load->database();
		$this->sigefirrhh=$this->load->database('sigefirrhh',TRUE);
	}
	
	function getTramites(){
		$permisos=$this->session->userdata('permisos');
		
		if($permisos['analista']=='t'){
			$sql="select id_tramite, cod_tramite, nombre from tramite where estatus in ('A','S') order by nombre";
		}else{
			$sql="select id_tramite, cod_tramite, nombre from tramite where estatus='A' order by nombre";
		}
		
		
		$res=$this->db->query($sql);
		$arr=$res->result_array();
		foreach ($arr as $key=>$value) {
			$arr[$key]['nombre']=utf8_encode($arr[$key]['nombre']);
		}
		
		return $arr;
	}
	
	function getTramite($codTramite){
		$sql="select * from tramite where cod_tramite='$codTramite'";
		$res=$this->db->query($sql);
		
		if ($res->num_rows > 0){
			$tramite= $res->result_array();
			
			return $tramite[0];
		}else{
			return FALSE;
		}
		
	}
	
	function getRecaudos($codTramite){
		$arr['detalle']=FALSE;
		$sql="select * from tramite where cod_tramite='$codTramite'";
		$resTramite=$this->db->query($sql);
		$titulo=$resTramite->row();
		$arr['titulo']=$titulo->nombre;
		
		$sql="select r.id_recaudo, t.id_tramite,t.cod_tramite, r.nombre as nombre_recaudo, ruta_planilla, t.nombre as nombre_tramite, r.campo, r.tipo as tipo_campo, r.valor  from tramiterecaudo tr
				inner join recaudo r using(id_recaudo)
				inner join tramite t using(id_tramite) 
				where id_tramite in (select id_tramite from tramite where cod_tramite='$codTramite')";
		
		$res=$this->db->query($sql);
		if ($res->num_rows()>0){
			$arr['detalle']=$res->result_array();
			
			foreach($arr['detalle'] as $row){
				$arr['detallado'][]=array($row['nombre_recaudo']);
			}
		}
		
		return $arr;
	}
	
	function insertCaso($arr){
		$this->db->insert('solicitud',$arr);		
	}
	
	function getRecaudosForm($codTramite){
		$html="";
		$this->load->helper('form');
		$arr['detalle']=FALSE;
		$sql="select * from tramite where cod_tramite='$codTramite'";
		$resTramite=$this->db->query($sql);
		$titulo=$resTramite->row();
		$arr['titulo']=$titulo->nombre;
		$html.="<h3>$titulo->nombre</h3>";
		
		$sql="select r.id_recaudo, t.id_tramite,t.cod_tramite, r.nombre as nombre_recaudo, ruta_planilla, t.nombre as nombre_tramite, r.campo, r.tipo as tipo_campo, r.valor, tooltip  from tramiterecaudo tr
				inner join recaudo r using(id_recaudo)
				inner join tramite t using(id_tramite) 
				where id_tramite in (select id_tramite from tramite where cod_tramite='$codTramite')";
		
		$res=$this->db->query($sql);
		
		if ($res->num_rows()>0){
			$arr=$res->result();
			$html.= "<form name='frmsolicitud' id='frmsolicitud'>";
			$html.= form_hidden("id_tramite",$arr[0]->id_tramite);
			$html.="<table>";
			
			foreach ($arr as $valor){
				switch (trim($valor->tipo_campo)){
					case "select":
						if (substr(strtolower($valor->valor),0,6)=='select'){
							$resSolicitud=$this->db->query($valor->valor);
							if ($resSolicitud->num_rows > 0){
								$lista[0]="Elija una opción";
								foreach ($resSolicitud->result() as $valorSol){
									$lista[$valorSol->id_nivel_academico]=$valorSol->descripcion;
								}
							}
							$html.="<tr><td><label for='$valor->campo'>$valor->nombre_recaudo</label></td><td>".form_dropdown($valor->campo,$lista,''," title='$valor->tooltip'")."</td></tr>";
						}else{
							$lista=split(",",$valor->valor);						
							$html.="<tr><td><label for='$valor->campo'>$valor->nombre_recaudo</label></td><td>".form_dropdown($valor->campo,$lista,'',"title='$valor->tooltip'")."</td></tr>";
						}
						
						break;
					case "hidden":
						$html.=form_hidden($valor->campo,$valor->valor);
						break;
					case "texto":
						$html.="<tr><td><label for='$valor->campo'>$valor->nombre_recaudo</label></td><td>".form_input($valor->campo,"","class ='required' title='$valor->tooltip'")."</td></tr>";
						//$html.="<label for='$valor->valor'>$valor->nombre_recaudo</label>".form_input($valor->campo);
						break;
					case "checkbox":
						$html.="<tr><td>$valor->nombre_recaudo</td<td>".form_checkbox($valor->campo,$valor->valor)."</td></tr>";
						
						break;
				}
			}
			$html.="</table></form>";
			
		}
		
		return $html;
	}
	
	function setSolicitud($arr, $imprimePlanilla=TRUE){
		$this->load->helper('funciones');
		
		$str="";
		$error=0;
		$cedula=0;
		$idTramite=0;
		$idSolicitud=0;
		$codTramite="";
		$sol=FALSE;
		
		if(key_exists('cedula',$arr)){
			$cedula=$arr['cedula'];
		}
		
		$sql="select * from trabajador where cedula=$cedula and estatus='A'";
		$resVerificaActivo=$this->sigefirrhh->query($sql);
		
		if ($resVerificaActivo->num_rows()==0){
			echo mensajeGrowl("El trabajador se encuentra egresado por tanto no puede hacer esta solicitud","error");
			return $sol;	
		}
		
		if (key_exists('codtramite',$arr)){
			$codTramite=$arr['codtramite'];
			$res=$this->getTramite($codTramite);
			if ($res){
				$resultado=$res['id_tramite'];
				$arr['id_tramite']=$resultado;
				
			}
		}
		
		if (key_exists('id_tramite',$arr)){
			$idTramite=$arr['id_tramite'];			
		}
		
		$sql="select * from solicitud where cedula=$cedula and id_tramite=$idTramite and estatus not in ('P')";
		
		$res=$this->db->query($sql);
		if ($res->num_rows>0){
			echo mensajeGrowl("Este trámite ya se encuentra en proceso ".$this->session->userdata('usuario').", si desea mas información al respecto, haga click aqui <a style='color:#0f0;text-weight:strong;' href='".base_url()."/index.php/main/consultatramite'>Consulta de Trámite</a>","alert");
			$sol['id_solicitud']=$res->row()->id_solicitud;
		}else{
			$sql="select nextval('solicitud_id_solicitud_seq') as proximo";
			$resNextVal=$this->db->query($sql);
			$idSolicitud=$resNextVal->row()->proximo;
			$sol['id_solicitud']=$idSolicitud;
			$sol['usuario']=$this->session->userdata('usuario');
			$sol['fecha']=date("Y-m-d H:i");
			$sol['cedula']=$cedula;
			if ($idTramite>0){
				$sol['id_tramite']=$idTramite;
			}
			
			$this->db->insert('solicitud',$sol);
			
			foreach ($arr as $key=>$valor){
				$str.="<br>".$key.": ".$valor."";
				$sql="";
				$detSol['id_solicitud']=$idSolicitud;
				$detSol['parametro']=$key;
				$detSol['valor']=$valor;
				
				if ($key=="cedula"){
					$sol['cedula']=$valor;
				}
				
				$res=$this->db->insert('detallesolicitud',$detSol);
				
			}
			
			$idUsuario=$this->session->userdata('idusuario');
			if ($idUsuario>0){
				$arrSeguimiento=array('cedula'=>$cedula,'id_solicitud'=>$idSolicitud, 'id_tramite'=>$idTramite,'fecha_solicitud'=>date("Y-m-d"),'id_usuario'=>$idUsuario);
			}else{
				$arrSeguimiento=array('cedula'=>$cedula,'id_solicitud'=>$idSolicitud, 'id_tramite'=>$idTramite,'fecha_solicitud'=>date("Y-m-d"));
			}
			
			$this->db->insert('seguimiento',$arrSeguimiento);
			
			if ($error==0){
				if ($imprimePlanilla==TRUE){
					echo mensajeGrowl("La solicitud se ha grabado correctamente, para imprimir la planilla haga click <a  style='color:#0f0;text-weight:strong;' href='".base_url()."index.php/imprimirplanilla/index/$idSolicitud'>aqui</a>");
				}else{
					echo mensajeGrowl("La solicitud se ha grabado correctamente con el número $idSolicitud. <br/>se ha enviado un correo a su cuenta para mayor información");
				}
				
				
			}else{
				echo mensajeGrowl("Hubo errores intentando grabar la solicitud","error");
			}
			
		}
		return $sol;
		
	}
	
	function _getTipoConstancia($idSolicitud){
		$return=1;
		$sql="select valor from detallesolicitud where id_solicitud=$idSolicitud and parametro='tipoconstancia'";
		$res=$this->db->query($sql);
		if ($res->num_rows()>0){
			$arr=$res->row();
			switch ($arr->valor) {
				case 1:
					$return="MENSUAL";
				break;
				
				case 2:
					$return="ANUAL";
				break;
				case 3:
					$return="AMBAS";
				break;
			}
		}
		
		return $return;
	}
	
	function getSolicitudes($idTramite=""){
		$html="";
		
		if ($idTramite==""){
			$sql="select *, solicitud.estatus as estat from solicitud 
				inner join tramite using(id_tramite) where solicitud.estatus in ('A','I') order by id_solicitud";
		}else{
			$sql="select *, solicitud.estatus as estat from solicitud 
				inner join tramite using(id_tramite) where id_tramite='$idTramite'";
		}
		
		$res=$this->db->query($sql);
		if ($res->num_rows>0){
			
			$html="<table class='display' cellspacing='0'; width='100%'><thead><tr><th>&nbsp;</th><th>Solicitud</th><th>C&eacute;dula</th><th>Trámite</th><th>Fecha de Solicitud</th><th>&nbsp;</th><th>&nbsp;</th></tr></thead><tbody>";
			$row=$res->result();
			foreach ($row as $value){
				$href="";
				$estatus=$value->estat;
				$permisos=$this->session->userdata('permisos');
				if ($permisos['administrador']=='t'){
					switch ($estatus) {
						case "I":
							$href="<a href='".base_url()."index.php/admin/actualizaProcesado/$value->id_solicitud'><img src='".base_url()."images/process.png' /></a>";
							break;
					}
				}
				
				
				
				if ($value->id_tramite==20){
					$tipoSolicitud=$this->_getTipoConstancia($value->id_solicitud);
					$html.="<tr height='30px'><td align='center'><a href='".base_url()."index.php/analista/detalleSolicitud/$value->id_solicitud' rel='#overlay1'><img src='".base_url()."images/search.png' /></a></td><td align='center'>$value->id_solicitud</td><td align='right'>".number_format($value->cedula,0,",",".")."</td><td>$value->nombre - $tipoSolicitud</td><td align='center'>$value->fecha</td><td><a href='".base_url()."index.php/imprimirplanilla/index/$value->id_solicitud'><img src='".base_url()."images/flechita.png' /></a>$href</td><td>$value->estat</td></tr>";
				}else{
					$html.="<tr height='30px'><td align='center'><a href='".base_url()."index.php/analista/detalleSolicitud/$value->id_solicitud' rel='#overlay1'><img src='".base_url()."images/search.png' /></a></td><td align='center'>$value->id_solicitud</td><td align='right'>".number_format($value->cedula,0,",",".")."</td><td>$value->nombre</td><td align='center'>$value->fecha</td><td><a href='".base_url()."index.php/imprimirplanilla/index/$value->id_solicitud'><img src='".base_url()."images/flechita.png' /></a>$href</td><td>$value->estat</td></tr>";
				}
				
			}
			$html.="</tbody></table>";
		}
		
		return $html;
	}
	
	function getSolicitudesPersona($cedula,$avanzado=FALSE){
		$html="";
		$sql="select id_solicitud,cod_tramite, nombre, fecha, solicitud.estatus from solicitud 
				inner join tramite using(id_tramite)
				where cedula=$cedula";
		
		$res=$this->db->query($sql);
		if ($res->num_rows>0){
			if ($avanzado==FALSE){
				$html="<h4 style='margin-bottom:5px;'>Solicitudes en proceso</h4><table width='100%'>";
				$row=$res->result();
				foreach ($row as $value){
					$html.="<tr><td><a href='".base_url()."index.php/main/consultatramite'>$value->nombre</a></td></tr>";
				}
				$html.="</table>";
			}else{
				$html=$res->result_array();
			}
			
			
		}
		
		return $html;
		
	}
	
	function deleteSolicitud($idSolicitud){
		$this->load->helper('funciones');
		$sql="delete from detallesolicitud where id_solicitud=$idSolicitud";
		$this->db->query($sql);
		
		$sql="delete from solicitud where id_solicitud=$idSolicitud";
		$this->db->query($sql);
		echo mensajeGrowl("La solicitud fué eliminada!!!");
		
	}
	
	function getParametrosSolicitud($idSolicitud){
		$this->load->helper('funciones');
		$sql="select * from solicitud 
				iner join tramite using(id_tramite)
				where id_solicitud=$idSolicitud";
		//echo $sql;
		
		$res=$this->db->query($sql);
		$row=$res->row();
		$arr['cedula']=$row->cedula;
		$arr['idTramite']=$row->id_tramite;
		$arr['tituloTramite']=$row->nombre;
		$arr['codTramite']=$row->cod_tramite;
		$arr['fecha']=pgDate($row->fecha);
		
		$sql="select parametro, ds.valor as valords, r.nombre from detallesolicitud ds
				inner join recaudo r on r.campo=ds.parametro
				where id_solicitud=$idSolicitud";
		//echo $sql;
		
		$ros=$this->db->query($sql);
		if($ros->num_rows>0){
			$arr['detalle']=$ros->result_array();
		}else{
			$arr['detalle']=FALSE;
		}
		
		return $arr;	
	}
	
	function getDatosSolicitud($fecha1,$fecha2){
		$sql="select * from solicitud s
			left join(
				select id_solicitud,valor as telcel from detallesolicitud where parametro='telcel') telefono using (id_solicitud)
			left join(
				select id_solicitud,valor as telhab from detallesolicitud where parametro='telhab') telefonohab using (id_solicitud)
			inner join tramite using(id_tramite)
			where s.estatus='P' and fecha between '$fecha1' and '$fecha2'
			order by cedula";
		$res=$this->db->query($sql);
		return $res->result_array();
	}
	
	function getDetalleSolicitud($idSolicitud){
		$sql="select * from solicitud s
			left join(
				select id_solicitud,valor as telcel from detallesolicitud where parametro='telcel') telefono using (id_solicitud)
			left join(
				select id_solicitud,valor as telhab from detallesolicitud where parametro='telhab') telefonohab using (id_solicitud)
			inner join tramite using(id_tramite)
			where  s.id_solicitud=$idSolicitud
			order by cedula";
		$sql="select nombre,ds.valor from detallesolicitud ds
				left join recaudo r on r.campo=ds.parametro
				where id_solicitud=$idSolicitud";
		$res=$this->db->query($sql);
		$arr=$res->result_array();
		$html="<table>";
		foreach ($arr as $value) {
			$html.="<tr><td>".$value['nombre']."</td><td>".$value['valor']."</td></tr>";
		}
		$html.="</table>";
		return $html;
		
	}
	
	function getSolicitud($idSolicitud){
		$sql="select * from solicitud where id_solicitud=$idSolicitud";
		
		$sql="select id_solicitud, id_tramite, cedula, cod_tramite, nombre, solicitud.estatus from solicitud
				inner join tramite t using(id_tramite) where id_solicitud=$idSolicitud";
		
		$res=$this->db->query($sql);
		
		$arr=$res->result_array();
		return $arr[0];
	}
	
	function setPlanillaImpresa($idSolicitud){
		$this->db->where('id_solicitud',$idSolicitud);
		$this->db->update('solicitud',array('estatus'=>"I"));
	}
	
	function getTramitesEnDeuda($fecha){
		$return=FALSE;
		$sql="select * from solicitud where fecha='$fecha'";
		$res=$this->db->query($sql);
		if ($res->num_rows()>0){
			$return=$res->result_array();
		}
		
		return $return;
	}
	
	
}