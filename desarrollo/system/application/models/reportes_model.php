<?php
class reportes_model extends Model{
	function __construct(){
		parent::Model();
		$this->load->database();
	}
	
	function getConstancias(){
		$sql="select id_tramite,nombre,'Solicitado' as estatus,1,  count(*) from solicitud
				inner join tramite using(id_tramite)
				where id_tramite=20
				group by id_tramite,nombre 
				union
				select id_tramite,nombre,'Impreso' as estatus, 2,count(*) from solicitud
				inner join tramite using(id_tramite)
				where solicitud.estatus='I' and id_tramite=20
				group by id_tramite,nombre 
				union
				select id_tramite,nombre,'Procesado' as estatus,3, count(*) from solicitud
				inner join tramite using(id_tramite)
				where solicitud.estatus='P' and id_tramite=20
				group by id_tramite,nombre 
				order by id_tramite,4";
		$res=$this->db->query($sql);
		return json_encode($res->result_array());
	}
	
	function getTotalConstancias($estatus=""){
		$filtro="";
		if ($estatus!=""){
			$filtro=" and estatus='".strtoupper($estatus)."'"; 
		}
		$sql="select date_part('month',fecha) as mes,count(*) as total from solicitud where id_tramite=20 $filtro group by date_part('month',fecha)";
		$res=$this->db->query($sql);
		return $res->result_array();
	}
	
	function getDataSolicitudesPie(){
		$sql="select t.nombre, date_part('month',fecha) as mes,count(*) from solicitud s
				inner join tramite t using (id_tramite)
				where date_part('month',fecha)=2
				group by t.nombre, date_part('month',fecha) order by mes";
		$res=$this->db->query($sql);
		return $res->result_array();
	}
	
	
}